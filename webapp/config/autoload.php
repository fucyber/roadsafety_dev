<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']  = array();
$autoload['libraries'] = array(
    'database',      // System
    'session',       // System
    'breadcrumb' // Application
    );

$autoload['drivers']   = array();
$autoload['helper']    = array(
    'date',         //SYSTEM
    'url',          //SYSTEM
    'form',          //SYSTEM
    'language',     //SYSTEM
    'application',  //APPLICATION
    );

$autoload['config']    = array();
$autoload['language']  = array('thai');
$autoload['model']     = array(
        'Common_model' => 'Common',
        'API_model' => 'API'
      );
