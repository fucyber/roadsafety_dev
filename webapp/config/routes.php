<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'event/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['dashboard']                 = "dashboard/index";
$route['dashboard/(:any)']          = "dashboard/index";
$route['dashboard/([a-z]+)']        = "dashboard/index";
$route['dashboard/([a-z]+)/(.*)']   = "dashboard/index";


//--@
$route['event']               = 'event/index';
$route['event/(:any)']        = 'event/index';
$route['event/([a-z]+)']      = 'event/index';
$route['event/([a-z]+)/(.*)'] = 'event/index';



//--@
$route['event-register']               = 'event_register/index';
$route['event-register/(:any)']        = 'event_register/index';
$route['event-register/(:any)/(:any)'] = 'event_register/index';
$route['event-register/(:any)/(:any)/(:any)'] = 'event_register/index';

//--@
$route['display']                       = 'display/index';
$route['display/(:any)']                = 'display/index';
$route['display/(:any)/(:any)']         = 'display/index';
$route['display/(:any)/(:any)/(:any)']  = 'display/index';

$route['event-checkin/(:num)']           = 'checkin/index';
$route['event-checkin/get_customer']     = 'checkin/get_customer';
$route['event-checkin/get_list']         = 'checkin/get_list';
$route['event-checkin/checkin_customer'] = 'checkin/checkin_customer';
$route['event-checkin/search_customer'] = 'checkin/search_customer';

#$route[LOGIN_PAGE] = 'user/login';
#$route['ajax_login'] = 'user/ajax_login';
#$route[AUTH_LOGOUT_PARAM] = 'user/logout';

//-----
$route['create_user']               = 'user/create_user';
$route['user/add']                  = 'user/form_add';
$route['user/edit/([0-9]+)']        = 'user/form_edit/$1';
$route['user/delete-data/([0-9]+)'] = 'user/delete_data/$1';
$route['user/list']                 = 'user/form_list';
$route['user']                      = 'user/form_list';
$route['user/list/([0-9]+)']        = 'user/form_list/$1';
$route['user/save-data']            = 'user/save_data';

//-----
$route['authen/add']                  = 'authen/form_add';
$route['authen/edit/([0-9]+)']        = 'authen/form_edit/$1';
$route['authen/delete-data/([0-9]+)'] = 'authen/delete_data/$1';
$route['authen']                      = 'authen/form_list';
$route['authen/list']                 = 'authen/form_list';
$route['authen/list/([0-9]+)']        = 'authen/form_list/$1';
$route['authen/save-data']            = 'authen/save_data';

//-----
$route['customer/add']                  = 'customer/form_add';
$route['customer/edit/([0-9]+)']        = 'customer/form_add/$1';
$route['customer/delete-data/([0-9]+)'] = 'customer/delete_data/$1';
$route['customer/list']                 = 'customer/form_list';
$route['customer/search']               = 'customer/form_list';
$route['customer/list/([0-9]+)']        = 'customer/form_list/$1';
$route['customer/save-data']            = 'customer/save_data';

//---Booth type
$route['event/(:num)/booth-type']               = 'booth_type/index';
$route['event/(:num)/booth-type/data-list']     = 'booth_type/data_list';      //-- ajax get list
$route['event/(:num)/booth-type/create']        = 'booth_type/create_form';
$route['event/(:num)/booth-type/update/(:num)'] = 'booth_type/create_form';
$route['event/(:num)/booth-type/save-data']     = 'booth_type/save_data';
$route['event/(:num)/booth-type/delete-data']   = 'booth_type/delete_data';

//---Booth
$route['event/(:num)/booth']               = 'booth/index';
$route['event/(:num)/booth/data-list']     = 'booth/data_list';      //-- ajax get list
$route['event/(:num)/booth/create']        = 'booth/create_form';
$route['event/(:num)/booth/update/(:num)'] = 'booth/create_form';
$route['event/(:num)/booth/save-data']     = 'booth/save_data';
$route['event/(:num)/booth/delete-data']   = 'booth/delete_data';

//---Setting
$route['appsetting']          = 'appsetting/index';
$route['appsetting/updateappsetting']   = 'appsetting/updateappsetting';

//-----
$route['reset_password/(:num)/(:any)'] = 'reset_password/index';
$route['save_new_password']            = 'reset_password/save';

$route['event/(:num)/review-pay-in/(:num)']   = 'invoice/pay_in_slip';

$route['event/(:num)/print-customer-card/(:any)'] = 'my_print/customer_card';
$route['event/(:num)/print-multi/(:any)'] = 'my_print/customer_multi';
$route['event/(:num)/booth-gen-qr'] = 'my_print/booth_list';


$route['web_register/(:num)']  = 'web_register/register_form';



$route['report/event_register']  = 'report';
$route['report/event_register_food']  = 'report/select_food';
