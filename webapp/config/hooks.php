<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['post_controller_constructor'] = array(
                                'class'    => 'check_login',
                                'function' => 'checked',
                                'filename' => 'Check_login.php',
                                'filepath' => 'hooks'
                                );