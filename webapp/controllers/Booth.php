<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booth extends MY_Controller {

    private $event_id;
    public function __construct()
    {
      parent::__construct();

      $this->event_id = $this->uri->segment(2);
      $this->load->model('Booth_type_model', 'Booth_type');
      $this->load->model('Booth_model', 'Booth');
      $this->load->model('Event_model', 'Event');
    }

    public function index()
    {
      //--breadcrumbs
      $this->breadcrumb->add('Dashboard', site_url('dashboard'));
      $this->breadcrumb->add('Event', site_url('event/list'));
      $this->breadcrumb->add('Booth', '#');
      //-----
      $this->data['js'][0]  = config('jsapp_url').'booth_list.js';
      $this->data['title']  = 'รายการบูธ';
      //-----
      $this->data['event_id'] = $this->event_id;
      $this->data['event']  = $this->Event->queryEventDetail($this->event_id);
      //-----
      $this->load->view('theme/_header', $this->data);
      $this->load->view('booth/booth_list');
      $this->load->view('theme/_footer');

    }

    public function data_list()
    {
      $inputs   = $this->input->post();
      $event_id = !empty($inputs['event_id']) ? $inputs['event_id'] : NULL;

      $this->data['pagination'] = '';
      $this->data['data_list']  = $this->Booth->getBooth($this->event_id);
      $this->load->view('booth/booth_list_datatable', $this->data);
    }


    public function create_form()
    {
      //--breadcrumbs
      $this->breadcrumb->add('Dashboard', site_url('dashboard'));
      $this->breadcrumb->add('Event', site_url('event/list'));
      $this->breadcrumb->add('Booth', site_url('event/'.$this->event_id.'/booth'));
      $this->breadcrumb->add('Create', '#');

      $booth_id = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : NULL;
      $this->data['js'][0]  = config('jsapp_url').'booth_form.js';

      $this->data['booth_types']  = $this->Booth_type->getBoothType($this->event_id, 100);
      $this->data['event']  = $this->Event->queryEventDetail($this->event_id);
      if(empty($booth_id)){
        $this->data['title']        = 'สร้างบูธใหม่';
      }else{
        $this->data['title']        = 'แก้ไขข้อมูลบูธ';
        $this->data['data']   = $this->Booth->getBoothId($this->event_id, $booth_id);
      }

      //--
      $this->data['event_id'] = $this->event_id;
      $this->data['booth_id'] = $booth_id;

      //----
      $this->load->view('theme/_header', $this->data);
      $this->load->view('booth/booth_form');
      $this->load->view('theme/_footer');
    }


    public function save_data()
    {
      $inputs      = $this->input->post();
      $booth_image = !empty($_FILES['booth_image']) ? $_FILES['booth_image'] : NULL;
      $document    = !empty($_FILES['document']) ? $_FILES['document'] : NULL;
      $booth_id      = !empty($inputs['booth_id']) ? $inputs['booth_id'] : NULL;
      //----
      $data['event_id']      = !empty($inputs['event_id']) ? $inputs['event_id'] : '';
      $data['booth_type_id'] = !empty($inputs['type_id']) ? $inputs['type_id'] : '';
      $data['booth_no']    = !empty($inputs['booth_no']) ? $inputs['booth_no'] : '';
      $data['booth_name']    = !empty($inputs['booth_name']) ? $inputs['booth_name'] : '';
      $data['department']    = !empty($inputs['department']) ? $inputs['department'] : '';
      $data['booth_excerpt']    = !empty($inputs['booth_excerpt']) ? $inputs['booth_excerpt'] : '';

      //------------
      if(empty($booth_id)){
        $result_id = $this->Booth->createBooth($data);
      }else{
        $result_id = $this->Booth->updateBooth($this->event_id, $booth_id, $data);
      }

      //------------
      if(!$result_id){
        $response = [
          'status' => FALSE,
          'message' => lang('data_save_failed')
          ];
        echo json_encode($response);
        die();
      }

      //$BoothNoData['booth_no']      = !empty($booth_no) ? $booth_no : ramdomNumber(4);
      //$this->Booth->updateBooth($this->event_id, $result_id, $BoothNoData);

      //---Update attach image & file
      $booth_dir = config('upload_dir').'/booths/';
      if(!empty($booth_image)){
        $image_result = $this->Common->saveFile($booth_image, 'booth_image', $booth_dir);
        if(!empty($image_result['file_name'])){
          $imageData['booth_image'] = $image_result['file_name'];
          $this->Booth->updateBooth($this->event_id, $result_id, $imageData);
        }
      }
      if(!empty($document)){
        $file_result = $this->Common->saveFile($document, 'document', $booth_dir);
        if(!empty($file_result['file_name'])){
          $fileData['booth_file'] = $file_result['file_name'];
          $this->Booth->updateBooth($this->event_id, $result_id, $fileData);
        }
      }

      //--succuess
      $response = [
        'status' => TRUE,
        'message' => lang('data_save_success')
        ];
      echo json_encode($response);
    }

    //------
    public function delete_data()
    {
      $inputs = $this->input->post();
      $booth_ids   = !empty($inputs['booth_ids']) ? $inputs['booth_ids'] : NULL;

      $result = $this->Booth->deleteBooth($this->event_id, $booth_ids);
      if(!$result){
        $response = [
          'status' => FALSE,
          'message' => lang('data_delete_failed')
          ];
        echo json_encode($response);
        die();
      }

      //--succuess
      $response = [
        'status' => TRUE,
        'message' => lang('data_delete_success')
        ];
      echo json_encode($response);
    }

    public function gen_qr_code()
    {
      $this->load->model('Barcode_model', 'Barcode');
      $event_id = $this->uri->segment(2);
      $data['booth_list'] = $this->Booth-> getBoothForQR($event_id);
      $this->load->view('print_template/booth_list', $data);
      // foreach ($booths as $val) {
      //    $code = $val->event_id.'##'.$val->booth_id;
      //    $url  = config('upload_url');
      //    $qr_url = $this->_QRCode($code, 'booth', $url, $val->event_id, $val->booth_id);
      //    echo '<img src="'.$qr_url.'"> '.$code.'<br>';
      // }

    }

    //---@
    private  function _QRCode($input_code, $prefix='', $url='', $event_id, $booth_id)
    {
        if(empty($input_code))
            return FALSE;

        $this->load->library('qrcode/ciqrcode');
        $image_dir  =  config('inside_upload_dir').'booths/';

        //--Config
        $config['cacheable']    = true;
        $config['cachedir']     = 'application/cache/qrcode/';
        $config['errorlog']     = 'application/cache/logs/';
        $config['imagedir']     = $image_dir;
        $config['quality']      = true;
        $config['size']         = 1024;
        $config['black']        = array(224,255,255);
        $config['white']        = array(70,130,180);
        $this->ciqrcode->initialize($config);

        //---Generate
        $params['data']     =   $input_code;
        $params['level']    =   'H';
        $params['size']     =   1024;
        $filename = $prefix.$event_id.$booth_id.'.png';
        $params['savename'] = $config['imagedir'].$filename;
        $this->ciqrcode->generate($params);
        if(!empty($url))
            return $url.'/booths/'.$filename;
        else
            return config('inside_upload_url').'booths/'.$filename;
    }
}