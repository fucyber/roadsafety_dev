<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct()
    {
      parent::__construct();
    }

    public function index()
    {
      $data['js'][] = config('jsapp_url').'login.js';
      $data['title']  = 'Event Administration Login';

      $this->load->view('login/form', $data);
    }

    public function check_login()
    {
      $inputs   = $this->input->post();
      $email    = !empty($inputs['email']) ? trim($inputs['email']) : NULL;
      $password = !empty($inputs['password']) ? trim($inputs['password']) : NULL;
      $result = $this->__checkLogin($email, $password);
      if(!$result){
        $response = [
          'status'  => FALSE,
          'message' => lang('login_failed')
          ];
        echo json_encode($response);
        die();
      }

        //--@
        $sessionData = [
            'level_id'     => $result->level_id,
            'level_prefix' => $result->level_prefix,
            'level_name'   => $result->level_name,
            'user_id'      => $result->user_id,
            'email'        => $result->email,
            'firstname'    => $result->firstname,
            'lastname'     => $result->lastname,
            'user_group'   => $result->user_group
        ];
        $this->session->set_userdata($sessionData);
        $this->__saveLostLogined($result->user_id);

        //--succuess
        $response = [
          'status'  => TRUE,
          'message' => lang('login_success')
          ];
        echo json_encode($response);
    }

    public function logout()
    {
        //--@
        $sessionData = array(
            'level_id',
            'level_prefix',
            'level_name',
            'user_id',
            'email',
            'firstname',
            'lastname',
            'user_group'
        );
        $this->session->unset_userdata($sessionData);
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

    //---
    private function __checkLogin($email, $password)
    {
      if(empty($email) || empty($password))
        return FALSE;

      $hash_password = $password;
      //---
      $query  = $this->db->select('L.level_id,L.level_prefix,L.level_name,L.user_group')
                          ->select('U.user_id,U.email,U.firstname,U.lastname,U.description')
                          ->from('users AS U')
                          ->join('user_levels AS L', 'L.level_id=U.level_id', 'LEFT')
                          ->where('U.email', $email)
                          ->where('U.status', 'active')
                          ->where('U.password', $hash_password)
                          ->limit(1)->get();
      if($query->num_rows() === 0)
        return FALSE;

      return $query->row();
    }

    //---
    private function __saveLostLogined($user_id)
    {
      $this->db->set('last_login', date('Y-m-d H:i:s'))
              ->where('user_id', $user_id)
              ->update('users');
      return TRUE;
    }


}