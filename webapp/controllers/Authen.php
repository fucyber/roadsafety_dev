<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authen extends MY_Controller {

    public function __construct()
    {
      parent::__construct();

      $this->load->model('Authen_model', 'Authen');
      $this->load->model('Event_model', 'Event');
    }

    public function index()
    {

      if(!$this->is_logged_in()){
        redirect(base_url('login'));
      }else{
        redirect(base_url('dashboard'));
      //  $this->logout();
      }
    }

    public function form_list()
    {
      $data['Authen'] = $this->Authen->UserLv();
      $data['page_title'] = 'จัดการสิทธฺ์ผู้ใช้งานระบบ';
      $this->load->view('theme/_header', $data);
      $this->load->view('authentication/list');
      $this->load->view('theme/_footer');
    }

    public function form_add()
    {
      $this->breadcrumb->add('User', site_url('authen/list/'));
      $this->breadcrumb->add('Add', site_url());

      $this->data['title']  = 'เพิ่มสิทธิ์';
      $this->getForm();
    }

    public function form_edit($user_id){
      if(empty($user_id)){
        $response['status'] = FALSE;
        $response['message'] = 'Customer id us empty';
        echo json_encode($response);
        exit;
      }
      $this->breadcrumb->add('User', site_url('authen/list/'));
      $this->breadcrumb->add('Edit', site_url());

      $this->data['title']  = 'แก้ไขสิทธิ์';
      $this->data['user_id']  = $user_id;

      $this->getForm();

    }

    public function getForm()
    {
      if ($this->uri->segment(3) == 1) {
       redirect('/authen/list');
      }else if($this->uri->segment(3)){
        $data = $this->data['authen'] = $this->Authen->editAuthen($this->uri->segment(3));
      }else{
        $this->data['authen'] = [
          'level_id' => '',
          'level_name' => '',
          'level_prefix' => '',
          'user_group' => '',
          'level_status' => ''
        ];
      }


      //event_title  event_id
      $this->load->view('theme/_header', $this->data);
      $this->load->view('authentication/form');
      $this->load->view('theme/_footer');
    }


    public function save_data(){

       $inputs = $this->input->post();

       if($inputs['level_id'] != ''){
         $response = $this->Authen->edit_user($inputs);
       }else{
         // print_r($inputs);die();
         $response = $this->Authen->create_user($inputs);
       }
       redirect('/authen/list');
    }

    public function delete_data(){
        $response = $this->Authen->delete_auth($this->uri->segment(3));
      // echo json_encode($response);
      redirect('/authen/list');
    }

}
