<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Barcode_model', 'Barcode');
        $this->load->model('Invoice_model', 'Invoice');
        $this->load->model('Event_register_model', 'Register');

        //$this->output->enable_profiler(TRUE);
    }

    public function pay_in_slip()
    {
        $event_id    = (int)$this->uri->segment(2);
        $customer_id = (int)$this->uri->segment(4);

        #------Query data
        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);
        if(!$invoice){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        #-----
        $get_agency = getCustomerAgency($customer_id);
        if($get_agency){
            $agency_name = $get_agency->agency_name;
        }

        #-----Assign data
        $params['company_logo']          = config('inside_upload_url').'event_settings/'.invoice_setting($event_id, 'organizer_logo');
        $params['company_name']          = invoice_setting($event_id, 'organizer_name');
        $params['company_address']       = invoice_setting($event_id, 'organizer_address');
        $params['company_tel_fax']       = 'โทร. '.invoice_setting($event_id, 'organizer_tel').', โทรสาร '.invoice_setting($event_id, 'organizer_fax');
        $params['company_website']       = 'Website: '.invoice_setting($event_id, 'organizer_website');
        $params['company_email']         = 'Email: '.invoice_setting($event_id, 'organizer_email');
        #-----
        $params['event_title']           = invoice_setting($event_id, 'invoice_title');
        $params['event_sub_title']       = invoice_setting($event_id, 'invoice_sub_title');
        #-----
        $params['customer_fullname']     = $invoice->customer_name_title.' '.$invoice->customer_firstname.' '.$invoice->customer_lastname;
        $params['customer_bill_address'] = prettyInvoiceAddress($invoice->billing_id);
        $params['customer_agency']       = $agency_name;

        #-----
        $params['invoice_ref_1']  = $invoice->invoice_ref_1;
        $params['invoice_ref_2']  = $invoice->invoice_ref_2;
        $params['bank_code']      = invoice_setting($event_id, 'invoice_bank_company_code');
        $params['bank_company_code']   = 'บจก.กรุงไทย Company Code: '.$params['bank_code'];
        $params['bank_logo']            = config('inside_upload_url').'event_settings/'.invoice_setting($event_id, 'invoice_bank_logo');
        $params['invoice_remark']       = invoice_setting($event_id, 'invoice_remark');
        $params['invoice_ref_1_barcode'] = $this->Barcode->barcode($params['invoice_ref_1']);
        $params['invoice_ref_2_barcode'] = $this->Barcode->barcode($params['invoice_ref_2']);
        $params['company_code_barcode']  = $this->Barcode->barcode($params['bank_code']);

        #-----
        $event_price               = $this->Invoice->getEventPrice($event_id);
        $session_total_price       = $this->Invoice->getTotalSessionPrice($event_id);
        $params['event_registers'] = $this->Invoice->getGroupParticipant($event_id, $customer_id);
        (int)$event_qty            = count($params['event_registers']);
        $event_unit_price          = !empty($event_price) ? $event_price : $session_total_price;
        $params['invoice_items'][] = array(
          'item_no'          =>  1,
          'item_description' =>  invoice_setting($event_id, 'invoice_title'),
          'item_qty'         =>  $event_qty,
          'item_price'       =>  number_format($event_unit_price, 0),
          'item_amount'      =>  number_format($event_qty * $event_unit_price, 0)
        );
        $invoice_grand_total_price = number_format($invoice->invoice_grand_totel, 0);
        $params['invoice_grand_total']   = $invoice_grand_total_price;
        $params['invoice_grand_total_string'] = numberToWordsThai($invoice_grand_total_price).'บาทถ้วน';


        $params['title'] = '';
        $this->load->view('invoice_template/pay_in_slip', $params);
    }


    //-----------------------
    public function receipt_slip()
    {
        $event_id    = (int)$this->uri->segment(2);
        $customer_id = (int)$this->uri->segment(4);

        #-- Create Receipt data
        $receipt_id = $this->Invoice->createReceiptDetail($event_id, $customer_id);
        if(!is_numeric($receipt_id)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }


        #------Query data
        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);
        if(!$invoice){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        #-----Assign data
        $params['company_logo']          = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'organizer_logo');
        $params['company_name']          = invoice_setting($event_id, 'organizer_name');
        $params['company_address']       = invoice_setting($event_id, 'organizer_address');
        $params['company_tel_fax']       = 'โทร. '.invoice_setting($event_id, 'organizer_tel').', โทรสาร '.invoice_setting($event_id, 'organizer_fax');
        $params['company_website']       = 'Website: '.invoice_setting($event_id, 'organizer_website');
        $params['company_email']         = 'Email: '.invoice_setting($event_id, 'organizer_email');
        #-----
        $params['event_title']           = invoice_setting($event_id, 'invoice_title');
        $params['event_sub_title']       = invoice_setting($event_id, 'invoice_sub_title');
        #-----
        $params['customer_fullname']     = $invoice->customer_name_title.' '.$invoice->customer_firstname.' '.$invoice->customer_lastname;
        $params['customer_bill_address'] = prettyInvoiceAddress($invoice->billing_id);
        #-----getReceiptNameType($customer_id)
        $ReceiptNameType = getReceiptNameType($customer_id);
        switch ($ReceiptNameType) {
            case 'personal':
                $params['customer_agency'] = $params['customer_fullname'];
                break;
            default:
                $params['customer_agency'] = getAgencyName($customer_id);
                break;
        }
        #-----
        $taxNo = getTaxNo($customer_id);
        $params['customer_agency_tax_no'] = !empty($taxNo) ? 'เลขประจำตัวผู้เสียภาษี '.$taxNo : '';

        $params['receipt_no']   =  getReceiptNo($event_id, $customer_id);
        $params['receipt_date'] =  getPaymentTime($event_id, $customer_id);
        $params['authorized_signature'] = invoice_setting($event_id, 'receipt_signature');
        #-----
        $params['invoice_ref_1']        = $invoice->invoice_ref_1;
        $params['invoice_ref_2']        = $invoice->invoice_ref_2;
        $params['banck_company_code']   = invoice_setting($event_id, 'invoice_bank_company_code');
        $params['bank_logo']            = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'invoice_bank_logo');
        $params['invoice_remark']       = invoice_setting($event_id, 'invoice_remark');
        $params['invoice_ref_1_barcode'] = $this->Barcode->barcode($params['invoice_ref_1']);
        $params['invoice_ref_2_barcode'] = $this->Barcode->barcode($params['invoice_ref_2']);
        $params['company_code_barcode']  = $this->Barcode->barcode($params['banck_company_code']);
        #-----
        $params['watermark_bg'] = config('inside_upload_dir').'watermark_copy.png';

        #-----
        $event_price               = $this->Register->getEventPrice($event_id);
        $session_total_price       = $this->Register->getTotalSessionPrice($event_id);
        $params['event_registers'] = $this->Register->getGroupParticipant($event_id, $customer_id);
        (int)$event_qty            = count($params['event_registers']);
        $event_unit_price          = !empty($event_price) ? $event_price : $session_total_price;
        $params['invoice_items'][] = array(
          'item_no'          =>  1,
          'item_description' =>  'ค่าลงทะเบียน'.invoice_setting($event_id, 'invoice_title'),
          'item_qty'         =>  $event_qty,
          'item_price'       =>  number_format($event_unit_price, 0),
          'item_amount'      =>  number_format($event_qty * $event_unit_price, 0)
        );
        $invoice_grand_total_price = number_format($invoice->invoice_grand_totel, 0);
        $params['invoice_grand_total']        = $invoice_grand_total_price;
        $params['invoice_grand_total_string'] = numberToWordsThai($invoice_grand_total_price).'บาทถ้วน';

        #--- Create Invoice----------
        $filename = '';
        ini_set('memory_limit','64M');
        try {
          ob_start();
          $html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 10, 10));
          $html2pdf_M = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 10, 10));
          $html2pdf->setDefaultFont("THSarabun");
          $html2pdf_M->setDefaultFont("THSarabun");
          $params['title'] = '';
          $content = $this->load->view('invoice_template/receipt_template_copy', $params, TRUE);
          $content_M = $this->load->view('invoice_template/receipt_template', $params, TRUE);
          $html2pdf->writeHTML($content);
          $html2pdf_M->writeHTML($content_M);
          ob_clean();
          $filename    = 'receipt'.'_'.$event_id.'_'.$customer_id.'_'.$params['invoice_ref_1'].'.pdf';
          $filename_M    = 'M_receipt'.'_'.$event_id.'_'.$customer_id.'_'.$params['invoice_ref_1'].'.pdf';
          $pdfFilePath = config('inside_upload_dir')."event_invoices/".$filename;
          $pdfFilePath_M = config('inside_upload_dir')."event_invoices/".$filename_M;
          $html2pdf->output($pdfFilePath, 'F');
          $html2pdf_M->output($pdfFilePath_M, 'F');

        }catch(Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            $result_execute['message'] = $formatter->getHtmlMessage();
        }

        #---------------
        if(!empty($filename)){
            $this->Invoice->updateReceiptFilename($event_id, $customer_id, $filename);
            $customer = $this->Customer->queryProfile($customer_id);
            #------Send email
            if(!empty($customer->customer_email)){
                $this->load->model('Sendmail_model', 'Sendmail');
                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;
                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;
                $sendmail = $this->Sendmail->sendReceiptSlip($customer->customer_email, $params);
                if($sendmail)
                    $this->Invoice->setReceiptSendmailStatus($receipt_id, '1');
            }
        }

        #------
        $result_execute['filename'] = $filename;
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_OK,
            config('field_rest_message')       => lang('data_save_success'),
            config('field_rest_data')          => $result_execute
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

}
