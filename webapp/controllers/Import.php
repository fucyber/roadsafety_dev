<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Import extends CI_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('Event_model', 'Event');

 

    }

    public function index()
    
        {
            $this->load->library('PHPExcel');
           
            $objPHPExcel = new PHPExcel();

$inputFileName =  "_uploads/temp/".$this->input->get('file');


//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName, PATHINFO_BASENAME).'": '.$e->getMessage());
}

$objWorksheet = $objPHPExcel->getActiveSheet();

$i=2;
foreach ($objWorksheet->getRowIterator() as $row) {
    if(!empty($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue())):
        $this->db->like('occupation_name',trim($objPHPExcel->getActiveSheet()->getCell("H$i")->getValue()) );
              $query = $this->db->get('master_occupations');
              if($query->num_rows()){
            $occupation_id= $query->row(0)->occupation_id; 
              }else{
                $occupation_id="";
              }
              $this->db->where('food_name',trim($objPHPExcel->getActiveSheet()->getCell("K$i")->getValue()) );
              $query = $this->db->get('master_food_types');
              if($query->num_rows()){
            $food_id= $query->row(0)->food_id; 
              }else{
                $food_id=trim($objPHPExcel->getActiveSheet()->getCell("K$i")->getValue());
              }
           $gender=   (trim($objPHPExcel->getActiveSheet()->getCell("F$i")->getValue()) =="ชาย") ? "male" :"female";
              
    $data[] = array(
        'email'           => trim($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue()),
        'name_title'      => trim($objPHPExcel->getActiveSheet()->getCell("B$i")->getValue()),
        'firstname'       => trim($objPHPExcel->getActiveSheet()->getCell("C$i")->getValue()),
        'lastname'        => trim($objPHPExcel->getActiveSheet()->getCell("D$i")->getValue()),
        'gender'          => $gender,
        'age'             => trim($objPHPExcel->getActiveSheet()->getCell("G$i")->getValue()),
        'tel'             => !empty($objPHPExcel->getActiveSheet()->getCell("O$i")->getValue()) ? trim($objPHPExcel->getActiveSheet()->getCell("O$i")->getValue()) : '',
        'mobile'          => trim($objPHPExcel->getActiveSheet()->getCell("J$i")->getValue()),
        'occupation_id'   =>  $occupation_id,
        'job_description' => !empty($objPHPExcel->getActiveSheet()->getCell("I$i")->getValue()) ? trim($objPHPExcel->getActiveSheet()->getCell("I$i")->getValue()) : '',
        'food_id'         => $food_id
      );
    $i++;
    endif;
}
// echo count($data);

echo json_encode($data);
return;
echo '<pre>';
print_r($data);
echo '</pre>';
return;
//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

// echo '<pre>';
// print_r($highestRow);
// //print_r($sheet);
// //print_r($highestColumn);
// echo '</pre>';
// return;

//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++){
    //  Read a row of data into an array
    $rowData['dd'] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn.$row, NULL, TRUE, FALSE);
    //  Insert row data array into your database of choice here
}
echo '<pre>';
print_r($rowData);
echo '</pre>';
            
        }

}