<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Display_model', 'Mdisplay');
    }


    public function index()
    {
      $this->data['event_id'] = $this->uri->segment(3);

      // $this->data = [];
      $this->load->view('theme/_header');
      $this->load->view('display/display_list', $this->data);
      $this->load->view('theme/_footer');
    }

    public function get_list()
    {
      echo json_encode($this->Mdisplay->get_event_registers($this->input->post('event_id')));
    }

    public function get_customer()
    {
      echo json_encode($this->Mdisplay->get_customer($this->input->post()));
    }

    public function checkin_customer()
    {
      echo json_encode($this->Mdisplay->checkin_customer($this->input->post()));
    }

    public function showsession()
    {
      echo json_encode($this->session->userdata('user_id'));
    }

}
