<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booth_type extends MY_Controller {

    private $event_id;
    public function __construct()
    {
      parent::__construct();

      $this->event_id = $this->uri->segment(2);
      $this->load->model('Event_model', 'Event');
      $this->load->model('Booth_type_model', 'Booth_type');
    }

    public function index()
    {
      //--breadcrumbs
      $this->breadcrumb->add('Dashboard', site_url('dashboard'));
      $this->breadcrumb->add('Event', site_url('event/list'));
      $this->breadcrumb->add('Booth', site_url('event/'.$this->event_id.'/booth'));
      $this->breadcrumb->add('Type', '#');

      $this->data['js'][0]  = config('jsapp_url').'booth_type_list.js';
      $this->data['title']  = 'รายการประเภทบูธ';
      //---
      $this->data['event']  = $this->Event->queryEventDetail($this->event_id);

      $this->data['event_id'] = $this->event_id;
      $this->load->view('theme/_header', $this->data);
      $this->load->view('booth/booth_type_list');
      $this->load->view('theme/_footer');

    }

    public function data_list()
    {
      $inputs = $this->input->post();
      $event_id = !empty($inputs['event_id']) ? $inputs['event_id'] : NULL;

      $this->data['data_list']  = $this->Booth_type->getBoothType($this->event_id);
      $this->data['pagination'] = '';
      $this->load->view('booth/booth_type_list_datatable', $this->data);
    }


    public function create_form()
    {
      //--breadcrumbs
      $this->breadcrumb->add('Dashboard', site_url('dashboard'));
      $this->breadcrumb->add('Event', site_url('event/list'));
      $this->breadcrumb->add('Booth', site_url('event/'.$this->event_id.'/booth'));
      $this->breadcrumb->add('Type', site_url('event/'.$this->event_id.'/booth-type'));

      //---
      $type_id = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : NULL;
      $this->data['js'][0]  = config('jsapp_url').'booth_type_form.js';

      //--
      if(empty($type_id)){
        $this->data['title']  = 'เพิ่มประเภทบูธ';
        $this->breadcrumb->add('Create', '#');
      }else{
        $this->data['title']  = 'แก้ไขประเภทบูธ';
        $this->breadcrumb->add('Update', '#');
      }
      $this->data['event'] = $this->Event->queryEventDetail($this->event_id);
      $this->data['data']  = $this->Booth_type->getBoothTypeId($this->event_id, $type_id);
      //--
      $this->data['event_id'] = $this->event_id;
      $this->data['type_id']  = $type_id;

      //----
      $this->load->view('theme/_header', $this->data);
      $this->load->view('booth/booth_type_form');
      $this->load->view('theme/_footer');
    }

    public function save_data()
    {
      $inputs = $this->input->post();
      $event_id  = !empty($inputs['event_id']) ? $inputs['event_id'] : NULL;
      $type_id   = !empty($inputs['type_id']) ? $inputs['type_id'] : NULL;
      $data['type_code'] = !empty($inputs['type_code']) ? $inputs['type_code'] : '';
      $data['type_name'] = !empty($inputs['type_name']) ? $inputs['type_name'] : '';

      if(empty($type_id)){
        $data['event_id'] = $event_id;
        $result = $this->Booth_type->createBoothType($data);
      }else{
        $result = $this->Booth_type->updateBoothType($event_id, $type_id, $data);
      }

      if(!$result){
        $response = [
          'status'  => FALSE,
          'message' => lang('data_save_failed')
          ];
        echo json_encode($response);
        die();
      }

      //--succuess
      $response = [
        'status' => TRUE,
        'message' => lang('data_save_success')
        ];
      echo json_encode($response);
    }

    public function delete_data()
    {
      $inputs = $this->input->post();
      $type_ids   = !empty($inputs['type_ids']) ? $inputs['type_ids'] : NULL;

      $result = $this->Booth_type->deleteBoothType($this->event_id, $type_ids);
      if(!$result){
        $response = [
          'status' => FALSE,
          'message' => lang('data_delete_failed')
          ];
        echo json_encode($response);
        die();
      }

      //--succuess
      $response = [
        'status' => TRUE,
        'message' => lang('data_delete_success')
        ];
      echo json_encode($response);
    }
}