<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Checkin_model', 'Checkin');
    }


    public function index()
    {
      $data['event_id'] = $this->uri->segment(2);
      $data['data']     = $this->Event->queryEventDetail($data['event_id']);
      // $this->data = [];
      $this->load->view('theme/_header');
      $this->load->view('checkin/checkin_list', $data);
      $this->load->view('theme/_footer');
    }

    public function get_list()
    {
      $event_id = $this->input->post('event_id');
      $result  = $this->Checkin->get_event_registers($event_id);
      echo json_encode($result);
    }

    public function search_customer()
    {
      echo json_encode($this->Checkin->search_customer($this->input->post()));
    }

    public function get_customer()
    {
      echo json_encode($this->Checkin->get_customer($this->input->post()));
    }

    public function checkin_customer()
    {
      echo json_encode($this->Checkin->checkin_customer($this->input->post()));
    }

    public function showsession()
    {
      echo json_encode($this->session->userdata('user_id'));
    }

}
