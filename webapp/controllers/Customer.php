<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

    public function __construct()
    {
      parent::__construct();

      // Force SSL
      //$this->force_ssl();
      $this->load->model('Customer_model', 'Customer');
      $this->load->model('Event_model', 'Event');
      $this->load->model('Event_register_model', 'EventRegister');
      // Form and URL helpers always loaded (just for convenience)
      $this->load->helper('url');
      $this->load->helper('form');
      $this->load->library('pagination');
    }

    public function index()
    {
      $this->form_list();
    }


    public function edit_customer($user_data = [])
  	{
      // Load resources
  		$this->load->helper('auth');
  		$this->load->library('form_validation');
      $this->form_validation->set_data( $user_data );

      $validation_rules = [
  			[
  				'field'  => 'customer_email',
  				'label'  => 'email',
  				'rules'  => 'trim|required|valid_email',
  				'errors' => [
  					'is_unique' => 'Email address already in use.'
  				]
  			]
  		];


  		$this->form_validation->set_rules( $validation_rules );

  		if( $this->form_validation->run() )
  		{

        $response = [];
        try {
          $this->EventRegister->updateCustomerProfile($user_data);
          $this->EventRegister->updateAffiliatedAgency($user_data);
          $this->EventRegister->updateBillingAddress($user_data);
          $invoice_id = $this->EventRegister->updateInvoice($user_data);
          $user_data['registrant_id'] = $user_data['customer_id'];
          $user_data['invoice_id'] = $invoice_id;
          $this->EventRegister->insertRegister($user_data);
          $this->EventRegister->uploadSlip($user_data);
          $response['status'] = TRUE;
          $response['message'] = 'Save success';
        } catch (Exception $e) {
          $response['status'] = FALSE;
          $response['message'] = $e;
        }
  			return ['status' => $response['status'],'message' => $response['message']];

  		}
  		else
  		{
        return ['status' => FALSE,'message' => validation_errors()];
  		}
    }
    public function create_customer($user_data = [])
  	{

  		$this->load->library('form_validation');

      $this->form_validation->set_data( $user_data );

      $validation_rules = [
  			[
  				'field' => 'customer_password',
  				'label' => 'password',
  				'rules' => [
  					'trim',
  					'required'
  				],
  				'errors' => [
  					'required' => 'The password field is required.'
  				]
  			],
  			[
  				'field'  => 'customer_email',
  				'label'  => 'email',
  				'rules'  => 'trim|required|valid_email|is_unique[customers.customer_email]',
  				'errors' => [
  					'is_unique' => 'Email address already in use.'
  				]
  			]
  		];


  		$this->form_validation->set_rules( $validation_rules );

  		if( $this->form_validation->run() )
  		{
        $response = [];
        try {
          $customer_id                = $this->EventRegister->insertCustomerProfile($user_data);
          $user_data['customer_id']   = $customer_id;
          $this->EventRegister->insertAffiliatedAgency($user_data);
          $this->EventRegister->insertBillingAddress($user_data);
          $invoice_id                 = $this->EventRegister->createInvoice($user_data);
          $user_data['registrant_id'] = $customer_id;
          $user_data['invoice_id']    = $invoice_id;
          $this->EventRegister->insertRegister($user_data);
          $this->EventRegister->uploadSlip($user_data);
          $response['status']         = TRUE;
          $response['message']        = 'Save success';
        } catch (Exception $e) {
          $response['status'] = FALSE;
          $response['message'] = $e;
        }
  			return ['status' => $response['status'],'message' => $response['message']];
  		}
  		else
  		{
        return ['status' => FALSE,'message' => validation_errors()];
  		}

  	}

    public function form_add(){
      $this->breadcrumb->add('Customer', site_url('customer/list/'));
      $this->breadcrumb->add('Add', site_url());

    //  $this->data['js'][0]  = config('jsapp_url').'event_list.js';
      $this->data['title']  = 'เพิ่มสมาชิก';
      $this->getForm();
    }

    public function form_edit($customer_id){
      if(empty($customer_id)){
        $response['status'] = FALSE;
        $response['message'] = 'Customer id us empty';
        echo json_encode($response);
        exit;
      }
      $this->breadcrumb->add('Customer', site_url('customer/list/'));
      $this->breadcrumb->add('Edit', site_url());

    //  $this->data['js'][0]  = config('jsapp_url').'event_list.js';
      $this->data['title']  = 'แก้ไขสมาชิก';
      $this->data['customer_id']  = $customer_id;

      $this->getForm();

    }

    public function getForm(){
      $this->data['occupation_lists'] =  $this->Customer->getOccupation();
      // print_r($this->data['occupation_lists'] =  $this->Customer->getOccupation());die();
      if($this->uri->segment(3)){
        $data = $this->data['customer'] = $this->Customer->editCustomer($this->uri->segment(3));
        // print_r($data);die();
      //   $this->data['event_id']  = $this->data['customer']['event_id'];
      }else{
          // $this->data['customer'] ="";
      //   $this->data['event_id']  = $this->input->get('event');
      //
        $this->data['customer'] = [
          'customer_id'         => '',
          'customer_email'      => '',
          'customer_password'   => '',
          'customer_name_title' => '',
          'customer_firstname'  => '',
          'customer_lastname'   => '',
          'customer_gender'     => '',
          'customer_age'        => '',
          'job_description'     => '',
          'occupation_id'       => '',
          'customer_mobile'     => '',

        ];
      }

      //event_title  event_id
      $this->load->view('theme/_header', $this->data);
      $this->load->view('customer/form');
      $this->load->view('theme/_footer');
    }

    public function form_list($offset = false)
    {

      $this->breadcrumb->add('Customer', site_url('customer/list/'));
      $this->breadcrumb->add('Add', site_url());

      $this->data['title'] = "จัดการสมาชิก";
      $total_records = $this->Customer->get_total($this->input->post('search'));
      // echo $total_records; die();
      $this->data['total_records'] = $total_records;

      $limit_per_page = 10;
      $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


      if ($total_records > 0)
        {
            // get current page records
            $this->data["customers"] = $this->Customer->getCustomer($limit_per_page, $start_index, $this->input->post('search'));
            $config['base_url'] = base_url() . 'customer/list';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            $config['cur_tag_open'] = '<span class="curlink">';
            $config['cur_tag_close'] = '</span>';

            $config['num_tag_open'] = '<span class="numlink">';
            $config['num_tag_close'] = '</span>';

            $this->pagination->initialize($config);

            // build paging links
            $this->data["links"]  = $this->pagination->create_links();
            $this->data['number'] = $start_index;
        }

      $this->load->view('theme/_header', $this->data);
      $this->load->view('customer/list');
      $this->load->view('theme/_footer');
    }


    public function upload(){
      $config['upload_path']          = config('upload_dir').'slips/';
      $config['allowed_types']        = 'gif|jpg|png';
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload('paymentslip'))
      {
              $data = array('error' => $this->upload->display_errors());
      }
      else
      {
        $data = array('upload_data' => $this->upload->data());
      }
      echo json_encode($data);
    }


    public function save_data(){

       $inputs = $this->input->post();
       $data['customer_id'] = $inputs['customer_id'];
       $data['customer_email'] = $inputs['customer_email'];
       // $data['customer_password'] = $inputs['customer_password'];
       $data['customer_name_title'] = $inputs['customer_name_title'];
       $data['customer_firstname'] = $inputs['customer_firstname'];
       $data['customer_lastname'] = $inputs['customer_lastname'];
       $data['customer_gender'] = $inputs['customer_gender'];
       $data['customer_age'] = $inputs['customer_age'];
       $data['occupation_id'] = $inputs['occupation_id'];
       $data['job_description'] = $inputs['job_description'];
       $data['customer_mobile'] = $inputs['customer_mobile'];

       if($data['customer_id']){
         $this->data['customer_id'] = $this->uri->segment(3);
         $response = $this->Customer->edit_customer($data);
       }else{
         $response = $this->Customer->create_customer($data);
       }

       // print_r($response); die();
       echo json_encode($response);
       redirect('/customer/list');
    }

    public function delete_data(){
        $response = $this->Customer->delete_customer($this->uri->segment(3));
      echo json_encode($response);
      redirect('/customer/list');
    }


    //--@District
    public function district()
    {
        $province_id  = $this->input->post('province_id');

        $result_obj = $this->Customer->getDistrict($province_id);
        if(!$result_obj){
            $response_data = array(
                'status'        => FALSE,
                'data'          => NULL
             );
            echo json_encode($response_data);
            exit();
        }

        $response_data = array(
                'status'        => TRUE,
                'data'          => $result_obj
                );
        echo json_encode($response_data);
    }

    //--@Sub district
    public function sub_district()
    {
        $district_id  = $this->input->post('district_id');

        $result_obj = $this->Customer->getSubDistrict($district_id);
        if(!$result_obj){
            $response_data = array(
                'status'        => FALSE,
                'data'          => NULL
             );
            echo json_encode($response_data);
            exit();
        }

        $response_data = array(
                'status'        => TRUE,
                'data'          => $result_obj
                );
        echo json_encode($response_data);
    }



    //--@zipcode
    public function zipcode()
    {
        $district_code  = $this->input->post('district_code');
        $result_obj = $this->Customer->getZipcode($district_code);
        if(!$result_obj){
            $response_data = array(
                'status'        => FALSE,
                'data'          => NULL
             );
            echo json_encode($response_data);
            exit();
        }

        $response_data = array(
                'status'       => TRUE,
                'data'          => $result_obj
                );
        echo json_encode($response_data);
    }

}
