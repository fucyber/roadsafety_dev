<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Except extends CI_Controller {

  //---@
  public function show_404()
  {
      show_404();
  }

  public function uri()
  {
    $this->load->view('welcome_message');
  }

}