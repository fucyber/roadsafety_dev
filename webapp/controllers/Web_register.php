<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_register extends CI_Controller
{
    //----@
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Register_model', 'Register');
    }

    public function register_form()
    {
      $event_id = $this->uri->segment(2);
      //--get data form api
      $end_point = config('inside_api_url').'/event/'.$event_id;
      $result = $this->API->get($end_point);
      if(!$result['status']){
        redirect('http://www.rvp.co.th/');
      }

      $event_detail = $result['data'];
      $data['data']        = $event_detail;
      $data['event_id']    = $event_id;
      $data['event_price']    = $event_detail['register_data']['session']['event_price'];
      $data['event_title'] = 'ลงทะเบียนเข้าร่วมงาน '.$event_detail['event_title'];

      $data['levels'] = $this->__getLevelList($event_id);
      $data['departments'] = $this->__getDepartmentsList($event_id);
      $data['ministries'] = $this->__getMinistriesList($event_id);
      $data['provinces'] = $this->__getProvincesList($event_id);
      
      $this->load->view('web_register/register_form', $data);
    }

    private function __getLevelList($event_id)
    {
      $query = $this->db->where('event_id', $event_id)
                        ->where('level_status', 'active')
                        ->order_by('level_name', 'ASC')
                        ->get('customer_levels');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
    private function __getDepartmentsList($event_id)
    {
      $query = $this->db->order_by('department_sort', 'ASC')
                        ->get('master_departments');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
    private function __getMinistriesList($event_id)
    {
      $query = $this->db->order_by('ministry_sort', 'ASC')
                        ->get('master_ministries');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
    private function __getProvincesList($event_id)
    {
      $query = $this->db->order_by('province_name_th', 'ASC')
                        ->get('address_provinces');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
     public function user_form()
    {
     $data['occupations']= $this->__getOccupationsList();
     $data['food_types']= $this->__getFoodTypesList();
      $this->load->view('web_register/user_form', $data);
    }
    private function __getOccupationsList()
    {
      $query = $this->db->order_by('occupation_sort', 'ASC')
                        ->get('master_occupations');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
    private function __getFoodTypesList()
    {
      $query = $this->db->order_by('food_id', 'DESC')->get('master_food_types');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }
    public function register()
    {
      extract( $this->input->post());
      print_r($email);
      // print_r($this->input->post());
      $invalidEmail=$this->Register->invalidEmail_register($email);
     
if($invalidEmail != false){
  print_r($validEmail);
 }
    }

}