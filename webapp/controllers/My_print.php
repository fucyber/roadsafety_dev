<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class My_print extends MY_Controller

{

    private $event_id;

    public function __construct()

    {

      parent::__construct();

      $this->event_id = $this->uri->segment(2);

      $this->load->model('Customer_model', 'Customer');

      $this->load->model('Event_model', 'Event');

      $this->load->model('Booth_model', 'Booth');

    }



    public function customer_card()

    {

      $customer_id  = $this->uri->segment(4);

      $data['data'] = $this->Customer->getCustomerEventCard($customer_id, $this->event_id);

      $this->load->view('print_template/customer_card', $data);

    }



    public function customer_multi()
    {

      $customer_id  = $this->uri->segment(4);
      $data['data'] = $this->Customer->getCustomerEventMulti($customer_id, $this->event_id);

      $this->load->view('print_template/customer_multi', $data);

    }





    public function booth_list()

    {

      $customer_id  = $this->uri->segment(4);

      $data['booth_list'] = $this->Booth->getBooth($this->event_id);

      $data['card_logo']  = config('upload_url').'event_settings/'.event_setting($this->event_id, 'card_logo');

      $data['card_title']     = event_setting($this->event_id, 'card_title');

      $data['card_sub_title'] = event_setting($this->event_id, 'card_sub_title');

      $data['card_venue']     = event_setting($this->event_id, 'card_venue');

      $this->load->view('print_template/booth_list', $data);

    }



}

