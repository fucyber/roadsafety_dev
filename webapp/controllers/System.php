<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends CI_Controller
{
    //----@
    public function __construct()
    {
      parent::__construct();
 
    }

    public function getDistrictsList()
    {
        $province_id = $this->uri->segment(3);
        $query = $this->db->where('province_id', $province_id)->order_by('district_name_th', 'ASC')
        ->get('address_districts');
 $this->output
 ->set_content_type('application/json')
 ->set_output(json_encode(array('result' =>  $query->result())));
    }
    public function getSubDistrictsList()
    {
        $district_id = $this->uri->segment(3);
        $query = $this->db->where('district_id', $district_id)->order_by('sub_district_name', 'ASC')
        ->get('address_sub_districts');
 $this->output
 ->set_content_type('application/json')
 ->set_output(json_encode(array('result' =>  $query->result())));
    }
}