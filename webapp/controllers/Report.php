<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Report extends MY_Controller {



    public function __construct()

    {

      parent::__construct();
      $this->load->model('Report_model');

    }



    public function index()
    {
      $datestart = $this->input->post('datestart');
      $dateend = $this->input->post('dateend');

      if( $datestart || $dateend ) {

        $data['datestart'] = $this->input->post('datestart');
        $data['dateend'] = $this->input->post('dateend');
        $data['data'] = $this->Report_model->get_event_register($datestart,$dateend);

        $this->load->view('theme/_header');

        $this->load->view('report/register_event',$data);

        $this->load->view('theme/_footer');

      }else{

        $data['data'] = $this->Report_model->get_event_register();

        $this->load->view('theme/_header');

        $this->load->view('report/register_event',$data);

        $this->load->view('theme/_footer');

      }
    }

    public function select_food()
    {

      $datestart = $this->input->post('datestart');
      $dateend = $this->input->post('dateend');
      $food = $this->input->post('food');
        if( $datestart || $dateend || $food  ) {

          $data['datestart'] = $this->input->post('datestart');
          $data['dateend'] = $this->input->post('dateend');
          $data['food'] = $this->input->post('food');
          $data['data'] = $this->Report_model->get_event_register_food($datestart,$dateend,$food);

          $this->load->view('theme/_header');

          $this->load->view('report/register_event_food',$data);

          $this->load->view('theme/_footer');

        }

        else{

        $data['data'] = $this->Report_model->get_event_register_food();

        $this->load->view('theme/_header');

        $this->load->view('report/register_event_food',$data);

        $this->load->view('theme/_footer');
      }
    }






}
