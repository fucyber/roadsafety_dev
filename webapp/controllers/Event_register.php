<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_register extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Event_register_model', 'Register');
        $this->load->model('Invoice_model', 'Invoice');
        $this->load->model('Customer_model', 'Customer');
    }

    //----
    public function index()
    {
        $action = $this->uri->segment(2);
        switch ($action) {
            case 'list':
                $this->__registerList();
                break;
            case 'data-list':
                $this->__registerDataList();
                break;
            case 'view-payment-detail':
                $this->__viewPaymentDetail();
                break;
            case 'save-payment-status':
                $this->__savePaymentStatus();
                break;
            case 'participant':
                $this->__participant();
                break;
            case 'save-billing':
                $this->__saveBilling();
                break;
             case 'import_excel':
                $this->__importExcel();
                 break;
              case 'upload_excel':
             $this->__uploadExcel();
              break;
              case 'event-register-update':
              $this->__event_register_update();
              break;
              case 'summary-data':
              $this->__summarydata();
              break;
              case 'save-event-register-customer':
              $this->__save_event_register_customer();
                break;
            default:
                break;
        }
    }
    public function __save_event_register_customer()
    {
      echo $this->Register->save_event_register_customer($this->input->post());
    }
    public function __event_register_update()
    {
      $post  = $this->input->post();
      $data  = array();
      if(!empty($_FILES['file']['name'])){
          $config['upload_path']          = './_uploads/slips';
          $config['allowed_types'] = 'jpg|png|jpeg|gif|pdf';
          $config['max_size']      = 1072*100;
          $config['max_width']     = 4024;
          $config['max_height']    = 4024;
          $config['file_name']     = $post['customer_id'].'_'.$post['event_id'].'_'.date('YmdHis').'_'.time();
          $this->load->library('upload', $config);
          $data = false;
          if ( ! $this->upload->do_upload('file'))
          {
                 $error = array('error' => $this->upload->display_errors());
          }
          else
          {
                 $data = array('upload_data' => $this->upload->data());
          }
      }

      $is_free = $this->checkFreeCustomer($post['event_id'], $post['customer_id']);
      if($is_free > 0){
          // print_r($this->input->post());
          if ($post['receipt_type'] == 'approved') {
                    $inputs = $this->input->post();
                    $event_id    = (int)$inputs['event_id'];
                    $invoice_id  = (int)$inputs['invoice_id'];
                    $customer_id = (int)$inputs['customer_id'];
                    $receipt_date = date('Y-m-d', strtotime($inputs['receipt_date']));
                    $status      = $inputs['status'];
                    $this->Invoice->updateReceiptDate($invoice_id, $receipt_date);
                    $api_uri     = config('inside_api_url').'event/'.$event_id.'/receipt-slip/'.$customer_id;
                    $api_execute = $this->API->get($api_uri);
                    if(!$api_execute['status']){
                        $response['status']  = FALSE;
                        $response['message'] = lang('system_timeout');
                        echo json_encode($response);
                    }
          }
          echo json_encode($this->Register->event_register_update($this->input->post(),$data));
      }else{
        $this->db->set('payment_status', 'approved');
        $this->db->where('event_id', $post['event_id']);
        $this->db->where('customer_id', $post['customer_id']);
        $this->db->where('status !=', 'discard');
        $this->db->update('event_invoices');

      }
    }

    public function checkFreeCustomer($event_id, $customer_id)
    {
        $query = $this->db->select('invoice_grand_totel')
                ->where('event_id', $event_id)
                ->where('customer_id', $customer_id)
                ->where('status !=', 'discard')
                ->get('event_invoices');

        return $query->row()->invoice_grand_totel;
    }

    private function __saveBilling()
    {
      $invoice_id= $this->input->post('invoice_id');
      $billing = $this->input->post('billing');
      $params = [
        'billing_dtm' => $billing
      ];
      if($this->Invoice->updatePayment($invoice_id, $params)){
        $response['status'] = TRUE;
        $response['message'] = lang('data_save_success');
      }else{
        $response['status'] = FALSE;
        $response['message'] = lang('HTTP_UNAUTHORIZED');
      }

      echo json_encode($response);

    }
    //--------
    private function __registerList()
    {
        //--breadcrumb
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('Event', site_url('event'));
        $this->breadcrumb->add('Register', '#');


        $this->data['plugin'][] = config('plugin_url').'fancybox/jquery.fancybox.min.js';
        $this->data['plugin'][] = config('plugin_url').'bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $this->data['css'][]    = config('plugin_url').'bootstrap-datepicker/css/bootstrap-datepicker.min.css';
        $this->data['css'][]    = config('plugin_url').'fancybox/jquery.fancybox.min.css';
        $this->data['js'][]  = config('jsapp_url').'event_register.js';

        $this->data['event_id'] = $this->uri->segment(3);
        $this->data['title']  = 'รายชื่อผู้ลงทะเบียนอีเว้นท์';

        $this->load->view('theme/_header', $this->data);
        $this->load->view('event/register_list');
        $this->load->view('theme/_footer');
    }


    //----
    private function __registerDataList()
    {
        $postData  = $this->input->post();
        $getData   = $this->input->get();
        $event_id = !empty($postData['event_id']) ? $postData['event_id'] : $getData['event_id'];
        $params   = $getData;

        //---Page control
        $page_current = !empty($getData['page']) ? $getData['page'] : NULL;
        $params['limit'] = config('page_limit');
        if(!empty($page_current) && $page_current > 1)
            $params['offset'] =(($params['limit'] * $page_current) - $params['limit']);
        else
            $params['offset'] = 0;

        unset($getData['page']);
        $filter_url = !empty($getData) ?  ''.http_build_query($getData) : NULL;

        //--Pagination
        $page_params['base_url']         = site_url('event-register/data-list?'.$filter_url);
        $page_params['total_rows']       = $this->Invoice->countTotalInvoice($event_id, $params);
        $page_params['per_page']         = config('page_limit');
        $page_params['uri_segment']      = 3;
        $this->data['pagination']        = getPagination($page_params);

        $this->data['start_index']       = $params['offset'];
        //-----
        $this->data['data_list']  = $this->Invoice->getInvoice($event_id, $params);
        $this->data['start_no'] = 1;
        $this->load->view('event/register_list_data', $this->data);
    }

    //----Ajax
    private function __viewPaymentDetail()
    {
        $event_id    = (int)$this->uri->segment(3);
        $customer_id = (int)$this->uri->segment(4);

        $this->data['plugin'][1] = config('plugin_url').'fancybox/jquery.fancybox.min.js';
        $this->data['css'][1]    = config('plugin_url').'fancybox/jquery.fancybox.min.css';
        $this->data['js'][0]     = config('jsapp_url').'event_register_payment.js?ver=1';
        $this->data['plugin'][] = config('plugin_url').'bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $this->data['css'][]    = config('plugin_url').'bootstrap-datepicker/css/bootstrap-datepicker.min.css';
        #-----
        $this->data['payment'] = $this->Invoice->getInvoicePayment($event_id, $customer_id);
        $this->load->view('theme/_blank_header', $this->data);
        $this->load->view('event/register_payment_detail', $this->data);
        $this->load->view('theme/_blank_footer');
    }

    //----
    private function __savePaymentStatus()
    {
        $inputs = $this->input->post();

        if(empty($inputs['status'])){
            $response['status'] = FALSE;
            $response['message'] = lang('HTTP_UNAUTHORIZED');
            echo json_encode($response);
            die();
        }

        $event_id    = (int)$inputs['event_id'];
        $invoice_id  = (int)$inputs['invoice_id'];
        $customer_id = (int)$inputs['customer_id'];
        $receipt_date = date('Y-m-d', strtotime($inputs['receipt_date']));
        $status      = $inputs['status'];

        $this->Invoice->updateReceiptDate($invoice_id, $receipt_date);


        $api_uri     = config('inside_api_url').'event/'.$event_id.'/receipt-slip/'.$customer_id;
        $api_execute = $this->API->get($api_uri);
        if(!$api_execute['status']){
            $response['status']  = FALSE;
            $response['message'] = lang('system_timeout');
            echo json_encode($response);
        }


        $this->Invoice->updatePaymentStatus($invoice_id, $status);
        $response['status'] = TRUE;
        $response['message'] = lang('data_save_success');
        echo json_encode($response);
    }

    //----
    private function __participant()
    {
        $event_id    = $this->uri->segment(3);
        $customer_id = $this->uri->segment(4);
        $this->data['event_id']      = $event_id;
        $invoice = $this->Invoice->getInvoiceDetail($event_id, $customer_id);
        $this->data['plugin'][1] = config('plugin_url').'momentjs/moment.js';
        $this->data['plugin'][2] = config('plugin_url').'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js';
        $this->data['css'][1]    = config('plugin_url').'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css';
        $this->data['js'][0]  = config('jsapp_url').'event_register_participant.js?v=2';
        #-----
        $this->data['participants'] = $this->Register->getEventParticipant($event_id, $customer_id);
        $this->data['address']      = $this->Invoice->getInvoiceAddress($invoice->billing_id);
        $this->data['bill']         = prettyInvoiceAddress($invoice->billing_id);
        $this->data['customer']     = $this->Customer->getCustomerDetail($customer_id);
        $this->data['invoice']      = $invoice;
        $this->load->view('theme/_blank_header', $this->data);
        $this->load->view('event/register_participant', $this->data);
        $this->load->view('theme/_blank_footer');
    }

    private function __uploadExcel()
        {
            $config['upload_path']          = './_uploads/temp';
            $config['allowed_types']        = 'xlsx';

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('fileupload'))
            {
                    $error = array('error' => $this->upload->display_errors());
                    echo json_encode( $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    echo json_encode( $data);
            }
        }
    private function __importExcel()
        {
                  //--breadcrumb
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
                $this->breadcrumb->add('Event', site_url('event'));
                $this->breadcrumb->add('Register', '#');


                $this->data['plugin'][] = config('plugin_url').'fancybox/jquery.fancybox.min.js';
                $this->data['plugin'][] = config('plugin_url').'bootstrap-datepicker/js/bootstrap-datepicker.min.js';
                $this->data['css'][]    = config('plugin_url').'bootstrap-datepicker/css/bootstrap-datepicker.min.css';
                $this->data['css'][]    = config('plugin_url').'fancybox/jquery.fancybox.min.css';
                $this->data['js'][]  = config('jsapp_url').'event_register_import.js';

                $this->data['event_id'] = $this->uri->segment(3);
                $this->data['title']  = 'นำเข้ารายชื่อผู้ลงทะเบียนอีเว้นท์แบบกลุ่ม';

                $this->load->view('theme/_header', $this->data);
                $this->load->view('event/register_import');
                $this->load->view('theme/_footer');
        }
        private function __summarydata()
            {
                $postData  = $this->input->post();
                        $getData   = $this->input->get();
                        $event_id = !empty($postData['event_id']) ? $postData['event_id'] : $getData['event_id'];

                    $data=  array("data"=> $this->Event->getInvoiceSummary($event_id));
                    $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));

            }

}
