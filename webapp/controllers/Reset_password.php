<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_password extends MY_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('Forgot_password_model', 'Forgot');
    }

    public function index()
    {
      $data['provinder']  = 'Road safety.';
      $data['title']  = 'Reset new password';

      $customer_id = (int)$this->uri->segment(2);
      $token = $this->uri->segment(3);
      if(!is_numeric($customer_id) || empty($token))
        show_404();

      $data['customer_id'] = $customer_id;
      $data['token'] = $token;
      $data['checked_result'] = $this->Forgot->checkToken($customer_id, $token);

      //-----
      $this->load->view('reset_password/reset_form', $data);

    }

    public  function save()
    {
      $inputs   = $this->input->post();
      $endpoint = config('inside_api_url').'web/reset-password';
      $result   = $this->API->post($endpoint, $inputs);
      if(!$result['status']){
        $response['status'] = false;
        $response['message'] = $result['status_message'];
        echo json_encode($response);
        die();
      }

      $response['status']  = true;
      $response['message'] = 'เปลี่ยนรหัสผ่านสำเร็จ.';
      echo json_encode($response);
    }
}