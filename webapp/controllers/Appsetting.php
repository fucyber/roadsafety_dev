<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appsetting extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('App_setting_model', 'AppSetting');
    }

    public function index()
    {
      $data['title'] = 'Application Setting';
      $data['appsetting'] = $this->AppSetting->getappsetting();
      $data['event_list'] = $this->Event->queryEventList();
      $this->load->view('theme/_header');
      $this->load->view('appsetting/index', $data);
      $this->load->view('theme/_footer');
    }

    public function updateappsetting()
    {
      echo $this->AppSetting->updateappsetting($this->input->post());
    }

}
