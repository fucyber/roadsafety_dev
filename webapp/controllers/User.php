<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {


      public function __construct()
      {
        parent::__construct();

        $this->load->model('User_model', 'User');
        $this->load->model('Event_model', 'Event');
        $this->load->model('Event_register_model', 'EventRegister');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('pagination');
      }

      public function index()
      {

        if(!$this->is_logged_in()){
          redirect(base_url('login'));
        }else{
          redirect(base_url('dashboard'));
        }
      }


      public function form_add(){
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('User', site_url('user/list/'));
        $this->breadcrumb->add('Add', '#');

        $this->data['title']  = 'เพิ่มผู้ใช้งานระบบ';
        $this->getForm();
      }

      public function form_edit($user_id){
        if(empty($user_id)){
          $response['status'] = FALSE;
          $response['message'] = 'Customer id us empty';
          echo json_encode($response);
          exit;
        }
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('User', site_url('user/list/'));
        $this->breadcrumb->add('Edit', '#');

        $this->data['title']  = 'แก้ไขสมาชิก';
        $this->data['user_id']  = $user_id;

        $this->getForm();

      }

      public function getForm(){
        $data = $this->data['level'] = $this->User->UserLv();
        if($this->uri->segment(3)){
          $data = $this->data['user'] = $this->User->editUser($this->uri->segment(3));
        }else{
          $this->data['user'] = [
            'user_id' => '',
            'email' => '',
            'firstname' => '',
            'lastname' => '',
            'level_name' => '',
            'level_id' => '',
            'status' => ''
          ];
        }
        //-----
        $this->data['js'][0]  = config('jsapp_url').'User.js';
        $this->data['title']  = 'จัดการผู้ใช้งานระบบ';
        //event_title  event_id
        $this->load->view('theme/_header', $this->data);
        $this->load->view('user/form');
        $this->load->view('theme/_footer');
      }

      public function form_list($offset = false){

        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('User', site_url('user/list/'));


        $total_records = $this->User->get_total($this->input->post('search'));
        // echo $total_records; die();
        $this->data['total_records'] = $total_records;

        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        if ($total_records > 0)
          {
              // get current page records
              $this->data["user"] = $this->User->getUser($limit_per_page, $start_index, $this->input->post('search'));
              $config['base_url'] = base_url() . 'user/list';
              $config['total_rows'] = $total_records;
              $config['per_page'] = $limit_per_page;
              $config["uri_segment"] = 3;

              $config['cur_tag_open'] = '<span class="curlink">';
              $config['cur_tag_close'] = '</span>';

              $config['num_tag_open'] = '<span class="numlink">';
              $config['num_tag_close'] = '</span>';

              $this->pagination->initialize($config);

              // build paging links
              $this->data["links"] = $this->pagination->create_links();
              $this->data['number'] = $start_index;
          }
        $this->load->view('theme/_header', $this->data);
        $this->load->view('user/list');
        $this->load->view('theme/_footer');
      }


      public function save_data(){

         $inputs = $this->input->post();
         $user_id = !empty($inputs['user_id']) ? $inputs['user_id'] : NULL;
         $checked = $this->User->checkDuplicateEmail($inputs['email'], $user_id);
         if($checked > 0){
          $result['status']  = false;
          $result['message'] = $inputs['email']." Email นี้มีผู้ใช้งานแล้ว กรุณาเลือกใหม่!";
          echo json_encode($result);
          die();
         }

          if(!empty($user_id))
            $response = $this->User->edit_user($inputs);
          else
             $response = $this->User->create_user($inputs);


          //---
         if (!$response) {
            $result['status']  = false;
            $result['message'] = lang('data_save_failed');
         }else{
            $result['status']  = true;
            $result['message'] = lang('data_save_success');
         }
         echo json_encode($result);
      }

      public function delete_data(){
          $response = $this->User->delete_user($this->uri->segment(3));
        // echo json_encode($response);
        redirect('/user/list');
      }





}
