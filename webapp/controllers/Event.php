<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Event_register_model', 'Register');
    }

    public function index()
    {
        $action = $this->uri->segment(2);
        switch ($action) {
            case 'load-data':
                $this->__loadData();
                break;
            case 'detail':
                $this->__eventDetail();
                break;
            case 'detail-tab-info':
                $this->__eventDetailInfo();
                break;
            case 'detail-tab-setting':
                $this->__eventDetailSetting();
                break;
            case 'detail-tab-document':
                $this->__eventDetailDocument();
                break;
            case 'update_document':
                $this->__update_document();
                break;
            case 'headdocument':
                $this->__headDocument();
                break;
            case 'delete_document':
                $this->__deleteDocument();
                break;
            case 'edit_head_document':
                $this->__editHeadDocument();
                break;
            case 'delete_head_document':
                $this->__deleteHeadDocument();
                break;
            case 'detail-tab-video':
                $this->__detailTabVideo();
                break;
            case 'edit_video':
                $this->__editVideo();
                break;
            case 'delete_video':
                $this->__deleteVideo();
                break;
            case 'detail-tab-event-customergroup':
                $this->__eventCustomerGroup();
                break;
            case 'edit_customergroup':
                $this->__editCustomerGroup();
                break;
            case 'delete_customergroup':
                $this->__deleteCustomerGroup();
                break;
            case 'detail-tab-event-gift':
                $this->__eventGift();
                break;
            case 'detail-tab-event-promotion':
                $this->__eventPromotion();
                break;
            case 'save-event-info':
                $this->__saveEventInfo();
                break;
            case 'save-event-card-setting':
                $this->__saveEventCardSetting();
                break;
            case 'save-event-invoice-setting':
                $this->__saveEventInvoiceSetting();
                break;
            case 'delete_gift':
                $this->__deleteGift();
                break;
            case 'save_gift':
                $this->__saveGift();
                break;
            case 'save_promotion':
                $this->__savePromotion();
              break;
            case 'delete_promotion':
                $this->__deletePromotion();
            break;
            default:
                $this->__listEvent();
                break;
        }
    }

    public function __eventGift()
    {
      $event_id = $this->input->post('event_id');
      $data['gifts'] = $this->Event->getGift($event_id);
      $data['event_id'] = $event_id;
      $this->load->view('event/event_detail_gift', $data);
    }
    public function __eventPromotion()
    {

      $event_id = $this->input->post('event_id');
      $data['promotions'] =  $this->Event->getPromotion($event_id);
      $data['event_id'] = $event_id;
      $this->load->view('event/event_detail_promotion', $data);
    }
    public function __deleteCustomerGroup()
    {
      $this->Event->deleteCustomerGroup($this->input->post('level_id'));
    }

    public function __editCustomerGroup()
    {
      $this->Event->editCustomerGroup($this->input->post());
    }

    public function __eventCustomerGroup()
    {
      // $event_id = $this->input->post('event_id');
      $this->data['customer_lv'] = $this->Event->getCustomerLv();
      // $this->data['event_id']  =  $event_id;
      $this->load->view('event/event_detail_customer_group', $this->data);
    }
    public function __deleteVideo()
    {
      $this->Event->deleteVideo($this->input->post('id'));
    }
    public function __editVideo()
    {
      echo json_encode($this->Event->editVideo($this->input->post(), $_FILES));
    }

    public function __detailTabVideo()
    {
      $event_id = $this->input->post('event_id');
      $this->data['video'] = $this->Event->getVideoAll($event_id);
      $this->data['event_id']  =  $event_id;
      $this->load->view('event/event_detail_video', $this->data);
    }
    public function __deleteHeadDocument()
    {
      $this->Event->deleteHeadDocuemnt($this->input->post('id'));
    }

    public function __editHeadDocument($value='')
    {
      $this->Event->editHeadDocuemnt($this->input->post());
    }
    public function __deleteDocument()
    {
      $this->Event->deleteDocuemnt($this->input->post('file_id'));
    }

    public function __headDocument()
    {

      $this->data['type'] = $this->Event->getDocuemntAllType($this->uri->segment(3));
      $this->data['head_doc_id']  =  $this->uri->segment(3);
      $this->load->view('theme/_header');
      $this->load->view('event/event_head_document', $this->data);
      $this->load->view('theme/_footer');

    }


    public function __update_document()
    {

      $config['upload_path']          = './_uploads/documents';
      $config['allowed_types'] = 'jpg|png|jpeg|gif|pdf';
      $config['max_size']      = 1072*100;
      $config['max_width']     = 4024;
      $config['max_height']    = 4024;
      $config['file_name']     = date('YmdHis').'_'.time();

      $this->load->library('upload', $config);

      $data = false;
      if ( ! $this->upload->do_upload('file'))
      {
             $error = array('error' => $this->upload->display_errors());
      }
      else
      {
             $data = array('upload_data' => $this->upload->data());
      }
      echo json_encode($this->Event->upload_file_document_event($this->input->post(),$data));
    }

    //----
    private function __listEvent()
    {
        //--breadcrumb
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('Event', site_url());

        $this->data['js'][0]  = config('jsapp_url').'event_list.js';
        $this->data['title']  = 'รายการอีเว้นท์';

        $this->load->view('theme/_header', $this->data);
        $this->load->view('event/event_list');
        $this->load->view('theme/_footer');
    }

    //---
    private function __loadData()
    {
        $this->data['data_list'] = $this->Event->queryEventList();

        $this->data['pagination'] = '';
        $this->load->view('event/event_list_datatable', $this->data);
    }

    //----
    private function __eventDetail()
    {
        $event_id = (int)$this->uri->segment(3);

        #--breadcrumb
        $this->breadcrumb->add('Dashboard', site_url('dashboard'));
        $this->breadcrumb->add('Event', site_url('event'));
        $this->breadcrumb->add('Detail', '#');

        $this->data['plugin'][] = config('plugin_url').'tinymce/tinymce.min.js';
        $this->data['plugin'][] = config('plugin_url').'jquery-dateFormat.min.js';
        $this->data['plugin'][] = config('plugin_url').'bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $this->data['css'][] = config('plugin_url').'bootstrap-datepicker/css/bootstrap-datepicker.min.css';

        $this->data['js'][0]     = config('jsapp_url').'event_detail.js';
        $this->data['title']     = 'Event Content';

        $this->data['current_tab'] = !empty($this->input->get('tab')) ? trim($this->input->get('tab')) : '';
        $this->data['data']        = $this->Event->queryEventDetail($event_id);
        $this->data['event_id']    = $event_id;
        #------
        $this->load->view('theme/_header', $this->data);
        $this->load->view('event/event_detail');
        $this->load->view('theme/_footer');
    }

    //------
    private function __eventDetailInfo()
    {
        $inputs = $this->input->post();
        $event  = array();
        $this->data['event_id']  =  '';
        if(!empty($inputs['event_id'])){
            $event = $this->Event->queryEventDetail($inputs['event_id']);
            $this->data['event_id'] = $inputs['event_id'];
        }
        $this->data['data'] = $event;
        $this->load->view('event/event_detail_info', $this->data);
    }

    //------
    private function __eventDetailSetting()
    {
        $event_id = $this->input->post('event_id');
        $this->data['event_id']  =  $event_id;
        $this->load->view('event/event_detail_setting', $this->data);
    }

    //-------

    public function __eventDetailDocument()
    {
      $event_id = $this->input->post('event_id');
      $this->data['document'] = $this->Event->getDocuemntAllFiles($event_id);
      $this->data['type'] = $this->Event->getDocuemntAllType($event_id);
      $this->data['event_id']  =  $event_id;
      $this->load->view('event/event_detail_document', $this->data);
    }

    //----
    private function __saveEventInfo()
    {
        $inputs = $this->input->post();
        $event_id = !empty($inputs['event_id']) ? $inputs['event_id'] : NULL;
        $data['event_title']   = $inputs['event_title'];
        $data['event_excerpt'] = $inputs['event_excerpt'];
        $data['event_start_dtm']    = date('Y-m-d H:i:s', strtotime($inputs['start_date'].' '.$inputs['start_time']));
        $data['event_end_dtm']      = date('Y-m-d H:i:s', strtotime($inputs['end_date'].' '.$inputs['end_time']));
        $data['event_venue']   = $inputs['event_venue'];
        $data['latitude']      = $inputs['latitude'];
        $data['longitude']     = $inputs['longitude'];

        //--------------UPLOAD-----------------------------------------------------------
        $config['upload_path']          = './_uploads/events';
        $config['allowed_types'] = 'jpg|png|jpeg|gif|pdf';
        $config['max_size']      = 1072*100;
        $config['max_width']     = 4024;
        $config['max_height']    = 4024;
        $config['file_name']     = date('YmdHis').'_'.time();

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image_cover'))
        {
               $data['event_image'] = $this->upload->data()['file_name'];
        }
        if ($this->upload->do_upload('file_pdf'))
        {
               $data['event_doc'] = $this->upload->data()['file_name'];
        }
        //--------------------------------------------------------------------------------

        if(empty($event_id)){
            $result = $this->Event->insertEvent($data);
        }else{
            $result = $this->Event->updateEvent($event_id, $data);
        }

        if(!$result){
            $response['status']  = false;
            $response['message'] = lang('insert_data_failed');
            echo json_encode($response);
            exit();
        }

        $response['status']  = true;
        $response['message'] = lang('insert_data_success');
        echo json_encode($response);

    }

    //-----
    private function __saveEventCardSetting()
    {
        $inputs   = $this->input->post();
        $event_id = $inputs['event_id'];
        if(empty($event_id) || empty($inputs['card'])){
            $response['status'] = false;
            $response['message'] = lang('data_not_found');
            echo json_encode($response);
            exit();
        }

        #----Update
        foreach ($inputs['card'] as $key => $val) {
            $result = $this->Event->updateCardSetting($event_id, $key, $val);
        }

        #----Upload logo
        if(!empty($_FILES['file_logo']['name'])){
            $upload_path = config('upload_dir').'event_settings/';
            $oldFilename = !empty($inputs['old_file_logo']) ? $inputs['old_file_logo'] : NULL;
            $file = $this->Common->saveFile($_FILES, 'file_logo', $upload_path, $oldFilename);
            if(!empty($file['file_name'])){
                $this->Event->updateCardSetting($event_id, 'card_logo', $file['file_name']);
            }
        }

        $response['status']  = true;
        $response['message'] = lang('insert_data_success');
        echo json_encode($response);
    }

    //-----
    private function __saveEventInvoiceSetting()
    {
        $inputs   = $this->input->post();
        $event_id = $inputs['event_id'];
        if(empty($event_id) || empty($inputs['setting'])){
            $response['status'] = false;
            $response['message'] = lang('data_not_found');
            echo json_encode($response);
            exit();
        }

        #----Update
        foreach ($inputs['setting'] as $key => $val) {
            $result = $this->Event->updateInvoiceSetting($event_id, $key, $val);
        }


        #----Upload image
        if(!empty($_FILES['organizer_logo']['name'])){
            $upload_path = config('upload_dir').'event_settings/';
            $oldFilename = !empty($inputs['old_organizer_logo']) ? $inputs['old_organizer_logo'] : NULL;
            $file = $this->Common->saveFile($_FILES['organizer_logo'], 'organizer_logo', $upload_path, $oldFilename);
            if(!empty($file['file_name'])){
                $this->Event->updateInvoiceSetting($event_id, 'organizer_logo', $file['file_name']);
            }
        }

        #----Upload image
        if(!empty($_FILES['bank_logo']['name'])){
            $upload_path = config('upload_dir').'event_settings/';
            $oldFilename = !empty($inputs['old_bank_logo']) ? $inputs['old_bank_logo'] : NULL;
            $file = $this->Common->saveFile($_FILES['bank_logo'], 'bank_logo', $upload_path, $oldFilename);
            if(!empty($file['file_name'])){
                $this->Event->updateInvoiceSetting($event_id, 'invoice_bank_logo', $file['file_name']);
            }
        }

        $response['status']  = true;
        $response['message'] = lang('insert_data_success');
        echo json_encode($response);
    }


    private function __deleteGift()
    {
        $event_id = $this->input->post('event_id');
        $gift_id  =  $this->input->post('gift_id');
        $this->Event->deleteGift($event_id, $gift_id);
    }

    private function __saveGift()
    {
        $post  = $this->input->post();
        if(!empty($_FILES['image']['name'])){
          $config['upload_path']          = './_uploads/events';
          $config['allowed_types'] = 'jpg|png|jpeg|gif';
          $config['max_size']      = 1024*100;
          $config['max_width']     = 4024;
          $config['max_height']    = 4024;
          $config['overwrite']     = false;
          $config['file_name']     = date('YmdHis').'_'.time();
         $this->load->library('upload', $config);
         if($this->upload->do_upload('image')){
           $file_data = $this->upload->data();
           $data['image']  = $file_data['file_name'];
         }
      }
      $data['name']      = $post['name'];
      $data['amount']    = $post['amount'];
      $data['redeem_point'] = $post['redeem_point'];
      if ($post['gift_id']) {
        $this->db->where('gift_id', $post['gift_id']);
        $this->db->update('event_gifts', $data);
      }else{
        $data['event_id']   = $post['event_id'];
        $this->db->insert('event_gifts', $data);
      }
    }

    private function __savePromotion()
    {
          $post     = $this->input->post();
          $event_id = (int)$post['event_id'];
          $air_gen  = ramdomString(6);
          $promotion_code = !empty($post['code']) ? trim($post['code']) : $this->__airGenPromotionCode($event_id, $air_gen);
          $data['name']           = $post['name'];
          $data['amount']         = $post['amount'];
          $data['balance']        = $post['amount'];
          $data['discount']       = $post['discount'];
          if($post['promotion_type'] =='code' ){
            $data['type']          = 'code';
            $data['code']           = $promotion_code;
          }else{
            $data['code']           = '';
            $data['type']           = 'period';
          }

          $data['start_datetime'] = date('Y-m-d', strtotime($post['date_start'])).' 00:00:00';
          $data['end_datetime']   = date('Y-m-d', strtotime($post['date_end'])).' 23:59:59';
          if ($post['pro_id']) {
            $data['created_dtm'] = date('Y-m-d H:i:s');
            $data['updated_dtm'] = date('Y-m-d H:i:s');
            $this->db->where('promotion_id', $post['pro_id']);
            $this->db->update('event_promotions', $data);
          }else{
            $data['updated_dtm'] = date('Y-m-d H:i:s');
            $data['event_id']   = $post['event_id'];
            $this->db->insert('event_promotions', $data);
          }
    }

    private function __airGenPromotionCode($event_id, $code)
    {
        $this->db->where('status', 'active');
        $this->db->where('event_id', $event_id);
        $this->db->where('code', $code);
        $query = $this->db->get('event_promotions');
        if($query->num_rows() > 0){
            $getCode = ramdomString(6);
            $result_code = $this->__airGenPromotionCode($event_id, $getCode);
        }else{
            $result_code = $code;
        }
        return $result_code;
    }

    private function __deletePromotion()
    {
        $event_id = $this->input->post('event_id');
        $promotion_id  =  $this->input->post('promotion_id');
        $this->db->set('status', 'discard');
        $this->db->where('event_id', $event_id);
        $this->db->where('promotion_id', $promotion_id);
        $this->db->update('event_promotions');
    }

}
