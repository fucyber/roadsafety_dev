<div class="block-header">
    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6">
        <div class="row text-right">

        </div>
    </div>
</div><!-- /.block-header -->

<div class="container-fluid">
    <!-- Bordered Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <h2><?php echo $page_title;?></h2>
                </div>
                <div class="body table-responsive">
                  <form class="" action="<?php echo base_url('authen/form_list') ?>" method="post">
                    <div class="row clearfix">
                      <div class="col-md-12 m-t-20">
                        <a href="<?php echo base_url('authen/add')?>" class="btn btn-warning float-right" style="float:right">
                          <i class="material-icons">add</i>
                          <span>เพิ่มสิทธิ์การใช้งาน</span>
                        </a>

                      </div>
                    </div>
                    </form>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>ประเภทผู้ใช้งาน</th>
                              <th>ชื่อ-สกุล</th>
                              <th class="text-center">สถานะ</th>
                              <th class="text-center">จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($Authen as $key => $a): ?>
                            <tr>
                                <td scope=""><?=$key+1?></td>
                                <td><?php echo $a['user_group'] ?></td>
                                <td><?php echo $a['level_name'] ?></td>
                                <td class="text-center">
                                  <?php echo $a['level_status'] ?>
                                </td>
                                <?php if ($a['level_id'] > 1): ?>
                                  <td class="text-center">
                                    <a href="<?php echo base_url('authen/edit/'.$a['level_id']);?>"><i class="material-icons">create</i></a>
                                    &nbsp;
                                    <a onclick="delete_cutomer('<?=$a['level_id']?>')" style="cursor:pointer"><i class="material-icons">delete</i></a>
                                  </td>
                                <?php endif; ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Bordered Table -->
</div>
<script type="text/javascript">

  function delete_cutomer(id) {
    swal({
      title: "Delete?",
      text: "คุณต้องการลบสิทธิ์ !!!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
      setTimeout(function () {
        window.location = BASE_URL +'authen/delete-data/'+id;
      }, 1000);
    });
  }
</script>
