<div class="block-header">
  <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6 ">
    <div class="row text-right">
      <?php echo $this->breadcrumb->output(); ?> </div>
  </div>
</div>
<!-- /.block-header -->
<div class="container-fluid">
  <!-- Input -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12">
      <form method="POST" action="<?php echo base_url('authen/save-data') ?>" novalidate="novalidate" enctype="multipart/form-data">
        <div class="card">
          <div class="header">
            <h2><?php echo $title;?></h2> </div>
          <div class="body">
            <?php if(!empty($authen['level_id'])): ?> <input type="hidden" name="level_id" value="<?php echo $authen['level_id'] ?>">
            <?php endif; ?>
            <div class="row clearfix">
              <div class="col-sm-6 m-b-0">
                <div class="row clearfix">
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0"> <label>ชื่อ</label> </div>
                  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">
                    <div class="form-group">
                      <div class="form-line"> <input type="text" value="<?php echo $authen['level_name'] ?>" class="form-control" name="level_name" required aria-required="true"> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 m-b-0">
                <div class="row clearfix">
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0"> <label>Prefix</label> </div>
                  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">
                    <div class="form-group">
                      <div class="form-line"> <input type="text" value="<?php echo $authen['level_prefix'] ?>" class="form-control" name="level_prefix" required aria-required="true"> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-sm-6 m-b-0">
                <div class="row clearfix">
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0"> <label>Group</label> </div>
                  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">
                    <div class="form-group">
                      <div class="form-line">
                        <select class="" name="user_group">
                          <option <?php echo ($authen['user_group'] == 'system' )?'selected':''  ?> value="system">System</option>
                          <option <?php echo ($authen['user_group'] == 'event' )?'selected':''  ?> value="event">Event</option>
                          <option <?php echo ($authen['user_group'] == 'booth' )?'selected':''  ?> value="booth">Booth</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 m-b-0">
                <div class="row clearfix">
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0"> <label>Status</label> </div>
                  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">
                    <div class="form-group">
                      <div class="form-line">
                        <div class="form-line">
                          <select class="" name="level_status" >
                            <option <?php echo ($authen['level_status'] == 'active' )?'selected':''  ?> value="active">Active</option>
                            <option <?php echo ($authen['level_status'] == 'inactive' )?'selected':''  ?> value="inactive">Inactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group form-btn">
                <button class="btn btn-primary waves-effect right" type="submit">บันทึกข้อมูล</button>
                <a href="<?=base_url('authen/list')?>" class="btn btn-danger waves-effect right">ยกเลิก</a>
              </div>

            </div>

          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- #END# Input -->
</div>
<!-- Modal -->
