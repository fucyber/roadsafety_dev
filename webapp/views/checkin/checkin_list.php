<style media="screen">

  .checkboxdisplaylist{

    float: left;

    display: block;

    width: 20px;

    height: 20px;

    position: initial !important;

    opacity: 1 !important;

    margin-top: 1px !important;

    margin-right: 6px !important;

  }

</style>



<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

    <div class="card">

      <div class="header">

        <h2>Check in : <?php echo !empty($data) ? $data->event_title : '';?> : วันที่ <?=date("d/m/Y")?> </h2>

      </div>

      <div class="body" >

          <form class="" id="formdpl">

            <input type="text" style="max-width:300px; display: initial;" class="form-control " id="entercode" placeholder="Input member Code / Scan QR / ชื่อ นามสกุล">

            <input type="submit" class="btn btn-primary" value="ยืนยัน">

            <button type="button"class="btn btn-warning"  name="button" style="margin-left: 10px;" data-toggle="modal" data-target="#searchModal">ค้นหา</button>
          </form>
          <div class="right" >

            <a type="button"class="btn btn-primary" style="margin-top: -50px;" href="/web_register/<?php echo $event_id;?>" target=_blank>ลงทะเบียนผ่านเว็บ</a>
          </div>


        <div class="row" style="margin-top:20px; margin-bottom:20px; padding-top:10px; padding-bottom:0px; background: #eee;">

          <div class="col-sm-4">

            <strong>ชื่อ-นามสกุล:</strong>  <span id="dl_name"></span>

          </div>

          <div class="col-sm-4">

            <strong>กลุ่มผู้สมัคร:</strong>  <span id="dl_type"></span>

          </div>

          <div class="col-sm-4">

            <strong>Receipt:</strong>  <span id="dl_receipt"></span>

          </div>



        </div>



        <div class="row">

          <div class="col-sm-1">

            สถานะ

          </div>

          <div class="col-sm-2 ">

            <input type="checkbox" id="status_1" class="form-control checkboxdisplaylist" checked disabled>

            ลงทะเบียน

          </div>

          <div class="col-sm-2">

            <input type="checkbox" id="status_2" class="form-control checkboxdisplaylist" checked disabled>

            เข้าร่วมงาน

          </div>

          <div class="col-sm-2">

            <input type="checkbox" id="status_3" class="form-control checkboxdisplaylist" checked disabled>

            รับเอกสาร

          </div>

          <div class="col-sm-2">

            <input type="checkbox" id="status_4" class="form-control checkboxdisplaylist" checked disabled>

            รับคูปอง

          </div>

          <div class="col-sm-2">

            <input type="checkbox" id="status_5" class="form-control checkboxdisplaylist" checked disabled>

            อื่นๆ

          </div>

        </div>

        <div class="row">

          <span id="beforcheckin">
          </span>
          <button type="button" class="btn btn-primary" name="button" onclick="dp_list.actionupdate()" style="float: right;margin-right: 3%;">

            <i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึกข้อมูล

          </button>

        </div>

        <hr>

        <div class="row">

          <table class="table">

              <thead>

                 <tr>

                      <th class="col-sm-1  text-center">ID สมาชิก</th>

                      <th >ชื่อนามสกุล</th>

                      <th class="text-center" style="width:100px;">สถานะ 1</th>

                      <th class="text-center" style="width:100px;">สถานะ 2</th>

                      <th class="text-center" style="width:100px;">สถานะ 3</th>

                      <th class="text-center" style="width:100px;">สถานะ 4</th>

                      <th class="text-center" style="width:100px;">สถานะ 5</th>

                      <th class="text-center" style="width:100px;">Print</th>

                  </tr>

              </thead>

              <tbody id="adatatable">

              </tbody>

          </table>

        </div>



      </div>

    </div>

  </div>

</div>


<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ค้นหา</h5>
      </div>
      <div class="modal-body">
        <form class="" id="formsearch">
          <input type="text" style="max-width:300px; display: initial;" class="form-control " id="searchname" placeholder="ชื่อ หรือ นามสกุล">
          <input type="submit" class="btn btn-primary" value="ยืนยัน">
        </form>
        <div id="search_list">

        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>

  var print_url = '<?php echo base_url('event/'.$event_id.'/print-customer-card');?>';

  var dp_list = {

    event_id : <?=json_encode($event_id)?>,

    array_data : <?=0//json_encode($customer_lv)?>,

    now_select : [],

    selectcuscode : function(e) {
      $("#searchModal").modal("hide");
      $('#entercode').val(e);
      $('#formdpl').submit();
    },
    start : function() {

      dp_list.loadtable();


      $('#formsearch').submit(function (e){
        e.preventDefault();
        $.ajax({
          url: BASE_URL + "event-checkin/search_customer", // point to server-side controller method
            data: {
              event_id : dp_list.event_id,
              search : $('#searchname').val()
            },
            dataType: 'JSON',
            type: 'post',
            success: function (data) {
              var strings = '';
              for (a of data) {
                strings += '<div style="margin: 5px;padding: 10px;border-bottom: 1px solid #eee;"><button type="button"class="btn btn-warning" name="button" style="margin-right: 10px;" onclick="dp_list.selectcuscode(\''+a.customer_code+'\')">เลือก</button>'+a.customer_code+' '+a.customer_name_title+' '+a.customer_firstname+' '+a.customer_lastname+'</div>';
              }
              $('#search_list').html(strings);
            }
          });

      });

      $('#formdpl').submit(function () {

       $.ajax({

           url: BASE_URL + "event-checkin/get_customer", // point to server-side controller method

           data: {

             customer_code : $('#entercode').val(),

             event_id : dp_list.event_id

           },

           dataType: 'JSON',

           type: 'post',

           success: function (data) {

             $('#entercode').val('');

             var data_array = eval(data);

             var array = data_array.member;


             var checkin = data_array.checkin;



             if (array.length) {

               dp_list.now_select = array[0];

             }else {

               swal("ไม่พบข้อมูลลงทะเบียนอีเว้นท์", "", "warning");

               return false;

             }

             $('#dl_name').html(dp_list.now_select.customer_firstname+' '+dp_list.now_select.customer_lastname);

             $('#dl_type').html(dp_list.now_select.level_name);



             if (dp_list.now_select.filename) {

               $('#dl_receipt').html('<a href="/_uploads/event_invoices/M_'+dp_list.now_select.filename+'" target="_blank">'+dp_list.now_select.filename+'</a>');

             }else{

               $('#dl_receipt').html('');

             }





             $('.checkboxdisplaylist').prop('checked', true).prop('disabled', false);

             if (dp_list.now_select.regis_id) {

               $('#status_1').prop('checked', true).prop('disabled', true);

             }



             for (c of checkin) {

               $('#status_'+c.status_id).prop('checked', true).prop('disabled', true);

             }





             $('.checkboxdisplaylist').parent().show();

             if (dp_list.now_select.status_3_a == "no") {

              $('#status_3').parent().hide();

              $('#status_3').prop('checked', false).prop('disabled', true);

             }

             if (dp_list.now_select.status_4_a == "no") {

              $('#status_4').parent().hide();

              $('#status_4').prop('checked', false).prop('disabled', true);

             }

             if (dp_list.now_select.status_5_a == "") {

              $('#status_5').parent().hide();

              $('#status_5').prop('checked', false).prop('disabled', true);

             }


             var bf_string = '';
             for (a of data_array.beforcheckin) {
               var dates  = a.day_date.split('-');
                bf_string += '<div style="margin-left: 10px;">'+dates[2]+'/'+dates[1]+'/'+dates[0];
                if (a.status_2*1) {
                  bf_string += '<span class="btn btn-sm btn-danger" style="margin-left: 10px;">เข้ารวมงาน</span>';
                }
                if (a.status_3*1) {
                  bf_string += '<span class="btn btn-sm btn-warning" style="margin-left: 10px;">รับเอกสาร</span>';
                }
                if (a.status_4*1) {
                  bf_string += '<span class="btn btn-sm btn-primary" style="margin-left: 10px;">รับคูปอง</span>';
                }
                if (a.status_5*1) {
                  bf_string += '<span class="btn btn-sm btn-success" style="margin-left: 10px;">อื่นๆ</span>';
                }
                bf_string +='</div>';

             }

             $('#beforcheckin').html(bf_string);
           }

         });



       return false;

      });

    },

    loadtable : function() {

      $.ajax({

          url: BASE_URL + "event-checkin/get_list", // point to server-side controller method

          data: {

            event_id : dp_list.event_id

          },

          type: 'post',

          dataType: 'JSON',

          success: function (data) {

            var array = eval(data);

            var stringb = '';



            for (a of array) {

              var status_2 = '<span class="btn btn-sm btn-outline-secondary">เข้ารวมงาน</span>',

                  status_3 = '<span class="btn btn-sm btn-outline-secondary">รับเอกสาร</span>',

                  status_4 = '<span class="btn btn-sm btn-outline-secondary">รับคูปอง</span>',

                  status_5 = '<span class="btn btn-sm btn-outline-secondary">อื่นๆ</span>',

                  btn_print = '';



                  for (c of a.checkin) {

                    if (c.status_id == 2) {

                      status_2 = '<span class="btn btn-sm btn-danger">เข้ารวมงาน</span>';

                      btn_print = '<a href="'+print_url+'/'+a.customer_id+'" target="_blank" class="btn btn-sm btn-primary">Print</a>';

                    }else if (c.status_id == 3) {

                      status_3 = '<span class="btn btn-sm btn-warning">รับเอกสาร</span>';

                    }else if (c.status_id == 4) {

                      status_4 = '<span class="btn btn-sm btn-primary">รับคูปอง</span>';

                    }else if (c.status_id == 5) {

                      status_5 = '<span class="btn btn-sm btn-success">อื่นๆ</span>';

                    }

                  }



              // if (a.status_2 == 'yes') {

              //   status_2 = '<span class="btn btn-sm btn-danger">เข้ารวมงาน</span>';

              //   btn_print = '<a href="'+print_url+'/'+a.customer_id+'" target="_blank" class="btn btn-sm btn-primary">Print</a>';

              // }

              // if (a.status_3 == 'yes') {

              //   status_3 = '<span class="btn btn-sm btn-warning">รับเอกสาร</span>';

              // }

              // if (a.status_4 == 'yes') {

              //   status_4 = '<span class="btn btn-sm btn-primary">รับคูปอง</span>';

              // }

              // if (a.status_5 == 'yes') {

              //   status_5 = '<span class="btn btn-sm btn-success">อื่นๆ</span>';

              // }





              stringb +=' <tr><td class="text-center" style="width:100px;">'+a.customer_code+'</td> <td>'+a.customer_firstname+' '+a.customer_lastname+'</td><td class="text-center" style="width:100px;"><span class="btn btn-sm btn-info">ลงทะเบียน</span></td><td class="text-center" style="width:100px;">'+status_2+'</td><td class="text-center" style="width:100px;">'+status_3+'</td><td class="text-center" style="width:100px;">'+status_4+'</td><td class="text-center" style="width:100px;">'+status_5+'</td><td class="text-center" style="width:100px;">'+btn_print+'</td></tr>';

            }//for



            $('#adatatable').html(stringb);



          }

      });

    },

    actionupdate : function() {

      if ($('#status_1').prop('checked')) {

        $.ajax({

            url: BASE_URL + "event-checkin/checkin_customer", // point to server-side controller method

            data: {

              customer : dp_list.now_select.customer_id,

              status_2 : $('#status_2').prop('checked'),

              status_3 : $('#status_3').prop('checked'),

              status_4 : $('#status_4').prop('checked'),

              status_5 : $('#status_5').prop('checked'),

              event_id : dp_list.event_id

            },

            dataType: 'JSON',

            type: 'post',

            success: function () {

              swal('ลงทะเบียนเข้างานสำเร็จ','','success');

              dp_list.now_select = [];

              $('.checkboxdisplaylist').prop('checked', false).prop('disabled', false);

              $('#dl_name').html('');

              $('#dl_type').html('');

              $('#dl_receipt').html('');

              dp_list.loadtable();

            }

          });

      }



    }





  }



setTimeout(function () {

  dp_list.start();

}, 100);





</script>

