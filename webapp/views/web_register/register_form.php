<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Event register</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo config('assets_url');?>images/edispro_favicon.png"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- JS Variables -->
    <meta name="BASE_URL" content="<?php echo base_url();?>" />
    <meta name="PLUGIN_URL" content="<?php echo config("plugin_url");?>" />
    <meta name="ENVIRONMENT" content="<?php echo ENVIRONMENT;?>" />
    <!-- css -->
    <link href="<?php echo config('plugin_url');?>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo config('plugin_url');?>node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>animate-css/animate.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>waitme/waitMe.min.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/theme-blue.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/style.css" rel="stylesheet">
    <script src="<?php echo config('plugin_url');?>jquery/jquery-2.2.4.min.js"></script>
    <script>var $event_price=<?php echo $event_price;?>;</script>
</head>
<body>
  <form id="registerForm">
  <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id;?>">
  <div class="container">
    <div class="row">
      <h4><?php echo $event_title;?></h4>
      <?php //printr($data);?>
      <div class="card">
          <div class="body">
            <div class="row" style="padding:0px;margin:0px;">
              <div class="col-md-3" style="margin:0px;">
                <h4>เลือกประเภทการลงทะเบียน</h4>
              </div>
              <div class="col-md-9" style="margin:0px;">
                <label class="radio-inline">
                  <input type="radio" name="type" id="type1" value="single" checked> ประเภทเดี๋ยว
                </label>
                <label class="radio-inline">
                  <input type="radio" name="type" id="type2" value="group"> ประเภทกลุ่ม
                </label>
              </div>
            </div>
          </div><!--/.body-->
      </div><!--/.card-->

      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="header">
              <h2>
                  ค่าลงทะเบียน
              </h2>
            </div>
              <div class="body">
                <div class="row" style="padding:0px;margin:0px;">
                  <div class="col-md-4">
                  <label> ค่าลงทะเบียนอีเว้นท์<br>
                    <strong id="event_price"><?php echo number_format($event_price, 0);?></strong>
                    </label>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>ส่วนลดโปรโมชั่น/คน</label>
                      <input id="discount" name="discount" type="number" class="form-control"   style="padding:5px 7px; border: 1px solid #ccc;">
                      
                    
                   </div>
                  </div>
                  
                  <div class="col-md-4">
                    กลุ่มผู้ลงทะเบียน
                    <select name="level_id" id="level_id" class="form-control" >
                      <?php foreach ($levels as $val):?>
                        <option <?php echo ($val->level_id==1)?'selected':'';?>
                          value="<?php echo $val->level_id;?>"><?php echo $val->level_name;?></option>
                      <?php endforeach;?>
                    </select>

                  </div>
                  
                </div><!--/.row-->
                <div class="row" style="padding:0px;margin:0px;">
                <div class="col-md-12">
                  <div class="form-group">
                  <label>ส่วนลดโปรโมชั่นรวม <span id="discount_total_txt">0</span> บาท</label>
                  <input id="discount_total" name="discount_total" type="hidden" >
                  </div>
                  </div>
                  </div><!--/.row-->
              </div><!--/.body-->
          </div><!--/.card-->
        </div><!--/.col-md-6-->
        <div class="col-md-6">
          <div class="card">
            <div class="header">
              <h2>
                  ชื่อผู้นำเข้าข้อมูล <span class="required">*</span>
              </h2>
            </div>
              <div class="body">
                <div class="row" style="padding:0px;margin:0px;">
                    <div class="form-group">
                    <label>ชื่อ-นามสกุล</label>
                      <input id="import_by" name="import_by" type="text" class="form-control" required   style="padding:5px 7px; border: 1px solid #ccc;">
                      <p class="help-block">(เพื่อให้ทราบใครนำเข้ามูลชุดนี้)</p>
                    </div>
                </div><!--/.row-->
              </div><!--/.body-->
          </div><!--/.card-->
        </div><!--/.col-md-6-->
      </div><!--/.row -->

      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="header">
              <h2>
                  รายละเอียดหน่วยงาน 
              </h2>
            </div>
              <div class="body">
                <div class="row" style="padding:0px;margin:0px;">
                    <div class="form-group">
                      <label>ชื่อหน่วยงาน</label>
                      <input id="agency_name" name="agency_name" type="text" class="form-control"    style="padding:5px 7px; border: 1px solid #ccc;">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                      <div class="" style="padding:0px;margin:0px;">
                        <label>สังกัดหน่วยงาน</label> <span class="required">*</span>
                        <select name="department_id" id="department_id" class="form-control" required>
                        <?php foreach ($departments as $val):?>
                          <option <?php echo ($val->department_name=='อื่นๆ')?'selected':'';?>
                            value="<?php echo $val->department_id;?>"><?php echo $val->department_name;?></option>
                        <?php endforeach;?>
                      </select>
                     </div>
                    </div>
                    <div class="col-md-6" >
                      <div class="" style="padding:0px;margin:0px;">
                        <label>สังกัดกระทรวง</label><span class="required">*</span>
                        <select name="ministry_id" id="ministry_id" class="form-control" required >
                        <?php foreach ($ministries as $val):?>
                          <option <?php echo ($val->ministry_name=='อื่นๆ')?'selected':'';?>
                            value="<?php echo $val->ministry_id;?>"><?php echo $val->ministry_name;?></option>
                        <?php endforeach;?>
                      </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" style="padding:0px;margin:0px;">
                        <label>หมายเลขโทรศัพท์</label>
                        <input id="tel" name="tel" type="text" class="form-control" style="padding:5px 7px; border: 1px solid #ccc;">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" style="padding:0px;margin:0px;">
                      <label>แฟกซ์</label>
                      <input id="fax" name="fax" type="text" class="form-control"  style="padding:5px 7px; border: 1px solid #ccc;">
                   </div>
                    </div>
              
                    </div><!--/.row-->
                </div><!--/.row-->
              </div><!--/.body-->
          </div><!--/.card-->
        </div><!--/.col-md-6-->

        <div class="col-md-6">
          <div class="card">
            <div class="header">
              <h2>
                  ที่อยู่สำหรับออกใบเสร็จ/จัดส่งเอกสาร
              </h2>
            </div>
              <div class="body">
                <div class="row" style="padding:0px;margin:0px;">
                  <div class="row">
                    <div class="col-md-4">
                    <h5>ออกใบเสร็จในนาม</h5>
                  </div>
                  <div class="col-md-8">
                    <label class="radio-inline">
                      <input type="radio" name="receipt_name_type" id="type1" value="agency" checked> องค์กร/หน่วยงาน
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="receipt_name_type" id="type2" value="personal"> บุคคล
                    </label>
                  </div>
                  </div>
                    <div class="form-group">
                      <label>เลขประจำตัวผู้เสียภาษี</label>
                      <input id="tax_no" name="tax_no" type="text" class="form-control"    style="padding:5px 7px; border: 1px solid #ccc;">
                    </div>
                    <div class="form-group">
                      <label>ที่อยู่</label>
                      <input id="billing_address" name="billing_address" type="text" class="form-control"    style="padding:5px 7px; border: 1px solid #ccc;">
                    </div>
                    <div class="form-group">
                      <label>ถนน</label>
                      <input id="billing_road" name="billing_road" type="text" class="form-control"    style="padding:5px 7px; border: 1px solid #ccc;">
                    </div>
                    <div class="col-md-6">
                      <div  style="padding:0px;margin:0px;">
                        <label>จังหวัด</label>
                        <select name="billing_province_id" id="billing_province_id" class="form-control" >
                        <?php foreach ($provinces as $val):?>
                          <option <?php echo ($val->province_id==1)?'selected':'';?>
                            value="<?php echo $val->province_id;?>"><?php echo $val->province_name_th;?></option>
                        <?php endforeach;?>
                      </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div  style="padding:0px;margin:0px;">
                        <label>อำเภอ/เชต</label>
                        <select name="billing_district_id" id="billing_district_id" class="form-control" >
                       
                      </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div  style="padding:0px;margin:0px;">
                        <label>ตำบล/แขวง</label>
                        <select name="billing_sub_district_id" id="billing_sub_district_id" class="form-control" >
                       
                      </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div style="padding:0px;margin:0px;">
                        <label>รหัสไปรษณีย์</label>
                        <input id="billing_zipcode" name="billing_zipcode" type="text" class="form-control"  style="padding:5px 7px; border: 1px solid #ccc;">
                       
                      </select>
                      </div>
                    </div>
                   

                    </div><!--/.row-->
                </div><!--/.row-->
              </div><!--/.body-->
          </div><!--/.card-->
        </div><!--/.col-md-6-->
      </div><!--/.row-->


      <div class="card">
        <div class="header">
          <h2>
              ข้อมูลผู้ลงทะเบียน
          </h2>
        </div>
          <div class="body">
            <div class="row" style="padding:0px;margin:0px;">
            <table class="table table-bordered">
            <thead>
            <tr>
            <th>ลำดับ</th>
            <th>คำนำหน้า</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>อีเมลล์</th>
            <th>เพศ</th>
            <th>อายุ</th>
            <th>อาชีพ</th>
            <th>ลักษณะงานที่รับผิดชอบ</th>
            <th>โทรศัพท์มือถือ</th>
            <th>อาหาร</th>        
            </tr>
            </thead>
            <tbody id="userlist">
           
            </tbody>
            </table>
            <div class="col-md-12">
            <button class="btn btn-warning right" id="btn-removeusers" style="padding-top: 10px;">

            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>

                <span>ลบชื่อล่าสุด</span>

            </button> 
            <button class="btn btn-primary right" id="btn-addusers" style="padding-top: 10px;margin-right: 10px">

            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>

                <span>เพิ่มรายชื่อ</span>

            </button>
          
            </div>
            <div class="col-md-12">
           <div style="margin-left:auto;margin-right:auto;width:400px;">
            <button type="submit" class="btn btn-primary" id="btn-submit" style="padding-top: 10px;width:400px">

         
                <span>ยืนยันการลงทะเบียน</span>

            </button>
            </div>
            </div>
            </div><!--/.row-->
          </div><!--/.body-->
      </div><!--/.card-->


    </div><!--/.row-->
  </div><!--/.container-->
  </form>


    <script src="<?php echo config('plugin_url');?>bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo config('plugin_url');?>node-waves/waves.js"></script>
    <script src="<?php echo config('plugin_url');?>sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo config('plugin_url');?>bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo config('plugin_url');?>waitme/waitMe.min.js"></script>
    <script src="<?php echo config('assets_url');?>js/admin.js"></script>
    <script src="<?php echo config('jsapp_url');?>common.js"></script>
    <script src="<?php echo config('jsapp_url');?>lang/lang_global.js"></script>
    <script src="<?php echo config('jsapp_url');?>web_register.js"></script>
</body>
</html>