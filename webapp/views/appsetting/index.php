<div class="block-header">
    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6">
        <div class="row text-right">
          <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->
<div class="container-fluid">
    <!-- Bordered Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <h2><?php echo $title;?></h2>
                </div>
                <div class="body table-responsive">
                  <?php
                    $appsetting = json_decode($appsetting[0]->value);
                  ?>
                  <label class="custom-control custom-radio">
                    <input id="radio2" name="radio_setting" type="radio" class="custom-control-input" value="list" <?php if ($appsetting->event_display == 'list'){echo "checked";}?>>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">แสดงอีเว้นท์ทั้งหมด</span>
                  </label>
                  <br>
                  <label class="custom-control custom-radio">
                    <input id="radio1" name="radio_setting" type="radio" class="custom-control-input" value="single" <?php if ($appsetting->event_display == 'single'){echo "checked";}?>>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">แสดงอีเว้นท์เดียว</span>
                      <select class="custom-select" name ="event" style="padding: 5px;" id="selectevent">
                        <?php
                          foreach ($event_list as $key => $value) {
                            if ($appsetting->event_id == $value->event_id) {
                              echo '<option value="'.$value->event_id.'" selected>'.$value->event_title.'</option>';
                            }else{
                              echo '<option value="'.$value->event_id.'">'.$value->event_title.'</option>';
                            }
                          }
                        ?>
                      </select>
                  </label>
                  <div class="row">
                    <div class="col-md-2">
                      <input type="button" class="btn btn-info"name="" value="Save" id="savesetting" onclick="savesetting()">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Bordered Table -->
</div>
<script type="text/javascript">
  function savesetting() {
    $.ajax({
        url: BASE_URL+"/appsetting/updateappsetting",
        data: {
          event_display : $('input[name=radio_setting]:checked').val(),
          event_id : $('#selectevent').val()
        },
        type: 'post',
        success: function (data) {
          swal("บันทักสำเร็จ","","success");
        }
      });
  }
</script>
