<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Login : มูลนิธินโยบายถนนปลอดภัย</title>

    <link rel="shortcut icon" type="image/png" href="<?php echo config('assets_url');?>images/edispro_favicon.png"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- JS Variables -->
    <meta name="BASE_URL" content="<?php echo base_url();?>" />
    <meta name="PLUGIN_URL" content="<?php echo config("plugin_url");?>" />
    <meta name="ENVIRONMENT" content="<?php echo ENVIRONMENT;?>" />
    <!-- css -->
    <link href="<?php echo config('plugin_url');?>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo config('plugin_url');?>node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>animate-css/animate.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>waitme/waitMe.min.css" rel="stylesheet" />
    <link href="<?php echo config('assets_url');?>css/theme-blue.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/style.css" rel="stylesheet">
    <?php
      if(isset($css)):
        foreach ($css as $css_val):
          echo '<link type="text/css" href="'.$css_val.'" rel="stylesheet" />'. PHP_EOL;
        endforeach;
      endif;
    ?>
</head>
<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><?php echo $title;?></a>
            <!-- <small> </small> -->
        </div>
        <div class="card">
            <div class="body">
                <form id="loginForm" method="POST">
                    <div class="msg"></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Login email" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <!-- <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label> -->
                            <!-- <a href="forgot-password.html">Forgot Password?</a> -->
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">
                             SIGN IN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="<?php echo config('plugin_url');?>jquery/jquery-2.2.4.min.js"></script>
    <script src="<?php echo config('plugin_url');?>bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo config('plugin_url');?>node-waves/waves.js"></script>
    <script src="<?php echo config('plugin_url');?>sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo config('plugin_url');?>bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo config('plugin_url');?>waitme/waitMe.min.js"></script>
    <script src="<?php echo config('assets_url');?>js/admin.js"></script>
    <script src="<?php echo config('jsapp_url');?>common.js"></script>
    <script src="<?php echo config('jsapp_url');?>lang/lang_global.js"></script>
    <?php
      if(isset($js)):
        foreach ($js as $val):
          echo '<script src="'.$val.'"></script>'. PHP_EOL;
        endforeach;
      endif;
    ?>
</body>
</html>