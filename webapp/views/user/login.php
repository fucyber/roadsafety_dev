<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Community Auth - Ajax Login Form View
 *
 * Community Auth is an open source authentication application for CodeIgniter 3
 *
 * @package     Community Auth
 * @author      Robert B Gottier
 * @copyright   Copyright (c) 2011 - 2017, Robert B Gottier. (http://brianswebdesign.com/)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://community-auth.com
 */

?>
<meta name="BASE_URL" content="<?php echo base_url();?>" />
<link rel="stylesheet" href="<?php echo base_url() ?>/_resources/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>/_resources/assets/css/login.css">
<?php
if( ! isset( $on_hold_message ) )
{
?>

  <div class="container">
         <div class="card card-container">
             <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
             <h1>Expo System</h1>
             <p id="profile-name" class="profile-name-card"></p>
               <?php echo form_open( 'ajax_login', ['class' => 'form-signin'] ); ?>
                 <span id="reauth-email" class="reauth-email"></span>
                 <input value="admin" type="text" name="inputUserName" id="inputUserName" class="form-control" placeholder="บัญชีผู้ใช้งาน" required autofocus>
                 <input value="123qwe!@#QWE" type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="รหัสผ่าน" required
                  <?php if( config_item('max_chars_for_password') > 0 )
           					echo 'maxlength="'.config_item('max_chars_for_password').'"';
           			  ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');">
                 <?php if( config_item('allow_remember_me') ): ?>
                   <div id="remember" class="checkbox">
                       <label>
                           <input type="checkbox" value="remember-me"> Remember me
                       </label>
                   </div>
           			 <?php else: ?>
                   <br />
                 <?php endif; ?>
                <input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />
           			<input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="submit" value="Login" id="submit_button" >เช้าสู่ระบบ</button>
             </form><!-- /form -->
         </div><!-- /card-container -->
     </div><!-- /container -->
		</div>
	</form>

<?php } ?>

<script src="<?php echo base_url() ?>/_resources/plugins/jquery/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url() ?>/_resources/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/_resources/jsApps/common.js"></script>
<script src="<?php echo base_url() ?>/_resources/jsApps/login.js"></script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">แจ้งเตือน</h4>
      </div>
      <div class="modal-body">
        ไม่สามารถเข้าสู่ระบบได้ Email หรือ Password ผิด
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
