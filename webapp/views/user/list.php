
<div class="block-header">
    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6">
        <div class="row text-right">
          <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->
<div class="container-fluid">
    <!-- Bordered Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <h2>จัดการผู้ใช้งาน</h2>
                </div>
                <div class="body table-responsive">
                  <!-- <form class="" action="<?php echo base_url('user/form_list') ?>" method="post"> -->
                    <div class="row clearfix">
                      <div class="col-md-3">
                        <!-- <label>Email/ Name</label>
                        <div class="form-group">
                            <div class="form-line">
                              <input type="text" name="search" class="form-control show-tick" value="">
                            </div>
                        </div> -->
                      </div>
                      <div class="col-md-1">
                        <!-- <button id="" type="submit" class="btn btn-primary waves-effect">
                          <i class="material-icons">search</i>
                          <span>ค้นหา</span>
                        </button> -->
                      </div>
                      <div class="col-md-8">
                        <a href="<?php echo base_url('user/add')?>" class="btn btn-warning float-right" style="float:right">
                          <i class="material-icons">add</i>
                          <span>เพิ่มผู้ใช้งานระบบ</span>
                        </a>

                       <!--  <a href="<?php echo base_url('authen')?>" class="btn btn-info float-right" style="float:right; margin-right:10px;">
                          <i class="material-icons">edit</i>
                          <span>จัดการ Level</span>
                        </a> -->

                      </div>
                    </div>
                    <!-- </form> -->
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>Email</th>
                              <th>Name</th>
                              <th>Level</th>
                              <th>Status</th>
                              <th>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($user as $key => $user): ?>
                            <tr>
                                <td scope=""><?=++$number?></td>
                                <th scope="row"><?php echo $user['email'] ?></th>
                                <td><?php echo $user['firstname']." ".$user['lastname'] ?></td>
                                <td><?php echo $user['level_name'] ?></td>
                                <td><?php echo $user['status'] ?></td>
                                <td>
                                  <a href="<?php echo base_url('user/edit/'.$user['user_id']);?>"><i class="material-icons">create</i></a>
                                  &nbsp;
                                  <a onclick="delete_cutomer('<?=$user['user_id']?>')" style="cursor:pointer"><i class="material-icons">delete</i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="right">
                      <nav>
                        <ul class="pagination">
                          <?php echo $links; ?>
                        </ul>
                      </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Bordered Table -->
</div>
<script type="text/javascript">
  var url = '<?= base_url() ?>';
  function delete_cutomer(id) {
    swal({
      title: "Delete?",
      text: "คุณต้องการลบข้อมูลสมาชิก !!!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
      setTimeout(function () {
        window.location = url+'user/delete-data/'+id;
      }, 1000);
    });
  }
</script>
