<div class="block-header">

    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6 ">

        <div class="row text-right">

            <?php echo $this->breadcrumb->output(); ?>

        </div>

    </div>

</div><!-- /.block-header -->

<div class="container-fluid">

    <!-- Input -->

    <div class="row clearfix">

        <div class="col-lg-12 col-md-12">



          <form id="userForm">
            <div class="card">

                <div class="header">

                    <h2>

                      <?php echo $title;?>

                    </h2>

                </div>

                <div class="body">

                <?php if(!empty($user_id)): ?>

                  <input type="hidden" name="user_id" value="<?php echo $user_id ?>">

                <?php endif; ?>

                  <div class="row clearfix">

                    <div class="col-sm-6  m-b-0">

                        <div class="row clearfix">

                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">

                              <label>ชื่อ</label>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">

                              <div class="form-group">

                                  <div class="form-line">

                                      <input type="text" value="<?php echo $user['firstname'] ?>" class="form-control" name="firstname" required aria-required="true">

                                  </div>

                              </div>

                          </div>

                        </div>

                    </div>

                    <div class="col-sm-6 m-b-0">

                        <div class="row clearfix">

                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">

                              <label>นามสกุล</label>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">

                              <div class="form-group">

                                  <div class="form-line">

                                      <input type="text" value="<?php echo $user['lastname'] ?>" class="form-control" name="lastname" required aria-required="true">

                                  </div>

                              </div>

                          </div>

                        </div>

                    </div>

                  </div>

                  <div class="row clearfix">

                    <div class="col-sm-6 m-b-0">

                        <div class="row clearfix">

                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">

                              <label>อีเมล</label>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">

                              <div class="form-group">

                                  <div class="form-line">

                                      <input type="email" value="<?php echo $user['email'] ?>" class="form-control" name="email" required aria-required="true">

                                  </div>

                              </div>

                          </div>

                        </div>

                    </div>

                    <div class="col-sm-6 m-b-0">

                        <div class="row clearfix">

                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">

                              <label>กำหนดสิทธิ์</label>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">

                              <div class="form-group">

                                  <div class="form-line">

                                    <select class="form-control show-tick" name="level_id">

                                      <?php foreach($level as $lv): ?>

                                      <option <?php echo ($lv['level_id'] == $user['level_id'] )?'selected':''  ?> value="<?=$lv['level_id']?>"><?=$lv['level_name']?></option>

                                      <?php endforeach; ?>

                                    </select>

                                  </div>

                              </div>

                          </div>

                        </div>

                    </div>


                    <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>สถานะ</label>
                          </div>
                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                  <div class="form-line">
                                    <select class="form-control show-tick" name="status">
                                      <option value="active" <?php echo ($user['status'] == 'active' )?'selected':''  ?>>Active</option>
                                      <option value="pending" <?php echo ($user['status'] == 'pending' )?'selected':''  ?>>Pending</option>
                                    </select>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>


                  </div>

                </div>

                <div class="header">

                    <h2>เปลี่ยน Password</h2>

                </div>

                <div class="body">

                  <div class="row clearfix">

                    <div class="col-sm-6 m-b-0">

                        <div class="row clearfix">

                          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label m-b-0">

                              <label>Password</label>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 m-b-0">

                              <div class="form-group">

                                  <div class="form-line">

                                    <input type="password"  class="form-control" name="password" <?php echo(isset($user_id) && !empty($user_id))?'':'required aria-required="true"' ?>>

                                  </div>

                              </div>

                          </div>

                        </div>

                    </div>

                  </div>

                  <div class="form-group form-btn">

                    <button class="btn btn-primary waves-effect right" type="submit">บันทึกข้อมูล</button>

                    <a href="<?=base_url('user/list')?>" class="btn btn-danger waves-effect right">ยกเลิก</a>
                  </div>

                </div>

            </div>

            </form>

        </div>

    </div>

    <!-- #END# Input -->

</div>

<!-- Modal -->

