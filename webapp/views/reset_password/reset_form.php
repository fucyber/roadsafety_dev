<!DOCTYPE html>
<html>
<head>
  <title><?php echo !empty($title) ? $title : '';?></title>

  <style type="text/css">
body {
  background: #384047;
  font-family: sans-serif;
  font-size: 12px;
  width: 100% !important;
}
h1,h2,h3,h4,h5{
  font-weight: normal;
}

.content_box {
  background: #fff;
  padding: 4em 4em 2em;
  max-width: 600px;
  margin: 50px auto 0;
  box-shadow: 0 0 1em #222;
  border-radius: 2px;
}

form h2 {
  margin: 0 0 50px 0;
  padding: 10px;
  text-align: center;
  font-size: 20px;
  color: #666666;
  border-bottom: solid 1px #e5e5e5;
}
form p {
  margin: 0 0 3em 0;
  position: relative;
}
form input {
  display: block;
  box-sizing: border-box;
  width: 100%;
  outline: none;
  margin: 0;
}
form input[type="text"],
form input[type="password"] {
  background: #fff;
  border: 1px solid #dbdbdb;
  font-size: 1.6em;
  padding: 0.8em 0.5em;
  border-radius: 2px;
  width: 100%;
}
form input[type="text"]:focus,
form input[type="password"]:focus {
  background: #fff;
}

form button[type="submit"] {
  background: rgba(148, 186, 101, 0.7);
  box-shadow: 0 3px 0 0 rgba(123, 163, 73, 0.7);
  border-radius: 2px;
  border: none;
  color: #fff;
  cursor: pointer;
  display: block;
  font-size: 2em;
  line-height: 1.6em;
  margin: 2em 0 0;
  outline: none;
  padding: 0.8em 0;
  text-shadow: 0 1px #68b25b;
  width:100%;
}
form button[type="submit"]:hover {
  background: #94af65;
  text-shadow: 0 1px 3px rgba(70, 93, 41, 0.7);
}
form label {
  position: absolute;
  left: 8px;
  top: 12px;
  color: #999;
  font-size: 16px;
  display: inline-block;
  padding: 4px 10px;
  font-weight: 400;
  background-color: rgba(255, 255, 255, 0);
  -moz-transition: color 0.3s, top 0.3s, background-color 0.8s;
  -o-transition: color 0.3s, top 0.3s, background-color 0.8s;
  -webkit-transition: color 0.3s, top 0.3s, background-color 0.8s;
  transition: color 0.3s, top 0.3s, background-color 0.8s;
}
form label.floatLabel {
  top: -11px;
  background-color: rgba(255, 255, 255, 0.8);
  font-size: 14px;
}
.help-text {
  display: block;
  padding: 2px 5px;
  color: #888;
}
.result_text,
.error{
  text-align: center;
  color: #e65100;
  margin-top: 20px;
}

  </style>
</head>
<body>

<div class="content_box">
<div id="result_text"></div>
<div id="form_section">
<?php if($checked_result === FALSE):?>
  <h2 class="error">ขออภัย!! ลิงค์นี้ใช้งานแล้วหรือเลยเวลาที่กำหนดไม่สามารถใช้งานได้!</h2>
<?php else: ?>
  <form id='resetPasswordForm'>
    <input type="hidden" name="customer_id" value="<?php echo $customer_id;?>">
    <input type="hidden" name="token" value="<?php echo $token;?>">
    <h2>กำหนดรหัสผ่านใหม่</h2>
      <p>
        <label for="password" class="floatLabel">Set New Password</label>
        <input id="password" name="password" type="password">
        <span class="help-text">รหัสผ่านขั้นต่ำ 4 ตัวอักษร</span>
      </p>
      <p>
        <label for="confirm_password" class="floatLabel">Confirm Password</label>
        <input id="confirm_password" name="confirm_password" type="password">
        <!-- <span>Your passwords do not match</span> -->
      </p>
      <p>
        <button type="submit" id="submit">
          <span id="loading" style="display:none;">
            <img src='<?php echo base_url('_resources/loading.gif');?>'>
          </span> Reset Password
        </button>
      </p>
    </form>
    </div>
    </div>
    <p style="text-align: center;color:#ccc;"><?php echo $provinder;?></p>


  <script src="<?php echo base_url('_resources/plugins/jquery/jquery-2.2.4.min.js');?>"></script>
  <link href="<?php echo base_url('_resources/plugins/sweetalert/sweetalert.css');?>" rel="stylesheet">
  <script src="<?php echo base_url('_resources/plugins/sweetalert/sweetalert-dev.js');?>"></script>
  <script>
  var BASE_URL = '<?php echo base_url();?>';
  //
  //

  $(function(){


   //---
   $( "body" ).on("submit", "form#resetPasswordForm", function(e) {
      var $password = $("#password");
      var $confirmPassword = $("#confirm_password");

      if($password.val().length < 4){
        swal("Warning", "กรุณากำหนดรหัสผ่านขั้นตำ 4 อักษรขึ้นไป!", "warning");
        return false;
      }
      if($password.val() != $confirmPassword.val()){
        swal("Warning", "ยืนยันรหัสผ่านไม่ตรงกัน กรุณายืนยันใหม่!", "warning");
        return false;
      }

      if($password.val() == ''){
        swal("Warning", "กรุณากรอกข้อมูลให้ครบถ้วน!", "warning");
        return false;
      }
      var formData = $('#resetPasswordForm').serialize();
      resetPassword(formData);
      e.preventDefault();
   });
  });

  //----`
  function resetPassword(params){
    $('#loading').show();
          $.ajax({
                url:  BASE_URL + 'save_new_password',
                type: 'POST',
                data: params,
                dataType: 'JSON',
                success: function(resp){
                  //console.log(formData);
                  if(resp.statu == false){
                    swal("Warning", resp.message, "warning");
                    return false;
                  }
                    swal("Successfully", resp.message, "success");
                    $('#result_text').html('<h2 style="text-align:center;">'+resp.message+'</h2>');
                    $('#form_section').hide();
                }
            }).always(function() {
                $('#loading').hide();
            });
            return false;
  }
  </script>
<?php endif;?>
</body>
</html>

