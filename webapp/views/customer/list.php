<div class="block-header">
    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6">
        <div class="row text-right">
          <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->
<div class="container-fluid">
    <!-- Bordered Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <h2><?php echo $title;?></h2>
                </div>
                <div class="body table-responsive">
                  <form class="" action="<?php echo base_url('customer/form_list') ?>" method="post">
                    <div class="row clearfix">
                      <div class="col-md-4">
                        <label>ค้นหาสมาชิก member Code/ โทรศัพท์</label>
                        <div class="form-group">
                            <div class="form-line">
                              <input type="text" name="search" class="form-control show-tick" value="">
                            </div>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <button id="" type="submit" class="btn btn-primary waves-effect">
                          <i class="material-icons">search</i>
                          <span>ค้นหาสมาชิก</span>
                        </button>
                      </div>
                      <div class="col-md-7">
                        <a href="<?php echo base_url('customer/add')?>" class="btn btn-warning float-right" style="float:right">
                          <i class="material-icons">add</i>
                          <span>เพิ่มสมาชิก</span>
                        </a>
                      </div>
                    </div>
                    </form>
                    <?php if(empty($customers)):?>
                      <h3>ไม่พบข้อมูล</h3>
                    <?php else:?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>Member Code</th>
                              <th>ชื่อสมาชิก</th>
                              <th>โทรศัพท์</th>
                              <th>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($customers as $customer): ?>
                            <tr>
                                <td scope=""><?php echo ++$number; ?></td>
                                <th scope="row"><?php echo $customer['customer_code'] ?></th>
                                <td><?php echo $customer['customer_name_title']." ".$customer['customer_firstname']." ".$customer['customer_lastname'] ?></td>
                                <td><?php echo $customer['customer_mobile'] ?></td>
                                <td>
                                  <a href="<?php echo base_url('customer/edit/'.$customer['customer_id']);?>"><i class="material-icons">create</i></a>
                                  &nbsp;
                                  <a onclick="delete_cutomer('<?=$customer['customer_id']?>')" style="cursor:pointer"><i class="material-icons">delete</i></a>
                                  <!-- <a class="" href="<?php echo base_url('customer/delete-data/'.$customer['customer_id']);?>"><i class="material-icons">delete</a> -->
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="right">
                      <nav>
                        <ul class="pagination">
                          <?php echo $links; ?>
                        </ul>
                      </nav>
                    </div>
                  <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Bordered Table -->
</div>
<script type="text/javascript">
  var url = '<?= base_url() ?>';
  function delete_cutomer(id) {
    swal({
      title: "Delete?",
      text: "คุณต้องการลบข้อมูลสมาชิก !!!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
      setTimeout(function () {
        window.location = url+'customer/delete-data/'+id;
      }, 1000);
    });
  }
</script>
