<div class="block-header">
    <div class="col-sm-6 col-xs-6 col-sm-offset-6 col-xs-offset-6 ">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->
<div class="container-fluid">
    <!-- Input -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form method="POST" action="<?php echo base_url('customer/save-data') ?>" novalidate="novalidate"   enctype="multipart/form-data">
            <div class="card">
                <div class="header">
                    <h2>จัดการสมาชิก</h2>
                </div>
                <div class="body">
                  <div class="row clearfix">
                    <div class="col-sm-6">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                          <label>อีเมล</label>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                          <div class="form-group">
                              <div class="form-line">
                                <input type="hidden" name="customer_id" value="<?php echo $customer['customer_id'] ?>">
                                <input type="text" value="<?php echo $customer['customer_email'] ?>" class="form-control" name="customer_email" required aria-required="true">
                              </div>
                          </div>
                      </div>
                    </div>
                    <!-- <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>Password</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                  <div class="form-line">
                                    <input type="password" value="<?php echo $customer['customer_password'] ?>" class="form-control" name="customer_password" <?php echo(isset($customer_id) && !empty($customer_id))?'':'required aria-required="true"' ?>>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div> -->
                  </div>
                  <hr/>

                </div>
                <div class="body">
                  <div class="row clearfix">
                    <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>คำนำหน้า</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                  <div class="form-line">
                                      <input type="text" value="<?php echo $customer['customer_name_title'] ?>" class="form-control" name="customer_name_title" required aria-required="true">
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row clearfix">
                    <div class="col-sm-6  m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>ชื่อ</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                  <div class="form-line">
                                      <input type="text" value="<?php echo $customer['customer_firstname'] ?>" class="form-control" name="customer_firstname" required aria-required="true">
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>นามสกุล</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="<?php echo $customer['customer_lastname'] ?>" class="form-control" name="customer_lastname" required aria-required="true">
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row clearfix">
                    <div class="col-sm-6  m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>เพศ</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                            <div class="form-group">
                              <select class="form-control show-tick" name="customer_gender">
                                <option value="male"<?php if($customer['customer_gender'] == 'male') echo 'selected'; ?>>ชาย</option>
                                <option value="female" <?php if($customer['customer_gender'] == 'female') echo 'selected'; ?>>หญิง</option>
                              </select>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>อายุ</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="<?php echo $customer['customer_age'] ?>" class="form-control" name="customer_age" required aria-required="true">
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row clearfix">
                    <div class="col-sm-6  m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>อาชีพ</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                            <div class="form-group">
                              <select class="form-control show-tick" name="occupation_id">
                                  <?php
                                  // print_r($customer); die();
                                  foreach($occupation_lists as $occupation):
                                    ?>
                                    <option <?php if($occupation['occupation_id'] == $customer['occupation_id']) echo 'selected'; ?> value="<?php echo $occupation['occupation_id']; ?>"><?php echo $occupation['occupation_name']; ?></option>
                                    <?php
                                  endforeach;
                                  ?>
                              </select>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6  m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>โทรศัพท์</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                            <div class="form-group">
                              <div class="form-line">
                                  <input type="text" value="<?php echo $customer['customer_mobile'] ?>" class="form-control" name="customer_mobile" required aria-required="true">
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-0">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label m-b-0">
                              <label>ลักษณะงาน</label>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 m-b-0">
                              <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="<?php echo $customer['job_description'] ?>" class="form-control" name="job_description" required aria-required="true">
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>

                  </div>

                  <hr/>
                  <div class="form-group form-btn p-b-20">
                    <button class="btn btn-primary waves-effect right" type="submit"> <i class="material-icons">save</i><span>บันทึกข้อมูล</span></button>
                    <a href="<?php echo base_url('/customer/list') ?>" class="btn btn-danger waves-effect right" type="reset"><i class="material-icons">close</i><span>ยกเลิก</span></a>
                  </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- #END# Input -->
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body"  id="myModalBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="myModalConfirmHeader"></div>
            <div class="modal-body" id="myModalConfirmBody"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok" onclick="FucDelete('<?php echo $customer_id ?>')" >Delete</a>
            </div>
        </div>
    </div>
</div>
