<div class="block-header">
    <div class="col-sm-6 col-xs-6">
        <div class="row">
            <h2><?php echo $title;?></h2>
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->

<div class="row clearfix">
  <div class="col-sm-12 col-xs-12">
  <div class="card">
    <div class="body">
      <?php //printr($this->session->userdata());?>
      <div id="display_datatable"></div>
    </div>
  </div>
  <div class="col-sm-12 col-xs-12">
</div><!-- /.row clearfix -->
