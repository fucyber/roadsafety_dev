<div class="block-header">

    <div class="col-sm-6 col-xs-6">

        <div class="row">

            <h2><?php echo $title;?></h2>

        </div>

    </div>

    <div class="col-sm-6 col-xs-6">

        <div class="row text-right">

            <?php echo $this->breadcrumb->output(); ?>

        </div>

    </div>

</div><!-- /.block-header -->



<!-- Content -->

<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

  <div class="card" style="padding:0px;">

    <div class="header" style="padding:6px 8px;">

        <h2>นำเข้า</h2>

    </div>

    <div class="body">

    <?php echo form_open_multipart('/event-register/upload_excel/'.$event_id);?>

      <input type="hidden" id="event_id" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">

      <div class="row">

        <div class="col-sm-3">

            <div class="form-group form-float">

                <div class="form-line">
               
                <input  type="file"  name="fileupload"  required id="fileupload" onchange="fileSelected();" >

                   

                </div>

            </div>

        </div>
        <div class="col-sm-9">
        <div style="height:50px">
        </div>
        </div>






        <div class="col-sm-6">
    <h5 id="error" >ต้องใช้ไฟล์ excel นามสกุล xlsx เท่านั้น. และรายชื่อต้องอยู่ Sheet แรก</h5>
        
        </div>

      

        <div class="col-sm-4"></div>

      

        <div class="col-sm-1">

            <button class="btn btn-primary waves-effect right" id="btn-filter" type="submit">

            <i class="fa fa-floppy-o" aria-hidden="true"></i>

                <span>นำเข้า</span>

            </button>

        </div>

    </div>

    </form>



    </div>

  </div>

  </div><!-- /.col-sm-12 -->

</div><!-- /.row clearfix -->





<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

  <div class="card">

    <div class="body">

      <div id="display_datalist"></div>

    </div>

  </div>

  <div class="col-sm-12 col-xs-12">

</div><!-- /.row clearfix -->

