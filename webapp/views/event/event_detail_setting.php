<div class="row clearfix">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="card">
        <div class="header bg-blue-grey">
          <h2> บัตรเข้างาน <small>(กำหนดข้อมูลบัตรเข้างาน)</small></h2>
        </div><!--/.header -->
        <div class="body">
          <form id="cardSettingForm">
              <input type="hidden" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">
              <label for="card_title">หัวข้อหลัก: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="card[card_title]" id="card_title"
                        value="<?php echo event_setting($event_id, 'card_title');?>"
                        placeholder="ระบุหัวข้อหลัก" class="form-control"/>
                  </div>
              </div>
              <label for="card_sub_title">หัวข้อย่อย: </label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="card[card_sub_title]" id="card_sub_title"
                        value="<?php echo event_setting($event_id, 'card_sub_title');?>"
                        placeholder="ระบุหัวข้อหลัก" class="form-control"/>
                  </div>
              </div>
              <label for="card_venue">วันที่และสถานที่จัดงาน: </label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="card[card_venue]" id="card_venue"
                        value="<?php echo event_setting($event_id, 'card_venue');?>"
                        placeholder="ระบุวันที่และสถานที่จัดงาน" class="form-control"/>
                  </div>
              </div>
              <label for="card_description">หมายเหตุ: </label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="card[card_description]" id="card_description"
                        value="<?php echo event_setting($event_id, 'card_description');?>"
                        placeholder="ระบุหมายเหตุ" class="form-control"/>
                  </div>
              </div>
              <div class="row">
                <div class="col-sm-8">
                  <label for="file_logo">Logo บัตรเข้างาน: </label>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="file" name="file_logo" id="file_logo" class="form-control"/>
                          <input type="hidden" name="old_file_logo" value="<?php echo event_setting($event_id, 'card_logo');?>"/>
                      </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php
                    $card_logo = event_setting($event_id, 'card_logo');
                    if(!empty($card_logo)){
                      echo '<img src="'.config('upload_url').'event_settings/'.$card_logo.'" style="width:100px;" />';
                    }
                  ?>
                </div>
              </div>
            <div class="text-right">
              <hr style="border-top: 3px double #8c8b8b;padding:0px;margin:5px 0px;">
              <button type="submit" id="btn-submit" class="btn btn-primary waves-effect">
                <i class="material-icons text-small">&#xE161;</i> SAVE </button>
            </div>
          </form>
        </div><!--/.body -->
      </div><!--/.card -->
   </div><!--/.col -->

   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="card">
        <div class="header bg-blue-grey">
          <h2> กำหนดค่าใบ Pay-in-slip </h2>
        </div><!--/.header -->
        <div class="body">
          <form id="invoiceSettingForm">
              <input type="hidden" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">
              <div class="row">
                <div class="col-sm-8">
                  <label for="organizer_logo">Logo ใบเสร็จ: </label> <span class="required">*</span> <small class="col-pink">(ขนาด 150x150px)</small>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="file" name="organizer_logo" id="organizer_logo" class="form-control"/>
                          <input type="hidden" name="old_organizer_logo" value="<?php echo invoice_setting($event_id, 'organizer_logo');?>"/>
                      </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php
                    $organizer_logo = invoice_setting($event_id, 'organizer_logo');
                    if(!empty($organizer_logo)){
                      echo '<img src="'.config('upload_url').'event_settings/'.$organizer_logo.'" style="width:100px;" />';
                    }
                  ?>
                </div>
              </div>
              <label for="organizer_name">ชื่อผู้มีอำนาจลงนาม: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[receipt_signature]" id="receipt_signature"
                        value="<?php echo invoice_setting($event_id, 'receipt_signature');?>"
                        placeholder="ระบุหัวข้อหลัก" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_name">ชื่อผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_name]" id="organizer_name"
                        value="<?php echo invoice_setting($event_id, 'organizer_name');?>"
                        placeholder="ระบุหัวข้อหลัก" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_address">ที่อยู่ผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_address]" id="organizer_address"
                        value="<?php echo invoice_setting($event_id, 'organizer_address');?>"
                        placeholder="ระบุที่อยู่ผู้ออกใบเสร็จ" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_tel">เบอร์โทรศัพท์ผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_tel]" id="organizer_tel"
                        value="<?php echo invoice_setting($event_id, 'organizer_tel');?>"
                        placeholder="ระบุวันที่และสถานที่จัดงาน" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_fax">เบอร์แฟกซ์ผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_fax]" id="organizer_fax"
                        value="<?php echo invoice_setting($event_id, 'organizer_fax');?>"
                        placeholder="ระบุบอร์แฟกซ์ผู้ออกใบเสร็จ" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_email">อีเมล์ผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_email]" id="organizer_email"
                        value="<?php echo invoice_setting($event_id, 'organizer_email');?>"
                        placeholder="ระบุอีเมล์ผู้ออกใบเสร็จ" class="form-control"/>
                  </div>
              </div>
              <label for="organizer_website">website ผู้ออกใบเสร็จ: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[organizer_website]" id="organizer_website"
                        value="<?php echo invoice_setting($event_id, 'organizer_website');?>"
                        placeholder="ระบุwebsite ผู้ออกใบเสร็จ" class="form-control"/>
                  </div>
              </div>
              <hr style="border-top: 1px dotted #8c8b8b;">
              <label for="invoice_bank_company_code">รหัสชำระเงินธนาคาร: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[invoice_bank_company_code]" id="invoice_bank_company_code"
                        value="<?php echo invoice_setting($event_id, 'invoice_bank_company_code');?>"
                        placeholder="ระบุwebsite ผู้ออกใบเสร็จ" class="form-control"/>
                  </div>
              </div>
              <div class="row">
                <div class="col-sm-8">
                  <label for="file_logo">Logo ธนาคาร: </label> <span class="required">*</span> <small class="col-pink">(ขนาด 150x150px)</small>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="file" name="bank_logo" id="file_logo" class="form-control"/>
                          <input type="hidden" name="old_bank_logo" value="<?php echo invoice_setting($event_id, 'invoice_bank_logo');?>"/>
                      </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php
                    $bank_logo = invoice_setting($event_id, 'invoice_bank_logo');
                    if(!empty($bank_logo)){
                      echo '<img src="'.config('upload_url').'event_settings/'.$bank_logo.'" style="width:40px;" />';
                    }
                  ?>
                </div>
              </div>
              <hr style="border-top: 1px dotted #8c8b8b;">
              <label for="invoice_title">ห้วข้องาน: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[invoice_title]" id="invoice_title"
                        value="<?php echo invoice_setting($event_id, 'invoice_title');?>"
                        placeholder="ระบุห้วข้องาน" class="form-control"/>
                  </div>
              </div>
              <label for="invoice_sub_title">ห้วข้อย่อย: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" name="setting[invoice_sub_title]" id="invoice_sub_title"
                        value="<?php echo invoice_setting($event_id, 'invoice_sub_title');?>"
                        placeholder="ระบุห้วข้อย่อย" class="form-control"/>
                  </div>
              </div>
              <label for="invoice_remark">หมายเหตุการชำระเงิน: <span class="required">*</span></label>
              <div class="form-group">
                  <div class="form-line">
                        <textarea name="setting[invoice_remark]" rows="10" id="editor_tinymce" class="form-control"
                          placeholder="ระบุหมายเหตุการชำระเงิน"><?php echo invoice_setting($event_id, 'invoice_remark');?></textarea>
                  </div>
              </div>

            <div class="text-right">
              <hr style="border-top: 3px double #8c8b8b;padding:0px;margin:5px 0px;">
              <button type="submit" id="btn-submit" class="btn btn-primary waves-effect">
                <i class="material-icons text-small">&#xE161;</i> SAVE </button>
            </div>
          </form>
        </div><!--/.body -->
      </div><!--/.card -->
   </div><!--/.col -->
</div><!--/.row -->