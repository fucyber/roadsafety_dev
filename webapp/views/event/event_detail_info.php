<form id="eventInfoForm">
<input type="hidden" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">
      <div class="row">
        <div class="col-sm-6">
          <?php //printr($data);?>
          <div class="form-group">
              <div class="form-line">
                <div class="" style="font-size: 15px;font-weight: bold;">
                  หัวข้อหลัก :
                </div>
                  <input type="text" name="event_title" id="event_title"
                    value="<?php echo !empty($data) ? $data->event_title :'';?>"
                    class="form-control" placeholder="Event title" />
              </div>
          </div>
          <div class="form-group">
              <div class="form-line">
                  <textarea name="event_excerpt" id="event_excerpt" class="form-control"
                    placeholder="ข้อมูล Event สั้นๆ" rows="5"><?php echo !empty($data) ? $data->event_excerpt :'';?></textarea>
              </div>
          </div>
          <?php /*
          <label for="image_cover">ภาพ Event (ขนาด 1024x460px)</label>
          <div class="form-group">
              <div class="form-line">
                  <textarea name="event_detail" id="event_detail" class="form-control" placeholder="รายละเอียดข้อมูล Event" rows="10"></textarea>
              </div>
          </div> * ?*/?>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <div class="" style="font-size: 15px;font-weight: bold;">
                  วันที่เริ่มต้น :
                </div>
                  <div class="form-line">
                      <input type="text" name="start_date" id="start_date"
                        value="<?php echo !empty($data) ? date('d-m-Y', strtotime($data->event_start_dtm)) :'';?>"
                        class="form-control" placeholder="วันที่เริ่มงาน" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <div class="form-group">
                <div class="" style="font-size: 15px;font-weight: bold;">
                  เวลาที่เริ่มต้น :
                </div>
                  <div class="form-line">
                      <input type="text" name="start_time" id="start_time"
                        value="<?php echo !empty($data) ? date('H:i', strtotime($data->event_start_dtm)) :'';?>"
                        class="form-control" placeholder="เวลาเริ่มงาน" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <div class="form-group">
                  <div class="form-line">
                    <div class="" style="font-size: 15px;font-weight: bold;">
                      วันที่สิ้นสุด :
                    </div>
                      <input type="text" name="end_date" id="end_date"
                        value="<?php echo !empty($data) ? date('d-m-Y', strtotime($data->event_end_dtm)) :'';?>"
                        class="form-control" placeholder="วันที่สิ้นสุดงาน" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <div class="form-group">
                  <div class="form-line">
                    <div class="" style="font-size: 15px;font-weight: bold;">
                      เวลาที่สิ้นสุด :
                    </div>
                      <input type="text" name="end_time" id="end_time"
                        value="<?php echo !empty($data) ? date('H:i', strtotime($data->event_end_dtm)) :'';?>"
                        class="form-control" placeholder="เวลาปิดงาน" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-12">
              <div class="form-group">
                  <div class="form-line">
                    <div class="" style="font-size: 15px;font-weight: bold;">
                      สถานที่จัดงาน :
                    </div>
                      <input type="text" name="event_venue" id="event_venue"
                        value="<?php echo !empty($data) ? $data->event_venue :'';?>"
                        class="form-control" placeholder="สถานที่จัดงาน" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <div class="form-group">
                  <div class="form-line">
                    <div class="" style="font-size: 15px;font-weight: bold;">
                      latitude :
                    </div>
                      <input type="text" name="latitude" id="latitude"
                        value="<?php echo !empty($data) ? $data->latitude :'';?>"
                        class="form-control" placeholder="พิกัด Latitude" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <div class="form-group">
                  <div class="form-line">
                    <div class="" style="font-size: 15px;font-weight: bold;">
                      longitude :
                    </div>
                      <input type="text" name="longitude" id="longitude"
                        value="<?php echo !empty($data) ? $data->longitude :'';?>"
                        class="form-control" placeholder="พิกัด Longitude" />
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <label for="image_cover">ภาพ Event (ขนาด 1024x460px)</label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="file" name="image_cover" id="image_cover" class="form-control"/>
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <label for="image_cover">อัพโหลดเอกสารข้อมูลสัมนา</label>
              <div class="form-group">
                  <div class="form-line">
                      <input type="file" name="file_pdf" id="file_pdf" class="form-control"/>
                  </div>
              </div>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <?php if(!empty($data) && !empty($data->event_image)): ;?>
                <img src="<?php echo config('upload_url').'events/'.$data->event_image;?>" class="img-responsive" alt="Event cover">
              <?php endif;?>
            </div><!--/.col-sm -->
            <div class="col-sm-6">
              <?php if(!empty($data) && !empty($data->event_image)): ;?>
                <a href="<?php echo config('upload_url').'events/'.$data->event_doc;?>"><?=$data->event_doc?></a>
              <?php endif;?>
            </div><!--/.col-sm -->
          </div><!--/.row -->
        </div>
      </div>
      <div class="text-right">
        <hr>
        <button type="submit" id="btn-submit" class="btn btn-primary waves-effect">
          <i class="material-icons text-small">&#xE161;</i> SAVE </button>
      </div>
</form>
