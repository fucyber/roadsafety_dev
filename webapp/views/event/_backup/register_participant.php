<section class="container-fluid" style="background:#fff;padding-top:10px;padding-bottom:10px;">

<?php if(empty($invoice)):?>

  <?php echo lang('data_not_found');?>

<?php else:?>

  <?php //printr($invoice);?>

  <?php //printr($customer);?>

  <?php //printr($participants);?>

  <table class="table" style="width:100%;margin:0 auto;">

    <tr style="background-color:#EEEEEE;">

      <th colspan="4" class="text-primary">ข้อมูลการลงทะเบียน</th>

    </tr>

    <tr>

      <th style="width:150px;">ชื่อผู้ลงทะเบียน:</th>

      <td ><?php echo $customer->customer_name_title.' '.$customer->customer_firstname.' &nbsp;&nbsp;'.$customer->customer_lastname;?></td>

      <th style="width:150px;">อีเมล:</th>

      <td><?php echo $customer->customer_email;?></td>

    </tr>

    <tr>

      <th>โทรศัพท์:</th>

      <td ><?php echo $customer->customer_mobile;?></td>

      <th></th>

      <td></td>

    </tr>

    <tr>

      <th>หน่วยงาน:</th>

      <td ><?php echo $customer->agency_name;?></td>

      <th>เลขที่ผู้เสียภาษี:</th>

      <td><?php echo $customer->tax_no;?></td>

    </tr>

    <tr>

      <th>ที่อยู่รับเอกสาร:</th>

      <td colspan="3"><?php echo $bill;?></td>

    </tr>

    <tr style="background-color:#EEEEEE;">

      <th colspan="4" class="text-primary">ข้อมูลการชำระเงิน</th>

    </tr>

    <tr>

      <th>รหัสชำ Ref 1:</th>

      <td ><?php echo $invoice->invoice_ref_1;?></td>

      <th>รหัสชำ Ref 2:<br><small style="font-weight:normal;">รหัสบุคคล/กลุ่ม</small></th>

      <td><?php echo $invoice->invoice_ref_2;?></td>

    </tr>

    <tr>

      <th>ยอดชำระทั้งหมด</th>

      <td><?php echo number_format($invoice->invoice_grand_totel, 0);?></td>

      <th>การชำระเงิน:</th>

      <td><?php echo paymentStatus($invoice->payment_status);?></td>

    </tr>

    <tr>

      <th>รหัสใบเสร็จรับเงิน:</th>

      <td></td>

      <th>วันที่ออกใบเสร็จ</th>

      <td>

        <input type='text' placeholder='' class="datepicker" id="billing_dtm" value="<?php echo preg_replace('/(\d{2}:\d{2}):00/', '${2}', $this->data['invoice']->billing_dtm) ?>">

        <button onclick="save_billing_dtm('<?php echo $this->data['invoice']->invoice_id ?>')">บันทึกวันที่ออกใบเสร็จ</button>

      </td>

    </tr>

    <tr style="background-color:#EEEEEE;">

      <th colspan="4" class="text-primary">รายชื่อผู้ลงทะเบียนอีเว้นท์</th>

    </tr>

    <tr>

      <td colspan="4">

        <table class="table table-hover" style="padding:5px;width:100%;">

          <tr>

            <th class="text-center" style="width:60px;">No.</th>

            <th>ชื่อ-สกุล</th>

            <th style="width:250px;">Email</th>

            <th style="width:150px;">โทรศัพท์</th>

          </tr>

          <?php $no=0;

          if(!empty($participants)):

          foreach ($participants as $val):?>

            <tr>

              <td class="text-center"><?php echo  ++$no;?></td>

              <td><?php echo $val->name_title.' '.$val->customer_firstname.' &nbsp;'.$val->customer_lastname;?></td>

              <td><?php echo $val->customer_email;?></td>

              <td><?php echo $val->customer_mobile;?></td>

            </tr>

          <?php endforeach;

                endif; ?>

        </table>

      </td>

    </tr>

  </table>

<?php endif;?>

</section>

