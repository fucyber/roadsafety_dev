<div class="row">
  <div class="table-responsive">
    <div class="header" style="border-bottom:0;">&nbsp;
        <ul class="header-dropdown">
          <li>
            <button id="btn-action" class="btn btn-warning waves-effect" onclick="add_document()">
            <!-- <i class="material-icons">&#xE146;</i> -->
            เพิ่มกลุ่มผู้สมัคร</button>
          </li>
        </ul>
    </div>


      <table class="table" >
          <thead>
             <tr>
                  <th class="col-sm-1 ">No.</th>
                  <th class="col-sm-3 ">รายชื่อกลุ่ม</th>
                  <th class="text-center">สถานะ3</th>
                  <th class="text-center">สถานะ4</th>
                  <th class="text-center">สถานะ5</th>
                  <th class="col-sm-2 text-center" >Actions</th>
              </tr>
          </thead>

          <tbody>
              <?php foreach ($customer_lv as $key => $val):?>
              <tr>
                <td>
                  <?=$key+1 ?>
                </td>
                <td id="row_cl_name_<?=$key?>">
                  <?=$val->level_name ?>
                </td>
                <td class="text-center" id="row_cl_status_1_<?=$key?>">
                  <?php
                    if ($val->status_1 == "yes") {
                      echo '<span class="btn btn-sm btn-warning">รับเอกสาร</span>';
                    }else{
                      echo '<span class="btn btn-sm btn-outline-secondary">รับเอกสาร</span>';
                    }
                  ?>
                </td>
                <td class="text-center" id="row_cl_status_2_<?=$key?>">
                  <?php
                    if ($val->status_2 == "yes") {
                      echo '<span class="btn btn-sm btn-info">รับคูปอง</span>';
                    }else{
                      echo '<span class="btn btn-sm btn-outline-secondary">รับคูปอง</span>';
                    }
                  ?>
                </td>
                <td class="text-center" id="row_cl_status_3_<?=$key?>">
                  <?php
                    if ($val->status_3 != "") {
                      echo '<span class="btn btn-sm btn-success">อื่นๆ</span>';
                    }else{
                      echo '<span class="btn btn-sm btn-outline-secondary">อื่นๆ</span>';
                    }
                  ?>
                </td>

                  <td class="col-sm-1 text-center">
                    <button type="button" class="" data-toggle="modal" data-target="#ModalCusLv" onclick="getmodal(<?=$key?>)">
                      <i class="fa fa-list-ul" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                    </button>
                    <button type="button" class="" onclick="deletedoc(<?=$key?>)">
                      <i class="fa fa-trash-o" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                    </button>
                  </td>
              </tr>
              <?php  endforeach;?>
          </tbody>
      </table>
  </div>
</div>

<!-- Modal -->
<div id="ModalCusLv" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <form class="" id="form_input_cl" method="post">
      <div class="modal-body">
        <div class="input-group input-group-md">
          <span class="input-group-addon">ชื่อกลุ่ม</span>
          <input id="f_cl_name" name="f_video_name" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon" style="width:150px; text-align:right;">รับเอกสาร</span>
          <input id="f_cl_group1" type="checkbox" class="form-control" name="" value="doc1" style="display: block;width: 17px;height: 19px;position: initial;opacity: 1;">
          <!-- <input id="f_cl_url" name="f_video_url" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;"> -->
        </div>

        <div class="input-group input-group-md">
          <span class="input-group-addon" style="width:150px; text-align:right;">รับคูปองอาหาร</span>
          <input id="f_cl_group2" type="checkbox" class="form-control" name="" value="doc1" style="display: block;width: 17px;height: 19px;position: initial;opacity: 1;">
        </div>

        <div class="input-group input-group-md">
          <span class="input-group-addon" style="width:150px; text-align:right;">อื่นๆ</span>
          <input id="f_cl_group3" type="checkbox" class="form-control" name="" value="doc1" style="display: block;width: 17px;height: 19px;position: initial;opacity: 1;" onclick="open_f_cl_group3input()">
          <input id="f_cl_group3input" name="f_video_name" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;width: 250px;height: 27px;margin-left: 25px;" disabled>
        </div>

      </div>
      </form>

      <div class="modal-footer">
        <!-- <input type="submit" name="" class="btn btn-primary" value="บันทึก"> -->
        <button type="button" class="btn btn-primary form_btn_submit" onclick="editdocument()"></button>
        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
      </div>

    </div>

  </div>
</div>

<script>
  var array_data = <?=json_encode($customer_lv)?>;
  var eventid = <?=0//json_encode($event_id)?>;
  var nowselect = -1;
  $(function(){
  });

  function deletedoc(data) {
    swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
          $.ajax({
              url: "../delete_customergroup", // point to server-side controller method
              data: { level_id : array_data[data].level_id },
              type: 'post',
              success: function (response) {
                Event._loadTabCustomerGroup();
              }
          });
    });

  }


  function getmodal(data) {
    $('.modal-title').html("แก้ไขกลุ่ม");
    $('.form_btn_submit').html("แก้ไข");
    nowselect = data;
    // console.log(array_data[data]);
    $('#f_cl_name').val(array_data[data].level_name);
    $('#f_cl_group1').prop('checked',false);
    $('#f_cl_group2').prop('checked',false);
    $('#f_cl_group3').prop('checked',false);
    if (array_data[data].status_1 == 'yes') {
      $('#f_cl_group1').prop('checked',true);
    }
    if (array_data[data].status_2 == 'yes') {
      $('#f_cl_group2').prop('checked',true);
    }
    if (array_data[data].status_3 != ''){
      $('#f_cl_group3').prop('checked',true);
      $('#f_cl_group3input').val(array_data[data].status_3);
    }
    // $('#f_video_url').val(array_data[data].video_link);
    open_f_cl_group3input();
  }

  function open_f_cl_group3input() {
    if ($('#f_cl_group3').prop('checked')) {
      $('#f_cl_group3input').prop('disabled', false);
    }else{
       $('#f_cl_group3input').val('');
        $('#f_cl_group3input').prop('disabled', true);
    }
  }

  function add_document() {
    $('.modal-title').html("เพิ่มกลุ่ม");
    $('.form_btn_submit').html("เพิ่ม");
    $('#ModalCusLv').modal('show');
    nowselect = -1;

    $('#f_cl_name').val('');
    $('#f_cl_group1').prop('checked',false);
    $('#f_cl_group2').prop('checked',false);
    $('#f_cl_group3').prop('checked',false);
    open_f_cl_group3input();
  }


  function editdocument() {
    if (array_data[nowselect]) {
      var d_id = array_data[nowselect].level_id;
    }else{
      var d_id = 0;
    }

    var status_1 = 'no';
    var status_2 = 'no';
    var status_3 = '';
    if ($('#f_cl_group1').prop('checked')) {
      status_1 = 'yes';
    }
    if ($('#f_cl_group2').prop('checked')) {
      status_2 = 'yes';
    }
    if ($('#f_cl_group3').prop('checked')) {
      status_3 = $('#f_cl_group3input').val();
    }


    $('#ModalCusLv').modal('hide');
    $.ajax({
        url: "../edit_customergroup", // point to server-side controller method
        data: {
          level_id : d_id,
          status_1 : status_1,
          status_2 : status_2,
          status_3 : status_3,
          level_name : $('#f_cl_name').val()
        },
        type: 'post',
        success: function (response) {
          setTimeout(function() {
            Event._loadTabCustomerGroup();
          }, 1000);
        }
    });
  }


</script>
