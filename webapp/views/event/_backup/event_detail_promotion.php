<div class="row">

  <div class="table-responsive">

    <div class="header" style="border-bottom:0;">&nbsp;

        <ul class="header-dropdown">

          <li>

            <button id="btn-action" class="btn btn-warning waves-effect" onclick="addPromotion()">

            <!-- <i class="material-icons">&#xE146;</i> -->

            เพิ่มโปรโมชั่น</button>

          </li>

        </ul>

    </div>

  <?php if(!empty($promotions)):?>

      <table class="table" >

          <thead>

             <tr>

                  <th style="width:40px;" class="text-center ">No.</th>

                  <th>ชื่อรางวัล</th>

                  <th class="col-sm-1 text-center" >รหัสโปรฯ</th>
                  <th class="col-sm-1 text-center" >ใช้ไป/ทั้งหมด</th>
                  <th class="col-sm-1 text-center" >คงเหลือ</th>
                  <th class="col-sm-1 text-center" >ส่วนลด</th>
                  <th class="col-sm-1 text-center" >เริ่มต้น</th>
                  <th class="col-sm-1 text-center" >สิ้นสุด</th>
                  <th class="col-sm-1 text-center" >Actions</th>
              </tr>
          </thead>
          <tbody>
              <?php $no=1;
              foreach ($promotions as $key => $val):?>
              <tr>
                <td class="text-center">
                  <?php echo  $no++; ?>
                </td>
                <td id="row_title_<?php echo $key;?>">
                  <?php echo $val->name;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->code;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo ($val->amount-$val->balance);?>/
                    <?php echo $val->amount;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->balance;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->discount;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo date('d-m-Y', strtotime($val->start_datetime));?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo date('d-m-Y', strtotime($val->end_datetime));?>
                </td>
                  <td class="col-sm-1 text-center">

                    <a data-toggle="modal" href="javascript:void(0)"  data-target="#ModalPromotion" onclick="getmodal('<?php echo $key;?>')">

                      <i class="material-icons text-warning">&#xE254;</i>

                    </a>

                    <a data-toggle="modal" href="javascript:void(0)" onclick="deletePromotion('<?php echo $key;?>')">

                      <i class="material-icons text-danger">&#xE92B;</i>

                    </a>

                  </td>

              </tr>

              <?php  endforeach;?>

          </tbody>

      </table>

    <?php endif;?>

  </div>

</div>



<!-- Modal -->

<div id="ModalPromotion" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title"></h4>

      </div>



      <form class="" id="promotionForm" method="post">

        <input type="hidden" name="event_id" value="<?php echo $event_id;?>">

        <input type="hidden" name="pro_id" id="pro_id" value="">

      <div class="modal-body">

        <div class="form-group">

          <label>ชื่อโปรโมชั่น</label>

          <input id="name" name="name" type="text" class="form-control"   style="border: 1px solid #ccc;">

        </div>

        <div class="form-group">

          <label>จำนวนโปรโมชั่น</label>

          <input id="amount" name="amount" type="number" class="form-control" placeholder="ตัวเลขเท่านั้น"  style="border: 1px solid #ccc;">

        </div>

        <div class="row">



        <div class="col-sm-6">

        <div class="form-group">

          <label>ส่วนลดโปรโมชั่น</label>

          <input id="discount" name="discount" type="number" class="form-control" placeholder="ตัวเลขเท่านั้น"   style="border: 1px solid #ccc;">

        </div>

      </div>

        <div class="col-sm-6">

        <div class="form-group">

          <label>รหสโปรโมชั่น </label>

          <input id="code" name="code" type="text" class="form-control"   style="border: 1px solid #ccc;">

          <span class="help-block">!ห้ามซ้ำที่อยู่แล้ว/หากไม่กำหนดระบบจะกำหนดให้อัตโนมัติ</span>

        </div>

      </div>



          <div class="col-sm-6">

            <div class="form-group">

              <label>วันที่เริ่มต้น</label>

              <input id="date_start" name="date_start" data-provide="datepicker" data-date-format="dd-mm-yyyy" type="text" class="form-control"   style="border: 1px solid #ccc;">

            </div>

          </div>

          <div class="col-sm-6">

            <div class="form-group">

              <label>วันที่หมดเขต </label>

              <input id="date_end" name="date_end" data-provide="datepicker" data-date-format="dd-mm-yyyy" type="text" class="form-control"   style="border: 1px solid #ccc;">

            </div>

          </div>

        </div>





      </div>

      </form>



      <div class="modal-footer">

        <!-- <input type="submit" name="" class="btn btn-primary" value="บันทึก"> -->



        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>

        <button type="button" class="btn btn-primary form_btn_submit" onclick="editPromotion()"></button>

      </div>



    </div>



  </div>

</div>



<script>

  var array_data = <?php echo json_encode($promotions);?>;

  var eventid    =    <?php echo json_encode($event_id);?>;

  var nowselect = -1;



    function deletePromotion(data)

  {

    swal({

      title: "Are you sure?",

      text: "ยืนยันลบข้อมูลหรือไม่?",

      type: "warning",

      showCancelButton: true,

      confirmButtonClass: "btn-danger",

      confirmButtonText: "Yes, delete it!",

      closeOnConfirm: false

    },

    function(){

      swal("Deleted!", "ลบข้อมูลสำเร็จ", "success");

          $.ajax({

              url: "../delete_promotion",

              data: {

                promotion_id : array_data[data].promotion_id,

                event_id : array_data[data].event_id

              },

              type: 'post',

              success: function (response) {

                Event._loadTabPromotion();

              }

          });

    });



  }





  function getmodal(data) {

    $('.modal-title').html("แก้ไขโปรโมชั่น");

    $('.form_btn_submit').html("แก้ไขโปรโมชั่น");

    nowselect = data;

    // console.log(array_data[data]);



    $('#pro_id').val(array_data[data].promotion_id);

    $('#discount').val(array_data[data].discount);

    $('#name').val(array_data[data].name);

    $('#amount').val(array_data[data].amount);

    $('#code').val(array_data[data].code);

    var start_date = $.format.date(array_data[data].start_datetime, "dd-MM-yyyy");

    var date_end = $.format.date(array_data[data].end_datetime, "dd-MM-yyyy");

    $('#date_start').val(start_date);

    $('#date_end').val(date_end);

  }



  function addPromotion() {

    $('.modal-title').html("เพิ่มโปรโมชั่น");

    $('.form_btn_submit').html("เพิ่มโปรโมชั่น");

    $('#ModalPromotion').modal('show');

    nowselect = -1;



    $('#pro_id').val('');

    $('#discount').val('');

    $('#name').val('');

    $('#amount').val('');

    $('#code').val('');

    $('#date_start').val('');

    $('#date_end').val('');

  }





  function editPromotion() {
    var form     = $('form#promotionForm')[0];
    var formData = new FormData(form);
    $('#ModalPromotion').modal('hide');
    $.ajax({
        url: "../save_promotion",
        data: formData,
        contentType: false,
        processData: false,
        type: 'post',
        success: function (response) {
          setTimeout(function() {
            Event._loadTabPromotion();
          }, 1000);
        }
    });

  }



</script>

