<div class="block-header">

    <div class="col-sm-6 col-xs-6">

        <div class="row">

            <h2><?php echo $title;?></h2>

        </div>

    </div>

    <div class="col-sm-6 col-xs-6">

        <div class="row text-right">

            <?php echo $this->breadcrumb->output(); ?>

        </div>

    </div>

</div><!-- /.block-header -->



<!-- Content -->
<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

  <div class="card">

    <div class="body">

    <div class="row">
      <div class="col-sm-3 eventsummary_box">
       <span class="eventsummary">Unpaid <span id="unpaid_summary"></span> คน</span>
      </div>
      <div class="col-sm-3 eventsummary_box">
      <span class="eventsummary eventsummary_left">Paid <apsn id="paid_summary"></span> คน</span>
     </div>
     <div class="col-sm-3 eventsummary_box">
       <span class="eventsummary eventsummary_left">Cancel <span id="cancel_summary"></span> คน</span>
      </div>
      <div class="col-sm-3">
       <span class="eventsummary eventsummary_left">Approved <span id="approved_summary"></span> คน</span>
      </div>
      </div>

    </div>

  </div>



</div>
</div>

<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

  <div class="card" style="padding:0px;">

    <div class="header" style="padding:6px 8px;">

        <h2>ค้นหาข้อมูล</h2>

    </div>

    <div class="body">

      <form id="filterForm">

      <input type="hidden" id="event_id" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">
 
      <div class="row">

        <div class="col-sm-3">

            <div class="form-group form-float">

                <div class="form-line">

                    <input type="text" name="name" class="form-control"/>

                    <label class="form-label">ชื่อ/นาสกุล</label>

                </div>

            </div>

        </div>

        <div class="col-sm-3">

            <div class="form-group form-float">

                <div class="form-line">

                    <input type="text" name="ref_1" class="form-control"/>

                    <label class="form-label">Ref 1/Ref 1</label>

                </div>

            </div>

        </div>

        <div class="col-sm-2">

            <div class="form-group form-float">

                <div class="form-line">

                    <select name="payment_status" class="form-control">

                        <option value="">- สถานะชำระเงิน -</option>

                        <option value="unpaid">Unpaid</option>

                        <option value="paid">Paid</option>

                        <option value="failed">Cancel</option>

                        <option value="approved">Approved</option>

                    </select>

                </div>

            </div>

        </div>

        <div class="col-sm-2">

            <div class="form-group form-float">

                <div class="form-line">

                    <input type="text" name="date_start" class="form-control"/>

                    <label class="form-label">ค้นหาตามวันที่</label>

                </div>

            </div>

        </div>

        <div class="col-sm-2">

            <div class="form-group form-float">

                <div class="form-line">

                    <input type="text" name="date_end" class="form-control"/>

                    <label class="form-label">- ถึงวันที่</label>

                </div>

            </div>

        </div>



        <div class="col-sm-3">
     <!--   <button class="btn btn-primary waves-effect left" id="btn_import_excel">

                <i class="fa fa-floppy-o" aria-hidden="true"></i>

                <span>นำเข้า Excel</span>

            </button>
        -->
        </div>

        <div class="col-sm-3"></div>

        <div class="col-sm-4"></div>

        <div class="col-sm-1">

          <button class="btn btn-warning waves-effect right" id="btn_reset_filter">

                <i class="material-icons">&#xE863;</i>

                <span>รีเซต</span>

            </button>

        </div>

        <div class="col-sm-1">

            <button class="btn btn-primary waves-effect right" id="btn-filter">

                <i class="material-icons">&#xE8B6;</i>

                <span>ค้นหา</span>

            </button>

        </div>

    </div>

    </form>



    </div>

  </div>

  </div><!-- /.col-sm-12 -->

</div><!-- /.row clearfix -->





<div class="row clearfix">

  <div class="col-sm-12 col-xs-12">

  <div class="card">

    <div class="body">

      <div id="display_datalist"></div>

    </div>

  </div>

  <div class="col-sm-12 col-xs-12">

</div><!-- /.row clearfix -->

