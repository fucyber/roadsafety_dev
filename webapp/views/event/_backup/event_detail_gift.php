<div class="row">
  <div class="table-responsive">
    <div class="header" style="border-bottom:0;">&nbsp;
        <ul class="header-dropdown">
          <li>
            <button id="btn-action" class="btn btn-warning waves-effect" onclick="add_gift()">
            <!-- <i class="material-icons">&#xE146;</i> -->
            เพิ่มรางวัล</button>
          </li>
        </ul>
    </div>
      <?php if(!empty($gifts)):?>
      <table class="table" >
          <thead>
             <tr>
                  <th style="width:40px;" class="text-center ">No.</th>
                  <th class="col-sm-1 text-center" >รูป</th>
                  <th>ชื่อรางวัล</th>
                  <th class="col-sm-1 text-center" >คะแนนแลก</th>
                  <th class="col-sm-1 text-center" >ทั้งหมด</th>
                  <th class="col-sm-1 text-center" >คงเหลือ</th>
                  <th class="col-sm-1 text-center" >แลกไปแล้ว</th>
                  <th class="col-sm-1 text-center" >Actions</th>
              </tr>
          </thead>
          <tbody>
              <?php $no=1;
              foreach ($gifts as $key => $val):?>
              <tr>
                <td class="text-center">
                  <?php echo  $no++; ?>
                </td>
                <td class="text-center">
                   <?php if(!empty($val->image)):?>
                    <img style="max-width:80px;" src="<?php echo config('upload_url');?>events/<?php echo $val->image;?>" alt="">
                  <?php endif;?>
                </td>
                <td id="row_title_<?php echo $key;?>">
                  <?php echo $val->name;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->redeem_point;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->amount;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->amount - $val->redeemed;?>
                </td>
                <td class="text-center" id="row_video_link_<?php echo $key;?>">
                    <?php echo $val->redeemed;?>
                </td>
                  <td class="col-sm-1 text-center">
                    <a data-toggle="modal" href="javascript:void(0)"  data-target="#ModalGift" onclick="getmodal('<?php echo $key;?>')">
                      <i class="material-icons text-warning">&#xE254;</i>
                    </a>
                    <a data-toggle="modal" href="javascript:void(0)" onclick="deletedoc('<?php echo $key;?>')">
                      <i class="material-icons text-danger">&#xE92B;</i>
                    </a>
                  </td>
              </tr>
              <?php  endforeach;?>
          </tbody>
      </table>
    <?php endif;?>
  </div>
</div>

<!-- Modal -->
<div id="ModalGift" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <form id="giftForm" method="post">
        <input type="hidden" name="event_id" value="<?php echo $event_id;?>">
        <input type="hidden" name="gift_id" id="gift_id" value="">
      <div class="modal-body">
        <div class="form-group">
          <label>รูปรางวัล</label>
          <input id="image" name="image" type="file" >
        </div>
        <div class="form-group">
          <label>ชื่อรางวัล</label>
          <input id="name" name="name" required type="text" class="form-control"   style="border: 1px solid #ccc;">
        </div>
        <div class="form-group">
          <label>จำนวนของรางวัล</label>
          <input id="amount" name="amount" required type="number" class="form-control"   style="border: 1px solid #ccc;">
        </div>
        <div class="form-group">
          <label>คะแนนที่ใช้แลกของรางวัล</label>
          <input id="redeem_point" name="redeem_point" required type="number" class="form-control"   style="border: 1px solid #ccc;">
        </div>

      </div>
      </form>

      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary form_btn_submit" onclick="editGift()"></button>
      </div>

    </div>

  </div>
</div>

<script>
  var array_data = <?php echo json_encode($gifts);?>;
  var eventid    =    <?php echo json_encode($event_id);?>;
  var nowselect = -1;

  function deletedoc(data)
  {
    swal({
      title: "Are you sure?",
      text: "ยืนยันลบข้อมูลหรือไม่?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "ลบข้อมูลสำเร็จ", "success");
          $.ajax({
              url: "../delete_gift",
              data: {
                gift_id : array_data[data].gift_id,
                event_id : array_data[data].event_id
              },
              type: 'post',
              success: function (response) {
                Event._loadTabGift();
              }
          });
    });

  }


  function getmodal(data) {
    $('.modal-title').html("แก้ไขของรางวัล");
    $('.form_btn_submit').html("แก้ไขของรางวัล");
    nowselect = data;
    // console.log(array_data[data]);
    $('#gift_id').val(array_data[data].gift_id);
    $('#name').val(array_data[data].name);
    $('#amount').val(array_data[data].amount);
    $('#redeem_point').val(array_data[data].redeem_point);
  }

  function add_gift() {
    $('.modal-title').html("เพิ่มของรางวัล");
    $('.form_btn_submit').html("เพิ่มของรางวัล");
    $('#ModalGift').modal('show');
    nowselect = -1;

    $('#gift_id').val('');
    $('#image').val('');
    $('#name').val('');
    $('#amount').val('');
    $('#redeem_point').val('');
  }


  function editGift() {

    var form     = $('form#giftForm')[0];
    var formData = new FormData(form);

    $('#ModalGift').modal('hide');
    $.ajax({
        url: "../save_gift",
        data: formData,
        contentType: false,
        processData: false,
        type: 'post',
        success: function (response) {
          setTimeout(function() {
            Event._loadTabGift();
          }, 1000);
        }
    });
  }

</script>
