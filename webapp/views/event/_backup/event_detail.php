<div class="block-header">
    <div class="col-sm-6 col-xs-6">
        <div class="row">
            <h2><?php echo $title;?></h2>
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->
<input type="hidden" id="current_tab" name="current_tab" value="<?php echo !empty($current_tab) ? $current_tab : '';?>">
<input type="hidden" id="event_id" name="event_id" value="<?php echo !empty($event_id) ? $event_id : '';?>">
<div class="row clearfix">
  <div class="col-sm-12 col-xs-12">
    <div class="card">
    <div class="body" style="padding-top:0px;">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#event-info" data-toggle="tab" id="tab-event-info">
                <i class="material-icons">&#xE88F;</i> Event info
            </a>
        </li>
        <li role="presentation">
            <a href="#event-setting" data-toggle="tab" id="tab-event-setting">
              <i class="material-icons">&#xE8B9;</i> Settings
            </a>
        </li>
        <li role="presentation">
            <a href="#event-document" data-toggle="tab" id="tab-event-document">
              <i class="fa fa-file-text-o" aria-hidden="true" style="font-size: 20px;margin-top: 9px;"></i> เอกสารที่เกี่ยวข้อง
            </a>
        </li>
        <li role="presentation">
            <a href="#event-video" data-toggle="tab" id="tab-event-video">
              <i class="fa fa-video-camera" aria-hidden="true" style="font-size: 20px;margin-top: 9px;"></i> วีดีโอ
            </a>
        </li>
        <li role="presentation">
            <a href="#event-gift" data-toggle="tab" id="tab-event-gift">
              <i class="fa fa-gift" aria-hidden="true" style="font-size: 20px;margin-top: 9px;"></i> ของรางวัล
            </a>
        </li>
        <li role="presentation">
            <a href="#event-promotion" data-toggle="tab" id="tab-event-promotion">
              <i class="fa fa-tags" aria-hidden="true" style="font-size: 20px;margin-top: 9px;"></i> โปรโมชั่น
            </a>
        </li>
        <li role="presentation">
            <a href="#event-customergroup" data-toggle="tab" id="tab-event-customergroup">
              <i class="fa fa-users" aria-hidden="true" style="font-size: 20px;margin-top: 9px;"></i> กลุ่มผู้สมัคร
            </a>
        </li>



    </ul>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade in active" id="event-info">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-setting">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-document">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-video">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-gift">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-promotion">
      </div><!--/#event-info -->
      <div role="tabpanel" class="tab-pane fade" id="event-customergroup">
      </div><!--/#event-info -->
    </div><!-- /.tab-content -->
    </div> </div>

  </div><!-- /.col-sm-12 -->
</div><!-- /.row clearfix -->
