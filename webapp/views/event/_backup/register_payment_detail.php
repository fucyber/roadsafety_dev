<section class="container-fluid" style="background:#fff;padding-top:10px;padding-bottom:10px;">
<?php if(empty($payment)):?>
  <?php echo lang('data_not_found');?>
<?php else:?>
  <?php //printr($payment);?>
<table class="table table-striped" style="width:700px;margin:0 auto;">
  <tr>
    <th colspan="2" class="text-center">
      รายละเอียดการชำระเงิน:
    </th>
  </tr>
  <tr>
    <td colspan="2" class="text-center">
      <div class="col-sm-3"></div>
      <div class="col-sm-3">
        <?php if($payment->payment_status !== 'approved'):?>
        <input type="text"
          class="form-control"
          required id="receipt_date" name="receipt_date" value=""
          data-provide="datepicker" data-date-format="dd-mm-yyyy">
        <?php endif;?>
      </div>
      <div class="col-sm-6 text-left">
      <?php if($payment->payment_status !== 'approved'):?>
        <button type="button" class="btn btn-success waves-effect"
          id="btn-approved"
          data-event_id="<?php echo $payment->event_id;?>"
          data-customer_id="<?php echo $payment->customer_id;?>"
          data-invoice_id="<?php echo $payment->invoice_id;?>"
          title="ยืนยันการชำระเงินครบถ้วน">
            <i class="material-icons">&#xE86C;</i>
            <?php echo lang('approved');?></button>
      <?php endif;?>
      <?php if($payment->payment_status === 'approved'):?>
            <span class="text-success">
                <i class="material-icons">&#xE86C;</i>
                <?php echo lang('approved');?></span>
      <?php endif;?>
      </div>
    </td>
  </tr>
  <tr>
    <th class="col-md-3">วันที่รับชำระ:</th>
    <td><?php echo !empty($payment->billing_dtm) ? date('d-m-Y', strtotime($payment->billing_dtm)):'-';?></td>
  </tr>
  <tr>
    <th class="col-md-3">Ref 1:</th>
    <td><?php echo $payment->invoice_ref_1;?></td>
  </tr>
  <tr>
    <th>Ref 2:</th>
    <td><?php echo $payment->invoice_ref_2;?></td>
  </tr>
  <tr>
    <th>ยอดที่ต้องชำระ:</th>
    <td><?php echo number_format($payment->invoice_grand_totel, 0);?></td>
  </tr>
  <tr>
    <th>ผู้ชำระ:</th>
    <td><?php echo $payment->customer_name_title;?>
      <?php echo $payment->customer_firstname;?>&nbsp;
      <?php echo $payment->customer_lastname;?></td>
  </tr>
  <tr>
    <th>เวลาชำระเงิน:</th>
    <td><?php echo show_datetime_inline($payment->payment_dtm);?></td>
  </tr>
  <tr>
    <th>ข้อมูลการชำระเงิน:</th>
    <td>
      <?php if(empty($payment->payment_slip)):?>
        ผู้ใช้ไม่ได้แนบ slip
      <?php else:?>
        <img style="max-width:450px;" src="<?php echo config('upload_url').'slips/'.$payment->payment_slip;?>" alt="<?php echo $payment->payment_slip;?>">
      <?php endif;?>
    </td>
  </tr>
</table>
<?php endif;?>
</section>
