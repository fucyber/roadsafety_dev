<div class="row">
  <div class="table-responsive">
    <div class="header" style="border-bottom:0;">&nbsp;
        <ul class="header-dropdown">
          <li>
            <a href="<?php echo base_url('event/headdocument/'.$event_id.'')?> " target="_blank">
              <button id="btn-action" class="btn btn-primary waves-effect">หัวข้อเอกสาร</button>
            </a>
          </li>
          <li>
            <button id="btn-action" class="btn btn-warning waves-effect" onclick="add_document()">
            <!-- <i class="material-icons">&#xE146;</i> -->
            เพิ่มเอกสาร</button>
          </li>
        </ul>
    </div>


      <table class="table">
          <thead>
             <tr>
                  <?php /*<th data-tooltip title="<?php echo lang('action_select_all');?>"
                      style="width:40px;">
                      <input type="checkbox" class="ts_checkbox_all">
                  </th> */?>
                  <th class="col-sm-1  text-center">No.</th>
                  <th class="col-sm-2  ">ชื่อหัวข้อ</th>
                  <th class="col-sm-2  ">ชื่อเอกสาร</th>
                  <th class="col-sm-3  ">รายละเอียด</th>
                  <th class="col-sm-2 text-center" >Actions</th>
              </tr>
          </thead>

          <tbody>
              <?php foreach ($document as $key => $val):?>
              <tr>
                <td class="text-center">
                  <?=$key+1 ?>
                </td>
                <td id="row_title_<?=$key?>">
                  <?=$val->title ?>
                </td>
                <td id="row_file_title_<?=$key?>">
                  <?=$val->file_title ?>
                </td>
                <td id="row_file_name_<?=$key?>">
                  <a href="<?=config('upload_url').'documents/'.$val->file_name ?>">
                    <?=$val->file_name ?>
                  </a>

                </td>
                  <td class="col-sm-1 text-center">
                    <button type="button" class="" data-toggle="modal" data-target="#myModal" onclick="getmodal(<?=$key?>)">
                      <i class="fa fa-list-ul" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                    </button>
                    <button type="button" class="" onclick="deletedoc(<?=$key?>)">
                      <i class="fa fa-trash-o" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                    </button>
                  </td>
              </tr>
              <?php  endforeach;?>
          </tbody>
      </table>
  </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">แก้ไขเอกสาร</h4>
      </div>

      <form class="" id="form_input" method="post">
      <div class="modal-body">
        <div class="input-group input-group-md">
          <span class="input-group-addon" >ชื่อหัวข้อ</span>
          <select class="select_head" name="f_select" id="f_select_head" ></select>
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon">ชื่อเอกสาร</span>
          <input id="f_name" name="f_name" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon" >ไฟล์เอกสาร</span>
          <span>
            <span class="form-control"  id="f_file"></span>
            <input id="f_file_input" name="f_filee" type="file" class="form-control" aria-describedby="sizing-addon1">
          </span>
        </div>
      </div>
      </form>




      <div class="modal-footer">
        <!-- <input type="submit" name="" class="btn btn-primary" value="บันทึก"> -->
        <button type="button" class="btn btn-primary" onclick="editdocument()" id="form_btn_submit">แก้ไขเอกสาร</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
      </div>

    </div>

  </div>
</div>

<script>
  var array_data = <?=json_encode($document)?>;
  var array_type = <?=json_encode($type)?>;
  var url_upload = "<?=config('upload_url').'documents/'?>";
  var nowselect = -1;

  $(function(){
    makeselect();
  });

  function deletedoc(data) {

    swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
          $.ajax({
              url: "../delete_document", // point to server-side controller method
              data: { file_id : array_data[data].file_id },
              type: 'post',
              success: function (response) {
                Event._loadTabDocument();

              }
          });
    });



  }

  function makeselect() {
    var stringb = '';
    for (a of array_type) {
      stringb += '<option value="'+a.id+'">'+a.title+'</option>'
    }
    $('.select_head').html(stringb);
  }

  function getmodal(data) {
    $('.modal-title').html("แก้ไขเอกสาร");
    $('#form_btn_submit').html("แก้ไข");
    nowselect = data;
    // console.log(array_data[data]);
    $('#f_name').val(array_data[data].file_title);
    $('#f_file').html(array_data[data].file_name);
    $('#f_select_head').val(array_data[data].id);
  }

  function add_document() {
    $('.modal-title').html("เพิ่มเอกสาร");
    $('#form_btn_submit').html("เพิ่ม");
    $('#myModal').modal('show');
    nowselect = -1;

    $('#f_name').val('');
    $('#f_file').html('');
    $('#f_select_head').val(0);
  }


  function editdocument() {

    $('#row_title_'+nowselect).html($("#f_select_head option:selected").text());
    if (array_data[nowselect]) {
      array_data[nowselect].id = $('#f_select_head').val()*1;
    }

    $('#row_file_title_'+nowselect).html($('#f_name').val());
    if (array_data[nowselect]) {
      array_data[nowselect].file_title = $('#f_name').val();
    }
    var form_data = new FormData($('#form_input')[0]);
    var file_data = $('#f_file_input').prop('files')[0];
    form_data.append('file', file_data);
    if (array_data[nowselect]) {
      form_data.append('id', array_data[nowselect].file_id);
    }
    $.ajax({
        url: "../update_document", // point to server-side controller method
        dataType: 'text', // what to expect back from the server
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
          setTimeout(function () {
            Event._loadTabDocument();
          }, 100);
          var data = eval('['+response+']')[0];
            var string =`<a href="`+url_upload+`/`+data.file_name+`">
                            `+data.file_name+`
                          </a>`;
            $('#row_file_name_'+nowselect).html(string);
        }
    });

    $('#myModal').modal('hide');
    $('#f_name').val('');


  }
</script>
