<?php if(empty($data_list)):?>
  <h4><?php echo lang('empty_data');?></h4>
<?php else:?>
<?php //printr($data_list); ?>
<div class="table-responsive">
  <table class="table table-striped table-hover">
    <thead>
        <tr>
            <th  class="text-center" style="width:40px;">
              <!-- <input type="checkbox" class="ts_checkbox_all"> -->
              #
            </th>
            <th>ชื่อผู้ลงทะเบียน</th>
            <th class="text-center" style="width:100px;">Ref 1</th>
            <th class="text-center" style="width:100px;">Ref 2</th>
            <th class="text-center" style="width:120px;" title="วันที่ลงทะเบียน">วันที่</th>
            <th class="text-center" style="width:120px;" title="ยอดชำระทั้งหมด">ราคา</th>
            <th class="text-center" style="width:120px;" title="การชำระเงิน">การชำระเงิน</th>
            <th class="text-center" style="width:120px;" title="">Review</th>
            <th class="text-center" style="width:120px;">รายละเอียด</th>
        </tr>
    </thead>
    <tbody>
      <?php foreach ($data_list as $key => $val):?>
        <tr>
            <td class="text-center">
              <?php echo ++$start_index;?>.
              <!-- <input type="checkbox" class="ts_checkbox" /> -->
            </td>
            <td>
                <?php echo $val->customer_name_title;?>
                <?php echo $val->customer_firstname;?>&nbsp; <?php echo $val->customer_lastname;?>
            </td>
            <td class="text-center"><?php echo $val->invoice_ref_1;?></td>
            <td class="text-center" title="<?php echo mb_substr($val->invoice_ref_2, 0, 1);?>">
              <?php echo $val->invoice_ref_2;?>
            </td>
            <td class="text-center">
              <?php echo show_datetime($val->invoice_created_at); ?>
            </td>
            <td class="text-center">
              <?php echo number_format($val->invoice_grand_totel, 0); ?>
            </td>
            <td class="text-center">
                <?php echo paymentStatus($val->payment_status); ?>
                <?php
                  if($val->payment_status !== 'unpaid'):
                    $image_url = !empty($val->payment_slip) ? config('upload_url').'slips/'.$val->payment_slip : NULL;
                ?>
                  <br>
                  <a data-fancybox data-type="iframe"   data-toggle="tooltip" title="View payment" href="javascript:;"
                    data-src="<?php echo base_url('event-register/view-payment-detail/'.$val->event_id.'/'.$val->customer_id);?>">
                    <i class="material-icons">&#xE2BC;</i>
                  </a>
                <?php endif;?>
            </td>
            <td class="text-center">
                  <a data-toggle="tooltip" target="_blank"  title="View pay-in-slip"
                    href="<?php echo config('upload_url').'event_invoices/'.$val->invoice_file;?>">
                    Pay-in-slip
                  </a>
                  <?php if($val->payment_status == 'approved'): ?>
                  <a data-toggle="tooltip" target="_blank"  title="View Reciept"
                    href="<?php echo config('upload_url').'event_invoices/'.getReceiptFileName($val->event_id, $val->invoice_id);?>">
                    <br>Reciept
                  </a>
                  <?php endif;?>
            </td>
            <td class="text-center">
              <a data-fancybox data-type="iframe"   data-toggle="tooltip" title="รายละเอียดผู้สมัครเข้างานอีเว้นท์" href="javascript:;"
                data-src="<?php echo base_url('event-register/participant/'.$val->event_id.'/'.$val->customer_id);?>">
                <i class="material-icons">&#xE7EF;</i>
              </a>
            </td>
        </tr>
      <?php endforeach;?>
    </tbody>
  </table>
  <?php if(!empty($pagination)):?>
    <div class="text-right">
      <?php echo $pagination; ?>
    </div>
  <?php endif;?>
<?php endif;?>
