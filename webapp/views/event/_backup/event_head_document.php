<div class="row col-sm-12 ">
  <div class="card">
    <div class="table-responsive">
      <div class="header" style="border-bottom:0;">&nbsp;

          <ul class="header-dropdown">
            <li>
              <button id="btn-action" class="btn btn-warning waves-effect" onclick="add_document()">
              <!-- <i class="material-icons">&#xE146;</i> -->
              เพิ่มหัวข้อ</button>
            </li>
          </ul>
      </div>


        <table class="table">
            <thead>
               <tr>
                    <?php /*<th data-tooltip title="<?php echo lang('action_select_all');?>"
                        style="width:40px;">
                        <input type="checkbox" class="ts_checkbox_all">
                    </th> */?>
                    <th class="col-sm-1  text-center">No.</th>
                    <th class="col-sm-2  ">ชื่อหัวข้อ</th>
                    <th class="col-sm-2  ">รายละเอียด</th>
                    <th class="col-sm-2 text-center" >Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($type as $key => $val):?>
                <tr>
                  <td class="text-center">
                    <?=$key+1 ?>
                  </td>
                  <td id="row_title_<?=$key?>">
                    <?=$val->title ?>
                  </td>
                  <td id="row_description_<?=$key?>">
                    <?=$val->description ?>
                  </td>
                    <td class="col-sm-1 text-center">
                      <button type="button" class="" data-toggle="modal" data-target="#myModal" onclick="getmodal(<?=$key?>)">
                        <i class="fa fa-list-ul" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                      </button>
                      <button type="button" class="" onclick="deletedoc(<?=$key?>)">
                        <i class="fa fa-trash-o" aria-hidden="true" style="font-size: 20px;display: table-caption;"></i>
                      </button>
                    </td>
                </tr>
                <?php  endforeach;?>
            </tbody>
        </table>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">แก้ไขหัวข้อเอกสาร</h4>
      </div>

      <form class="" id="form_input" method="post">
      <div class="modal-body">
        <div class="input-group input-group-md">
          <span class="input-group-addon">ชื่อหัวข้อ</span>
          <input id="f_name" name="f_name" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon">รายละเอียด</span>
          <input id="f_des" name="f_des" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>
      </div>
      </form>


      <div class="modal-footer">
        <!-- <input type="submit" name="" class="btn btn-primary" value="บันทึก"> -->
        <button type="button" class="btn btn-primary" onclick="editdocument()" id="form_btn_submit">แก้ไขเอกสาร</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
      </div>

    </div>

  </div>
</div>

<script>
  var array_type = <?=json_encode($type)?>;
  var eventid = <?=json_encode($head_doc_id)?>;


  var nowselect = -1;

  // $(function(){
  //
  // });



  function getmodal(data) {
    $('.modal-title').html("แก้ไขหัวข้อเอกสาร");
    $('#form_btn_submit').html("แก้ไข");
    nowselect = data;
    // console.log(array_data[data]);
    $('#f_name').val(array_type[data].title);
    $('#f_des').val(array_type[data].description);
  }


  function add_document() {
    $('.modal-title').html("เพิ่มหัวข้อเอกสาร");
    $('#form_btn_submit').html("เพิ่ม");
    $('#myModal').modal('show');
    nowselect = -1;

    $('#f_name').val('');
    $('#f_file').html('');
    $('#f_select_head').val(0);
  }


  function editdocument() {
    if (array_type[nowselect]) {
      var d_id = array_type[nowselect].id;
    }else{
      var d_id = 0;
    }
    $.ajax({
        url: "../edit_head_document", // point to server-side controller method
        data: {
          id : d_id,
          event_id : eventid,
          title : $('#f_name').val(),
          description : $('#f_des').val()
        },
        type: 'post',
        success: function (response) {
          location.reload();
        }
    });
  }

  function deletedoc(data) {

    swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
          $.ajax({
              url: "../delete_video", // point to server-side controller method
              data: { id : array_type[data].id },
              type: 'post',
              success: function (response) {
                location.reload();
              }
          });
    });
  }
</script>
