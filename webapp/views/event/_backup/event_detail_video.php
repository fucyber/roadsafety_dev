<div class="row">
  <div class="table-responsive">
    <div class="header" style="border-bottom:0;">&nbsp;
        <ul class="header-dropdown">
          <li>
            <button id="btn-action" class="btn btn-warning waves-effect" onclick="add_document()">
            <!-- <i class="material-icons">&#xE146;</i> -->
            เพิ่มวีดีโอ</button>
          </li>
        </ul>
    </div>
    <?php if(!empty($video)):?>
      <table class="table" >
          <thead>
             <tr>
                  <th style="width:40px;" class="text-center">No.</th>
                  <th class="col-sm-1 text-center">Thumbnail</th>
                  <th>ชื่อวีดีโอ</th>
                  <th class="col-sm-4  ">URL</th>
                  <th class="col-sm-1 text-center" >Actions</th>
              </tr>
          </thead>

          <tbody>
              <?php $no=1;
              foreach ($video as $key => $val):?>
              <tr>
                <td class="text-center">
                  <?php echo  $no++; ?>
                </td>
                <td class="text-center">
                   <?php if(!empty($val->image)):?>
                    <img style="max-width:80px;" src="<?php echo config('upload_url');?>events/<?php echo $val->image;?>" alt="">
                  <?php endif;?>
                </td>
                <td id="row_title_<?=$key?>">
                  <?=$val->title ?>
                </td>
                <td id="row_video_link_<?=$key?>">
                    <?=$val->video_link ?>
                </td>
                  <td class="col-sm-1 text-center">
                    <a data-toggle="modal" href="javascript:void(0)"  data-target="#ModalVideo" onclick="getmodal(<?=$key?>)">
                      <i class="material-icons text-warning">&#xE254;</i>
                    </a>
                    <a data-toggle="modal" href="javascript:void(0)" onclick="deletedoc(<?=$key?>)">
                      <i class="material-icons text-danger">&#xE92B;</i>
                    </a>
                  </td>
              </tr>
              <?php endforeach;?>
          </tbody>
      </table>
    <?php endif;?>
  </div>
</div>

<!-- Modal -->
<div id="ModalVideo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <form class="" id="form_input_video" method="post">
        <input type="text" name="event_id" value="<?php echo $event_id;?>">
        <input type="text" name="video_id" id="f_video_id" value="">
      <div class="modal-body">
        <div class="input-group input-group-md">
          <span class="input-group-addon">Thumbnai Video</span>
          <input name="image" type="file" >
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon">ชื่อวีดีโอ</span>
          <input id="f_video_name" name="f_video_name" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>
        <div class="input-group input-group-md">
          <span class="input-group-addon">Youtube URL</span>
          <input id="f_video_url" name="f_video_url" type="text" class="form-control" aria-describedby="sizing-addon1" style="border: 1px solid #ccc;">
        </div>

      </div>
      </form>

      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary form_btn_submit" onclick="editdocument()">
          บันทึกข้อมูล
        </button>
      </div>

    </div>

  </div>
</div>

<script>
  var array_data = <?php echo json_encode($video);?>;
  var eventid    =    <?php echo json_encode($event_id);?>;
  var nowselect = -1;
  $(function(){
  });

  function deletedoc(data) {
    swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
          $.ajax({
              url: "../delete_video", // point to server-side controller method
              data: { id : array_data[data].id },
              type: 'post',
              success: function (response) {
                Event._loadTabVideo();
              }
          });
    });

  }


  function getmodal(data) {
    $('.modal-title').html("แก้ไขวีดีโอ");
    $('.form_btn_submit').html("แก้ไข video");
    nowselect = data;
    // console.log(array_data[data]);
    $('#f_video_name').val(array_data[data].title);
    $('#f_video_url').val(array_data[data].video_link);
    $('#f_video_id').val(array_data[data].id);
  }

  function add_document() {
    $('.modal-title').html("เพิ่มวีดีโอ");
    $('.form_btn_submit').html("เพิ่ม video");
    $('#ModalVideo').modal('show');
    nowselect = -1;

    $('#f_video_name').val('');
    $('#f_video_url').html('');
    $('#f_video_id').val('');
  }


  function editdocument() {
    if (array_data[nowselect]) {
      var d_id = array_data[nowselect].id;
    }else{
      var d_id = 0;
    }
    var form     = $('form#form_input_video')[0];
    var formData = new FormData(form);

    $('#ModalVideo').modal('hide');
    $.ajax({
        url: "../edit_video", // point to server-side controller method
        // data: {
        //   id : d_id,
        //   event_id : eventid,
        //   title : $('#f_video_name').val(),
        //   video_link : $('#f_video_url').val()
        //   image : formData
        // },
        data: formData,
        contentType: false,
        processData: false,
        type: 'post',
        success: function (response) {
          setTimeout(function() {
            Event._loadTabVideo();
          }, 1000);
        }
    });
  }


</script>
