<section class="container-fluid" style="background:#fff;padding-top:10px;padding-bottom:10px;">
<?php if(empty($invoice)):?>
  <?php echo lang('data_not_found');?>
<?php else:?>
  <?php //printr($invoice);?>
  <?php //printr($customer);?>
  <?php //printr($participants);?>

  <table class="table" style="width:100%;margin:0 auto;">

    <tr style="background-color:#EEEEEE;">
      <th colspan="4" class="text-primary">
        ข้อมูลการลงทะเบียน
        <button type="button" style="float:right;" class="btn btn-warning" name="button" id="editeventregistercustomer">แก้ไขข้อมูล</button>
        <button type="button" style="float:right;margin-left: 10px;" class="btn btn-warning" name="button" id="canceleventregistercustomer">ยกเลิก</button>
        <button type="button" style="float:right;" class="btn btn-info" name="button" id="saveeventregistercustomer">บันทึก</button>
      </th>
    </tr>
    <tr>
      <th style="width:150px;">ชื่อผู้ลงทะเบียน:</th>
      <td style="width:300px;"> <span id="customer_name_title"><?=$customer->customer_name_title?></span> <span id="customer_firstname"><?=$customer->customer_firstname?></span> <span id="customer_lastname"><?=$customer->customer_lastname?></span></td>
      <th style="width:150px;">อีเมล:</th>
      <td><span id="customer_email"><?php echo $customer->customer_email;?></span></td>
    </tr>
    <tr>
      <th>โทรศัพท์:</th>
      <td ><span id="customer_mobile"><?php echo $customer->customer_mobile;?></span></td>
      <th></th>
      <td></td>
    </tr>
    <tr>
      <th>หน่วยงาน:</th>
      <td ><span id="agency_name"><?php echo $customer->agency_name;?></span></td>
      <th>เลขที่ผู้เสียภาษี:</th>
      <td><span id="tax_no"><?php echo $customer->tax_no;?></span></td>
    </tr>
    <tr>
      <th>ที่อยู่รับเอกสาร:</th>
      <td colspan="3">
        จังหวัด <span id="province_name" style="margin-right:10px"><?=$address->province_name?></span>
        อำเภอ/เขต <span id="district_name" style="margin-right:10px"><?=$address->district_name?></span>
        ตำบล/แขวง <span id="sub_district_name" style="margin-right:10px"><?=$address->sub_district_name?></span>
        <br>
        ที่อยู่ <span id="address" style="margin-right:10px"><?=$address->address?></span>
        ถนน <span id="address_road" style="margin-right:10px"><?=$address->address_road?></span>
        รหัสไปรษณีย์ <span id="zipcode" style="margin-right:10px"><?=$address->zipcode?></span>
      </td>
    </tr>
    <tr style="background-color:#EEEEEE;">
      <th colspan="4" class="text-primary">ข้อมูลการชำระเงิน</th>
    </tr>
    <tr>
      <th>รหัสชำ Ref 1:</th>
      <td ><?php echo $invoice->invoice_ref_1;?></td>
      <th>รหัสชำ Ref 2:<br><small style="font-weight:normal;">รหัสบุคคล/กลุ่ม</small></th>
      <td><?php echo $invoice->invoice_ref_2;?></td>
    </tr>
    <tr>
      <th>ยอดชำระทั้งหมด</th>
      <td><?php echo number_format($invoice->invoice_grand_totel, 0);?></td>
      <th>การชำระเงิน:</th>
      <td><?php echo paymentStatus($invoice->payment_status);?></td>
    </tr>
    <tr>
      <th>รหัสใบเสร็จรับเงิน:</th>
      <td></td>
    </tr>
    <tr style="background-color:#EEEEEE;">
      <th colspan="4" class="text-primary">รายชื่อผู้ลงทะเบียนอีเว้นท์</th>
    </tr>
    <tr>
      <td colspan="4">
        <table class="table table-hover" style="padding:5px;width:100%;">
          <tr>
            <th class="text-center" style="width:60px;">No.</th>
            <th>ชื่อ-สกุล</th>
            <th style="width:250px;">Email</th>
            <th style="width:150px;">โทรศัพท์</th>
          </tr>
          <?php $no=0;
          if(!empty($participants)):
          foreach ($participants as $val):?>
            <tr>
              <td class="text-center"><?php echo  ++$no;?></td>
              <td><?php echo $val->name_title.' '.$val->customer_firstname.' &nbsp;'.$val->customer_lastname;?></td>
              <td><?php echo $val->customer_email;?></td>
              <td><?php echo $val->customer_mobile;?></td>
            </tr>
          <?php endforeach;
                endif; ?>
        </table>
      </td>
    </tr>
  </table>
  <div>
    <button type="button" style="float:right;margin-left: 10px;background-color: #7e35ff !important;" class="btn btn-info" name="button" onclick="resent_receilpt()" id="resent_receilpt">Re-Sent Receilpt</button>
    <button type="button" style="float:right;" class="btn btn-info" name="button" onclick="resent_payin_slip()" id="resent_payin_slip">Re-Sent Pay-In-Slip</button>
  </div>
<?php endif;?>
</section>

<script type="text/javascript">
  var editcustomerid = <?=json_encode($customer->customer_id)?>;
  var editeventid = <?=json_encode($event_id)?>;
  var customeraddress = <?=json_encode($address)?>;
</script>
