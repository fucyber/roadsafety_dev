<?php if(empty($data_list)): ?>
    <h3><?php echo lang('empty_data');?></h3>
<?php else:?>
<div class="table-responsive">
    <table class="table">
        <thead>
           <tr>
                <th class="col-sm-1  text-center">ภาพประกอบ</th>
                <th>อีเว้นท์</th>
                <th class="col-sm-1  text-center">สถานะ</th>
                <th class="col-sm-1  text-center">ผู้ลงทะเบียน</th>
                <th class="col-sm-1  text-center">Check in</th>
                <th class="col-sm-1  text-center">ข้อมูล</th>
                <th class="col-sm-2  text-center">บูธ</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data_list as $val):?>
            <tr>
                <?php /*<td>
                    <input type="checkbox" value="<?php echo $val->event_id;?>">
                </td>*/?>
                <td class="col-sm-1 text-center">
                    <?php if(!empty($val->event_image)):?>
                        <img class="img-response"
                            src="<?php echo config('event_url').$val->event_image;?>"
                            style="max-width:100px;" alt="<?php echo $val->event_title;?>">
                    <?php endif;?>
                </td>
                <td>
                    <?php echo $val->event_title;?>
                </td>
                <td class="col-sm-1 text-center">
                    <?php echo event_status($val->event_status); ?>
                </td>
                <td class="col-sm-1 text-center">
                    <a data-toggle="tooltip"  title="รายชื่อผู้ลงทะเบียนอีเว้นท์"
                        href="<?php echo base_url('event-register/list/'.$val->event_id);?>">
                        <i class="material-icons">&#xE8D3;</i>

                    </a>
                </td>
                <td class="col-sm-1 text-center">

                    <a data-toggle="tooltip"  title="Check in เข้างานอีเว้นท์"
                        href="<?php echo base_url('event-checkin/'.$val->event_id);?>">
                        Check in
                    </a>
                </td>
                <td class="col-sm-1 text-center">
                    <a data-toggle="tooltip"  title="ข้อมูลอีเว้นท์"
                        href="<?php echo base_url('event/detail/'.$val->event_id);?>">
                        <i class="fa fa-info-circle" aria-hidden="true" style="    font-size: 20px;   "></i>
                    </a>
                </td>
                <td class="col-sm-1 text-center">
                    <a data-toggle="tooltip"  title="ประเภทบูท"
                          href="<?php echo base_url('event/'.$val->event_id.'/booth-type')?>">
                        <i class="fa fa-list-ul" aria-hidden="true" style="    font-size: 20px;   "></i>
                    </a>
                    &nbsp;&nbsp;
                    <a data-toggle="tooltip"  title="รายการบูท"
                          href="<?php echo base_url('event/'.$val->event_id.'/booth')?>">
                        <i class="fa fa-th" aria-hidden="true" style="    font-size: 20px;   "></i>
                    </a>
                    &nbsp;&nbsp;
                    <a data-toggle="tooltip" target="_blank"   title="Booth QR Code"
                          href="<?php echo base_url('event/'.$val->event_id.'/booth-gen-qr')?>">
                        <i class="material-icons">&#xE329;</i>
                    </a>
                </td>
            </tr>
            <?php  endforeach;?>
        </tbody>
    </table>
</div>

<div style="padding-bottom:10px;" class="uk-text-center">
  <?php echo $pagination;?>
</div>
<?php endif; ?>
