<style media="screen">
  .buttons-html5{
    color: #e9e9e9 !important;
background: #1976d2 !important;
  }
</style>

<div class="card">
  <div class="body table-responsive">
    <div class="col-xs-12">
      <form class="form-inline" id="from_food" action="event_register_food" method="POST">
        <div >
            <input type="date" id="date-start" name="datestart" class="form-control" value="<?php if(!empty($datestart)){ echo $datestart;}?>">
            <input type="date" id="date-end" name="dateend" class="form-control"  value="<?php if(!empty($dateend)){ echo $dateend;}?>">
            <select class="form-control" id="food" name="food">
              <option value="0" <?php if(!empty($food)){ if($food == ''){ echo "selected";}}?>>ทั้งหมด</option>
              <option value="1" <?php if(!empty($food)){ if($food == 1){ echo "selected";}}?>>ฮาลาล</option>
              <option value="2" <?php if(!empty($food)){ if($food == 2){ echo "selected";}}?>>มังสวิรัติ</option>
              <option value="3" <?php if(!empty($food)){ if($food == 3){ echo "selected";}}?>>ทั่วไป</option>
            </select>
            <button type="submit" id="submit" class="btn btn-default">ค้นหา</button>
        </div>
      </form>
    </div>

  <div class="row clearfix">

        <table id="table_event_register" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>ลำดับ</th>
                <th>ชื่ออีเว้นท์</th>
                <th>ชื่อผู้ลงทะเบียน</th>
                <th>โทรศัพท์</th>
                <th>อีเมล</th>
                <th>หน่วยงาน</th>
                <th>วันที่สมัคร</th>
                <th>อาหาร</th>
              </tr>
          </thead>
          <tbody>
            <?php $num=1; foreach ($data as $value):?>
              <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $value->event_title; ?></td>
                <td><?php echo $value->customer_name_title.' '.$value->customer_firstname.' '.$value->customer_lastname ; ?></td>
                <td><?php echo $value->customer_tel; ?></td>
                <td><?php echo $value->customer_email; ?></td>
                <td><?php echo $value->department_name; ?></td>
                <td><?php echo $value->regis_dtm; ?></td>
                <td><?php echo $value->food_name; ?></td>

              </tr>
            <?php $num++; endforeach; ?>

            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>รวม</td>
              <td align="center"><?php echo $num-1;?></td>
            </tr>
          </tbody>

      </table>



  </div>
</div>
</div>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
        $('#table_event_register').DataTable( {
           "searching": false,
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excelHtml5',
        }],
        "order" : [[ 1,"desc" ]]
    } );




    // $( "#submit" ).click(function() {
    //   if ($('#date-end').val() >= $('#date-start').val()) {
    //     $.ajax({
    //         url: BASE_URL + "report/event_register_food", // point to server-side controller method
    //         data: {
    //           datestart : $('#date-start').val(),
    //           dateend : $('#date-end').val(),
    //           food : $('#food').val(),
    //         },
    //         dataType: 'JSON',
    //         type: 'post',
    //         success: function () {
    //           // swal('ลงทะเบียนเข้างานสำเร็จ','','success');
    //
    //         }
    //
    //       });
    //   }else{
    //     swal('วันที่สิ้นสุดต้องมากกว่าวันที่เริ่มต้น');
    //   }
    //
    //
    // });

} );
</script>
