</section>

    <script src="<?php echo config('plugin_url');?>jquery/jquery-2.2.4.min.js"></script>
    <script src="<?php echo config('plugin_url');?>bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo config('plugin_url');?>node-waves/waves.js"></script>
    <script src="<?php echo config('plugin_url');?>waitme/waitMe.js"></script>
    <script src="<?php echo config('plugin_url');?>sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo config('plugin_url');?>bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo config('assets_url');?>js/admin.js"></script>
    <script src="<?php echo config('jsapp_url');?>common.js"></script>
    <script src="<?php echo config('jsapp_url');?>lang/lang_global.js"></script>
    <?php
      //--Load plugin
      if(isset($plugin)):
        foreach ($plugin as $val):
          echo '<script src="'.$val.'"></script>'. PHP_EOL;
        endforeach;
      endif;
      //----Load config
      if(isset($js)):
        foreach ($js as $val):
          echo '<script src="'.$val.'"></script>'. PHP_EOL;
        endforeach;
      endif;
    ?>
</body>
</html>