<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>มูลนิธินโยบายถนนปลอดภัย</title>

    <link rel="shortcut icon" type="image/png" href="<?php echo config('assets_url');?>images/edispro_favicon.png"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- JS Variables -->
    <meta name="BASE_URL" content="<?php echo base_url();?>" />
    <meta name="PLUGIN_URL" content="<?php echo config("plugin_url");?>" />
    <meta name="ENVIRONMENT" content="<?php echo ENVIRONMENT;?>" />
    <!-- css -->
    <link href="<?php echo config('plugin_url');?>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo config('plugin_url');?>node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>animate-css/animate.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>waitme/waitMe.min.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/theme-blue.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/style.css" rel="stylesheet">
    <?php
      if(isset($css)):
        foreach ($css as $css_val):
          echo '<link type="text/css" href="'.$css_val.'" rel="stylesheet" />'. PHP_EOL;
        endforeach;
      endif;
    ?>
    <script src="<?php echo config('plugin_url');?>jquery/jquery-2.2.4.min.js"></script>
</head>
<body class="theme-blue">

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo site_url();?>">TRS Admin</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="pull-right">
                        <a  href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">assignment_ind</i>
                            <ul class="dropdown-menu ">
                                <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                <li role="seperator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                <li role="seperator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                            </ul>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <?php $this->load->view('theme/_sidebar');?>
        </div><!-- #Menu -->
    </aside> <!-- #END# Left Sidebar -->

    <section class="content" id="page_content">
        <div class="container-fluid">
