<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Edispro active Co.,Ltd.</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- JS Variables -->
    <meta name="BASE_URL" content="<?php echo base_url();?>" />
    <meta name="PLUGIN_URL" content="<?php echo config("plugin_url");?>" />
    <meta name="PROJECT_URL" content="<?php echo config("project_url");?>" />
    <meta name="ENVIRONMENT" content="<?php echo ENVIRONMENT;?>" />
    <!-- css -->
    <link href="<?php echo config('plugin_url');?>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo config('plugin_url');?>node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>waitme/waitMe.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?php echo config('plugin_url');?>animate-css/animate.css" rel="stylesheet" />
    <link href="<?php echo config('assets_url');?>css/theme-blue.css" rel="stylesheet">
    <link href="<?php echo config('assets_url');?>css/style.css" rel="stylesheet">
    <?php
      if(isset($css)):
        foreach ($css as $css_val):
          echo '<link type="text/css" href="'.$css_val.'" rel="stylesheet" />'. PHP_EOL;
        endforeach;
      endif;
    ?>
</head>
<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div><!-- #END# Page Loader -->
<section class="container-fluid" style="background:#fff;padding-top:10px;padding-bottom:10px;">