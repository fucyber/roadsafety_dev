<ul class="list">
    <li class="header">MAIN NAVIGATION</li>
    <li>
        <a href="<?php echo base_url('event/list');?>">
            <i class="material-icons">event</i>
            <span>จัดการ Event</span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url('customer/list');?>">
            <i class="material-icons">&#xE85E;</i>
            <span>รายชื่อสมาชิก</span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url('user');?>">
            <i class="material-icons">&#xE7EF;</i>
            <span>ผู้ใช้งานระบบ</span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url('appsetting') ?>" title="Click to login">
            <i class="material-icons dp48">settings</i>
            <span>SettingApplication</span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url('login/logout') ?>" title="Click to login">
            <i class="material-icons">&#xE879;</i>
            <span>Logout</span>
        </a>
    </li>
</ul>
