<?php if(empty($data_list)): ?>
    <h3><?php echo lang('empty_data');?></h3>
<?php else:?>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
           <tr>
                <?php /*<th data-tooltip title="<?php echo lang('action_select_all');?>"
                    style="width:40px;">
                    <input type="checkbox" class="ts_checkbox_all">
                </th> */?>
                <th class="col-sm-2">ประเภทบูธ</th>
                <th class="col-sm-1  text-center">รหัสบูธ</th>
                <th class="col-sm-3" >ชื่อบูธ</th>
                <th class="col-sm-1 text-center">จำนวนโหวด</th>
                <th class="col-sm-1 text-center">คะแนนเฉลี่ย</th>
                <th class="col-sm-1 text-center">เอกสาร</th>
                <th class="col-sm-1 text-center">สถานะ</th>
                <th class="col-sm-2" style="text-align: center;">Actions</th>
            </tr>
        </thead>
        <tbody>
          <script>console.log(<?=json_encode($data_list)?>)</script>
            <?php foreach ($data_list as $val):?>
            <tr>
                <td>
                    <?php echo $val->type_name;?>
                </td>
                <td class="text-center">
                    <?php echo $val->booth_no;?>
                </td>
                <td>
                    <?php echo $val->booth_name;?>
                </td>
                <td class="text-center">
                  <?php echo $val->count_vote;?>
                </td>
                <td class="text-center">
                  <?php if ($val->vote_point) {
                    echo number_format($val->vote_point/$val->count_vote,2);
                  }else{
                    echo "ไม่มีคะแนน";
                  } ?>
                </td>
                <td class="text-center">
                    <?php if(!empty($val->booth_file)): ;?>
                        <a target="_blank" data-toggle="tooltip" title="เอกสารแนบ"
                            href="<?php echo base_url('_uploads/booths/'.$val->booth_file);?>">
                            <i class="material-icons">&#xE2BC;</i>
                        </a>
                    <?php endif; ;?>
                </td>
                <td class="col-sm-1 text-center">
                    <?php echo event_status($val->booth_status); ?>
                </td>
                <td class="col-sm-1 text-center">
                    <a data-toggle="tooltip" title="แก้ไขข้อมูล"
                        href="<?php echo base_url('event/'.$val->event_id.'/booth/update/'.$val->booth_id);?>">
                        <i class="material-icons text-warning">&#xE254;</i>
                    </a>
                    &nbsp;
                    <a data-toggle="tooltip"  title="ลบข้อมูล" id="btn-delete" data-booth_id="<?php echo $val->booth_id;?>"
                          href="javascript:;">
                        <i class="material-icons text-danger">&#xE92B;</i>
                    </a>
                </td>
            </tr>
            <?php  endforeach;?>
        </tbody>
    </table>
</div>

<div style="padding-bottom:10px;" class="uk-text-center">
  <?php echo $pagination;?>
</div>
<?php endif; ?>
