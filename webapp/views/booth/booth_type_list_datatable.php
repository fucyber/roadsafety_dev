<?php if(empty($data_list)): ?>
    <h3><?php echo lang('empty_data');?></h3>
<?php else:?>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
           <tr>
                <?php /*<th data-tooltip title="<?php echo lang('action_select_all');?>"
                    style="width:40px;">
                    <input type="checkbox" class="ts_checkbox_all">
                </th>*/?>
                <th class="col-sm-2">ID ประเภทบูท</th>
                <th>ชื่อ ประเภทบูท</th>
                <th class="col-sm-1 text-center">Status</th>
                <th class="col-sm-3 text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data_list as $val):?>
            <tr>
                <?php /*<td>
                    <input type="checkbox" value="<?php echo $val->event_id;?>">
                </td>*/?>
                <td>
                    <?php echo $val->type_code;?>
                </td>
                <td>
                    <?php echo $val->type_name; ?>
                </td>
                <td class="text-center">
                    <?php echo event_status($val->type_status); ?>
                </td>
                <td class="text-center">
                    <a data-toggle="tooltip" title="แก้ไขข้อมูล"
                        href="<?php echo base_url('event/'.$val->event_id.'/booth-type/update/'.$val->type_id);?>">
                        <i class="material-icons text-warning" style="color: #ffa100;">&#xE254;</i>
                    </a>
                    &nbsp;
                    <a data-toggle="tooltip"
                        title="ลบข้อมูล" id="btn-delete"
                        data-type_id="<?php echo $val->type_id;?>"
                        href="javascript:;">
                        <i class="material-icons text-danger">&#xE92B;</i>
                    </a>
                </td>
            </tr>
            <?php  endforeach;?>
        </tbody>
    </table>
</div>

<div style="padding-bottom:10px;" class="uk-text-center">
  <?php echo $pagination;?>
</div>
<?php endif; ?>
