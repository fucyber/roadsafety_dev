<style>
  *{
    font-family: "Prompt",Helvetica,sans-serif,Arial;
  }
</style>

<div class="block-header">
    <div class="col-sm-6 col-xs-6">
        <div class="row">
            <h2><?php echo $title;?></h2>
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->


<div class="row clearfix">
  <div class="col-md-7">
  <div class="card">
    <div class="header">
      <h2><?php echo !empty($event) ? 'Event: '.$event->event_title : '';?></h2>
    </div>
    <div class="body">
      <form id="boothTypeForm" class="form-horizontal">
        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id;?>">
        <input type="hidden" name="type_id" value="<?php echo (!empty($data) ? $data->type_id : '');?>">
      <div class="row clearfix">
            <div class="col-md-3 form-control-label">
                <label for="type_code">ID ประเภทบูธ : <span class="required">*</span></label>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" required id="type_code"
                          value="<?php echo (!empty($data) ? $data->type_code : '');?>"
                          name="type_code" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-md-3 form-control-label">
                <label for="type_name">ชื่อประเภทบูธ: <span class="required">*</span></label>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" required id="type_name"
                          value="<?php echo (!empty($data) ? $data->type_name : '');?>"
                          name="type_name" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
          <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <a href="<?php echo base_url('event/'.$event_id.'/booth-type');?>" class="btn btn-warning waves-effect">
                        <i class="material-icons">&#xE317;</i> BACK </a>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <button type="reset" class="btn btn-default waves-effect"> -->
                        <!-- <i class="material-icons">&#xE863;</i> CLEAR</button> -->
                    <button type="submit" class="btn btn-primary waves-effect" style="font-size: 16px;">
                        <i class="material-icons">&#xE161;</i> บันทึกข้อมูล</button>
                </div>
            </div>
          </div>
        </div>
      </form>

    </div>
  </div>

</div><!-- /.row clearfix -->
