
<div class="block-header">
    <div class="col-sm-6 col-xs-6">
        <div class="row">
            <h2><?php echo $title;?></h2>
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->

<input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id;?>">
<div class="row clearfix">
  <div class="col-sm-12 col-xs-12">
  <div class="card">
    <div class="header">
      <h2><?php echo !empty($event) ? 'Event: '.$event->event_title : '';?></h2>
         <ul class="header-dropdown">
            <li>
              <a href="<?php echo base_url('event/'.$event_id.'/booth-type/create');?>">
                  <button id="btn-action" class="btn btn-warning waves-effect">
                  <!-- <i class="material-icons">&#xE146;</i> -->
                  เพิ่มประเภท</button>
              </a>
            </li>
        </ul>
    </div>
    <div class="body">
      <div id="display_datatable"></div>
    </div>
  </div>
  <div class="col-sm-12 col-xs-12">
</div><!-- /.row clearfix -->
