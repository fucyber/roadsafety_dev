<style>
  *{
    font-family: "Prompt",Helvetica,sans-serif,Arial;
  }
  .col-md-6>.col-md-3{
  }
  .form-control-label{
    text-align: left;
  }
  .form-group .form-line {
    margin: 0px 10px;
  }
</style>
<div class="block-header">
    <div class="col-sm-6 col-xs-6">
        <div class="row">
            <h2><?php echo $title;?></h2>
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="row text-right">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
    </div>
</div><!-- /.block-header -->

<input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id;?>">
<input type="hidden" name="booth_id" id="booth_id" value="<?php echo $booth_id;?>">

<div class="row clearfix">
  <div class="col-md-10">
  <div class="card">
    <div class="header">
      <h2><?php echo !empty($event) ? 'Event: '.$event->event_title : '';?></h2>
    </div>
    <div class="body">
      <form id="boothForm" class="form-horizontal">
        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id;?>">
        <input type="hidden" name="type_id"  value="<?php echo (!empty($data) ? $data->booth_type_id : '');?>">
        <input type="hidden" name="booth_id" value="<?php echo (!empty($data) ? $data->booth_id : '');?>">




      <div class="col-md-6">
          <div class="form-control-label">
              <label for="booth_no">รหัสบูธ: </label>
          </div>
          <div class="">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="booth_no" required
                        value="<?php echo (!empty($data) ? $data->booth_no : '');?>"
                        name="booth_no" class="form-control">
                  </div>
              </div>
          </div>
      </div>

      <div class="col-md-6">
          <div class="form-control-label">
              <label for="booth_name">ชื่อบูธ: <span class="required">*</span></label>
          </div>
          <div class="">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" required id="booth_name"
                        value="<?php echo (!empty($data) ? $data->booth_name : '');?>"
                        name="booth_name" class="form-control">
                  </div>
              </div>
          </div>
      </div>

      <div class="col-md-6">
            <div class="form-control-label">
                <label for="type_code">ประเภทบูธ : <span class="required">*</span></label>
            </div>
            <div class="">
                <div class="form-group">
                    <div class="form-line">
                        <select required id="type_id" name="type_id" class="form-control ">
                          <option value="">::: เลือกประเภทบูธ :::</option>
                          <?php foreach ($booth_types as $val):?>
                            <option <?php echo (!empty($data) && $val->type_id == $data->booth_type_id) ? 'selected' : '';?>
                                value="<?php echo $val->type_id?>">
                                    <?php echo $val->type_name?>
                            </option>
                          <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class=" form-control-label">
                <label for="booth_name">หน่วยงาน: </label>
            </div>
            <div class="">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="department"
                          value="<?php echo (!empty($data) ? $data->department : '');?>"
                          name="department" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class=" form-control-label">
                <label for="booth_name">โปรโมท: </label>
            </div>
            <div class="">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="booth_excerpt"
                          value="<?php echo (!empty($data) ? $data->booth_excerpt : '');?>"
                          name="booth_excerpt" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class=" form-control-label">
                <label for="type_name">รูปภาพบูธ: </label>
            </div>
            <div class="">
                <?php if(!empty($data->booth_image)):?>
                    <a target="_blank" data-toggle="tooltip" title="รูปภาพบูธ"
                        href="<?php echo base_url('_uploads/booths/'.$data->booth_image);?>">
                        <img class="img-responsive" style="max-width:100px;"  src="<?php echo base_url('_uploads/booths/'.$data->booth_image);?>" alt="">
                    </a>
                <?php endif;?>
                <div class="form-group">
                    <div class="form-line">
                        <input type="file"  name="booth_image" class="form-control">
                        <?php if(!empty($data->booth_image)):?>
                            <input type="hidden"  name="old_booth_image" value="<?php echo $data->booth_image;?>">
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-control-label">
                <label for="type_name">เอกสาร: </label>
            </div>
            <div class="">
                <div class="form-group">
                    <div class="form-line">
                        <input type="file"  name="document" class="form-control">
                        <?php if(!empty($data->booth_file)):?>
                            <input type="hidden"  name="old_booth_file" value="<?php echo $data->booth_file;?>">
                        <?php endif;?>
                    </div>
                </div>
                <?php if(!empty($data->booth_file)):?>
                <a target="_blank" data-toggle="tooltip" title="เอกสารแนบ"
                    href="<?php echo base_url('_uploads/booths/'.$data->booth_file);?>">
                    <i class="material-icons">&#xE2BC;</i>
                </a>
                <?php endif;?>
            </div>
        </div>
        <div class="row clearfix">
          <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <a href="<?php echo base_url('event/'.$event_id.'/booth');?>" class="btn btn-warning waves-effect">
                        <i class="material-icons">&#xE317;</i> BACK </a>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <button type="reset" class="btn btn-default waves-effect">
                        <i class="material-icons">&#xE863;</i> CLEAR</button> -->
                    <button type="submit" class="btn btn-primary waves-effect" style="font-size: 16px;">
                        <i class="material-icons">&#xE161;</i> บันทึกข้อมูล</button>
                </div>
            </div>
          </div>
        </div>
      </form>

    </div>
  </div>

</div><!-- /.row clearfix -->
