<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Booth List</title>

  <link rel="stylesheet" href="<?php echo config('assets_url');?>css/normalize.min.css">
  <link rel="stylesheet" href="<?php echo config('assets_url');?>css/paper.min.css">

  <style>@page { size: A4 }</style>
</head>
<!-- Set "A5", "A4" or "A3" for class name -->
<body class="A4">
  <?php //printr($booth_list); return;?>
  <div style="text-align:center;margin:2px;">
    <input type="button" onclick="printDiv('printableArea')" value="Click to Print" />
  </div>
  <div id="printableArea">


      <?php
        $count_member = 0;
        foreach ($booth_list as $i => $val) {
          if ($count_member%4 == 0 || $count_member == 0) {
            echo '<section class="sheet" style="padding: 0mm;">';
          }
          ?>
            <table style="width:50%; height: 50%;border: 1px solid #c3c3c3;float: left;" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="width:100%;">
                   <table style="width:100%;border:0px #eee solid;">
                     <tr>
                       <td style="width:60px;">
                         <img src="<?php echo $card_logo;?>" style="width:60px;" alt="">
                       </td>
                       <td valign="top">
                         <p style="padding:0px;margin:2px;font-size:12px;"><?php echo $card_title;?></p>
                         <p style="font-weight:bold;padding:0px;margin:2px;font-size:14px;"><?php echo $card_sub_title;?></p>
                         <p style="padding:0px;margin:2px;font-size:12px;"><?php echo $card_venue;?></p>
                       </td>
                     </tr>
                     <tr >
                         <td colspan="2" style="text-align:center;">
                           <?php
                           $code = $val->event_id.'##'.$val->booth_id;
                           $url  = config('upload_url');
                           ?>
                         <img src="<?php echo getBoothQRCode($code, 'booth', $url, $val->event_id, $val->booth_id);?>" style="width:150px;margin-top: 65px;" alt="">
                         <h4 style="font-weight:normal;padding-top:50px;margin:2px;"><?php echo $val->booth_name;?></h5>
                         <h4 style="font-weight:normal;padding-top:20px;margin:2px;">เลขที่บูธ: <?php echo $val->booth_no;?></h5>
                         </td>
                     </tr>
                   </table>
                </td>
                <td></td>
              </tr>
            </table>
          <?php
          $count_member++;
          if ($count_member%4 == 0 || $count_member == 0) {
            echo "</section>";
          }
        }
       ?>
  </div>

<script>
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
</script>
</body>
</html>
