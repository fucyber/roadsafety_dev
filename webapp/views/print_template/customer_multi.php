<!DOCTYPE html>

<html lang="en">



<head>

  <meta charset="utf-8">

  <title>Customer card</title>



  <link rel="stylesheet" href="<?php echo config('assets_url');?>css/normalize.min.css">

  <link rel="stylesheet" href="<?php echo config('assets_url');?>css/paper.min.css">



  <style>@page { size: A4 }</style>

</head>

<!-- Set "A5", "A4" or "A3" for class name -->

<body class="A4">

  <?php //printr($data);?>

  <div style="text-align:center;margin:2px;">

    <input type="button" onclick="printDiv('printableArea')" value="Click to Print" />

  </div>

  <div id="printableArea">





  <?php

    $count_member = 0;

    foreach ($data['customers'] as $key => $value) {

      if ($count_member%4 == 0 || $count_member == 0) {
        echo '<section class="sheet" style="padding: 0mm;">';

      }

      ?>

        <table style="width:50%; height: 50%;border: 1px solid #c3c3c3;float: left;" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td style="width:100%;">

               <table style="width:100%;border:0px #eee solid;">

                <tr>

                  <td style="width:60px;">

                    <img src="<?php echo $data['card_logo'];?>" style="width:60px;" alt="">

                  </td>

                  <td valign="top">

                    <p style="padding:0px;margin:2px;font-size:12px;"><?php echo $data['card_title'];?></p>

                    <p style="font-weight:bold;padding:0px;padding-top:5px;margin:2px;font-size:14px;"><?php echo $data['card_sub_title'];?></p>

                    <p style="padding:0px;;padding-top:5px;margin:2px;font-size:12px;"><?php echo $data['card_venue'];?></p>

                  </td>

                </tr>

                <tr>

                  <td colspan="2" style="text-align:center;">

                    <h5 style="padding:0px;padding-top:10px;margin:2px;">บัตรเข้าร่วมงาน</h5>

                    <h5 style="padding:0px;padding-top:10px;margin:2px;"><?php echo $value['customer_name_title'];?></h5>

                    <h3 style="padding:0px;margin:2px;"><?php echo $value['customer_firstname'];?></h3>

                    <h3 style="padding:0px;margin:2px;"><?php echo $value['customer_lastname'];?></h3>



                    <img src="<?php echo $value['receipt_no_qr'];?>" style="width:150px;padding-top:10px;" alt="">

                    <h5 style="font-weight:normal;padding-top:10px;margin:0px;">เลขที่สมาชิก <?php echo $value['customer_code'];?></h5>

                    <h5 style="font-weight:normal;padding-top:5px;margin:0px;">รหัสลงทะเบียน <?php echo $data['register_no'];?></h5>

                    <h5 style="font-weight:normal;padding-top:5px;margin:0px;">เลขที่ใบเสร็จ: <?php echo $value['receipt_no'];?></h5>

                    <h5 style="font-weight:normal;padding-top:20px;margin:4px;">เพื่อความสะดวกรวดเร็วในการลงทะเบียนและเข้าร่วมกิจกรรมโปรดเก็บบัตรนี้ไว้จนเสร็จงาน</h5>

                  </td>

                </tr>

               </table>

            </td>

            <td></td>

          </tr>

        </table>

      <?php

      $count_member++;

      if ($count_member%4 == 0 || $count_member == 0) {

        echo "</section>";

      }

    }

   ?>



    <!-- <section class="sheet" style="padding: 0mm;">



      <script>console.log(<?=json_encode($data)?>)</script>



      <table style="width:50%; height: 50%;border: 1px solid #c3c3c3;float: left;" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td style="width:100%;">

             <table style="width:100%;border:0px #eee solid;">

              <tr>

                <td style="width:60px;">

                  <img src="<?php echo $data['card_logo'];?>" style="width:60px;" alt="">

                </td>

                <td valign="top">

                  <p style="padding:0px;margin:2px;font-size:12px;"><?php echo $data['card_title'];?></p>

                  <p style="font-weight:bold;padding:0px;padding-top:5px;margin:2px;font-size:14px;"><?php echo $data['card_sub_title'];?></p>

                  <p style="padding:0px;;padding-top:5px;margin:2px;font-size:12px;"><?php echo $data['card_venue'];?></p>

                </td>

              </tr>

              <tr>

                <td colspan="2" style="text-align:center;">

                  <h5 style="padding:0px;padding-top:10px;margin:2px;">บัตรเข้าร่วมงาน</h5>

                  <h5 style="padding:0px;padding-top:10px;margin:2px;"><?php echo $data['customer_name_title'];?></h5>

                  <h3 style="padding:0px;margin:2px;"><?php echo $data['customer_firstname'];?></h3>

                  <h3 style="padding:0px;margin:2px;"><?php echo $data['customer_lastname'];?></h3>



                  <img src="<?php echo $data['receipt_no_qr'];?>" style="width:150px;padding-top:10px;" alt="">

                  <h5 style="font-weight:normal;padding-top:10px;margin:0px;">เลขที่สมาชิก <?php echo $data['customer_code'];?></h5>

                  <h5 style="font-weight:normal;padding-top:5px;margin:0px;">รหัสลงทะเบียน <?php echo $data['register_no'];?></h5>

                  <h5 style="font-weight:normal;padding-top:5px;margin:0px;">เลขที่ใบเสร็จ: <?php echo $data['receipt_no'];?></h5>

                  <h5 style="font-weight:normal;padding-top:20px;margin:4px;">เพื่อความสะดวกรวดเร็วในการลงทะเบียนและเข้าร่วมกิจกรรมโปรดเก็บบัตรนี้ไว้จนเสร็จงาน</h5>

                </td>

              </tr>

             </table>

          </td>

          <td></td>

        </tr>

      </table>

    </section> -->





  </div>



<script>

  function printDiv(divName) {

     var printContents = document.getElementById(divName).innerHTML;

     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;

}

</script>

</body>

</html>

