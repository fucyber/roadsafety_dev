<?php defined('BASEPATH') OR exit('No direct script access allowed');

require realpath(APPPATH . '../vendor/global_funtions.php');

//---@
    function getBoothQRCode($input_code, $prefix='', $url='', $event_id, $booth_id)
    {
        if(empty($input_code))
            return FALSE;

        $CI =& get_instance();
        $CI->load->library('qrcode/ciqrcode');
        $image_dir  =  config('inside_upload_dir').'booths/';

        //--Config
        $config['cacheable']    = true;
        $config['cachedir']     = 'application/cache/qrcode/';
        $config['errorlog']     = 'application/cache/logs/';
        $config['imagedir']     = $image_dir;
        $config['quality']      = true;
        $config['size']         = 1024;
        $config['black']        = array(224,255,255);
        $config['white']        = array(70,130,180);
        $CI->ciqrcode->initialize($config);

        //---Generate
        $params['data']     =   $input_code;
        $params['level']    =   'H';
        $params['size']     =   1024;
        $filename = $prefix.$event_id.$booth_id.'.png';
        $params['savename'] = $config['imagedir'].$filename;
        $CI->ciqrcode->generate($params);
        if(!empty($url))
            return $url.'/booths/'.$filename;
        else
            return config('inside_upload_url').'booths/'.$filename;
    }