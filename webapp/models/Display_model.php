<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Display_model extends CI_Model
{
    //---

    public function get_event_registers($id)
    {
        $this->db->where('event_id',$id);
        $this->db->join('customers', 'customers.customer_id = event_registers.customer_id');
        $this->db->order_by('update_date', 'DESC');
        $this->db->limit(20);
        $query = $this->db->get('event_registers');
        return $query->result();
    }


    public function get_customer($data)
    {
        $this->db->select('customers.* , event_registers.* , customer_levels.level_name , customer_levels.status_1 as status_3_a , customer_levels.status_2 as status_4_a , customer_levels.status_3 as status_5_a , customers.customer_id as customer_id');
        $this->db->where('customers.customer_code',$data['customer_code']);
        $this->db->join('event_registers', 'customers.customer_id = event_registers.customer_id and event_registers.event_id = '.$data['event_id'].'');
        $this->db->join('customer_levels', 'customer_levels.level_id = customers.level_id');
        $query = $this->db->get('customers');
        return $query->result();
    }


    public function checkin_customer($data)
    {
      $this->db->where('customer_id',$data['customer']);
      $this->db->where('event_id',$data['event_id']);
      $result = $this->db->get('event_registers')->result();

      $post = array(
        'status_2' => 'no',
        'status_3' => 'no',
        'status_4' => 'no',
        'status_5' => 'no',
      );

      if ($data['status_2'] == "true") {
        $post['status_2'] = 'yes';
      }
      if ($data['status_3'] == "true") {
        $post['status_3'] = 'yes';
      }
      if ($data['status_4'] == "true") {
        $post['status_4'] = 'yes';
      }
      if ($data['status_5'] == "true") {
        $post['status_5'] = 'yes';
      }

      if ($result) {
          $post['checked_in_staff_id'] = $this->session->userdata('user_id');
          $this->db->set($post);
          $this->db->where('customer_id',$data['customer']);
          $this->db->where('event_id',$data['event_id']);
          $this->db->update('event_registers');
      }else{
        $post['customer_id'] = $data['customer'];
        $post['event_id'] = $data['event_id'];
        $this->db->insert('customer_levels', $post);
      }

    }

    // public function editCustomerGroup($post)
    // {
    //   if ($post['level_id']) {
    //     $this->db->set($post);
    //     $this->db->where('level_id',$post['level_id']);
    //     $this->db->update('customer_levels');
    //   }else{
    //     unset($post['id']);
    //     // print_r($post);
    //     $this->db->insert('customer_levels', $post);
    //   }
    // }
}
