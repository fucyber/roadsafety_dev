<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{

  public function UserLv()
  {
    return $this->db->where('level_status', 'active')
          ->order_by('level_id', 'ASC')
          ->get("user_levels")->result_array();
  }


  public function getUser($limit, $start, $data)
  {

    $this->db->limit($limit, $start);
    if ($data) {
      $query = $this->db->where('status !=', 'discard')
      ->join('user_levels', 'users.level_id = user_levels.level_id', 'LEFT')
      ->like('email', $data)
      ->or_like('firstname', $data)
      ->or_like('lastname', $data)
      ->get("users");
    }else {
      $query = $this->db->where('status !=', 'discard')
      ->join('user_levels', 'users.level_id = user_levels.level_id', 'LEFT')
      ->get("users");
    }

      if ($query->num_rows() > 0)
      {
          return $query->result_array();
      }

      return false;
  }

  public function get_total($data)
  {
    if ($data) {
        $query = $this->db->where('status !=', 'discard')
        ->like('email', $data)
        ->or_like('firstname', $data)
        ->or_like('lastname', $data)
        ->get("users");
        return $query->num_rows();
    }
    $query = $this->db->where('status !=', 'discard')->get("users");
    return $query->num_rows();
  }

  public function getOccupation()
  {
    $query = $this->db->get('master_occupations');
    return $query->result_array();
  }

  public function create_user($data_form)
  {
    if ($this->db->where('email', $data_form['email'])->get("users")->num_rows())
    {
      return false;
    }else{
      $this->db->trans_start();
      // $data_form['status'] = 'active';
      // print_r($data_form);die();
      $this->db->insert('users', $data_form);
      $this->db->trans_complete();
      return true;
    }
  }

  public function checkDuplicateEmail($email='', $widthout_id =NULL)
  {
    if(!empty($widthout_id))
      $this->db->where_not_in('user_id', $widthout_id);
    $this->db->where('email', $email);
    $this->db->where('status !=', 'discard');
    $query =  $this->db->get('users');

    return $query->num_rows();
  }


  public function edit_user($data_form)
  {

    $this->db->trans_start();
    $this->db->where('user_id', $data_form['user_id']);
    $this->db->update('users', $data_form);
    $this->db->trans_complete();

    return $this->db->trans_status();
  }

  public function delete_user($id)
  {
    $this->db->trans_start();
    $data = array(
      'status' => 'discard',
    );

    $this->db->update('users', $data, array('user_id' => $id));
    $this->db->trans_complete();

    return $this->db->trans_status();
  }

  public function editUser($id)
  {
    $query = $this->db->where('user_id', $id)->get("users");
    return $query->result_array()[0];
  }


}
