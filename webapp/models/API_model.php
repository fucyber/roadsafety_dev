<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API_model extends CI_Model
{
    private $client;

    public  function __construct()
    {
        parent::__construct();
    }

     /**
     * @return array
     */
    public function get($api_uri, $input_params=array())
    {
        if(empty($api_uri))
          return FALSE;

        $tokens['token_key'] = 'guest';
        $params = array_merge($tokens,$input_params);

        $service_url = $api_uri.'?'.http_build_query($params);

        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = \Httpful\Request::get($service_url)->expectsJson()->send();

        return $response->body;
    }

    /**
     * @return array
     */
    public function post($endPoint, $params=array())
    {
        if(empty($endPoint))
          return FALSE;

        $tokens['token_key'] = 'guest';
        if(!empty($params)){
            $params = array_merge($tokens, $params);
        }else{
            $params = $tokens;
        }

        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = Httpful\Request::post($endPoint)
            ->body($params, Httpful\Mime::FORM)->expectsJson()->send();
        return $response->body;
    }

}
