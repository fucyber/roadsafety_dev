<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Customer_model extends CI_Model
{
    //---
    public function getCustomerDetail($customer_id)
    {
        $this->db->select('A.agency_id,A.agency_name,A.agency_tel,A.agency_tel_ext,A.agency_fax,A.tax_no');
        $this->db->select('C.*');
        $this->db->select('A.department_id,A.ministry_id');
        $this->db->from(config('tb_customer').' AS C');
        $this->db->join(config('tb_customer_agency').' AS A', 'A.customer_id=C.customer_id', 'LEFT');
        $this->db->where('C.customer_id', $customer_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0){
          return FALSE;
        }

        return $query->row();
    }

    public function getCustomer($limit, $start, $data)
    {
      // $query = $this->db->get('customers');
      // return $query->result_array();

      $this->db->limit($limit, $start);
      if ($data) {
        $query = $this->db->where('customer_status !=', 'discard')->like('customer_code', $data)->or_like('customer_firstname', $data)->or_like('	customer_mobile', $data)->get("customers");
      }else {
        $query = $this->db->where('customer_status !=', 'discard')->get("customers");
      }

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }

        return false;
    }

    public function get_total($data)
    {
      if ($data) {
          $query = $this->db->where('customer_status !=', 'discard')->like('customer_code', $data)->or_like('customer_firstname', $data)->or_like('	customer_mobile', $data)->get("customers");
          return $query->num_rows();
      }
      $query = $this->db->where('customer_status !=', 'discard')->get("customers");
      return $query->num_rows();
    }

    public function getOccupation()
    {
      $query = $this->db->get('master_occupations');
      return $query->result_array();
    }

    public function create_customer($data_form)
    {
      $this->db->trans_start();

      $this->db->insert('customers', $data_form);
      $id = $this->db->insert_id();
      $data = array(
        'customer_code' => generateCustomerCode($id),
      );
      // return $id;
      $this->db->where('customer_id', $id);
      $this->db->update('customers', $data);
      $this->db->trans_complete();

      return $this->db->trans_status();
    }

    public function edit_customer($data_form)
    {
      $this->db->trans_start();
      $this->db->where('customer_id', $data_form['customer_id']);
      $this->db->update('customers', $data_form);
      $this->db->trans_complete();

      return $this->db->trans_status();
    }

    public function delete_customer($id)
    {
      $this->db->trans_start();
      $data = array(
        'customer_status' => 'discard',
      );

      $this->db->update('customers', $data, array('customer_id' => $id));
      $this->db->trans_complete();

      return $this->db->trans_status();
    }

    public function editCustomer($id)
    {
      $query = $this->db->where('customer_id', $id)->get("customers");
      return $query->result_array()[0];
    }

    //-----
    public function getCustomerEventCard($customer_id, $event_id)
    {
        $this->load->model('Barcode_model', 'Barcode');
        $this->load->model('Invoice_model','Invoice');
        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);

        $customer = $this->getCustomerDetail($customer_id);
        $response['card_title']     = event_setting($event_id, 'card_title');
        $response['card_sub_title'] = event_setting($event_id, 'card_sub_title');
        $response['card_venue']     = event_setting($event_id, 'card_venue');
        $response['card_logo']      = config('upload_url').'event_settings/'.event_setting($event_id, 'card_logo');
        $response['customer_name_title']   = $customer->customer_name_title;
        $response['customer_firstname']    = $customer->customer_firstname;
        $response['customer_lastname']     = $customer->customer_lastname;


        $response['customer_code']     = $customer->customer_code;
        $response['register_no']           = 'Ref2: '.$invoice->invoice_ref_2.' (Ref1: '.$invoice->invoice_ref_1.')';


        $response['receipt_no']        = getReceiptNo($event_id, $customer_id);

        // $response['receipt_no']     = $invoice->receipt_no;
        // print_r($invoice);die();

        $file_exists = config('upload_url').'qr_codes/'.$customer->customer_code.'.png';
        if(!file_exists($file_exists))
            $qr_code = $this->Barcode->QRCode($customer->customer_code, '', config('upload_url'));
        else
            $qr_code = $file_exists;

        $response['receipt_no_qr'] = $qr_code;
        return $response;
    }

    public function getCustomerEventMulti($customer_id, $event_id)
    {
      $this->load->model('Barcode_model', 'Barcode');
      $this->load->model('Invoice_model','Invoice');
      $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);


      $response['card_title']     = event_setting($event_id, 'card_title');
      $response['card_sub_title'] = event_setting($event_id, 'card_sub_title');
      $response['card_venue']     = event_setting($event_id, 'card_venue');
      $response['card_logo']      = config('upload_url').'event_settings/'.event_setting($event_id, 'card_logo');
      $response['register_no']           = 'Ref2: '.$invoice->invoice_ref_2.' (Ref1: '.$invoice->invoice_ref_1.')';

      $customer_list = $this->getCustomerDetailList($customer_id);
      foreach ($customer_list as $key => $value) {
        $customers[] = $value;
        $customers[$key]['receipt_no']      = getReceiptNo($event_id, $value['customer_id']);
        $file_exists = config('upload_url').'qr_codes/'.$value['customer_code'].'.png';
        $customers[$key]['receipt_no_qr'] = $this->Barcode->QRCustomer($value['customer_code'], '', config('upload_url'));
      }
      $response['customers'] = $customers;
      return $response;
    }

    public function getCustomerDetailList($customer_id)
    {
        $this->db->from('event_registers as er');
        $this->db->join('customers as c', 'c.customer_id = er.customer_id');
        // $this->db->join('event_invoices as ei', 'ei.customer_id = er.customer_id');
        $this->db->where('er.registrant_id', $customer_id);
        $query = $this->db->get()->result_array();
        return $query;
    }

}
