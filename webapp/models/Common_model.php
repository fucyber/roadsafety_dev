<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{

    private $encryption_key ;

    public function __construct()
    {
        parent::__construct();
        $this->encryption_key = $this->config->item('encryption_key');
    }

    //----
    public function encryptionPassword($input_password =  NULL)
    {
        if(empty($input_password))
            return FALSE;

        $salted = explode('.', $this->encryption_key);

        return md5(sha1($salted[0] . $input_password) . $salted[1]);
    }


    //--
    public function saveFile($file, $fileName, $fullPath, $oldFilename = NULL)
    {
        $response['file_name'] = NULL;
        $response['message']   = 'File is empty.';

        if(empty($file) || empty($fileName))
            return $response;

        //--@Upload config
         $config['upload_path']   = $fullPath;
         $config['allowed_types'] = 'jpg|png|jpeg|gif|pdf|txt';
         $config['max_size']      = 1072*10;
         $config['max_width']     = 4024;
         $config['max_height']    = 4024;
         $config['overwrite']     = false;
         $config['file_name']     = date('Ymd').time();
         $this->load->library('upload', $config);

         if (!$this->upload->do_upload($fileName)) {
            $response['message']      = strip_tags($this->upload->display_errors());
         }else{
            //--@Delete old file
            if(!empty($oldFilename))
                @unlink( $fullPath.$oldFilename);

            $uploadData               = $this->upload->data();
            $response['file_name']    = $uploadData['file_name'];
            $response['file_size']    = $uploadData['file_size'];
            $response['image_width']  = $uploadData['image_width'];
            $response['image_height'] = $uploadData['image_height'];
         }
        return $response;
    }

}
