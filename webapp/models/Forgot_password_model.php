<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot_password_model extends CI_Model
{
    public function checkToken($customer_id, $token)
    {
        if(empty($customer_id) || empty($token))
          return FALSE;

        $this->db->where('ref_id', $customer_id);
        $this->db->where('token', $token);
        $this->db->limit(1);
        $query = $this->db->get('password_forgots');
        if($query->num_rows() === 0)
          return FALSE;

        return TRUE;
    }
}