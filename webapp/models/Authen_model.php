<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authen_model extends CI_Model
{
    public function UserLv()
    {
      return $this->db->where('level_status !=', 'deleted')
                      ->order_by('level_id', 'ASC')
                      ->get("user_levels")
                      ->result_array();
    }

    public function editAuthen($data)
    {
      return $this->db->where('level_id', $data)
            ->get("user_levels")
            ->result_array()[0];
    }


    public function create_user($data_form)
    {
        $this->db->trans_start();
        // $data_form['status'] = 'active';
        // print_r($data_form);die();
        $this->db->insert('user_levels', $data_form);
        $this->db->trans_complete();
        return true;
    }
    public function edit_user($data_form)
    {
      # code...
      $this->db->trans_start();
      $this->db->where('level_id', $data_form['level_id']);
      $this->db->update('user_levels', $data_form);
      $this->db->trans_complete();

      return $this->db->trans_status();
    }

    public function delete_auth($id)
    {
      $this->db->trans_start();
      $data = array(
        'level_status' => 'deleted',
      );
      $this->db->update('user_levels', $data, array('level_id' => $id));
      $this->db->trans_complete();
      return $this->db->trans_status();
    }
}
