<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_setting_model extends CI_Model
{
    //---
    public function getappsetting()
    {
      $this->db->where('prefix', 'app_setting_event_display');
      $query = $this->db->get('app_setting')->result();
      return $query;
    }

    public function updateappsetting($post)
    {
      $json = json_encode($post);
      $array = array('value' => $json);
      $this->db->set($array);
      $this->db->where('prefix','app_setting_event_display');
      $this->db->update('app_setting');
      return true;
    }

}
