<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkin_model extends CI_Model
{
    //---

    public function get_event_registers($event_id)
    {
        $this->db->select('R.*,C.*');
        $this->db->from('event_registers AS R');
        $this->db->join('customers AS C', 'C.customer_id = R.customer_id', 'LEFT');
        $this->db->where('R.event_id', $event_id);
        $this->db->where('R.status', 'active');
        $this->db->order_by('R.update_date', 'DESC');
        $this->db->limit(30);
        $query = $this->db->get()->result();

        foreach ($query as $key => $value) {
          $this->db->select('*');
          $this->db->where('customer_id',$value->customer_id);
          $this->db->where('event_id',$event_id);
          $this->db->where('date(datetime) = CURDATE()', null, false);
          $checkin = $this->db->get('event_checkin')->result();
          $query[$key]->checkin = $checkin;
        }
        return $query;
    }


    public function get_customer($data)
    {
        $this->db->select('customers.* , event_registers.* , event_receipts.*  , customer_levels.level_name , customer_levels.status_1 as status_3_a , customer_levels.status_2 as status_4_a , customer_levels.status_3 as status_5_a , customers.customer_id as customer_id');
        $this->db->where('customers.customer_code',$data['customer_code']);
        $this->db->join('event_registers', 'customers.customer_id = event_registers.customer_id and event_registers.event_id = '.$data['event_id'].'');
        $this->db->join('customer_levels', 'customer_levels.level_id = customers.level_id');
        $this->db->join('event_receipts', 'event_receipts.event_id = event_registers.event_id and event_receipts.customer_id = event_registers.customer_id' , 'left');
        $query = $this->db->get('customers')->result();

        if (count($query) == 0 && $data['customer_code'] && count(explode(" ", $data['customer_code'])) > 1 )  {
          $name = explode(" ", $data['customer_code']);
          $this->db->select('customers.* , event_registers.* , event_receipts.* , customer_levels.level_name , customer_levels.status_1 as status_3_a , customer_levels.status_2 as status_4_a , customer_levels.status_3 as status_5_a , customers.customer_id as customer_id');
          $this->db->where('customers.customer_firstname',$name[0]);
          $this->db->where('customers.customer_lastname',$name[1]);
          $this->db->join('event_registers', 'customers.customer_id = event_registers.customer_id and event_registers.event_id = '.$data['event_id'].'');
          $this->db->join('customer_levels', 'customer_levels.level_id = customers.level_id');
          $this->db->join('event_receipts', 'event_receipts.event_id = event_registers.event_id and event_receipts.customer_id = event_registers.customer_id' , 'left');
          $query = $this->db->get('customers')->result();
        }

        $checkin = [];
        if (count($query)) {
          $this->db->select('*');
          $this->db->where('customer_id',$query[0]->customer_id);
          $this->db->where('event_id',$data['event_id']);
          $this->db->where('date(datetime) = CURDATE()', null, false);
          $checkin = $this->db->get('event_checkin')->result();
        }

        $queryn = "SELECT
                  date(ec.datetime) day_date,
                  (select count(*) from event_checkin as ea where ec.customer_id = ea.customer_id and ec.event_id = ea.event_id and date(ec.datetime) = date(ea.datetime) and ea.status_id = 2 limit 1) as status_2,
                  (select count(*) from event_checkin as ea where ec.customer_id = ea.customer_id and ec.event_id = ea.event_id and date(ec.datetime) = date(ea.datetime) and ea.status_id = 3 limit 1) as status_3,
                  (select count(*) from event_checkin as ea where ec.customer_id = ea.customer_id and ec.event_id = ea.event_id and date(ec.datetime) = date(ea.datetime) and ea.status_id = 4 limit 1) as status_4,
                  (select count(*) from event_checkin as ea where ec.customer_id = ea.customer_id and ec.event_id = ea.event_id and date(ec.datetime) = date(ea.datetime) and ea.status_id = 5 limit 1) as status_5
                  FROM `customers` as c
                  inner join  `event_checkin` as ec ON c.customer_id = ec.customer_id
                  where c.customer_code = '".$data['customer_code']."'
                  and ec.event_id = '".$data['event_id']."'
                  and date(datetime) != CURDATE()
                  group by date(datetime)";
        $beforcheckin = $this->db->query($queryn)->result();


        return  array(
          'beforcheckin' => $beforcheckin ,
          'member' => $query ,
          'checkin' => $checkin
        );
    }


    public function checkin_customer($data)
    {
      $this->db->where('customer_id',$data['customer']);
      $this->db->where('event_id',$data['event_id']);
      $result = $this->db->get('event_registers')->result();

      $post = array(
        'status_2' => 'no',
        'status_3' => 'no',
        'status_4' => 'no',
        'status_5' => 'no',
      );

      if ($data['status_2'] == "true") {
        $post['status_2'] = 'yes';

        $this->db->where('customer_id',$data['customer']);
        $this->db->where('event_id',$data['event_id']);
        $this->db->where('status_id','2');
        $this->db->where('date(datetime) = CURDATE()', null, false);
        $status = $this->db->get('event_checkin')->result();
        if (!count($status)){
          $datapost = array(
            'customer_id'       => $data['customer'],
            'event_id'          => $data['event_id'],
            'status_id'         => '2'
          );
          $this->db->insert('event_checkin', $datapost);
        }
      }
      if ($data['status_3'] == "true") {
        $post['status_3'] = 'yes';

        $this->db->where('customer_id',$data['customer']);
        $this->db->where('event_id',$data['event_id']);
        $this->db->where('status_id','3');
        $this->db->where('date(datetime) = CURDATE()', null, false);
        $status = $this->db->get('event_checkin')->result();
        if (!count($status)) {
          $datapost = array(
            'customer_id'       => $data['customer'],
            'event_id'          => $data['event_id'],
            'status_id'         => '3'
          );
          $this->db->insert('event_checkin', $datapost);
        }
      }
      if ($data['status_4'] == "true") {
        $post['status_4'] = 'yes';

        $this->db->where('customer_id',$data['customer']);
        $this->db->where('event_id',$data['event_id']);
        $this->db->where('status_id','4');
        $this->db->where('date(datetime) = CURDATE()', null, false);
        $status = $this->db->get('event_checkin')->result();
        if (!count($status)) {
          $datapost = array(
            'customer_id'       => $data['customer'],
            'event_id'          => $data['event_id'],
            'status_id'         => '4'
          );
          $this->db->insert('event_checkin', $datapost);
        }
      }
      if ($data['status_5'] == "true") {
        $post['status_5'] = 'yes';

        $this->db->where('customer_id',$data['customer']);
        $this->db->where('event_id',$data['event_id']);
        $this->db->where('status_id','5');
        $this->db->where('date(datetime) = CURDATE()', null, false);
        $status = $this->db->get('event_checkin')->result();
        if (!count($status)) {
          $datapost = array(
            'customer_id'       => $data['customer'],
            'event_id'          => $data['event_id'],
            'status_id'         => '5'
          );
          $this->db->insert('event_checkin', $datapost);
        }
      }

      if ($result) {
          $post['checked_in_staff_id'] = $this->session->userdata('user_id');
          $this->db->set($post);
          $this->db->where('customer_id',$data['customer']);
          $this->db->where('event_id',$data['event_id']);
          $this->db->update('event_registers');
      }else{
        $post['customer_id'] = $data['customer'];
        $post['event_id'] = $data['event_id'];
        $this->db->insert('customer_levels', $post);
      }
    }

    public function search_customer($post)
    {
      $queryn = "SELECT * from customers as c
                 Inner Join event_registers as e on c.customer_id = e.customer_id
                 WHERE (c.customer_firstname LIKE '%".$post['search']."%' OR c.customer_lastname LIKE '%".$post['search']."%')
                 AND event_id = ".$post['event_id']."
                ";
      $querysearch = $this->db->query($queryn)->result();
      return $querysearch;
    }

    // public function editCustomerGroup($post)
    // {
    //   if ($post['level_id']) {
    //     $this->db->set($post);
    //     $this->db->where('level_id',$post['level_id']);
    //     $this->db->update('customer_levels');
    //   }else{
    //     unset($post['id']);
    //     // print_r($post);
    //     $this->db->insert('customer_levels', $post);
    //   }
    // }
}
