<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model
{
    //---
    public function deleteCustomerGroup($id)
    {
      $this->db->where('level_id', $id);
      $this->db->delete('customer_levels');
    }
    public function editCustomerGroup($post)
    {
      if ($post['level_id']) {
        $this->db->set($post);
        $this->db->where('level_id',$post['level_id']);
        $this->db->update('customer_levels');
      }else{
        unset($post['id']);
        // print_r($post);
        $this->db->insert('customer_levels', $post);
      }
    }
    public function getCustomerLv()
    {
        $this->db->where('level_status !=', 'deleted');
        $query = $this->db->get('customer_levels');
        return $query->result();
    }

    public function deleteVideo($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('event_video');
    }
    public function editVideo($post, $FILE='')
    {
      if($FILE['image']['size']){
          $config['upload_path']          = './_uploads/events';
          $config['allowed_types'] = 'jpg|png|jpeg|gif';
          $config['max_size']      = 1024*100;
          $config['max_width']     = 900;
          $config['max_height']    = 600;
          $config['overwrite']     = false;
          $config['file_name']     = date('YmdHis').'_'.time();
         $this->load->library('upload', $config);
         if($this->upload->do_upload('image')){
           $file_data = $this->upload->data();
           $data['image']  = $file_data['file_name'];
         }else{
           return array('status' => false );
         }
      }

      $data['title']      = $post['f_video_name'];
      $data['video_link'] = $post['f_video_url'];
      if ($post['video_id']) {
        $this->db->where('id', $post['video_id']);
        $this->db->update('event_video', $data);
      }else{

        $data['event_id']   = $post['event_id'];
        $this->db->insert('event_video', $data);
      }
      return array('status' => true );
    }
    public function getVideoAll($id)
    {
      $this->db->where('event_id', $id);
      $query = $this->db->get('event_video');
      return $query->result();
    }


    public function deleteHeadDocuemnt($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('event_attach_types');
    }

    public function editHeadDocuemnt($post)
    {
      if ($post['id']) {
        $this->db->set($post);
        $this->db->where('id',$post['id']);
        $this->db->update('event_attach_types');
      }else{
        unset($post['id']);
        // print_r($post);
        $post['type'] = 'file_type';
        $this->db->insert('event_attach_types', $post);
      }

    }

    public function deleteDocuemnt($file_id)
    {
      $this->db->where('file_id', $file_id);
      $this->db->delete('attach_files');
    }

    public function upload_file_document_event($post,$file)
    {
      // print_r($file['upload_data']);die();
      if ($file) {
        $data = array(
          'file_title' => $post['f_name'],
          'ref_id' => $post['f_select'],
          'type' => 'event',
          'file_name' => $file['upload_data']['file_name'],
          'file_type' => $file['upload_data']['file_ext'],
          'file_size' => $file['upload_data']['file_size']
        );
      }else{
        $data = array(
          'file_title' => $post['f_name'],
          'ref_id' => $post['f_select']
        );
      }
      if (isset($post['id'])) {
        $this->db->set($data);
        $this->db->where('file_id',$post['id']);
        $this->db->update('attach_files');
      }else{
        $this->db->insert('attach_files', $data);
      }

      // echo $this->db->last_query();
      return array('file_name' => $file['upload_data']['file_name']);
    }

    public function queryEventList($params=array())
    {
        $this->db->order_by('event_sort', 'ASC');
        $this->db->where('event_status !=', 'deleted');
        $query = $this->db->get( config('tb_event') );
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }


    //---
    public function queryEventDetail($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('event_status !=', 'deleted');
        $query = $this->db->get( config('tb_event') );
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

    //----
    public function insertEvent($data=array())
    {
        if(empty($data)){
            return FALSE;
        }
        $query = $this->db->insert(config('tb_event'), $data);
        if(!$query){
            return FALSE;
        }
        return (int)$this->db->insert_id();

    }

    //----
    public function updateEvent($event_id, $data=array())
    {
        if( empty($event_id) || empty($data)){
            return FALSE;
        }

        $this->db->where('event_id', $event_id);
        $query = $this->db->update(config('tb_event'), $data);
        return (int)$event_id;
    }

    //---
    public function updateCardSetting($event_id, $prefix, $value)
    {
        $this->db->set('setting_value', $value);
        $this->db->where('event_id', $event_id);
        $this->db->where('setting_prefix', $prefix);
        $this->db->update(config('tb_event_setting'));
        return TRUE;
    }

    //----
    public function updateInvoiceSetting($event_id, $prefix, $value)
    {
        $this->db->set('setting_value', $value);
        $this->db->where('event_id', $event_id);
        $this->db->where('setting_prefix', $prefix);
        $this->db->update(config('tb_invoice_setting'));
        return TRUE;
    }

    public function getDocuemntAllFiles($event_id)
    {
        $this->db->where('event_attach_types.event_id', $event_id);
        $this->db->where('event_attach_types.type', 'file_type');
        $this->db->join('attach_files', 'event_attach_types.id = attach_files.ref_id');
        $query = $this->db->get('event_attach_types');
       return $query->result();
    }

    public function getDocuemntAllType($event_id)
    {
        $this->db->where('event_attach_types.event_id', $event_id);
        $this->db->where('event_attach_types.type', 'file_type');
        $query = $this->db->get('event_attach_types');
       return $query->result();
    }

    public function getGift($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('event_gifts');
       return $query->result();
    }

    public function deleteGift($event_id, $gift_id)
    {
        $this->db->set('status', 'deleted');
        $this->db->where('event_id', $event_id);
        $this->db->where('gift_id', $gift_id);
        $this->db->update('event_gifts');
        return TRUE;
    }

    public function getPromotion($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('status !=', 'discard');
        $query = $this->db->get('event_promotions');
       return $query->result();
    }
    public function getInvoiceSummary($event_id)
        {
          $this->db->select("sum(IF(I.payment_status = 'unpaid',1,0)) as unpaid,
          sum(IF(I.payment_status = 'paid',1,0)) as paid,
          sum(IF(I.payment_status = 'approved',1,0)) as approved,
          sum(IF(I.payment_status = 'failed',1,0)) as cancel");
          $this->db->from(config('tb_invoice').' AS I');
                  $this->db->join(config('tb_customer').' AS M', 'M.customer_id=I.customer_id', 'LEFT');
            $this->db->where('event_id', $event_id);
            $this->db->where('M.customer_status !=', 'discard');
             $this->db->where('I.status', 'active');
               $query = $this->db->get();
           return $query->row(0);
        }
}
