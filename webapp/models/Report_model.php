<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Report_model extends CI_Model

{

 public function get_event_register($datestart = '', $dateend = '')
 {
   $this->db->select('events.event_title,customers.customer_name_title,customers.customer_firstname,customers.customer_lastname,
   customers.customer_tel,customers.customer_email,master_departments.department_name,event_invoices.payment_status,event_registers.regis_dtm,count(customers.customer_name_title) as num
   ');
   $this->db->from('event_registers');
   $this->db->join('customers' , 'event_registers.registrant_id = customers.customer_id');
   $this->db->join('events','event_registers.event_id = events.event_id');
   $this->db->join('customer_agencies','customers.customer_id = customer_agencies.customer_id');
   $this->db->join('master_departments','master_departments.department_id = customer_agencies.department_id');
   $this->db->join('event_invoices','event_invoices.event_id = events.event_id');

   if($datestart != '' and $dateend != ''){
     $this->db->where('regis_dtm >=' ,$datestart);
     $this->db->where('regis_dtm <=' ,$dateend);
   }
   else if($datestart != '' and $dateend == ''){
     $this->db->where('regis_dtm >=' ,$datestart);
   }
   else if($datestart == '' and $dateend != ''){
     $this->db->where('regis_dtm <=' ,$dateend);
   }

   $this->db->group_by('registrant_id');
   $query = $this->db->get();
   return $query->result();
 }

 public function get_event_register_food($datestart = '', $dateend = '' , $food = '' )
 {

   $this->db->select('events.event_title,
                      customers.customer_name_title,
                      customers.customer_firstname,
                      customers.customer_lastname,
                      customers.customer_tel,
                      customers.customer_email,
                      master_departments.department_name,
                      event_registers.regis_dtm,
                      master_food_types.food_name'
                    );
   $this->db->from('event_registers');
   $this->db->join('customers' , 'event_registers.registrant_id = customers.customer_id');
   $this->db->join('events','event_registers.event_id = events.event_id');
   $this->db->join('customer_agencies','customers.customer_id = customer_agencies.customer_id');
   $this->db->join('master_departments','master_departments.department_id = customer_agencies.department_id');
   $this->db->join('master_food_types','master_food_types.food_id = customers.food_id');

   if($datestart != '' and $dateend != '' and $food != ''){
     $this->db->where('regis_dtm >=' ,$datestart);
     $this->db->where('regis_dtm <=' ,$dateend);
     $this->db->where('master_food_types.food_id' ,$food);
   }
   else{

     if($datestart != '' and $dateend != ''){
       $this->db->where('regis_dtm >=' ,$datestart);
       $this->db->where('regis_dtm <=' ,$dateend);
     }
     else if($datestart != '' and $dateend == ''){
       $this->db->where('regis_dtm >=' ,$datestart);
     }
     else if($datestart == '' and $dateend != ''){
       $this->db->where('regis_dtm <=' ,$dateend);
     }

     if($food != ''){
       $this->db->where('master_food_types.food_id' ,$food);
     }

   }

   $this->db->group_by('registrant_id');
   $query = $this->db->get();
   return $query->result();



 }
}
