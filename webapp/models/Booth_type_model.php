<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booth_type_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBoothType($event_id, $limit=20, $offset=0, $filters=[])
    {
      $this->db->where('event_id', $event_id);
      $this->db->where('type_status !=', 'deleted');
      $this->db->limit($limit, $offset);
      $this->db->order_by('type_name', 'ASC');
      $this->db->order_by('type_code', 'ASC');
      $query = $this->db->get('booth_types');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }

    public function getBoothTypeId($event_id, $type_id)
    {
      if(empty($event_id) || empty($type_id))
        return FALSE;

      $this->db->where('type_id', $type_id);
      $this->db->where('event_id', $event_id);
      $this->db->where('type_status !=', 'deleted');
      $query = $this->db->get('booth_types');
      if($query->num_rows() === 0)
        return FALSE;

      return $query->row();
    }

    public function createBoothType($data=[])
    {
      if(!is_array($data) || empty($data['event_id']))
        return FALSE;

      $result = $this->db->insert('booth_types', $data);
      if($result)
        return $this->db->insert_id();

      return FALSE;
    }

    public function updateBoothType($event_id, $type_id, $data=[])
    {
      if(empty($event_id) || empty($type_id) || !is_array($data))
        return FALSE;

      $this->db->where('type_id', $type_id);
      $this->db->where('event_id', $event_id);
      $result = $this->db->update('booth_types', $data);
      return $type_id;
    }

    public function deleteBoothType($event_id, $type_ids=NULL)
    {
      if(empty($event_id) || empty($type_ids))
        return FALSE;

      if(is_array($type_ids)){
        foreach ($type_ids as $type_id) {
          $this->db->set('type_status', 'deleted');
          $this->db->where('event_id', $event_id);
          $this->db->where('type_id', $type_id);
          $this->db->update('booth_types');
        }
      }else{
        foreach ($booth_ids as $booth_id) {
          $this->db->set('type_status', 'deleted');
          $this->db->where('event_id', $event_id);
          $this->db->where('type_id', $type_id);
          $this->db->update('booth_types');
        }
      }
      return TRUE;
    }
}