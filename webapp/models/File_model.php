<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_model extends CI_Model
{
    //---@
    public function uploadFile($FILE, $fileName, $path,$oldName = NULL)
    {
        //--
        if(empty($FILE) || empty($fileName) || empty($path))
            return $response;

        //--@Upload config
         $config['upload_path']   = $path;
         $config['allowed_types'] = 'gif|jpg|png|jpeg';
         $config['max_size']      = 3072;
         $config['max_width']     = 1024;
         $config['max_height']    = 1024;
         $config['overwrite']     = false;
         $config['file_name']     = date('YmdHis').time();
         $this->load->library('upload', $config);

         if (!$this->upload->do_upload($fileName)) {
            $response  = array(
                    'status'  => FALSE,
                    'message' =>  strip_tags($this->upload->display_errors())
                );
         }else{

            //--@Delete old file
            if(!empty($oldName))
                @unlink( $path.$oldName);

            $response  = array(
                    'status'  => TRUE,
                    'data'    =>  $this->upload->data()
                );
         }
        return $response;
    }
}