<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booth_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBooth($event_id, $limit=10, $offset=0, $filters=[])
    {
      $this->db->select('*');
      $this->db->select('(select count(*) from booths_vote as bv where bv.event_id = B.event_id and bv.booth_id = B.booth_id limit 1) as count_vote');
      $this->db->select('(select sum(vote_point) from booths_vote as bv where bv.event_id = B.event_id and bv.booth_id = B.booth_id limit 1) as vote_point');
      $this->db->from('booths AS B');
      $this->db->join('booth_types AS T', 'T.type_id=B.booth_type_id', 'LEFT');
      $this->db->where('B.event_id', $event_id);
      $this->db->where('B.booth_status !=', 'deleted');
      $query = $this->db->get();
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }

    public function getBoothId($event_id, $booth_id)
    {
      if(empty($event_id) || empty($booth_id))
        return FALSE;

      $this->db->from('booths AS B');
      $this->db->join('booth_types AS T', 'T.type_id=B.booth_type_id', 'LEFT');
      $this->db->where('B.booth_id', $booth_id);
      $this->db->where('B.event_id', $event_id);
      $this->db->where('B.booth_status !=', 'deleted');
      $query = $this->db->get();
      if($query->num_rows() === 0)
        return FALSE;

      return $query->row();
    }

    public function createBooth($data=[])
    {
      if(!is_array($data) || empty($data['event_id']))
        return FALSE;

      $result = $this->db->insert('booths', $data);
      if($result)
        return $this->db->insert_id();

      return FALSE;
    }

    public function updateBooth($event_id, $booth_id, $data=[])
    {
      if(empty($event_id) || empty($booth_id) || !is_array($data))
        return FALSE;

      $this->db->where('booth_id', $booth_id);
      $result = $this->db->update('booths', $data);
      return $booth_id;
    }

    public function deleteBooth($event_id, $booth_ids=NULL)
    {
      if(empty($event_id) || empty($booth_ids))
        return FALSE;

      if(is_array($booth_ids)){
        foreach ($booth_ids as $booth_id) {
          $this->db->set('booth_status', 'deleted');
          $this->db->where('event_id', $event_id);
          $this->db->where('booth_id', $booth_id);
          $this->db->update('booths');
        }
      }else{
        foreach ($booth_ids as $booth_id) {
          $thsi->db->set('booth_status', 'deleted');
          $this->db->where('event_id', $event_id);
          $this->db->where('booth_id', $booth_id);
          $this->db->update('booths');
        }
      }
      return TRUE;
    }

    public function getBoothForQR($event_id)
    {

      $this->db->select('booth_id,event_id');
      $this->db->from('booths');
      $this->db->where('event_id', $event_id);
      $this->db->where('booth_status !=', 'deleted');
      $query = $this->db->get();
      if($query->num_rows() === 0)
        return FALSE;

      return $query->result();
    }

}
