<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// date_default_timezone_set('Asia/Bangkok');

class Event_register_model extends CI_Model
{
    //---
    public function getEventParticipant($event_id, $registrant_id)
    {
        $this->db->select('R.regis_id,R.regis_dtm,R.registrant_id,R.checked_in,R.checked_in_staff_id');
        $this->db->select('R.regis_type,R.event_id');
        $this->db->select('M.customer_name_title as name_title,M.customer_firstname,M.customer_lastname,M.customer_email,M.customer_mobile');
        $this->db->select('M.customer_tel');
        $this->db->from(config('tb_event_register').' AS R');
        $this->db->join(config('tb_customer').' AS M', 'M.customer_id=R.customer_id', 'LEFT');
        $this->db->where('R.event_id', $event_id);
        $this->db->where('R.registrant_id', $registrant_id);
        $this->db->where('R.status', 'active');
        $this->db->order_by('M.customer_firstname', 'ASC');
        $query = $this->db->get();
        if($query->num_rows() === 0){
          return FALSE;
        }

        return $query->result();
    }

    public function event_register_update($post,$image)
    {

      $array = array();

      if (isset($image['upload_data'])) {
        $array['payment_slip'] = $image['upload_data']['file_name'];
      }

      if ($post['receipt_date'] != 'undefined') {
        $arraydate = explode("-",$post['receipt_date']);
        $newdate = $arraydate[2].'-'.$arraydate[1].'-'.$arraydate[0];
        $array['billing_dtm'] = $newdate;
      }

      if ( $post['receipt_type'] != 'undefined') {
        $array['payment_status'] = $post['receipt_type'];
      }

      if ($array) {
        $this->db->set($array);
        $this->db->where('invoice_id',$post['invoice_id']);
        $this->db->update('event_invoices');
      }
    }


    public function save_event_register_customer($post)
    {
      // print_r($post);

      $array = array(
        "customer_name_title" => $post['customer_name_title'] ,
        "customer_firstname" => $post['customer_firstname'] ,
        "customer_lastname" => $post['customer_lastname'] ,
        // "customer_email" => $post['customer_email'] ,
        // "customer_mobile" => $post['customer_mobile']
      );
      $this->db->set($array);
      $this->db->where('customer_id',$post['customer_id']);
      $this->db->update('customers');
      $array2 = array(
        "agency_name" => $post['agency_name']
      );
      $this->db->set($array2);
      $this->db->where('customer_id',$post['customer_id']);
      $this->db->update('customer_agencies');
      $array3 = array(
        "tax_no" => $post['tax_no'],
        "address" => $post['address'],
        "address_road" => $post['address_road'],
        "zipcode" => $post['zipcode'],
        "province_id" => $post['province'],
        "district_id" => $post['district'],
        "sub_district_id" => $post['sub_district']
      );
      $this->db->set($array3);
      $this->db->where('customer_id',$post['customer_id']);
      $this->db->update('customer_address');
    }

}
