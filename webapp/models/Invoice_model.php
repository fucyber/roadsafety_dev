<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_model extends CI_Model
{

    //---
    public function countTotalInvoice($event_id, $params=array())
    {
        $this->db->select('I.*');
        $this->db->select('M.customer_code,M.customer_name_title,M.customer_firstname,M.customer_lastname,M.customer_mobile');
        $this->db->from(config('tb_invoice').' AS I');
        $this->db->join(config('tb_customer').' AS M', 'M.customer_id=I.customer_id', 'LEFT');
        if(!empty($params['name'])){
            $this->db->like('M.customer_firstname', trim($params['name']));
            $this->db->or_like('M.customer_lastname', trim($params['name']));
        }
        if(!empty($params['ref_1'])){
            $this->db->like('I.invoice_ref_1', trim($params['ref_1']));
            $this->db->or_like('I.invoice_ref_2', trim($params['ref_1']));
        }
        if(!empty($params['payment_status']))
            $this->db->where('I.payment_status', $params['payment_status']);

        if(!empty($params['date_start']) && !empty($params['date_end'])){
            $this->db->where('I.invoice_created_at >=', date('Y-m-d', strtotime($params['date_start'])));
            $this->db->where('I.invoice_created_at <=', date('Y-m-d', strtotime($params['date_end'])));
        }
        if(!empty($params['date_start']) && empty($params['date_end'])){
            $this->db->where('I.invoice_created_at >=', date('Y-m-d', strtotime($params['date_start'])));
        }
        $this->db->where('I.event_id', $event_id);
        $this->db->where('M.customer_status !=', 'discard');
        $this->db->where('I.status', 'active');
        $this->db->order_by('I.invoice_id', 'DESC');
        $query = $this->db->get();
        return $query->num_rows() ;
    }

    //---
    public function getInvoice($event_id, $params=array())
    {
        $limit  = (!empty($params['limit'])  ? $params['limit']: config('page_limit'));
        $offset = (!empty($params['offset']) ? $params['offset']: 0);


        $this->db->select('I.*');
        $this->db->select('M.customer_code,M.customer_name_title,M.customer_firstname,M.customer_lastname,M.customer_mobile');
        $this->db->from(config('tb_invoice').' AS I');
        $this->db->join(config('tb_customer').' AS M', 'M.customer_id=I.customer_id', 'LEFT');
        if(!empty($params['firstname'])){
            $this->db->like('M.customer_firstname', trim($params['firstname']));
        }
        if(!empty($params['lastname'])){
            $this->db->like('M.customer_lastname', trim($params['lastname']));
        }

        if(!empty($params['ref_1'])){
            $this->db->like('I.invoice_ref_1', trim($params['ref_1']));
        }
        if(!empty($params['ref_2'])){
            $this->db->like('I.invoice_ref_2', trim($params['ref_2']));
        }

        if(!empty($params['payment_status']))
            $this->db->where('I.payment_status', $params['payment_status']);

        if(!empty($params['date_start']) && !empty($params['date_end'])){
            $this->db->where('I.invoice_created_at >=', date('Y-m-d', strtotime($params['date_start'])));
            $this->db->where('DATE_ADD(I.invoice_created_at, INTERVAL -1 DAY) <=', date('Y-m-d', strtotime($params['date_end'])));
        }
        if(!empty($params['date_start']) && empty($params['date_end'])){
            $this->db->where('I.invoice_created_at >=', date('Y-m-d', strtotime($params['date_start'])));
        }
        $this->db->where('I.event_id', $event_id);
        $this->db->where('M.customer_status !=', 'discard');
        $this->db->where('I.status', 'active');
        $this->db->order_by('I.invoice_id', 'DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        if($query->num_rows() === 0){
          return FALSE;
        }

        return $query->result();
    }

    //---
    public function getInvoiceDetail($event_id, $customer_id)
    {
        $this->db->select('I.*');
        $this->db->select('C.customer_code,C.customer_name_title as name_title,C.customer_firstname,C.customer_lastname,C.customer_mobile');
        $this->db->from(config('tb_invoice').' AS I');
        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=I.customer_id', 'LEFT');
        $this->db->where('I.event_id', $event_id);
        $this->db->where('C.customer_id', $customer_id);
        $this->db->where('I.status', 'active');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0){
          return FALSE;
        }
        return $query->row();
    }

    //---
    public function getInvoicePayment($event_id, $customer_id)
    {
        if(empty($event_id) || empty($customer_id))
            return FALSE;

        $this->db->select('I.*');
        $this->db->select('M.customer_code,M.customer_name_title,M.customer_firstname,M.customer_lastname,M.customer_mobile');
        $this->db->from(config('tb_invoice').' AS I');
        $this->db->join(config('tb_customer').' AS M', 'M.customer_id=I.customer_id', 'LEFT');
        $this->db->where('I.event_id', $event_id);
        $this->db->where('I.customer_id', $customer_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0){
          return FALSE;
        }

        return $query->row();
    }

    //---
    public function updatePaymentStatus($invoice_id, $status=NULL)
    {
        if(empty($invoice_id) || empty($status))
            return FALSE;

        $this->db->set('payment_status', $status);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update(config('tb_invoice'));
        return TRUE;
    }

    //---
    public function updateReceiptDate($invoice_id, $date='')
    {
        if(empty($invoice_id))
            return FALSE;

        $this->db->set('billing_dtm', $date);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update(config('tb_invoice'));
        return TRUE;
    }

     //---
    public function getInvoiceAddress($address_id)
    {
        $this->db->select('S.sub_district_name,D.district_name_th AS district_name,P.province_name_th AS province_name');
        $this->db->select('C.address_id,C.address,C.address_soi,C.address_road,C.zipcode,C.remark,C.district_id,C.province_id,C.sub_district_id');
        $this->db->from(config('tb_customer_address').' AS C');
        $this->db->join(config('tb_privince').' AS P', 'P.province_id=C.province_id', 'LEFT');
        $this->db->join(config('tb_district').' AS D', 'D.district_id=C.district_id', 'LEFT');
        $this->db->join(config('tb_sub_district').' AS S', 'S.sub_district_id=C.sub_district_id', 'LEFT');
        $this->db->where('C.address_id', $address_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

    //---
    public function updatePayment($invoice_id, $params=[])
    {
        if(empty($invoice_id))
            return FALSE;

        $this->db->where('invoice_id', $invoice_id);
        return $this->db->update(config('tb_invoice'), $params);
    }

    //---
    public function queryInvoice($customer_id, $event_id)
    {
        $this->db->select('E.event_title,E.event_image');
        $this->db->select('I.*');
        // $this->db->select('ER.*');
        $this->db->select('C.customer_id,C.customer_code,C.occupation_id,C.food_id');
        $this->db->select('C.customer_name_title,C.customer_firstname,C.customer_lastname,C.customer_nickname,C.customer_email');
        $this->db->from(config('tb_invoice').' AS I');
        $this->db->join(config('tb_event').' AS E', 'E.event_id=I.event_id', 'LEFT');
        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=I.customer_id', 'LEFT');
        // $this->db->join('event_receipts AS ER', 'ER.event_id=I.event_id and ER.customer_id=I.customer_id and ER.invoice_id=I.invoice_id ', 'LEFT');
        $this->db->where('I.customer_id', $customer_id);
        $this->db->where('I.event_id', $event_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

    //---
    public function getInvoiceTotalPrice($invoice_id)
    {
        $this->db->select('sum(item_price) AS total_price');
        $this->db->where('invoice_id', $invoice_id);
        $this->db->limit(1);
        $query = $this->db->get(config('tb_invoice_item'));
        if($query->num_rows() === 0)
            return FALSE;

        return number_format($query->row()->total_price, 0);
    }

    //---
    public function countRegisterByInvoice($event_id, $customer_id)
    {
        $this->db->select('regis_id');
        $this->db->where('event_id', $event_id);
        $this->db->where('customer_id', $customer_id);
        $this->db->where('status', 'active');
        $query = $this->db->get(config('tb_event_register'));
        return $query->num_rows();
    }

    //---
    public function getInvoiceRegister($event_id, $registrant_id)
    {
        $this->db->select('R.customer_id');
        $this->db->select('C.customer_name_title,C.customer_firstname,C.customer_lastname');
        $this->db->from(config('tb_event_register').' AS R');
        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=R.customer_id', 'LEFT');
        $this->db->where('R.registrant_id', $registrant_id);
        $this->db->where('R.event_id', $event_id);
        $this->db->where('R.status', 'active');
        $this->db->order_by('C.customer_firstname', 'ASC');
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->result();
    }

    //---
    public function queryBillAddress($bill_id)
    {
        // $this->db->select('address_id,address,address_soi,address_road,province_id,district_id,sub_district_id,zipcode,remark');
        $this->db->select('*');
        $this->db->from(config('tb_customer_address'));
        $this->db->where('address_id', $bill_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

     //---
    public function getEventPrice($event_id)
    {
        $this->db->select('event_price_type,event_price');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get(config('tb_event'));
        if($query->num_rows() === 0)
            return 0;

        $event = $query->row();
        if($event->event_price_type === 'summary'){
            return $event->event_price;
        }

        $this->db->select('sum(session_price) AS total_price');
        $this->db->where('event_id', $event_id);
        $query2 = $this->db->get(config('tb_event_session'));
        if($query2->num_rows() === 0)
            return 0;

        return $query2->row()->total_price;
    }

        //------
    public function getTotalSessionPrice($event_id)
    {
        $this->db->select('sum(session_price) as total_price');
        $this->db->where('event_id', $event_id);
        $this->db->where('session_status !=', 'deleted');
        $query = $this->db->get( config('tb_event_session') );
        if($query->num_rows() === 0)
            return 0;

        return $query->row()->total_price;
    }

     //----
    public function getGroupParticipant($event_id, $registrant_id)
    {
        $this->db->from(config('tb_event_register').' AS R');
        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=R.customer_id', 'LEFT');
        $this->db->where('R.registrant_id', $registrant_id);
        $this->db->where('R.event_id', $event_id);
        $this->db->where('R.status', 'active');
        $query = $this->db->get();
        if($query->num_rows() == 0)
            return '';

        $data_list = $query->result();
        foreach ($data_list as $key => $val) {
            $response_data[$key]['regis_id']         = $val->regis_id;
            //$response_data[$key]['regis_dtm']         = $val->regis_dtm;
            $response_data[$key]['customer_id']         = $val->customer_id;
            $response_data[$key]['customer_code']         = $val->customer_code;
            $response_data[$key]['customer_email']         = $val->customer_email;
            $response_data[$key]['customer_name_title'] = $val->customer_name_title;
            $response_data[$key]['customer_firstname']  = $val->customer_firstname;
            $response_data[$key]['customer_lastname']   = $val->customer_lastname;
            //$response_data[$key]['customer_nickname']   = $val->customer_nickname;
            $response_data[$key]['customer_gender']   = $val->customer_gender;
            $response_data[$key]['customer_age']   = $val->customer_age;
            $response_data[$key]['occupation_id']   = $val->occupation_id;
            $response_data[$key]['job_description']   = $val->job_description;
            $response_data[$key]['customer_mobile']   = $val->customer_mobile;
            $response_data[$key]['food_id']   = $val->food_id;
        }
        return $response_data;
    }


}
