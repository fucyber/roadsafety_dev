<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//--https://zendframework.github.io/zend-barcode/usage
//--https://framework.zend.com/manual/2.1/en/modules/zend.barcode.objects.html
use Zend\Barcode\Barcode;


class Barcode_model extends CI_Model
{

    //---@Generate Bard code
    public  function barcode($gen_code=NULL, $url='')
    {
        if(empty($gen_code)){
            echo 'Empty data';
            return;
        }
        #-----
        $barcodeOptions = array(
                  'text'          => $gen_code, //required
                  'factor'        => 3.98,
                  'drawText'      => false,
                  'withBorder'    => false,
                  //'barHeight'   => 60,
                  'barThickWidth' => 6,
                  'barThinWidth'  => 2
                );
        #-----No required options.
        $rendererOptions = array('imageType'=>'gif','horizontalPosition' => 'left','leftOffset' => 0);
        $file = Barcode::factory(
            'code128',  //code128,code39
            'image',
            $barcodeOptions,
            $rendererOptions
        )->draw();
        $code = $gen_code.'.gif';
        $store_image = imagegif($file, config('inside_upload_dir')."/barcodes/$code");

        if(!empty($url))
            return $url.'/barcodes/'.$code;
        else
            return config('inside_upload_dir')."barcodes/".$code;
    }

    //---@
    public  function QRCode($input_code = NULL, $prefix='', $url='')
    {
        if(empty($input_code))
            return FALSE;

        $this->load->library('qrcode/ciqrcode');
        $image_dir  =  config('inside_upload_dir').'booths/';

        //--Config
        $config['cacheable']    = true;
        $config['cachedir']     = 'application/cache/qrcode/';
        $config['errorlog']     = 'application/cache/logs/';
        $config['imagedir']     = $image_dir;
        $config['quality']      = true;
        $config['size']         = 1024;
        $config['black']        = array(224,255,255);
        $config['white']        = array(70,130,180);
        $this->ciqrcode->initialize($config);

        //---Generate
        $params['data']     =   $input_code;
        $params['level']    =   'H';
        $params['size']     =   1024;
        $filename = $prefix.time().'.png';
        $params['savename'] = $config['imagedir'].$filename;
        $this->ciqrcode->generate($params);
        if(!empty($url))
            return $url.'/booths/'.$filename;
        else
            return config('inside_upload_url').'booths/'.$filename;
    }

    public  function QRCustomer($input_code = NULL, $prefix='', $url='')
    {
        if(empty($input_code))
            return FALSE;

        $this->load->library('qrcode/ciqrcode');
        $image_dir  =  config('inside_upload_dir').'customer_qr/';

        //--Config
        $config['cacheable']    = true;
        $config['cachedir']     = 'application/cache/qrcode/';
        $config['errorlog']     = 'application/cache/logs/';
        $config['imagedir']     = $image_dir;
        $config['quality']      = true;
        $config['size']         = 1024;
        $config['black']        = array(224,255,255);
        $config['white']        = array(70,130,180);
        $this->ciqrcode->initialize($config);

        //---Generate
        $params['data']     =   $input_code;
        $params['level']    =   'H';
        $params['size']     =   1024;
        $filename = $input_code.'.png';
        $params['savename'] = $config['imagedir'].$filename;
        $this->ciqrcode->generate($params);
        if(!empty($url))
            return $url.'customer_qr/'.$filename;
        else
            return config('inside_upload_url').'customer_qr/'.$filename;
    }
}
