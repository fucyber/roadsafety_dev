<?php

//--@System message
$lang['data_save_success']        = "บันทึกข้อมูลสำเร็จ"; //Data has been successfully saved.
$lang['data_save_failed']         = "บันทึกข้อมูลล้มเหลว กรุณาลองใหม่"; //save data failed.
$lang['data_delete_success']      = "ลบข้อมูลเรียบร้อย";
$lang['data_delete_failed']       = "ลบข้อมูลล้มเหลว กรุณาลองใหม่";
$lang['unauthorized_access']      = "การเข้าถึงที่ไม่ได้รับอนุญาต";  //Unauthorized access
 //--500
$lang['system_error']             = 'System error, Please wait a minute and try again or contact support for assistance.';
$lang['data_invalid']             = 'Invalid get data';
$lang['system_timeout']         = 'System timeout, Please try again.';


//--400
$lang['HTTP_NO_CONTENT']          = 'ไม่มีข้อมูล'; //NO CONTENT
$lang['HTTP_UNAUTHORIZED']        = "การเข้าถึงไม่ได้รับอนุญาต"; //Unauthorized access
$lang['require_content']          = "'Context-param' ไม่สมบูรณ์"; //Require data: The content of element 'context-param' is not complete
$lang['data_not_found']           = 'ไม่พบข้อมูล'; //Data not found.
$lang['parameters_not_found']     = 'พารามิเตอร์ไม่ครบถ้วน';
$lang['require_email_tel']        = "ไม่พบข้อมูลอีเมล์เบอร์โทรศัพท์"; //Require email/tel data.


$lang['title'] = '';
$lang['action_select_all'] = "เลือกทั้งหมด";
$lang['empty_data'] = "ยังไม่มีข้อมูล";

$lang['text_delete'] = "ลบข้อมูล";
$lang['text_update'] = "แก้ไขข้อมูล";
$lang['text_view']   = "ข้อมูล";


$lang['insert_data_success'] = "บันทึกข้อมูลสำเร็จ";
$lang['insert_data_failed']  = "ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจตรวจสอบและลองใหม่";

$lang['unpaid']   = 'Unpaid';
$lang['paid']     = 'Paid';
$lang['cancel']   = 'Cancel';
$lang['approved'] = 'Approved';
$lang['confirm'] = 'Confirm';

$lang['login_failed']   = 'ข้อมูล็อกอินไม่ถูกต้อง กรุณาตรวจสอบ!';
$lang['login_success']  = 'เข้าสู้ระบบสำเร็จ....';
