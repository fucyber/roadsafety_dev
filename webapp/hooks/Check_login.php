<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check_login {

  private $ci;
  public function __construct()
  {
    $this->ci = & get_instance();

  }
  public function checked()
  {
    $controller = $this->ci->router->class;
    $method     = $this->ci->router->method;


     if( $controller != 'login' && $controller != 'reset_password' && $controller != 'save_new_password'){
         if( empty($_SESSION['level_id']) ||  empty($_SESSION['user_id'])){
             redirect( base_url('login') );
         }
     }

  }

}
?>