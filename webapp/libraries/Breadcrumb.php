<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Breadcrumb {

    private $breadcrumbs = array();
    private $separator = '';
    private $start = '<ul class="breadcrumb" style="padding:0px;"><i class="material-icons small">&#xE871;</i>  ';
    private $end = '</ul>';

    public function __construct($params = array()){
        if (count($params) > 0){
            $this->initialize($params);
        }
    }

    /**
     * Initialize class
     *
     * @param array $params settings
     * @return void
     * @access private
     */
    private function initialize($params = array()){
        if (count($params) > 0){
            foreach ($params as $key => $val){
                if (isset($this->{'_' . $key})){
                    $this->{'_' . $key} = $val;
                }
            }
        }
    }

    /**
     * add bread crumb value class
     *
     * @param $title  = wording text
     * @param $href  =  link url
     * @return void
     * @access public
     */
    public function add($title, $href){
        if (!$title OR !$href) return;
        $this->breadcrumbs[] = array('title' => $title, 'href' => $href);
    }


    /**
     * Show bootstrap Breadcrumb class
     *
     * @param  null
     * @return bootstrap 3.1  Breadcrumb html  style
     * @access public
     */
    public function output(){
        if ($this->breadcrumbs) {
            //echo '<pre>'; print_r($this->breadcrumbs); echo '</pre>';
            $output = $this->start;

            foreach ($this->breadcrumbs as $key => $crumb) {
                if ($key){
                    $output .= $this->separator;
                }
                $arr_key = array_keys($this->breadcrumbs);
                if (end($arr_key) == $key) {
                    $output .= '<li class="uk-active"><span>' . $crumb['title'] . '</span></li>';
                } else {
                    $output .= '<li><a href="' . $crumb['href'] . '">' . $crumb['title'] . '</a></li>';
                }
            }

            return $output . $this->end . PHP_EOL;
        }

        return '';
    }

}

/* End of file breadrumb_lib.php */
/* Location: ./application/libraries/breadrumb_lib.php */