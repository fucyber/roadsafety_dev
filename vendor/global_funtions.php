<?php
if(stristr(htmlentities($_SERVER['PHP_SELF']), "global_funtions.php")) {
   header("Location: ../");
   die();
}
/**
 * Debugs
 * **********************************************************************
 */
function printr($data = '' )
{
  echo '<pre>';
  print_r($data);
  echo '</pre>';
}


function vardump($data = '' )
{
  echo '<pre>';
  var_dump($data);
  echo '</pre>';
}


/**
 * Application Setting
 * **********************************************************************
 */
function invoice_setting($event_id=NULL, $prefix=NULL)
{
    $not_found = '';
    //---
    if(empty($event_id) || empty($prefix))
        return $not_found;

    $CI =& get_instance();

    $CI->db->select('setting_value');
    $CI->db->where('event_id', $event_id);
    $CI->db->where('setting_prefix', $prefix);
    $CI->db->where('setting_status', 'active');
    $CI->db->limit(1);
    $query = $CI->db->get(config('tb_invoice_setting'));
    if($query->num_rows() === 0)
        return $not_found;

    return $query->row()->setting_value;
}


//--
function event_setting($event_id=NULL, $prefix=NULL)
{
    $not_found = '';
    //---
    if(empty($event_id) || empty($prefix))
        return $not_found;

    $CI =& get_instance();

    $CI->db->select('setting_value');
    $CI->db->where('event_id', $event_id);
    $CI->db->where('setting_prefix', $prefix);
    $CI->db->where('setting_status', 'active');
    $CI->db->limit(1);
    $query = $CI->db->get(config('tb_event_setting'));
    if($query->num_rows() === 0)
        return $not_found;

    return $query->row()->setting_value;
}


function config($value = NULL)
{
    if($value === NULL)
        return '';

    $CI =& get_instance();

    if($CI->config->item($value))
        return $CI->config->item($value);
    else
        return 'Not found ['.$value.'] config key.';
}

function event_status($status=null)
{
    switch ($status) {
        case 'active':
            $result = '<span class="label label-success">Active</span>';
            break;
        case 'inctive':
            $result = '<span class="label label-success">Disable</span>';
            break;
        default:
            $result = '';
            break;
    }
    return $result;
}

function registerType($type=NULL)
{
    if($value === NULL)
        $result = '';

    $CI =& get_instance();

    if($CI->config->item($value))
        $result = $CI->config->item($value);

    return $result;
}

function paymentStatus($status='')
{
    switch ($status) {
        case 'paid':
            $html = '<span class="badge bg-cyan">'.lang('paid').'</span>';
            break;
        case 'cancel':
            $html = '<span class="badge bg-red">'.lang('cancel').'</span>';
            break;
        case 'approved':
            $html = '<span class="badge bg-green">'.lang('approved').'</span>';
            break;
        case 'reject':
            $html = '<span class="badge bg-red">Reject</span>';
            break;
        default:
            $html = '<span class="badge">'.lang('unpaid').'</span>';
            break;
    }
    return $html;
}

function paymentAmount($status='', $amount='')
{
    switch ($status) {
        case 'paid':
        case 'cancel':
        case 'approved':
            $result = $amount;
            break;
        default:
            $result = '';
            break;
    }
    return $result;
}

function show_datetime($datetime='')
{
    return mdate('%d %M %Y <br>%H:%i:%s', strtotime($datetime));
}

function show_datetime_inline($datetime='')
{
    return mdate('%d %M %Y  (%H:%i:%s)', strtotime($datetime));
}

function prettyInvoiceAddress($billing_id)
{
    $CI =& get_instance();
    $CI->load->model('Invoice_model', 'Invoice');
    $address = $CI->Invoice->getInvoiceAddress($billing_id);
    if(!$address)
        return '';

    $str  = $address->address;
    if(!empty($address->address_soi))
        $str .= ' ซอย'.$address->address_soi;
    if(!empty($address->address_road))
        $str .= ' ถนน'.$address->address_road;
    $str .= ' '.$address->sub_district_name;
    $str .= ' '.$address->district_name;
    $str .= ' '.$address->province_name;
    $str .= ' '.$address->zipcode;
    return $str;
}

function getOccupationName($occupation_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('occupation_name')
                ->where('occupation_id', $occupation_id)
                ->get(config('tb_occupation'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->occupation_name;
}

function getFoodName($food_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('food_name')
                ->where('food_id', $food_id)
                ->get(config('tb_food_type'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->food_name;
}

function getMinistryName($ministry_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('ministry_name')
                ->where('ministry_id', $ministry_id)
                ->get(config('tb_ministry'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->ministry_name;
}

function getEducationName($education_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('education_name')
                ->where('education_id', $education_id)
                ->get(config('tb_education'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->education_name;
}


function getDepartmentName($department_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('department_name')
                ->where('department_id', $department_id)
                ->get(config('tb_department'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->department_name;
}


function getBusinessTypeName($type_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('type_name')
                ->where('type_id', $type_id)
                ->get(config('tb_business_type'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->type_name;
}


function getCustomerLevelName($level_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('level_name')
                ->where('level_id', $level_id)
                ->where('level_status !=', 'deleted')
                ->get(config('tb_customer_level'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->level_name;
}


function getTaxNo($customer_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('tax_no')
                ->where('customer_id', $customer_id)
                ->where('agency_status !=', 'deleted')
                ->get(config('tb_customer_agency'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->tax_no;
}

function getReceiptNameType($customer_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('receipt_name_type')
                ->where('customer_id', $customer_id)
                ->get(config('tb_customer_address'));
    if($query->num_rows() === 0)
        return '';

    return $query->row()->receipt_name_type;
}

function getAgencyName($customer_id)
{
    $CI =& get_instance();
    #-------
    $CI->db->select('agency_name');
    $CI->db->from(config('tb_customer_agency'));
    $CI->db->where('customer_id', $customer_id);
    $CI->db->where('agency_status !=', 'deleted');
    $query = $CI->db->get();
    if($query->num_rows() === 0)
        return FAlSE;

    return $query->row()->agency_name;
}




function getCustomerAgency($customer_id)
{
    $CI =& get_instance();
    #-------
    $CI->db->select('A.agency_id,A.agency_name,A.agency_tel,A.agency_tel_ext,A.agency_fax');
    $CI->db->select('D.department_id,D.department_name,M.ministry_id,M.ministry_name');
    $CI->db->from(config('tb_customer_agency').' AS A');
    $CI->db->join(config('tb_ministry').' AS M', 'M.ministry_id = A.ministry_id', 'LEFT');
    $CI->db->join(config('tb_department').' AS D', 'D.department_id = A.department_id', 'LEFT');
    $CI->db->where('customer_id', $customer_id);
    $CI->db->where('agency_status !=', 'deleted');
    $query = $CI->db->get();
    if($query->num_rows() === 0)
        return FAlSE;

    return $query->row();
}

function getPaymentTime($event_id, $customer_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('billing_dtm')
                ->where('event_id', $event_id)
                ->where('customer_id', $customer_id)
                ->get(config('tb_invoice'));
    if($query->num_rows() === 0)
        return '';

    if(empty($query->row()->billing_dtm))
        return '';

    $timestampe  = $query->row()->billing_dtm;
    $day   = date('d', strtotime($timestampe));
    $month = date('m', strtotime($timestampe));
    $year  = date('Y', strtotime($timestampe))+543;
    return $day.'/'.$month.'/'.$year;
}

function getReceiptNo($event_id, $customer_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('receipt_no')
                ->where('event_id', $event_id)
                ->where('customer_id', $customer_id)
                ->get(config('tb_receipt'));
    if($query->num_rows() === 0)
        return '';
    return $query->row()->receipt_no;
}

function getTotalReceipt($event_id, $customer_id)
{
    $CI =& get_instance();
    $query = $CI->db->select('amount')
                ->where('event_id', $event_id)
                ->where('customer_id', $customer_id)
                ->get(config('tb_receipt'));
    if($query->num_rows() === 0)
        return '';
    return $query->row()->amount;
}



/**
 * Tools
 * *************************************************************************************************
 */

function generateNo($digit, $prefix='')
{
  return $prefix.sprintf('%03d', $digit);
}

function numberToWordsThai($num){
    $num=str_replace(",","",$num);
    $num_decimal=explode(".",$num);
    $num=$num_decimal[0];
    $returnNumWord;
    $lenNumber=strlen($num);
    $lenNumber2=$lenNumber-1;
    $kaGroup=array("","สิบ","ร้อย","พัน","หมื่น","แสน","ล้าน","สิบ","ร้อย","พัน","หมื่น","แสน","ล้าน");
    $kaDigit=array("","หนึ่ง","สอง","สาม","สี่","ห้า","หก","เจ็ต","แปด","เก้า");
    $kaDigitDecimal=array("ศูนย์","หนึ่ง","สอง","สาม","สี่","ห้า","หก","เจ็ต","แปด","เก้า");
    $ii=0;
    for($i=$lenNumber2;$i>=0;$i--){
        $kaNumWord[$i]=substr($num,$ii,1);
        $ii++;
    }
    $ii=0;
    $returnNumWord='';
    for($i=$lenNumber2;$i>=0;$i--){
        if(($kaNumWord[$i]==2 && $i==1) || ($kaNumWord[$i]==2 && $i==7)){
            $kaDigit[$kaNumWord[$i]]="ยี่";
        }else{
            if($kaNumWord[$i]==2){
                $kaDigit[$kaNumWord[$i]]="สอง";
            }
            if(($kaNumWord[$i]==1 && $i<=2 && $i==0) || ($kaNumWord[$i]==1 && $lenNumber>6 && $i==6)){
                if($kaNumWord[$i+1]==0){
                    $kaDigit[$kaNumWord[$i]]="หนึ่ง";
                }else{
                    $kaDigit[$kaNumWord[$i]]="เอ็ด";
                }
            }elseif(($kaNumWord[$i]==1 && $i<=2 && $i==1) || ($kaNumWord[$i]==1 && $lenNumber>6 && $i==7)){
                $kaDigit[$kaNumWord[$i]]="";
            }else{
                if($kaNumWord[$i]==1){
                    $kaDigit[$kaNumWord[$i]]="หนึ่ง";
                }
            }
        }
        if($kaNumWord[$i]==0){
            if($i!=6){
                $kaGroup[$i]="";
            }
        }
        $kaNumWord[$i]=substr($num,$ii,1);
        $ii++;
        $returnNumWord.=$kaDigit[$kaNumWord[$i]].$kaGroup[$i];
    }
    if(isset($num_decimal[1])){
        $returnNumWord.="จุด";
        for($i=0;$i<strlen($num_decimal[1]);$i++){
                $returnNumWord.=$kaDigitDecimal[substr($num_decimal[1],$i,1)];
        }
    }
    return $returnNumWord;
}



function ramdomString($length = 40)
{
    $characters = '0123mnqwert456789ABCZabcdefghijklyuioplkjhgfdDEFGHIJKLMNOPQRSTUVWXYsazxcvbnm';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function ramdomNumber($length = 10)
{
    return rand(pow(10, $length-1), pow(10, $length)-1);
}


function getPagination($params = array())
{
    $CI =& get_instance();
    $CI->load->library('pagination');
    //--
    $config['base_url']    = $params['base_url'];
    $config['total_rows']  = $params['total_rows'];
    $config['per_page']    = $params['per_page'];
    // $config["uri_segment"] = $params['uri_segment'];
    $config["num_links"]   = floor($config["total_rows"] / $config["per_page"]);
    $config['enable_query_strings'] = TRUE;
    $config['use_page_numbers']     = TRUE;
    $config['query_string_segment'] = 'page';
    $config['page_query_string']    = TRUE;
    //--
    $config['first_link']           = TRUE;
    $config['last_link']            = TRUE;
    $config['full_tag_open']        = "<ul class='pagination'>";
    $config['full_tag_close']       ="</ul>";
    $config['cur_tag_open']         = '<span class="curlink">';
    $config['cur_tag_close']        = '</span>';
    $config['num_tag_open']         = '<span class="numlink">';
    $config['num_tag_close']        = '</span>';

    $CI->pagination->initialize($config);
    $result = $CI->pagination->create_links();
    return  $result;
}


function genNo($prefix='', $id=1)
{
  return $prefix.sprintf('%05d',$id);
}

function formatBytes($bytes, $precision = 2) {
    $units = array('KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    switch($units[$pow])
    {
      case 'MB':
      case 'GB':
      case 'TB':
        $bytes = $bytes/1024;
    }

    return round($bytes, $precision) . ' ' . $units[$pow];
}

function generateCustomerCode($id)
{
    if(empty($id)) return FALSE;

    $prefix = 'M';
    $code = $prefix.sprintf('%05d',$id);

    $checked = checkDuplicateCustomerCode($code);
    if($checked === FALSE){
        generateCustomerCode($id);
    }else{
        $result = $code;
    }
    return $result;
}

//---@
function checkDuplicateCustomerCode($code)
{
    if(empty($code)) return FALSE;

    $CI =& get_instance();
    $CI->db->where('customer_code',$code);
    $query = $CI->db->get( config('tb_customer') );
    if($query->num_rows() > 0)
        return FALSE;

    return TRUE;
}

function getReceiptFileName($event_id, $invoice_id)
{
    if(empty($event_id) || empty($invoice_id))
        return FALSE;

    $CI =& get_instance();

    $CI->db->select('filename');
    $CI->db->where('event_id',$event_id);
    $CI->db->where('invoice_id',$invoice_id);
    $CI->db->limit(1);
    $query = $CI->db->get('event_receipts');
    if($query->num_rows() === 0)
        return FALSE;

    return $query->row()->filename;
}
/**
 * Tools
 * *************************************************************************************************
 */
