<?php
if(stristr(htmlentities($_SERVER['PHP_SELF']), "global_database_config.php")) {
   header("Location: ../");
   die();
}

//---Switch Host
switch ($_SERVER['HTTP_HOST']) {
    case 'appprompt.com':
    case 'roadsafety.appprompt.com':
        $db['default']['hostname']     = 'localhost';
        $db['default']['username']     = 'edispro_roadsafety';
        $db['default']['password']     = 'qc7IkM5S';
        $db['default']['database']     = 'edispro_roadsafety';
        break;
    case 'localhost':
        $db['default']['hostname']     = 'localhost';
        $db['default']['username']     = 'root';
        $db['default']['password']     = 'test';
        $db['default']['database']     = 'edispro_roadsafety';
        break;
    default:
        $db['default']['hostname']     = 'mariadb';
        $db['default']['username']     = 'roadsafety';
        $db['default']['password']     = 'qc7IkM5S';
        $db['default']['database']     = 'roadsafety';
}

$db['default']['dbdriver']     = 'mysqli';
$db['default']['dbprefix']     = '';
$db['default']['pconnect']     = FALSE;
$db['default']['db_debug']     = (ENVIRONMENT !== 'production');
$db['default']['cache_on']     = FALSE;
$db['default']['cachedir']     = '';
$db['default']['char_set']     = 'utf8';
$db['default']['dbcollat']     = 'utf8_general_ci';
$db['default']['swap_pre']     = '';
$db['default']['encrypt']      = FALSE;
$db['default']['compress']     = FALSE;
$db['default']['stricton']     = FALSE;
$db['default']['failover']     = array();
$db['default']['save_queries'] = TRUE;