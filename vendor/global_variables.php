<?php
if(stristr(htmlentities($_SERVER['PHP_SELF']), "global_variables.php")) {
   header("Location: ../");
   die();
}

/*
 * switch host
 */
switch ($_SERVER['HTTP_HOST']) {
    case 'appprompt.com':
    case 'roadsafety.appprompt.com':  //Develpment
        $config['base_url']    = 'http://roadsafety.appprompt.com/';
        $config['api_url']     = $config['base_url'].'api/';
        break;
    case 'localhost': //Localhost
        $config['base_url']    = 'http://localhost/edispro/roadsafety_webap/';
        $config['api_url']     = $config['base_url'].'api/';
        break;
    default:  //Production
        $config['base_url']    = 'http://trs.rvp.co.th/';
        $config['api_url']     = $config['base_url'].'api/';
}


$config['index_page']   = 'index.php';

//----@
$config['resource_url'] = $config['base_url'].'_resources/';
$config['plugin_url']   = $config['resource_url'].'plugins/';
$config['assets_url']   = $config['resource_url'].'assets/';
$config['jsapp_url']    = $config['resource_url'].'jsApps/';


#----Media folders
$config['upload_url']  = $config['base_url'].'_uploads/';

$current_path          = str_replace('\\', '/', realpath(dirname(__FILE__))) . '/';
$config['upload_dir']  = str_replace('\\', '/', realpath($current_path . '../')).'/_uploads/';

/*
 * switch host
 * For production not allow  call owner domain name
 */
switch ($_SERVER['HTTP_HOST']) {
    case 'appprompt.com':
    case 'roadsafety.appprompt.com':
        $config['inside_base_url']     = 'http://roadsafety.appprompt.com/';
        $config['inside_api_url']     =  $config['inside_base_url'].'api/';
        $config['inside_upload_url']   = $config['upload_url'];
        $config['inside_upload_dir']   = $config['upload_dir'];
        break;
    case 'localhost':
        $config['inside_base_url']     = 'http://localhost/edispro/roadsafety_webap/';
        $config['inside_api_url']     =  $config['inside_base_url'].'api/';
        $config['inside_upload_url']   = $config['upload_url'];
        $config['inside_upload_dir']   = $config['upload_dir'];
        break;
    default:
        $config['inside_base_url']     = 'http://'.$_SERVER['SERVER_ADDR'].'/';
        $config['inside_api_url']     =  $config['inside_base_url'].'api/';
        $config['inside_upload_url']   = $config['inside_base_url'].'_uploads/';
        $config['inside_upload_dir']   = $_SERVER['DOCUMENT_ROOT'].'/_uploads/';
}

//----@





//---@API Config
$config['api_title']         = 'API v0.1';
$config['api_start_date']    = date("Y-m-d H:i:s");
$config['api_expired_date']  = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($config['api_start_date'])) . " + 365 day"));
$config['api_guest_key']     = 'guest';
$config['api_page_limit']    = 15;


//--------------------------
$config['project_title'] = '';

//--@Mail
$config['mail_account_host']     = 'in.mailjet.com';
$config['mail_account_port']     = 465;
$config['mail_account_SMTPsecure'] = 'ssl'; //tls;
$config['mail_account_reply']    = 'noreply@appprompt.com';
$config['mail_account_username'] = '5749464d2c4f0bc30997192bf84b84d9';
$config['mail_account_password'] = 'af230e1db6b264d495ba4e80368e386c';


//--@Page config
$config['page_limit'] = 20;

//---@customer
$config['customer_url']         = $config['upload_url'].'customers/';
$config['customer_dir']         = $config['upload_dir'].'customers/';

//---Event
$config['event_url']    = $config['upload_url'].'events/';
$config['sponsor_url']  = $config['upload_url'].'sponsors/';

/*
|--------------------------------------------------------------------------
| Database
|--------------------------------------------------------------------------
*/
$config['tb_api']             = 'api_keys';
$config['tb_api_log']         = 'api_logs';
$config['tb_file']            = 'attach_files';

//--Master Table
$config['tb_setting']              = 'app_setting';
$config['tb_education']            = 'master_educations';
$config['tb_occupation']           = 'master_occupations';
$config['tb_business_type']        = 'master_business_types';
$config['tb_ministry']             = 'master_ministries';
$config['tb_department']           = 'master_departments';
$config['tb_food_type']            = 'master_food_types';

//--Address Table
$config['tb_geography']        = 'address_geographys';
$config['tb_privince']         = 'address_provinces';
$config['tb_district']         = 'address_districts';
$config['tb_sub_district']     = 'address_sub_districts';
$config['tb_zipcode']          = 'address_zipcodes';

//--Customer Table
$config['tb_customer']         = 'customers';
$config['tb_customer_level']   = 'customer_levels';
$config['tb_forgot']           = 'password_forgots';
$config['tb_customer_agency']  = 'customer_agencies';
$config['tb_customer_address'] = 'customer_address';

//--Event Table
$config['tb_event']            = 'events';
$config['tb_event_setting']    = 'event_settings';
$config['tb_event_register']   = 'event_registers';
$config['tb_event_sponsor']    = 'event_sponsors';
$config['tb_booth']            = 'event_booths';
$config['tb_stamp']            = 'event_stamps';

//--Event invoice
$config['tb_invoice']         = 'event_invoices';
$config['tb_invoice_item']    = 'event_invoice_items';
$config['tb_invoice_setting'] = 'event_invoice_settings';
$config['tb_event_session']   = 'event_sessions';
$config['tb_bank']            = 'event_banks';
$config['tb_receipt']         = 'event_receipts';

//--User Table
$config['tb_user']             = 'users';

$config['type'] = [
  '*'      => 'ทั้งหมด',
  'group'  => 'กลุ่ม',
  'single' => 'บุคคล',
  ''       => '-'
];

$config['status'] = [
  'approved' => '<span style="color:green">สำเร็จ</span>',
  'unpaid'   => '<span style="color:orange">รอชำระ</span>',
  'paid'     => '<span style="color:blue">รออนุมัติ</span>',
  ''         => '-'
];

$config['sex'] = [
  'female' => 'หญิง',
  'male'   => 'ชาย'
];
