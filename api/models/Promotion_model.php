<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Promotion_model extends CI_Model
{
    //--@
    public function  checkPromotionCode($event_id, $code)
    {
        $today = date('Y-m-d H:i:s');
        $query = $this->db->where('status', 'active')
                  ->where('type', 'code')
                  ->where('code', $code)
                  ->where('event_id', $event_id)
                  ->where('balance > ', 0)
                  ->where('start_datetime <=', $today)
                  ->where('end_datetime >=', $today)
                  ->limit(1)
                  ->get('event_promotions');
        if($query->num_rows() === 0)
            return FALSE;
        $result['promotion_code'] = $query->row()->code;
        $in_date = date('d/m', strtotime($query->row()->end_datetime));
        $result['message']        = sprintf(lang('promotion_discount_message'), $in_date);
        $result['discount']       = number_format($query->row()->discount, 0);
      return $result;
    }
    //--@
    public function  updatePromotionBalance($event_id, $code)
    {
      $this->db->set('balance', 'balance - 1', FALSE);
      $this->db->where('code', $code);
      $this->db->where('event_id', $event_id);
      $this->db->where('status !=', 'discard');
      $this->db->update('event_promotions');
      return TRUE;
    }

    public function updatePromotionBalanceByPeriod($event_id)
    {
      $this->db->set('balance', 'balance - 1', FALSE)
                  ->where('status', 'active')
                  ->where('stype', 'period')
                  ->where('balance > ', 0)
                  ->where('event_id', $event_id)
                  ->where('start_datetime <=', $today)
                  ->where('end_datetime >=', $today)
                  ->update('event_promotions');
      return TRUE;
    }
    //--@
    public function  checkPeriodPromotion($event_id)
    {
        $today = date('Y-m-d H:i:s');
        $query = $this->db->where('status', 'active')
                  ->where('stype', 'period')
                  ->where('balance > ', 0)
                  ->where('event_id', $event_id)
                  ->where('start_datetime <=', $today)
                  ->where('end_datetime >=', $today)
                  ->limit(1)
                  ->get('event_promotions');
        if($query->num_rows() === 0)
            return 0;

      return $query->row()->discount;
    }
}