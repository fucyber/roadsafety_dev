<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Address_model extends CI_Model
{
    //---@Get Province
    public function getProvince($geo_id = NULL)
    {
        if(isset($geo_id))
            $this->db->where('geo_id', $geo_id);
        $this->db->order_by('province_name_th', 'ASC');
        $query = $this->db->get( config('tb_privince') );
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    //---@Get Districts
    public function getDistrict($province_id = NULL)
    {
        if(empty($province_id))
             return FALSE;

        $this->db->where('province_id', $province_id);
        $this->db->order_by('district_name_th', 'ASC');
        $query = $this->db->get( config('tb_district') );
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function getSubDistrict($district_id = NULL)
    {
        if(empty($district_id))
             return FALSE;

        $this->db->where('district_id', $district_id);
        $this->db->order_by('sub_district_name', 'ASC');
        $query = $this->db->get( config('tb_sub_district') );
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    //---@Get Zipcodes
    public function getZipcode($district_code = NULL)
    {
        if(empty($district_code))
             return FALSE;

        $this->db->where('district_code', $district_code);
        $this->db->order_by('zipcode', 'ASC');
        $query = $this->db->get( config('tb_zipcode') );
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    //--------
    //---@Get Province
    public function getProvinceData($province_id)
    {
        $query = $this->db->select('province_id,province_name_th,province_name_eng,province_name_ch')
                          ->where('province_id', $province_id)
                          ->get( config('tb_privince') );
        if($query->num_rows() === 0)
            return FALSE;

       return $query->row();
    }

    //---@Get Districts
    public function getDistrictData($district_id)
    {
        $query = $this->db->select('district_id,district_name_th,district_name_eng')
                          ->where('district_id', $district_id)
                          ->get( config('tb_district') );
        if($query->num_rows() === 0)
            return FALSE;

       return $query->row();
    }

    public function getSubDistrictData($sub_district_id)
    {
        $query = $this->db->select('sub_district_id,sub_district_name')
                          ->where('sub_district_id', $sub_district_id)
                          ->get( config('tb_sub_district') );
        if($query->num_rows() === 0)
            return FALSE;

       return $query->row();
    }

    //---Get name
    public function getProvinceName($province_id)
    {
        $province = $this->getDataProvince($province_id);
        if(!$province)
            return '';
        return trim($province->province_name_th);
    }

    public function getDistrictName($district_id)
    {
        $district = $this->getDataDistrict($district_id);
        if(!$district)
            return '';
        return trim($district->district_name_th);
    }

    public function getSubDistrictName($sub_district_id)
    {
        $subDistrict = $this->getDataSubDistrict($sub_district_id);
        if(!$subDistrict)
            return '';
        return trim($subDistrict->sub_district_name);
    }


}