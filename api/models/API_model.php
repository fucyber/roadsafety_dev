<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API_model extends CI_Model
{
    private $client;

    public  function __construct()
    {
        parent::__construct();
    }

    public function get($endPoint, $input_params=array())
    {
        if(empty($endPoint))
          return FALSE;


        $tokens['token_key'] = 'guest';
        if(!empty($input_params)){
            $params = array_merge($tokens, $input_params);
        }else{
            $params = $tokens;
        }
        $service_url = $endPoint.'?'.http_build_query($params);
        //-----
        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = \Httpful\Request::get($service_url)->expectsJson()->send();

        return $response->body;
    }

    public function post($endPoint, $params=array())
    {
        if(empty($endPoint))
          return FALSE;

        $tokens['token_key'] = 'guest';
        if(!empty($params)){
            $params = array_merge($tokens, $params);
        }else{
            $params = $tokens;
        }

        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = Httpful\Request::post($endPoint)
            ->body($params, Httpful\Mime::FORM)->expectsJson()->send();
        return $response->body;
    }

}