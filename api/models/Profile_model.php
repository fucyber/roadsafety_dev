<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Profile_model extends CI_Model

{

    private $client;



    public  function __construct()

    {

        parent::__construct();

        $this->load->model('Stamp_model', 'Stamp');

    }





    public function profile_edit($customer_id, $profiles)

    {

      $this->db->where('customer_id', $customer_id);

      $this->db->update('customers', $profiles);

      return TRUE;

    }



    public function profile_event($customer_id)

    {



        $tokens['token_key'] = 'guest';

        $this->db->select('events.event_id,DATE_FORMAT(event_start_dtm, "%d/%m") as date_start,event_title');

        $this->db->from('event_registers');

        $this->db->join('events', 'events.event_id = event_registers.event_id');



        $this->db->where("event_end_dtm >=  NOW()");

        $this->db->where("event_registers.customer_id",$customer_id);

        $this->db->order_by('regis_id desc');

        $sql = $this->db->get();

        $event_curent = $sql->result_array();

        //-----------

        $this->db->select('events.event_id,DATE_FORMAT(event_start_dtm, "%d/%m") as date_start,event_title');

        $this->db->from('event_registers');

        $this->db->join('events', 'events.event_id = event_registers.event_id');



        $this->db->where("event_end_dtm <=  NOW()");

        $this->db->where("event_registers.customer_id",$customer_id);

        $this->db->order_by('regis_id desc');

        $sql = $this->db->get();

        $event_history = $sql->result_array();



        $data=array(

          'event_curent'=> $event_curent,

          'event_history'=> $event_history

        );



        return $data;



    }



    public function getCustomerEventDetail($customer_id, $event_id)

    {

        $this->db->select('events.event_id,DATE_FORMAT(event_start_dtm, "%d/%m") as date_start,DATE_FORMAT(event_end_dtm, "%d/%m") as date_end,event_title,DATE_FORMAT(event_start_dtm, "%H.%i") as time_start,DATE_FORMAT(event_end_dtm, "%H.%i") as time_end,event_venue,event_image');

        $this->db->from('event_registers');

        $this->db->join('events', 'events.event_id = event_registers.event_id');



        $this->db->where("events.event_id",$event_id);

        $this->db->where("event_registers.customer_id",$customer_id);

        $sql   = $this->db->get();

        $event = $sql->row_array();

        $point_history=array(

          '1'=>'5 คะแนน คุกกี้ hand made จำนวน 1 ถุง',

          '2'=>'10 คะแนน พวงกุญแจน้องอุ่นใจ จำนวน 1 ชิ้น',

        );

        $data_other=array(

          'point_all'=>'แต้มอีเว้นท์ 15 คะแนน',

          'point_history'=>$point_history

        );

        $data=array_merge($event,$data_other);

        return $data;

    }





    public function event_detail($customer_id, $event_id)

    {

        $this->db->select('events.event_id,DATE_FORMAT(event_start_dtm, "%d/%m") as date_start,DATE_FORMAT(event_end_dtm, "%d/%m") as date_end,event_title,DATE_FORMAT(event_start_dtm, "%H.%i") as time_start,DATE_FORMAT(event_end_dtm, "%H.%i") as time_end,event_venue,event_image');

        $this->db->from('event_registers');

        $this->db->join('events', 'events.event_id = event_registers.event_id');



        $this->db->where("events.event_id",$event_id);

        $this->db->where("event_registers.customer_id",$customer_id);

        $sql   = $this->db->get();

        if($sql->num_rows() === 0){

            return FALSE;

        }



        $redeem_histories = array();

        $redeem_transactions = $this->Stamp->getRedemmTransaction($event_id, $customer_id);

        if(!empty($redeem_transactions)){

            foreach ($redeem_transactions as $val) {

                $redeem_histories[] = 'ใช้ '.$val->redeem_point.' คะแนนแลก '.$val->name;

            }

        }





        $event_point = $this->Stamp->getCustomerBalancePoint($event_id, $customer_id);

        $result = array(

            'event_id'      => $sql->row()->event_id,

            'event_title'   => $sql->row()->event_title,

            'event_venue'   => $sql->row()->event_venue,

            'event_image'   => config('upload_url').'events/'.$sql->row()->event_image,

            'date_start'    => $sql->row()->date_start,

            'date_end'      => $sql->row()->date_end,

            'time_start'    => $sql->row()->time_start,

            'time_end'      => $sql->row()->time_end,

            'event_point'   => 'แต้มอีเว้นท์ '.$event_point.' คะแนน',

            'point_history' => $redeem_histories

            );



        return $result;



    }



    public function checkDuplicateEmailWidthOutId($customer_id, $email)

    {

        $query = $this->db->where_not_in('customer_id', $customer_id)

            ->where('customer_email', trim($email))

            ->where('customer_status !=', 'discard')

            ->get('customers');



        return $query->num_rows();

    }

}

