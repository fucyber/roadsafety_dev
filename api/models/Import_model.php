<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Import_model extends CI_Model{



    public function __construct()

    {

        parent::__construct();

        $this->load->model('Event_model', 'Event');

        $this->load->model('Customer_model', 'Customer');

        $this->load->model('Invoice_model', 'Invoice');

        $this->load->model('Barcode_model', 'Barcode');

        $this->load->model('Event_register_model', 'Register');

    }





    public function insertRegister($event_id, $registers_list=[], $params)

    {

        if(!is_array($registers_list))

            return FALSE;



        $customer_id = array();

        // ob_start();

        foreach ($registers_list as $val){

            //----

            $email = trim($val['email']);

            $this->db->select('customer_id,customer_code');

            $this->db->where('customer_email', $email);

            $this->db->where('customer_status', 'active');

            $query    = $this->db->get('customers');

            $num_rows = $query->num_rows();



            //----

            $user_data['email']           = $email;

            $user_data['name_title']      = !empty($val['name_title']) ? trim($val['name_title']) : '';

            $user_data['firstname']       = !empty($val['firstname']) ? trim($val['firstname']) : '';

            $user_data['lastname']        = !empty($val['lastname']) ? trim($val['lastname']) : '';

            $user_data['gender']          = !empty($val['gender']) ? trim($val['gender']) : '';

            $user_data['age']             = !empty($val['age']) ? trim($val['age']) : '';

            $user_data['job_description'] = !empty($val['job_description']) ? trim($val['job_description']) : '';

            $user_data['tel']             = !empty($val['tel']) ? trim($val['tel']) : '';

            $user_data['mobile']          = !empty($val['mobile']) ? trim($val['mobile']) : '';

            $user_data['occupation_id']   = !empty($val['occupation_id']) ? trim($val['occupation_id']) : '';

            $user_data['food_id']         = !empty($val['food_id']) ? trim($val['food_id']) : '';

            if($num_rows === 0){

                //--Create new customer & send login data to email

                $customer_id = $this->insertCustomerProfile($event_id, $email, $user_data);

            }else{

                $row = $query->row();

                $customer_id = $this->Register->updateCustomerProfile($row->customer_id, $user_data);

            }



            $this->insertAgency($customer_id, $params);

            $this->insertBillAddress($customer_id, $params);

            $result_ids[] = $customer_id;

        }

        return $result_ids;

        // ob_end_clean();



    }



    public function insertAgency($customer_id, $params=array())

    {

        #---

        $agency_data['customer_id']    = $customer_id ;

        $agency_data['department_id']  = $params['department_id'];

        $agency_data['ministry_id']    = $params['ministry_id'];

        $agency_data['agency_name']    = $params['agency_name'];

        $agency_data['agency_tel']     = $params['agency_tel'];

        $agency_data['agency_tel_ext'] = $params['agency_tel_ext'];

        $agency_data['agency_fax']     = $params['agency_fax'];

        $agency_data['tax_no']         = $params['tax_no'];

        //----

        $this->db->select('agency_id');

        $this->db->where('customer_id', $customer_id);

        $query = $this->db->get('customer_agencies');

        if($query->num_rows() == 0){

            $updated = $this->db->insert('customer_agencies', $agency_data);

        }else{

            $this->db->where('customer_id', $customer_id);

            $updated = $this->db->update('customer_agencies', $agency_data);

        }

        if (!$updated)

            return FALSE;



        return TRUE;

    }



    public function insertBillAddress($customer_id, $params=array())

    {

        #---

        $address_data['address_type']    = 'billing';

        $address_data['customer_id']     = $customer_id ;

        $address_data['address']         = $params['billing_address'];

        $address_data['address_soi']     = $params['billing_soi'];

        $address_data['address_road']    = $params['billing_road'];

        $address_data['province_id']     = $params['billing_province_id'];

        $address_data['district_id']     = $params['billing_district_id'];

        $address_data['sub_district_id'] = $params['billing_sub_district_id'];

        $address_data['zipcode']         = $params['billing_zipcode'];

        if(!empty($params['tax_no']))

            $address_data['tax_no']             = $params['tax_no'];

        if(!empty($params['receipt_name_type']))

            $address_data['receipt_name_type']  = $params['receipt_name_type'];



        //---

        $this->db->select('address_id');

        $this->db->where('customer_id', $customer_id);

        $query    = $this->db->get('customer_address');

        if($query->num_rows() === 0){

            $updated = $this->db->insert('customer_address', $address_data);

        }else{

            $this->db->where('customer_id', $customer_id);

            $updated = $this->db->update('customer_address', $address_data);

        }



        if(!$updated)

            return FALSE;



        return TRUE;

    }





    public function registerEvent($event_id, $registrant_id, $customer_ids=array(), $type='group', $from='mobile')

    {



        foreach ($customer_ids as $customer_id){

            $register_data['event_id']      = $event_id;

            $register_data['customer_id']   = $customer_id;

            $register_data['regis_dtm']     = date('Y-m-d H:i:s');

            $register_data['regis_from']    = $from;

            $register_data['regis_ip']      = $this->input->ip_address();

            $register_data['registrant_id'] = $registrant_id;

            $register_data['regis_type']    = $type;



            //---

            $this->db->select('regis_id');

            $this->db->where('event_id', $event_id);

            $this->db->where('customer_id', $customer_id);

            $this->db->where('status', 'active');

            $query    = $this->db->get('event_registers');

            if($query->num_rows() === 0){

                $updated = $this->db->insert('event_registers', $register_data);

            }else{

                $this->db->where('customer_id', $customer_id);

                $updated = $this->db->update('event_registers', $register_data);

            }

        }

        if(!$updated)

            return FALSE;



        return TRUE;

    }



    //----

    public function checkDuplicateInvoice($event_id, $customer_id)

    {

        $this->db->select('invoice_id');

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->where('status', 'active');

        $query = $this->db->get(config('tb_invoice'));

        return  $query->num_rows();

    }



    //--

    public function getCustomerEmail($customer_id)

    {

        $this->db->select('customer_email');

        $this->db->from(config('tb_customer'));

        $this->db->where('customer_id', $customer_id);

        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() === 0)

            return FALSE;



        return $query->row()->customer_email;

    }



    //---

    public function createInvoice($event_id, $registrant_id, $params=array())

    {



        $this->db->trans_start();



        #---Create event invoice

        $invoice_data['customer_id'] = $registrant_id;

        $invoice_data['event_id']    = $event_id;

        if(!empty($params['promotion_code']))

            $invoice_data['promotion_code'] =  $params['promotion_code'];

        $this->db->insert( config('tb_invoice') , $invoice_data);

        $invoice_id = $this->db->insert_id();



        #---Create invoice items

        $total_price = 0;

        foreach ($params['session_data'] as $key => $val) {

            $invoice_item['invoice_id'] = $invoice_id;

            $invoice_item['item_qty']   = 1;

            $invoice_item['session_id'] = $val['session_id'];

            $invoice_item['item_price'] = $val['session_price'];

            $this->db->insert( config('tb_invoice_item') , $invoice_item);

            #----

            $total_price += ($invoice_item['item_qty'] * $invoice_item['item_price']);

        }



        //--Calculate event grand total price

        $event_price = $this->getEventPrice($event_id);

        if($event_price === FALSE){

            $invoice_update['invoice_grand_totel'] = $total_price;

        }else{

            $total_register = $this->getTotalRegister($event_id, $registrant_id);

            $invoice_update['invoice_grand_totel'] = ($total_register * $event_price);

        }



        $invoice_update['invoice_ref_1']  = generateNo($invoice_id);

        switch ($params['register_type']) {

            case 'single':

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'S');

                break;

            default:

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'G');

                break;

        }

        $invoice_update['billing_id']  =  $this->getAddressIdByRegistrantUser($registrant_id);

        $invoice_update['status']  =  'active';

        $this->db->where('invoice_id',  $invoice_id)

                 ->where('event_id',  $event_id)

                 ->update( config('tb_invoice') , $invoice_update);



        //---Update register invoice id for tracking

        $this->db->set('invoice_id', $invoice_id)

                ->where('event_id', $event_id)

                ->where('registrant_id', $registrant_id)

                ->update('event_registers');



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return $invoice_id;

    }



    //---

    public function getTotalRegister($event_id, $registrant_id)

    {

        $query = $this->db->where('event_id', $event_id)

                ->where('registrant_id', $registrant_id)

                ->get('event_registers');

        return $query->num_rows();

    }



    //------

    public function getEventPrice($event_id)

    {

        $this->db->select('event_price_type, event_price');

        $this->db->where('event_id', $event_id);

        $this->db->limit(1);

        $query = $this->db->get( config('tb_event') );

        if($query->num_rows() === 0)

            return FALSE;



        $type  = $query->row()->event_price_type;

        $price = $query->row()->event_price;

        if($type === 'summary')

            return $price;



        return FALSE;

    }



    //---

    public function getAddressIdByRegistrantUser($registrant_id)

    {

        $query = $this->db->select('address_id')

                        ->where('customer_id', $registrant_id)

                        ->limit(1)

                        ->get('customer_address');

        if($query->num_rows() === 0)

            return FALSE;



        return $query->row()->address_id;

    }



    //-----------

    public function insertCustomerProfile($event_id, $email, $params)

    {

        $this->db->trans_start();

        #---Insert

        $customer_data['customer_email']       = $params['email'];

        $password = ramdomNumber(4);

        $customer_data['customer_password']    = $this->Common->encryptionPassword($password);

        $customer_data['customer_name_title']  = $params['name_title'];

        $customer_data['customer_firstname']   = $params['firstname'];

        $customer_data['customer_lastname']    = $params['lastname'];

        $customer_data['customer_gender']      = $params['gender'];

        $customer_data['customer_age']         = $params['age'];

        $customer_data['job_description']      = $params['job_description'];

        $customer_data['customer_tel']         = $params['tel'];

        $customer_data['customer_mobile']      = $params['mobile'];

        if(!empty($params['occupation_id']))

            $customer_data['occupation_id']    = $params['occupation_id'];

        if(!empty($params['food_id']))

            $customer_data['food_id']          = $params['food_id'];

        #----

        $customer_data['created_dtm']          = date('Y-m-d H:i:s');

        $customer_data['created_ip']           = $this->input->ip_address();

        $this->db->insert(config('tb_customer'), $customer_data);

        $custormer_id =   $this->db->insert_id();



        #---Update customer code

        $customer_code = $this->Customer->generateCustomerCode($custormer_id);

        $this->db->set('customer_code', $customer_code);

        $this->db->where('customer_id', $custormer_id);

        $this->db->update( config('tb_customer') );



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return $custormer_id;

    }

}