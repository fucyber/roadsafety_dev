<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Invoice_model extends CI_Model

{

    //---

    public function queryInvoice($customer_id, $event_id)

    {

        $this->db->select('E.event_title,E.event_image');

        $this->db->select('I.*');

        $this->db->select('C.customer_id,C.customer_code,C.occupation_id,C.food_id');

        $this->db->select('C.customer_name_title,C.customer_firstname,C.customer_lastname,C.customer_nickname,C.customer_email');

        $this->db->from(config('tb_invoice').' AS I');

        $this->db->join(config('tb_event').' AS E', 'E.event_id=I.event_id', 'LEFT');

        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=I.customer_id', 'LEFT');

        $this->db->where('I.customer_id', $customer_id);

        $this->db->where('I.event_id', $event_id);

        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() === 0)

            return FALSE;



        return $query->row();

    }



    //---

    public function getInvoiceTotalPrice($invoice_id)

    {

        $this->db->select('sum(item_price) AS total_price');

        $this->db->where('invoice_id', $invoice_id);

        $this->db->limit(1);

        $query = $this->db->get(config('tb_invoice_item'));

        if($query->num_rows() === 0)

            return FALSE;



        return number_format($query->row()->total_price, 0);

    }





    //---

    public function countRegisterByInvoice($event_id, $customer_id)

    {

        $this->db->select('regis_id');

        $this->db->where('event_id', $event_id);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('status', 'active');

        $query = $this->db->get(config('tb_event_register'));

        return $query->num_rows();

    }







    //---

    public function getInvoiceAddress($address_id)

    {

        $this->db->select('S.sub_district_name,D.district_name_th AS district_name,P.province_name_th AS province_name');

        $this->db->select('C.address_id,C.address,C.address_soi,C.address_road,C.zipcode,C.remark');

        $this->db->from(config('tb_customer_address').' AS C');

        $this->db->join(config('tb_privince').' AS P', 'P.province_id=C.province_id', 'LEFT');

        $this->db->join(config('tb_district').' AS D', 'D.district_id=C.district_id', 'LEFT');

        $this->db->join(config('tb_sub_district').' AS S', 'S.sub_district_id=C.sub_district_id', 'LEFT');

        $this->db->where('C.address_id', $address_id);

        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() === 0)

            return FALSE;



        return $query->row();

    }



    //---

    public function getInvoiceRegister($event_id, $registrant_id)

    {

        $this->db->select('R.customer_id');

        $this->db->select('C.customer_name_title,C.customer_firstname,C.customer_lastname');

        $this->db->from(config('tb_event_register').' AS R');

        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=R.customer_id', 'LEFT');

        $this->db->where('R.registrant_id', $registrant_id);

        $this->db->where('R.event_id', $event_id);

        $this->db->where('R.status', 'active');

        $this->db->order_by('C.customer_firstname', 'ASC');

        $query = $this->db->get();

        if($query->num_rows() === 0)

            return FALSE;



        return $query->result();

    }



    //----

    public function setSendmailStatus($event_id, $customer_id, $status)

    {

        $this->db->set('sendmail_status', $status);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->update(config('tb_invoice'));

        return TRUE;

    }



    //---

    public function queryBillAddress($bill_id)

    {

        // $this->db->select('address_id,address,address_soi,address_road,province_id,district_id,sub_district_id,zipcode,remark');

        $this->db->select('*');

        $this->db->from(config('tb_customer_address'));

        $this->db->where('address_id', $bill_id);

        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() === 0)

            return FALSE;



        return $query->row();

    }



    //---

    public function updateBillAddress($event_id, $customer_id, $bill_id)

    {

        $this->db->set('billing_id', $bill_id);

        $this->db->where('event_id', $event_id);

        $this->db->where('customer_id', $customer_id);

        $this->db->update(config('tb_invoice'));

        return TRUE;

    }



    //---

    public function getEventPrice($event_id)

    {

        $this->db->select('event_price_type,event_price');

        $this->db->where('event_id', $event_id);

        $query = $this->db->get(config('tb_event'));

        if($query->num_rows() === 0)

            return 0;



        $event = $query->row();

        if($event->event_price_type === 'summary'){

            return $event->event_price;

        }



        $this->db->select('sum(session_price) AS total_price');

        $this->db->where('event_id', $event_id);

        $query2 = $this->db->get(config('tb_event_session'));

        if($query2->num_rows() === 0)

            return 0;



        return $query2->row()->total_price;

    }





    public function createReceiptDetail($event_id, $customer_id)
    {
        $invoice = $this->queryInvoice($customer_id, $event_id);
        if(empty($invoice))
            return FALSE;

        $this->db->trans_start();

        #---Query for checked
        $this->db->select('receipt_id');
        $this->db->where('customer_id', $customer_id);
        $this->db->where('event_id', $event_id);
        $result = $this->db->get(config('tb_receipt'));

        #---
        $data['invoice_id']  = $invoice->invoice_id;
        $data['event_id']    = $event_id;
        $data['customer_id'] = $customer_id;
        $data['amount']      = $invoice->invoice_grand_totel;
        $data['reference']   = $invoice->invoice_ref_1;
        if($result->num_rows() === 0){
            $query      = $this->db->insert(config('tb_receipt'), $data);
            $receipt_id = $this->db->insert_id();
            #---Update receipt no
            $receipt_update['receipt_no'] = generateNo($receipt_id);
            $this->db->where('receipt_id', $receipt_id);
            $this->db->update( config('tb_receipt') , $receipt_update);
        }else{
            $this->db->where('receipt_id', $result->row()->receipt_id);
            $this->db->update( config('tb_receipt') , $data);
            $receipt_id = $result->row()->receipt_id;
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
            return FALSE;

        return  $receipt_id;
    }



    public function updateReceiptFilename($event_id, $customer_id, $filename='', $copy_filename='')
    {

        $this->db->set('filename', $filename);
        $this->db->set('copy_filename', $copy_filename);
        $this->db->where('event_id', $event_id);
        $this->db->where('customer_id', $customer_id);
        $this->db->update( config('tb_receipt'));
        return TRUE;
    }



    //----

    public function setReceiptSendmailStatus($receipt_id, $status=0)

    {

        $this->db->set('sendmail_status', $status);

        $this->db->where('receipt_id', $receipt_id);

        $this->db->update(config('tb_receipt'));

        return TRUE;

    }

}