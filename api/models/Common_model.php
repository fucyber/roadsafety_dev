<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Common_model extends CI_Model

{



    private $encryption_key ;



    public function __construct()

    {

        parent::__construct();

        $this->encryption_key = $this->config->item('encryption_key');

    }



    /**

     * [Set token key]

     * @return  String

     */

    public function  setTokenKey( $params = array() )

    {

        if(empty($params))

            return FALSE;



        $token_key = $this->ramdomKey();

        $data = array(

                'user_id'           => $params['data_id'],

                'user_email'        => isset($params['data_email']) ? $params['data_email'] : '',

                'user_group'        => $params['data_ref'],

                'key'               => $token_key,

                'ip_addresses'      => $this->input->ip_address(),

                'date_created'      => config('api_start_date'),

                'date_expired'      => config('api_expired_date')

                );

        $result = $this->saveApiTokenKey($data);

        if($result)

            return $token_key;

        else

            return FALSE;

    }



    //----@Save token key

    private  function  saveApiTokenKey($data = array())

    {

        if(empty($data) || !is_array($data))

            return FALSE;



        $execute = $this->db->insert(config('tb_api'), $data);

        if($execute)

            return $this->db->insert_id();

        else

            return FALSE;

    }



     /**

     * [Ramdom key by length]

     * @param   int

     * @return  String

     */

    public function ramdomKey($length = 60)

    {

        $characters = '0123XYZabcdefghijklm456789ABCDEFGHIJKLMNOPQRSTUVWfdsazxcvbnm';

        $charactersLength = strlen($characters);

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

        }

        return $randomString;

    }



    public function ramdomNumber($length = 10)

    {

        return rand(pow(10, $length-1), pow(10, $length)-1);

    }



    //--------

    public function encryptionPassword($input_password =  NULL)

    {

        if(empty($input_password))

            return FALSE;



        $salted = explode('.', $this->encryption_key);



        return md5(sha1($salted[0] . $input_password) . $salted[1]);

    }



    /**

     * Calculates the great-circle distance between two points, with

     * the Vincenty formula.

     * @param float $latitudeFrom Latitude of start point in [deg decimal]

     * @param float $longitudeFrom Longitude of start point in [deg decimal]

     * @param float $latitudeTo Latitude of target point in [deg decimal]

     * @param float $longitudeTo Longitude of target point in [deg decimal]

     * @param float $earthRadius Mean earth radius in [m]

     * @return float Distance between points in [m] (same as earthRadius)

     */

    public  function measureDistance(

                $latitudeFrom,

                $longitudeFrom,

                $latitudeTo,

                $longitudeTo,

                $earthRadius = 6371000)

    {

      // convert from degrees to radians

      $latFrom = deg2rad((float)$latitudeFrom);

      $lonFrom = deg2rad((float)$longitudeFrom);

      $latTo = deg2rad((float)$latitudeTo);

      $lonTo = deg2rad((float)$longitudeTo);



      $lonDelta = $lonTo - $lonFrom;

      $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);

      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);



      $angle = atan2(sqrt($a), $b);

      return $angle * $earthRadius;

    }



    //---return id

    public function getUserIdByTokenKey($token_key = NULL)

    {

        if(empty($token_key) || $token_key === config('api_guest_key'))

            return FALSE;



        $this->db->select('user_id,username_email');

        $this->db->where('key', $token_key);

        $query = $this->db->get( config('tb_api') );

        if($query->num_rows() === 0)

          return FALSE;



        return (int)$query->row()->user_id;

    }





    //---@

    public function genOrderNo($prefix='', $id=1)

    {

      return $prefix.sprintf('%03d',$id);

    }



    public function updatedCustomerPoint($event_id, $customer_id, $point)

    {

        //customer_point

        $query = $this->db->set('customer_point', 'customer_point + '. $point, FALSE)

                    ->where('event_id', $event_id)

                    ->where('customer_id', $customer_id)

                    ->limit(1)

                    ->update('event_registers');

        return TRUE;

    }



    public function getCustomerPoint($event_id, $customer_id)

    {

        //customer_point

        $query = $this->db->select('customer_point')

                    ->where('event_id', $event_id)

                    ->where('customer_id', $customer_id)

                    ->where('status', 'active')

                    ->limit(1)

                    ->get('event_registers');



        if($query->num_rows() === 0)

            return 0;



        return $query->row()->customer_point;

    }



    public function minusCustomerPoint($event_id, $customer_id, $point)

    {

        //customer_point

        $query = $this->db->set('customer_point', 'customer_point -'. $point, FALSE)

                    ->where('event_id', $event_id)

                    ->where('customer_id', $customer_id)

                    ->limit(1)

                    ->update('event_registers');

        return TRUE;

    }





}//End class