<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model{

    private $format_date;
    private $format_time;

    public function __construct() {
        parent::__construct();
        $this->format_date = 'd M';
        $this->format_time = 'H:i';
    }

    //---Query detail
    public function  queryEventDetail($event_id)
    {
        if(empty($event_id))
            return FALSE;

        $this->db->select('event_id,event_title,event_image,event_excerpt,event_detail');
        $this->db->select('event_venue,event_website,event_email,event_tel,event_fax');
        $this->db->select('event_start_dtm,event_end_dtm,facebook_link,survey_link,latitude,longitude,event_price');
        $this->db->select('sponsor_status,promotion_code_status');
        $this->db->from( config('tb_event') );
        $this->db->where('event_id', $event_id);
        $this->db->where('event_status', 'active');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

    public function getEventTitle($event_id)
    {
      $this->db->select('event_id,event_title');
      $this->db->from( config('tb_event') );
      $this->db->where('event_id', $event_id);
      $this->db->where('event_status', 'active');
      $this->db->limit(1);
      $query = $this->db->get();
      if($query->num_rows() === 0)
          return '';

        return $query->row()->event_title;
    }

    //---Query Sponsor
    public function  queryEventSponsor($event_id)
    {
        if(empty($event_id))
            return FALSE;

        $this->db->select('sponsor_id,sponsor_sort,sponsor_image,sponsor_name');
        $this->db->from( config('tb_event_sponsor') );
        $this->db->where('event_id', $event_id);
        //$this->db->order_by('sponsor_sort', 'ASC');
        //$this->db->order_by('sponsor_id',  'DESC');
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->result();
    }

    //---Data set
    public function  getEventSponsor($event_id)
    {
        $sponsors = $this->queryEventSponsor($event_id);
        if(!$sponsors)
            return array();

        foreach ($sponsors as $key => $val) {
            $data[] = config('sponsor_url').$val->sponsor_image;
        }
        return $data;
    }



    //--Data set
    public function getEventDetail($event_id)
    {
        $data = $this->queryEventDetail($event_id);
        if(!$data)
            return FALSE;
        //--@
        $response_data['event_id']         = $data->event_id;
        $response_data['event_image']      = !empty($data->event_image) ? config('event_url').$data->event_image : '';
        $response_data['event_title']      = $data->event_title;
        $response_data['event_excerpt']    = !empty($data->event_excerpt) ? $data->event_excerpt : '';
        $response_data['event_detail']     = !empty($data->event_detail) ? $data->event_detail : '';
        $response_data['event_venue']      = !empty($data->event_venue) ? $data->event_venue : '';
        $response_data['event_start_date'] = date($this->format_date, strtotime($data->event_start_dtm));
        $response_data['event_start_time'] = date($this->format_time, strtotime($data->event_start_dtm));
        $response_data['event_end_date']   = !empty($data->event_end_dtm) ? date($this->format_date, strtotime($data->event_end_dtm)) : '';
        $response_data['event_end_time']   = !empty($data->event_end_dtm) ? date($this->format_time, strtotime($data->event_end_dtm)) : '';
        $response_data['latitude']         = !empty($data->latitude) ? $data->latitude : '';
        $response_data['longitude']        = !empty($data->longitude) ? $data->longitude : '';
        $response_data['facebook_link']    = !empty($data->facebook_link) ? $data->facebook_link : '';
        $response_data['survey_link']      = !empty($data->survey_link) ? $data->survey_link : '';
        $response_data['sponsor_status']   = (int)$data->sponsor_status === 1 ? TRUE : FALSE;
        $response_data['sponsors']         = $this->getEventSponsor($event_id);

        $seminar_file = $this->getSeminarFile($event_id);
        $response_data['seminar_file']     = !empty($seminar_file) ? $seminar_file : '';
        //--Event menu status
        $response_data['seminar_file_status']  = !empty($seminar_file) ? TRUE : FALSE;
        $response_data['document_status']      = $this->getDocuemntStatus($event_id);
        $response_data['booth_status']         = $this->getBoothStatus($event_id);
        $response_data['video_status']         = $this->getVideoStatus($event_id);
        $response_data['point_status']         = $this->getPointStatus($event_id);
        $response_data['register_satus']       = $this->getRegisterStatus($data->event_end_dtm);
        //---
        $register_data  = $this->register_data($event_id, $data->promotion_code_status, $data->event_price);
        $response_data['register_data']    = !empty($register_data) ? $register_data : '';

        return $response_data;
    }

    //----Example fixed data
    public function register_data($event_id, $promotion_status, $event_price='')
    {
        if(empty($event_id))  return FALSE;

        #------
        $description = invoice_setting($event_id, 'invoice_title');
        $session_day = array(
            'subject'       => invoice_setting($event_id, 'event_register_description'),
            'event_price'   => !empty($event_price) ? $event_price : '',
            'session_list'  => $this->getSessionDay($event_id)
        );
        #---
        $response_data['promotion_code_status']   = (int)$promotion_status === 1 ? TRUE : FALSE;
        $response_data['descriptions'] = $description;
        $response_data['session']      = $session_day;
        return $response_data;
    }

    public function getSessionDay($event_id)
    {
        $this->db->select('session_id,session_title,session_price');
        $this->db->where('session_status', 'active');
        $this->db->where('event_id', $event_id);
        $this->db->order_by('session_sort', 'ASC');
        $this->db->order_by('session_id', 'ASC');
        $query = $this->db->get( config('tb_event_session') );

        if($query->num_rows() === 0)
            return null;

        $session_list =  $query->result();
        foreach ($session_list as $key => $data) {
            $response[$key]['session_id'] = $data->session_id;
            $response[$key]['session_title']  = $data->session_title;
            $response[$key]['session_price']  = $data->session_price;
        }
        return $response;
    }


    //---Query
    public function queryEventList($params=array())
    {
        (int)$limit  = !empty($params['limit']) ? $params['limit'] : config('api_page_limit');
        (int)$offset = !empty($params['offset']) ? $params['offset'] : 0;

        $this->db->select('event_id,event_title,event_image,event_excerpt');
        $this->db->select('event_venue,event_website,event_email,event_tel,event_fax');
        $this->db->select('event_start_dtm,event_end_dtm,facebook_link,latitude,longitude');
        $this->db->select('sponsor_status,promotion_code_status');
        $this->db->from( config('tb_event') );
        //---Event coming or history
        switch ($params['action']) {
            case 'history':
                $this->db->where('event_start_dtm <', date('Y-m-d 00:00:00'));
                break;
            default:
                $this->db->where('event_start_dtm >=', date('Y-m-d 00:00:00'));
                break;
        }
        $this->db->limit($limit, $offset);
        $this->db->order_by('event_sort', 'ASC');
        $this->db->order_by('event_start_dtm', 'ASC');
        $this->db->order_by('event_id', 'DESC');
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

       return $query->result();
    }


    //--Data set
    public function getEventList($params=array())
    {
        $event_list = $this->queryEventList($params);
        if(!$event_list) return FALSE;


        foreach ($event_list as $key => $data) {
            $response_data[$key]['event_id']         = $data->event_id;
            $response_data[$key]['event_image']      = !empty($data->event_image) ? config('event_url').$data->event_image : '';
            $response_data[$key]['event_title']      = $data->event_title;
            $response_data[$key]['event_excerpt']    = !empty($data->event_excerpt) ? $data->event_excerpt : '';
            $response_data[$key]['event_venue']      = !empty($data->event_venue) ? $data->event_venue : '';
            $response_data[$key]['event_start_date'] = date($this->format_date, strtotime($data->event_start_dtm));
            $response_data[$key]['event_start_time'] = date($this->format_time, strtotime($data->event_start_dtm));
            $response_data[$key]['event_end_date']   = !empty($data->event_end_dtm) ? date($this->format_date, strtotime($data->event_end_dtm)) : '';
            $response_data[$key]['event_end_time']   = !empty($data->event_end_dtm) ? date($this->format_time, strtotime($data->event_end_dtm)) : '';
            $response_data[$key]['latitude']         = !empty($data->latitude) ? $data->latitude : '';
            $response_data[$key]['longitude']        = !empty($data->longitude) ? $data->longitude : '';
            $response_data[$key]['facebook_link']    = !empty($data->facebook_link) ? $data->facebook_link : '';

            //--
            $sponsor_list = $this->getEventSponsor($data->event_id);
            $response_data[$key]['sponsor_status']   = (int)$data->sponsor_status === 1 && !empty($sponsor_list) ? TRUE : FALSE;
            $response_data[$key]['sponsors']         = $sponsor_list;
        }

        return $response_data;
    }


    //---Query
    public function getDocuemntDescription($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('type', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('event_attach_types');
        if($query->num_rows() === 0)
            return FALSE;

       return $query->row()->description;
    }

    //---Query
    public function getDocuemntType($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('type', 'file_type');
        $query = $this->db->get('event_attach_types');
        if($query->num_rows() === 0)
            return FALSE;

       $results =  $query->result();
       $responses = [];
       foreach($results as $key => $val){
         $responses[$key]['group_name'] = $val->title;
         $responses[$key]['files'] = $this->getDocuemntFiles($val->id);

       }
       return $responses;
    }

    //---Query
    public function getDocuemntFiles($ref_id)
    {
        $this->db->where('ref_id', $ref_id);
        $this->db->where('type', 'event');
        $this->db->order_by('sort', 'ACS');
        $this->db->order_by('file_id', 'DESC');
        $query = $this->db->get('attach_files');
        if($query->num_rows() === 0)
            return FALSE;

        $results =  $query->result();
        $responses = [];
        foreach($results as $key => $val){
          $responses[$key]['file_title'] = $val->file_title;
          $responses[$key]['file_name'] = config('upload_url').'documents/'.$val->file_name;
          $responses[$key]['file_size'] = formatBytes($val->file_size);
          $responses[$key]['created_at'] = date('d-m-Y H:i:s', strtotime($val->created_at));
        }
        return $responses;
    }

    public function getSeminarFile($event_id)
    {
      //event_seminar
      $this->db->where('ref_id', $event_id);
      $this->db->where('type', 'event_seminar');
      $this->db->limit(1);
      $query = $this->db->get('attach_files');
      if($query->num_rows() === 0)
          return '';

      return config('upload_url').'documents/'.$query->row()->file_name;
    }


    public function vdo_list($event_id)
    {

        $this->db->select('*');
        $this->db->from('event_video');
        $this->db->where('event_id', $event_id);
        $this->db->order_by('created_at','desc');
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        $data = array();
        foreach ($query->result() as $key => $val) {
            $data[$key]['id'] = $val->id;
            $data[$key]['title'] = $val->title;
            $data[$key]['video_link'] = $val->video_link;
            $data[$key]['description'] = $val->description;
            $data[$key]['created_at'] = date('d M y', strtotime($val->created_at));

            $data[$key]['image'] = !empty($val->image) ? config('upload_url').'booths/'.$val->image : config('upload_url').'road_video_thumbnail.jpg';
        }
        return $data;
    }

    public function getDocuemntStatus($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('type', 'file_type');
        $query = $this->db->get('event_attach_types');
        if($query->num_rows() === 0)
            return FALSE;
        return TRUE;
    }

    public function getVideoStatus($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('status', 'active');
        $query = $this->db->get('event_video');
        if($query->num_rows() === 0)
            return FALSE;
        return TRUE;
    }

    public function getBoothStatus($event_id)
    {
        $this->db->where('event_id', $event_id);
        $this->db->where('booth_status', 'active');
        $query = $this->db->get('booths');
        if($query->num_rows() === 0)
            return FALSE;
        return TRUE;
    }

    public function getPointStatus($event_id)
    {
        return $this->getBoothStatus($event_id);
    }

    public function getRegisterStatus($date='')
    {
        $event_date = strtotime($date);
        $today      = strtotime(date('Y-m-d H:i:s'));
        if($event_date > $today)
            return TRUE;
        else
            return FALSE;
    }
}
