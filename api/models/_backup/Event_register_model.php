<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Event_register_model extends CI_Model{



    public function __construct() {

        parent::__construct();

        $this->load->model('Event_model', 'Event');

        $this->load->model('Customer_model', 'Customer');

        $this->load->model('Invoice_model', 'Invoice');

        $this->load->model('Barcode_model', 'Barcode');

        $this->load->model('Sendmail_model', 'Sendmail');

        $this->load->model('Promotion_model', 'Promotion');



    }



    public function checkRegisterRedundant($customer_id, $event_id)

    {

        $this->db->select('regis_id');

        $this->db->where('event_id', $event_id);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('status !=', 'discard');

        $query = $this->db->get( config('tb_event_register') );

        return $query->num_rows();

    }





    //-----------

    public function insertCustomerProfile($event_id, $email, $params)

    {

        $this->db->trans_start();

        #---Insert

        $customer_data['customer_email']       = $params['email'];

        $password  = $this->__generatePassword($event_id, $email, $params);

        $customer_data['customer_password']    = $this->Common->encryptionPassword($password);

        $customer_data['customer_name_title']  = $params['name_title'];

        $customer_data['customer_firstname']   = $params['firstname'];

        $customer_data['customer_lastname']    = $params['lastname'];

        $customer_data['customer_gender']      = $params['gender'];

        $customer_data['customer_age']         = $params['age'];

        $customer_data['job_description']      = $params['job_description'];

        $customer_data['customer_tel']         = $params['tel'];

        $customer_data['customer_mobile']      = $params['mobile'];

        if(!empty($params['occupation_id']))

            $customer_data['occupation_id']    = $params['occupation_id'];

        if(!empty($params['food_id']))

            $customer_data['food_id']          = $params['food_id'];

        #----

        $customer_data['created_dtm']          = date('Y-m-d H:i:s');

        $customer_data['created_ip']           = $this->input->ip_address();

        $this->db->insert(config('tb_customer'), $customer_data);

        $custormer_id =   $this->db->insert_id();



        #---Update customer code

        $customer_code = $this->Customer->generateCustomerCode($custormer_id);

        $this->db->set('customer_code', $customer_code);

        $this->db->where('customer_id', $custormer_id);

        $this->db->update( config('tb_customer') );



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return $custormer_id;

    }



    //----

    private function __generatePassword($event_id, $email, $params)

    {

        $password = ramdomNumber(4);

        //---

        $params['fullname'] = $params['firstname'].' '.$params['lastname'];

        $params['username'] = $email;

        $params['password']   = $password ;

        $params['mailt_title'] = invoice_setting($event_id, 'invoice_title');

        $result = $this->Sendmail->resgier_login_info($email, $params);

        return !$result ?  FALSE : $password;

    }



    //-----------

    public function updateCustomerProfile($customer_id, $params)

    {

        #---

        $customer_data['customer_email']       = $params['email'];

        $customer_data['customer_name_title']  = $params['name_title'];

        $customer_data['customer_firstname']   = $params['firstname'];

        $customer_data['customer_lastname']    = $params['lastname'];

        $customer_data['customer_gender']      = $params['gender'];

        $customer_data['customer_age']         = $params['age'];

        $customer_data['job_description']      = $params['job_description'];

        $customer_data['customer_tel']         = $params['tel'];

        $customer_data['customer_mobile']      = $params['mobile'];

        if(!empty($params['occupation_id']))

            $customer_data['occupation_id']    = $params['occupation_id'];

        if(!empty($params['food_id']))

            $customer_data['food_id']          = $params['food_id'];

        #----

        $customer_data['created_dtm']          = date('Y-m-d H:i:s');

        $customer_data['created_ip']           = $this->input->ip_address();

        $this->db->where('customer_id', $customer_id);

        $this->db->update(config('tb_customer'), $customer_data);

        return $customer_id;

    }



    //-----------

    public function insertAffiliatedAgency($customer_id, $params)

    {

        #---Insert customer_affiliated_agencies

        $agency_data['customer_id']    = $customer_id ;

        $agency_data['department_id']  = $params['department_id'];

        $agency_data['ministry_id']    = $params['ministry_id'];

        $agency_data['agency_name']    = $params['agency_name'];

        $agency_data['agency_tel']     = $params['agency_tel'];

        $agency_data['agency_tel_ext'] = $params['agency_tel_ext'];

        $agency_data['agency_fax']     = $params['agency_fax'];

        $agency_data['tax_no']         = $params['tax_no'];

        $query = $this->db->insert(config('tb_customer_agency'), $agency_data);

        if ($query === FALSE)

            return FALSE;



        return TRUE;

    }



    //-----------

    public function updateAffiliatedAgency($customer_id, $params)

    {

        #---Insert customer_affiliated_agencies

        $agency_data['department_id']  = $params['department_id'];

        $agency_data['ministry_id']    = $params['ministry_id'];

        $agency_data['agency_name']    = $params['agency_name'];

        $agency_data['agency_tel']     = $params['agency_tel'];

        $agency_data['agency_tel_ext'] = $params['agency_tel_ext'];

        $agency_data['agency_fax']     = $params['agency_fax'];

        $agency_data['tax_no']         = $params['tax_no'];

        $this->db->where('customer_id', $customer_id);

        $this->db->update(config('tb_customer_agency'), $agency_data);

        return TRUE;

    }



    //-----------

    public function insertBillingAddress($customer_id, $params)

    {

        #---Insert billing address

        $address_data['address_type']    = 'billing';

        $address_data['customer_id']     = $customer_id ;

        $address_data['address']         = $params['billing_address'];

        $address_data['address_soi']     = $params['billing_soi'];

        $address_data['address_road']    = $params['billing_road'];

        $address_data['province_id']     = $params['billing_province_id'];

        $address_data['district_id']     = $params['billing_district_id'];

        $address_data['sub_district_id'] = $params['billing_sub_district_id'];

        $address_data['zipcode']         = $params['billing_zipcode'];

        if(!empty($params['tax_no']))

            $address_data['tax_no']             = $params['tax_no'];

        if(!empty($params['receipt_name_type']))

            $address_data['receipt_name_type']  = $params['receipt_name_type'];

        $query = $this->db->insert(config('tb_customer_address'), $address_data);

        if(!$query)

            return FALSE;

        return $this->db->insert_id();

    }



    //-----------

    public function updateBillingAddress($customer_id, $params)

    {

        $address_data['address']         = $params['billing_address'];

        $address_data['address_soi']     = $params['billing_soi'];

        $address_data['address_road']    = $params['billing_road'];

        $address_data['province_id']     = $params['billing_province_id'];

        $address_data['district_id']     = $params['billing_district_id'];

        $address_data['sub_district_id'] = $params['billing_sub_district_id'];

        $address_data['zipcode']         = $params['billing_zipcode'];

        if(!empty($params['tax_no']))

            $address_data['tax_no']         = $params['tax_no'];

        if(!empty($params['receipt_name_type']))

            $address_data['receipt_name_type']  = $params['receipt_name_type'];

        $this->db->where('customer_id', $customer_id);

        $this->db->update(config('tb_customer_address'), $address_data);

        return TRUE;

    }



    //---

    public function  saveEventParticipant($registrant_id, $customer_id, $event_id, $params=array())

    {

        if(empty($registrant_id) || empty($customer_id)

            || empty($event_id) || empty($params))

            return FALSE;



        $this->db->trans_start();

        #---Save event register

        $register_data['event_id']      = $event_id;

        $register_data['customer_id']   = $customer_id;

        $register_data['regis_dtm']     = date('Y-m-d H:i:s');

        $register_data['regis_from']    = 'mobile';

        $register_data['regis_ip']      = $this->input->ip_address();

        $register_data['registrant_id'] = $registrant_id;

        $query_register = $this->db->insert(config('tb_event_register'), $register_data);

        $register_id    = $this->db->insert_id();



        #---Create event invoice

        $discount = 0;

        $invoice_data['customer_id'] = $customer_id;

        $invoice_data['invoice_created_at'] = date('Y-m-d H:i:s');

        $invoice_data['invoice_updated_at'] = date('Y-m-d H:i:s');

        $invoice_data['event_id']    = $event_id;

        if(!empty($params['promotion_code'])){

            $invoice_data['promotion_code'] = $params['promotion_code'];

            $check_discount = $this->Promotion->checkPromotionCode(trim($params['promotion_code']));

            if(is_array($check_discount)){

                $discount = $check_discount['discount'];

                $this->Promotion->updatePromotionBalance($invoice_update['promotion_code']);

            }

        }

        $this->db->insert( config('tb_invoice') , $invoice_data);

        $invoice_id = $this->db->insert_id();



        #---Create invoice items

        $items = json_decode($params['session_data'], TRUE); //array

        $total_price = 0;

        foreach ($items as $key => $val) {

            $invoice_item['invoice_id'] = $invoice_id;

            $invoice_item['item_qty']   = 1;

            $invoice_item['session_id'] = $val['session_id'];

            $invoice_item['item_price'] = $val['session_price'];

            $this->db->insert( config('tb_invoice_item') , $invoice_item);

            #----

            $total_price += ( $invoice_item['item_qty'] * $invoice_item['item_price'] );

        }



        //--Update grand total price

        $get_event_price = $this->getEventPrice($event_id);

        if($get_event_price > 0){

            $invoice_update['invoice_grand_totel'] = $get_event_price - $discount;

        }else{

            $invoice_update['invoice_grand_totel'] = $total_price - $discount;

        }

        $invoice_update['invoice_ref_1'] = generateNo($invoice_id);

        $invoice_update['status']       = 'active';

        switch ($params['register_type']) {

            case 'single':

                $invoice_update['invoice_ref_2']    = generateNo($invoice_id, 'S');

                break;

            default:

                $invoice_update['invoice_ref_2']    = generateNo($invoice_id, 'G');

                break;

        }

        $invoice_update['promotion_discount'] = $discount;

        $this->db->where('invoice_id', $invoice_id);

        $this->db->update( config('tb_invoice') , $invoice_update);



        //---Update event to table [registers]

        $this->db->set('regis_type', $params['register_type']);

        $this->db->set('invoice_id', $invoice_id);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->update(config('tb_event_register'));



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return TRUE;

    }





    //------

    public function getEventPrice($event_id)

    {

        $this->db->select('event_price_type, event_price');

        $this->db->where('event_id', $event_id);

        $this->db->limit(1);

        $query = $this->db->get( config('tb_event') );

        if($query->num_rows() === 0)

            return FALSE;



        $type  = $query->row()->event_price_type;

        $price = $query->row()->event_price;

        if($type === 'summary')

            return $price;



        return 0;

    }





    //------

    public function getTotalSessionPrice($event_id)

    {

        $this->db->select('sum(session_price) as total_price');

        $this->db->where('event_id', $event_id);

        $this->db->where('session_status !=', 'deleted');

        $query = $this->db->get( config('tb_event_session') );

        if($query->num_rows() === 0)

            return 0;



        return $query->row()->total_price;

    }



    public function groupChildProgress($event_id, $customer_id)

    {

        //--

        $registerResult = $this->db->select('invoice_id,registrant_id,customer_id')

                        ->where('customer_id', $customer_id)

                        ->where('event_id', $event_id)

                        ->where('status', 'active')

                        ->limit(1)

                        ->get(config('tb_event_register'));

        if($registerResult->num_rows() === 0){

            return FALSE;

        }



        $registerData  = $registerResult->row();

        $registrant_id = $registerData->registrant_id;

        if($registerData->customer_id === $registerData->registrant_id){

            return FALSE;

        }

        #-------

        $result['step_one'] = array(

                        'status' =>  TRUE,

                        'data'   =>  array('rester_type'=>'group')

                    );

        $result['step_two'] = array(

                        'status' =>  TRUE,

                        'data'   =>  array()

                    );

        $result['step_three'] = array(

                        'status' =>  TRUE,

                        'data'   =>  array('payment_status'=> 'paid')

                    );

        $step_four = $this->__userPaymentStatus($registrant_id, $event_id);

        $step_four_status = (!$step_four) ? FALSE : TRUE;

        $result['step_four'] = array(

                        'status' =>  !$step_four_status || $step_four['payment_status'] !== 'approved' ? FALSE : TRUE,

                        'data'   =>  $step_four_status !== FALSE ? $step_four : array()

                    );

        $step_five = $this->__userChildCard($event_id, $registrant_id, $customer_id);

        $result['step_five'] = array(

                        'status' =>  ($step_five['payment_status'] !== 'approved') ? FALSE : TRUE,

                        'data'   =>  $step_five !== FALSE ? $step_five : array()

                    );

        return $result;

    }



    //---

    public function checkRegisterProgress($event_id, $customer_id)

    {

        $child_result = $this->groupChildProgress($event_id, $customer_id);

        if($child_result){

            return $child_result;

            return;

        }

        $step_one = $this->__userRegisterData($event_id, $customer_id);

        $step_one_status = (!empty($step_one['profile']) && !empty($step_one['agency']) && !empty($step_one['billing'])) ? TRUE : FALSE;

        //$step_one_status = (!empty($step_one['profile'])) ? TRUE : FALSE;

        $result['step_one'] = array(

                        'status' =>  $step_one_status,

                        'data'   =>  $step_one !== FALSE ? $step_one : array()

                    );

        $step_two   =  $this->__userPayInSlipData($customer_id, $event_id);

        $step_two_status  = (!$step_one_status  ||  !$step_two) ? FALSE : TRUE;

        $result['step_two'] = array(

                        'status' =>  $step_two_status,

                        'data'   =>  $step_two_status !== FALSE ? $step_two : array()

                    );

        $step_three = $this->__userPaymentData($customer_id, $event_id);

        $step_three_status = (!$step_one_status  ||  !$step_two_status || !$step_three) ? FALSE : TRUE;

        $result['step_three'] = array(

                        'status' =>  $step_three_status,

                        'data'   =>  $step_three_status !== FALSE ? $step_three : array()

                    );

        $step_four = $this->__userPaymentStatus($customer_id, $event_id);

        $step_four_status = (!$step_one_status  ||  !$step_two_status || !$step_three_status || !$step_four) ? FALSE : TRUE;

        $result['step_four'] = array(

                        'status' =>  (!$step_four_status || $step_four['payment_status'] !== 'approved') ? FALSE : TRUE,

                        'data'   =>  $step_four_status !== FALSE ? $step_four : array()

                    );

        $step_five = $this->__userEventCard($customer_id, $event_id);

        $step_five_status = (!$step_one_status  ||  !$step_two_status || !$step_three_status || !$step_four_status || !$step_five) ? FALSE : TRUE;

        $result['step_five'] = array(

                        'status' =>  $step_five_status,

                        'data'   =>  $step_five_status !== FALSE ? $step_five : array()

                    );

        return $result;

    }



    //------

    private function __userRegisterData($event_id, $customer_id)

    {

        $profile = $this->Customer->queryProfile($customer_id);

        $register = $this->getRegisterDataByCustomerId($event_id, $customer_id);

        $response_data['rester_type'] = $this->getRegisterType($event_id, $customer_id);

        $response_data['promotion_code'] = $this->getPromotionCode($event_id, $customer_id);

        $response_data['profile']  = array(

                "regis_id"            => !$register ? '' : $register->regis_id,

                //"regis_dtm"           => !$register ? '' : $register->regis_dtm,

                "customer_id"         => !empty($profile->customer_id) ? $profile->customer_id : '',

                "customer_code"       => !empty($profile->customer_code) ? $profile->customer_code : '',

                "customer_email"      => !empty($profile->customer_email) ? $profile->customer_email : '',

                "customer_name_title" => !empty($profile->customer_name_title) ? $profile->customer_name_title : '',

                "customer_firstname"  => !empty($profile->customer_firstname) ? $profile->customer_firstname : '',

                "customer_lastname"   => !empty($profile->customer_lastname) ? $profile->customer_lastname : '',

                //"customer_nickname"   => !empty($profile->customer_nickname) ? $profile->customer_nickname : '',

                "customer_gender"     => !empty($profile->customer_gender) ? $profile->customer_gender : '',

                "customer_age"        => !empty($profile->customer_age) ? $profile->customer_age : '',

                "occupation_id"       => !empty($profile->occupation_id) ? $profile->occupation_id : '',

                "job_description"     => !empty($profile->job_description) ? $profile->job_description : '',

                "customer_mobile"     => !empty($profile->customer_mobile) ? $profile->customer_mobile : '',

                "food_id"             => !empty($profile->food_id) ? $profile->food_id : '',

                );

        if($response_data['rester_type'] === 'single'){

            $response_data['group_list'] = array();

        }else{

            $response_data['group_list'] = $this->getGroupParticipant($event_id, $customer_id);

        }

        $response_data['agency']     = $this->getAgencyByCustomerId($customer_id);

        $response_data['billing']    = $this->getBillAddressByCustomerId($customer_id);

        return $response_data;

    }



    public function getPromotionCode($event_id, $customer_id)

    {

        $query = $this->db->select('promotion_code')

                            ->where('event_id', $event_id)

                            ->where('customer_id', $customer_id)

                            ->limit(1)->get('event_invoices');

        if($query->num_rows() === 0)

            return '';

        return !empty($query->row()->promotion_code) ? $query->row()->promotion_code : '';

    }



    public function getRegisterDataByCustomerId($event_id, $customer_id)

    {

        $query = $this->db->where('event_id', $event_id)

            ->where('customer_id', $customer_id)

            ->where('status', 'active')

            ->limit(1)

            ->get('event_registers');

        if($query->num_rows() === 0)

            return FALSE;

        return $query->row();

    }



    private function getAgencyByCustomerId($customer_id)

    {

        $query = $this->db->where('customer_id', $customer_id)->get('customer_agencies');

        if($query->num_rows() === 0)

            return array();



        return array(

                    "agency_id"      => $query->row()->agency_id,

                    "department_id"  => $query->row()->department_id,

                    "ministry_id"    => $query->row()->ministry_id,

                    "agency_name"    => $query->row()->agency_name,

                    "agency_tel"     => $query->row()->agency_tel,

                    "agency_tel_ext" => $query->row()->agency_tel_ext,

                    "agency_fax"     => $query->row()->agency_fax,

                    "tax_no"         => $query->row()->tax_no

                );

    }



    private function getBillAddressByCustomerId($customer_id)

    {

        $query = $this->db->where('customer_id', $customer_id)->get('customer_address');

        if($query->num_rows() === 0)

            return array();



        $bill = $query->row();

        return array(

                "address_id"      => $bill->address_id,

                "address"         => !empty($bill->address) ? $bill->address : '',

                "address_soi"     => !empty($bill->address_soi) ? $bill->address_soi : '',

                "address_road"    => !empty($bill->address_road) ? $bill->address_road : '',

                "province_id"     => !empty($bill->province_id) ? $bill->province_id : '',

                "district_id"     => !empty($bill->district_id) ? $bill->district_id : '',

                "sub_district_id" => !empty($bill->sub_district_id) ? $bill->sub_district_id : '',

                "zipcode"         => !empty($bill->zipcode) ? $bill->zipcode : '',

                "remark"          => !empty($bill->remark) ? $bill->remark : '',

                "receipt_name_type" => !empty($bill->receipt_name_type) ? $bill->receipt_name_type : ''

            );

    }

    //------

    private function __userRegisterData_back($event_id, $customer_id)

    {

        $this->db->select('invoice_id,customer_id,billing_id,event_id');

        $this->db->from(config('tb_invoice'));

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->limit(1);

        $query_invoice = $this->db->get();

        if($query_invoice->num_rows() === 0)

            return FALSE;



        $invoice = $query_invoice->row();

        $profile = $this->Customer->queryProfile($invoice->customer_id);



        $response_data['rester_type'] = $this->getRegisterType($invoice->event_id, $invoice->customer_id);

        $response_data['profile']  = array(

                "regis_id"            => !empty($profile->regis_id) ? $profile->regis_id : '',

                //"regis_dtm"           => !empty($profile->regis_dtm) ? $profile->regis_dtm : '',

                "customer_id"         => !empty($profile->customer_id) ? $profile->customer_id : '',

                "customer_code"       => !empty($profile->customer_code) ? $profile->customer_code : '',

                "customer_email"      => !empty($profile->customer_email) ? $profile->customer_email : '',

                "customer_name_title" => !empty($profile->customer_name_title) ? $profile->customer_name_title : '',

                "customer_firstname"  => !empty($profile->customer_firstname) ? $profile->customer_firstname : '',

                "customer_lastname"   => !empty($profile->customer_lastname) ? $profile->customer_lastname : '',

                "customer_nickname"   => !empty($profile->customer_nickname) ? $profile->customer_nickname : '',

                "customer_gender"     => !empty($profile->customer_gender) ? $profile->customer_gender : '',

                "customer_age"        => !empty($profile->customer_age) ? $profile->customer_age : '',

                "occupation_id"       => !empty($profile->occupation_id) ? $profile->occupation_id : '',

                "job_description"     => !empty($profile->job_description) ? $profile->job_description : '',

                "customer_mobile"     => !empty($profile->customer_mobile) ? $profile->customer_mobile : '',

                "food_id"             => !empty($profile->food_id) ? $profile->food_id : '',

                );

        if(!empty($profile->agency_id)){

            $response_data['agency']  = array(

                    "agency_id"      => !empty($profile->agency_id) ? $profile->agency_id : '',

                    "department_id"  => !empty($profile->department_id) ? $profile->department_id : '',

                    "ministry_id"    => !empty($profile->ministry_id) ? $profile->ministry_id : '',

                    "agency_name"    => !empty($profile->agency_name) ? $profile->agency_name : '',

                    "agency_tel"     => !empty($profile->agency_tel) ? $profile->agency_tel : '',

                    "agency_tel_ext" => !empty($profile->agency_tel_ext) ? $profile->agency_tel_ext : '',

                    "agency_fax"     => !empty($profile->agency_fax) ? $profile->agency_fax : '',

                    "tax_no"         => !empty($profile->tax_no) ? $profile->tax_no : '',

                );

        }else{

            $response_data['agency'] = array();

        }





        $response_data['billing']    = $this->queryBillingAddress($invoice->billing_id);

        $response_data['group_list'] = $this->getGroupParticipant($event_id, $customer_id);



        return $response_data;

    }





    //----

    public function queryBillingAddress($bill_id)

    {

        $bill = $this->Invoice->queryBillAddress($bill_id);

        if(empty($bill))

            return array();



        return array(

                "address_id"      => $bill->address_id,

                "address"         => !empty($bill->address) ? $bill->address : '',

                "address_soi"     => !empty($bill->address_soi) ? $bill->address_soi : '',

                "address_road"    => !empty($bill->address_road) ? $bill->address_road : '',

                "province_id"     => !empty($bill->province_id) ? $bill->province_id : '',

                "district_id"     => !empty($bill->district_id) ? $bill->district_id : '',

                "sub_district_id" => !empty($bill->sub_district_id) ? $bill->sub_district_id : '',

                "zipcode"         => !empty($bill->zipcode) ? $bill->zipcode : '',

                "remark"          => !empty($bill->remark) ? $bill->remark : '',

                "receipt_name_type" => !empty($bill->receipt_name_type) ? $bill->receipt_name_type : ''

            );



    }



    //---

    public function uploadSlip($customer_id, $event_id, $params)

    {



        if(empty($customer_id) || empty($event_id) || empty($params['data']))

            return FALSE;



        //---

        $encoded_string = $params['data'];

        $date_time      = !empty($params['date_time']) ? $params['date_time'] : NULL;

        $bank_id        = !empty($params['bank_id']) ? $params['bank_id'] : NULL;



        ob_start();

        $imgdata        = base64_decode($encoded_string);

        $file           = finfo_open();

        $mime_type      = finfo_buffer($file, $imgdata, FILEINFO_MIME_TYPE);

        $ext            = explode('/', $mime_type);

        $fileName       = $event_id.'_'.$customer_id.'_'.date('Ymd_His').'.'.$ext[1];

        $saveFile       = config('upload_dir').'slips/'.$fileName;

        //--

        $fullFileString    = 'data:'.$mime_type.';base64,'.$encoded_string;

        $source      = fopen($fullFileString, 'r');

        $destination = fopen($saveFile, 'w');

        stream_copy_to_stream($source, $destination);

        fclose($source);

        fclose($destination);

        ob_end_clean();



        if(file_exists($saveFile) !== TRUE)

            return FALSE;



        //---

        if(!empty($bank_id))

            $this->db->set('payment_bank_id', $bank_id);

        if(!empty($date_time))

            $this->db->set('payment_dtm', $date_time);

        $this->db->set('payment_status', 'paid');

        $this->db->set('payment_slip', $fileName);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->update(config('tb_invoice'));

        return TRUE;



    }



    //---Step two

    private function __userPayInSlipData($customer_id, $event_id)

    {

        $data = $this->Invoice->queryInvoice($customer_id, $event_id);

        if(!$data)

            return FALSE;



        $response_data['organizer_logo']    = config('upload_url').'event_settings/'.invoice_setting($event_id, 'organizer_logo');

        $response_data['organizer_name']    = invoice_setting($event_id, 'organizer_name');

        $response_data['organizer_address'] = invoice_setting($event_id, 'organizer_address');

        $response_data['organizer_tel']     = invoice_setting($event_id, 'organizer_tel');

        $response_data['organizer_fax']     = invoice_setting($event_id, 'organizer_fax');

        $response_data['organizer_email']   = invoice_setting($event_id, 'organizer_email');

        $response_data['organizer_website'] = invoice_setting($event_id, 'organizer_website');

        #---

        $response_data['invoice_title']     = invoice_setting($event_id, 'invoice_title');

        $response_data['invoice_sub_title']  = invoice_setting($event_id, 'invoice_sub_title');

        $response_data['invoice_remark']     = invoice_setting($event_id, 'invoice_remark');

        $response_data['invoice_ref_1']      = $data->invoice_ref_1;

        $response_data['invoice_ref_2']      = $data->invoice_ref_2;

        $response_data['invoice_company_code'] = invoice_setting($event_id, 'invoice_bank_company_code');

        $response_data['tax_no']             =  getTaxNo($customer_id);

        #---

        $response_data['invoice_address'] = prettyInvoiceAddress($data->billing_id);

        $response_data['invoice_price']   = number_format($data->invoice_grand_totel, 0);

        $response_data['promotion_code']       = !empty($data->promotion_code) ? $data->promotion_code : '';

        $response_data['promotion_discount']   = !empty($data->promotion_code) ? number_format($data->promotion_discount, 0) : '';

        //invoice_grand_totel

        #----

        //$items['description'] = $data->event_title;

        //$items['qty']         = $this->Invoice->countRegisterByInvoice($event_id, $customer_id);

        //$items['unit_price']  = $this->Invoice->getInvoiceTotalPrice($data->invoice_id);

        //$response_data['items']     =  $items;

        #---

        $response_data['registers'] = $this->__stringInvoiceRegister($event_id, $customer_id);

        return $response_data;

    }



    //---

    public  function __stringInvoiceRegister($event_id, $customer_id)

    {

        $registers = $this->Invoice->getInvoiceRegister($event_id, $customer_id);

        if(!$registers)

            return array();



        $fullname = array();

        foreach ($registers as $key => $data) {

            $title = !empty( $data->customer_name_title) ?  $data->customer_name_title : '';

            $fullname[] = $title.$data->customer_firstname.' '.$data->customer_lastname;

        }

        return $fullname;

    }



    //---Step three payment data

    private function __userPaymentData($customer_id, $event_id)

    {

        $data = $this->Invoice->queryInvoice($customer_id, $event_id);

        if(!$data)

            return FALSE;



        if($data->payment_status === 'unpaid')

            return FALSE;



        switch ($data->payment_status) {

            case 'failed':

                $payment_message = lang('event_invoice_payment_failed');

                break;

            case 'paid':

                $payment_message = lang('event_invoice_payment_paid');

                break;

            case 'approved':

                $payment_message = lang('event_invoice_payment_approved');

                break;

            default:

                $payment_message = lang('event_invoice_payment_unpaid');

                break;

        }



        //$response_data['invoice_id']     = $data->invoice_id;

        //$response_data['invoice_ref_1']     = $data->invoice_ref_1;

        $response_data['payment_status'] = $data->payment_status;

        $response_data['status_message'] = $payment_message;

        $response_data['payment_slip'] = config('upload_url').'slips/'.$data->payment_slip;

        $response_data['payment_date'] = date('d-m-Y', strtotime($data->payment_dtm));

        $response_data['payment_time'] = date('H:i', strtotime($data->payment_dtm));

        $response_data['payment_bank_id'] = $data->payment_bank_id;



        return $response_data;

    }



    //---Step payment status

    private function __userPaymentStatus($customer_id, $event_id)

    {

        $data = $this->Invoice->queryInvoice($customer_id, $event_id);

        if(!$data)

            return FALSE;



        switch ($data->payment_status) {

            case 'failed':

                $payment_message = lang('event_invoice_payment_failed');

                break;

            case 'paid':

                $payment_message = lang('event_invoice_payment_paid');

                break;

            case 'approved':

                $payment_message = lang('event_invoice_payment_approved');

                break;

            default:

                $payment_message = lang('event_invoice_payment_unpaid');

                break;

        }

        $response_data['payment_status'] = $data->payment_status;

        $response_data['status_message'] = $payment_message;



        $response_data['organizer_logo']    = config('upload_url').'event_settings/'.invoice_setting($event_id, 'organizer_logo');

        $response_data['organizer_name']    = invoice_setting($event_id, 'organizer_name');

        $response_data['organizer_address'] = invoice_setting($event_id, 'organizer_address');

        $response_data['organizer_tel']     = invoice_setting($event_id, 'organizer_tel');

        $response_data['organizer_fax']     = invoice_setting($event_id, 'organizer_fax');

        $response_data['organizer_email']   = invoice_setting($event_id, 'organizer_email');

        $response_data['organizer_website'] = invoice_setting($event_id, 'organizer_website');

        #---

        $response_data['receipt_title']      = invoice_setting($event_id, 'invoice_title');

        $response_data['receipt_sub_title']  = invoice_setting($event_id, 'invoice_sub_title');

        $response_data['receipt_remark']     = invoice_setting($event_id, 'invoice_remark');

        $response_data['receipt_no']         = getReceiptNo($event_id, $customer_id);

        $response_data['receipt_date']       = getPaymentTime($event_id, $customer_id);

        $response_data['receipt_ref_2']      = $data->invoice_ref_2;

        $response_data['receipt_company_code'] = invoice_setting($event_id, 'invoice_bank_company_code');

        $response_data['tax_no']   =  getTaxNo($customer_id);

        #---

        $response_data['receipt_address'] = prettyInvoiceAddress($data->billing_id);

        $response_data['total_receipt']   = getTotalReceipt($event_id, $customer_id);

        #----

        $response_data['registers'] = $this->__stringInvoiceRegister($event_id, $customer_id);

        return $response_data;

    }





    //-----Step five event card

    private function __userEventCard($customer_id, $event_id)

    {

        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);

        if(!$invoice)

            return FALSE;



        if($invoice->payment_status !== 'approved')

             return FALSE;



        $customer = $this->Customer->queryProfile($customer_id);

        $response['card_title']     = event_setting($event_id, 'card_title');

        $response['card_sub_title'] = event_setting($event_id, 'card_sub_title');

        $response['card_venue']     = event_setting($event_id, 'card_venue');

        $response['card_logo']      = config('upload_url').'event_settings/'.event_setting($event_id, 'card_logo');

        $response['customer_name_title']   = $customer->customer_name_title;

        $response['customer_firstname']    = $customer->customer_firstname;

        $response['customer_lastname']     = $customer->customer_lastname;

        $response['register_no']           = 'Ref2: '.$invoice->invoice_ref_2.' (Ref1: '.$invoice->invoice_ref_1.')';



        /*//----

        $receipt_no = getReceiptNo($event_id, $customer_id);

        $response['receipt_no']        = $receipt_no;

        $file_exists = config('upload_url').'qr_codes/RC_'.$receipt_no.'.png';

        if(!file_exists($file_exists))

            $qr_code = $this->Barcode->QRCode($receipt_no, 'RC_', config('upload_url'));

        else

            $qr_code = $file_exists;



        $response['receipt_no_qr'] = $qr_code;*/



        $response['receipt_no']        = getReceiptNo($event_id, $customer_id);

        $file_exists = config('upload_url').'qr_codes/'.$customer->customer_code.'.png';

        if(!file_exists($file_exists))

            $qr_code = $this->Barcode->QRCode($customer->customer_code, '', config('upload_url'));

        else

            $qr_code = $file_exists;



        $response['receipt_no_qr'] = $qr_code;

        return $response;

    }



    //-----Step five event card

    private function __userChildCard($event_id, $registrant_id, $customer_id)

    {

        $invoice = $this->Invoice->queryInvoice($registrant_id, $event_id);

        if(!$invoice)

            return FALSE;



        $customer = $this->Customer->queryProfile($customer_id);

        $response['payment_status'] = $invoice->payment_status;

        $response['card_title']     = event_setting($event_id, 'card_title');

        $response['card_sub_title'] = event_setting($event_id, 'card_sub_title');

        $response['card_venue']     = event_setting($event_id, 'card_venue');

        $response['card_logo']      = config('upload_url').'event_settings/'.event_setting($event_id, 'card_logo');

        $response['customer_name_title']   = $customer->customer_name_title;

        $response['customer_firstname']    = $customer->customer_firstname;

        $response['customer_lastname']     = $customer->customer_lastname;

        $response['register_no']           = 'Ref2: '.$invoice->invoice_ref_2.' (Ref1: '.$invoice->invoice_ref_1.')';



        //----

        $receipt_no = getReceiptNo($event_id, $registrant_id);

        $response['receipt_no']        = $receipt_no;

        $file_exists = config('upload_url').'qr_codes/RC_'.$receipt_no.'.png';

        if(!file_exists($file_exists))

            $qr_code = $this->Barcode->QRCode($receipt_no, 'RC_', config('upload_url'));

        else

            $qr_code = $file_exists;



        $response['receipt_no_qr'] = $qr_code;

        return $response;

    }



    public function updateInvoiceFilename($event_id, $customer_id, $filename)

    {

        $this->db->set('invoice_file', $filename);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->update(config('tb_invoice'));

        return  TRUE;

    }





    public function updateBillId($event_id, $customer_id, $billing_id)

    {

        $this->db->set('billing_id', $billing_id);

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->update(config('tb_invoice'));

        return  TRUE;

    }



    public function checkInvoiceRedundant($event_id, $customer_id)

    {

        $this->db->select('invoice_id');

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $this->db->where('status', 'active');

        $query = $this->db->get(config('tb_invoice'));

        return  $query->num_rows();

    }



    //---

    public function createInvoice($event_id, $customer_id, $total_participant, $params=array())

    {

        $this->db->trans_start();



        #---Create event invoice

        $invoice_data['customer_id'] = $customer_id;

        $invoice_data['event_id']    = $event_id;

        if(!empty($params['promotion_code']))

            $invoice_data['promotion_code'] = $params['promotion_code'];

        $this->db->insert( config('tb_invoice') , $invoice_data);

        $invoice_id = $this->db->insert_id();



        #---Create invoice items

        $total_price = 0;

        foreach ($params['session_data'] as $key => $val) {

            $invoice_item['invoice_id'] = $invoice_id;

            $invoice_item['item_qty']   = 1;

            $invoice_item['session_id'] = $val['session_id'];

            $invoice_item['item_price'] = $val['session_price'];

            $this->db->insert( config('tb_invoice_item') , $invoice_item);

            #----

            $total_price += ( $total_participant * $invoice_item['item_price'] );

        }



        //--Update grand total price

        $get_event_price = $this->getEventPrice($event_id);

        if($get_event_price > 0){

            $invoice_update['invoice_grand_totel'] = ($total_participant * $get_event_price);

        }else{

            $invoice_update['invoice_grand_totel'] = $total_price;

        }

        $invoice_update['invoice_ref_1']  = generateNo($invoice_id);

        switch ($params['register_type']) {

            case 'single':

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'S');

                break;

            default:

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'G');

                break;

        }

        $this->db->where('invoice_id', $invoice_id);

        $this->db->update( config('tb_invoice') , $invoice_update);



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return $invoice_id;

    }



    //---

    public function updateInvoice($event_id, $customer_id, $total_participant, $params=array())

    {

        $this->db->trans_start();



        #--query invoice data

        $this->db->select('invoice_id');

        $this->db->where('customer_id', $customer_id);

        $this->db->where('event_id', $event_id);

        $query = $this->db->get(config('tb_invoice'));

        $invoice_id = $query->row()->invoice_id;



        #--delete old register

        $this->db->where('invoice_id', $invoice_id);

        $this->db->delete(config('tb_event_register'));

        #--delete old items

        $this->db->where('invoice_id', $invoice_id);

        $this->db->delete(config('tb_invoice_item'));





        #---Insert new invoice items

        $total_price = 0;

        foreach ($params['session_data'] as $key => $val) {

            $invoice_item['invoice_id'] = $invoice_id;

            $invoice_item['item_qty']   = 1;

            $invoice_item['session_id'] = $val['session_id'];

            $invoice_item['item_price'] = $val['session_price'];

            $this->db->insert( config('tb_invoice_item') , $invoice_item);

            #----

            $total_price += ( $total_participant * $invoice_item['item_price'] );

        }



        //--Update grand total price

        $get_event_price = $this->getEventPrice($event_id);

        if($get_event_price > 0){

            $invoice_update['invoice_grand_totel'] = ($total_participant * $get_event_price);

        }else{

            $invoice_update['invoice_grand_totel'] = $total_price;

        }

        $invoice_update['invoice_ref_1']  = generateNo($invoice_id);

        switch ($params['register_type']) {

            case 'single':

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'S');

                break;

            default:

                $invoice_update['invoice_ref_2']  = generateNo($invoice_id, 'G');

                break;

        }

        $this->db->where('invoice_id', $invoice_id);

        $this->db->update( config('tb_invoice') , $invoice_update);



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return $invoice_id;

    }



    //-----

    public function insertRegister($event_id, $customer_id, $registrant_id, $invoice_id, $type='single')

    {

        //---Update event to table [registers]

        $register_data['event_id']      = $event_id;

        $register_data['customer_id']   = $customer_id;

        $register_data['regis_dtm']     = date('Y-m-d H:i:s');

        $register_data['regis_from']    = 'mobile';

        $register_data['regis_ip']      = $this->input->ip_address();

        $register_data['registrant_id'] = $registrant_id;

        $register_data['regis_type']    = $type;

        $register_data['invoice_id']    = $invoice_id;

        $query = $this->db->insert(config('tb_event_register'), $register_data);

        if (!$query) return FALSE;



        return TRUE;

    }



    //----

    public function getGroupParticipant($event_id, $registrant_id)

    {

        $this->db->from(config('tb_event_register').' AS R');

        $this->db->join(config('tb_customer').' AS C', 'C.customer_id=R.customer_id', 'LEFT');

        $this->db->where('R.registrant_id', $registrant_id);

        $this->db->where('R.event_id', $event_id);

        $this->db->where('R.status', 'active');

        $query = $this->db->get();

        if($query->num_rows() == 0)

            return '';



        $data_list = $query->result();

        foreach ($data_list as $key => $val) {

            $response_data[$key]['regis_id']         = $val->regis_id;

            //$response_data[$key]['regis_dtm']         = $val->regis_dtm;

            $response_data[$key]['customer_id']         = $val->customer_id;

            $response_data[$key]['customer_code']         = $val->customer_code;

            $response_data[$key]['customer_email']         = $val->customer_email;

            $response_data[$key]['customer_name_title'] = $val->customer_name_title;

            $response_data[$key]['customer_firstname']  = $val->customer_firstname;

            $response_data[$key]['customer_lastname']   = $val->customer_lastname;

            //$response_data[$key]['customer_nickname']   = $val->customer_nickname;

            $response_data[$key]['customer_gender']   = $val->customer_gender;

            $response_data[$key]['customer_age']   = $val->customer_age;

            $response_data[$key]['occupation_id']   = $val->occupation_id;

            $response_data[$key]['job_description']   = $val->job_description;

            $response_data[$key]['customer_mobile']   = $val->customer_mobile;

            $response_data[$key]['food_id']   = $val->food_id;

        }

        return $response_data;

    }



    //----

    public function getRegisterType($event_id, $registrant_id)

    {

        $this->db->from(config('tb_event_register'));

        $this->db->where('registrant_id', $registrant_id);

        $this->db->where('event_id', $event_id);

        $this->db->where('status', 'active');

        $this->db->group_by('registrant_id');

        $query = $this->db->get();

        if($query->num_rows() == 0)

            return FALSE;



        return $query->row()->regis_type;

    }



    //-------

    public function updateGroupRegisterProfile($customer_id, $data)

    {

        $this->db->where('customer_id', $customer_id);

        $this->db->update(config('tb_customer'), $data);

        return TRUE;

    }



    //-------

    public function deleteGroupRegisterProfile($event_id, $customer_id)

    {

        $event_price = $this->Invoice->getEventPrice($event_id);



        $this->db->trans_start();

        $query = $this->db->select('regis_id,customer_id,registrant_id,invoice_id')

                    ->where('status','active')

                    ->where('event_id', $event_id)

                    ->where('customer_id', $customer_id)

                    ->where('regis_type', 'group')

                    ->get('event_registers');



        if($query->num_rows() > 0){



            $regis_id   = $query->row()->regis_id;

            $invoice_id = $query->row()->invoice_id;



            //----Update status

            $this->db->set('status', 'discard');

            $this->db->where('regis_id', $regis_id);

            $this->db->where('event_id', $event_id);

            $this->db->update(config('tb_event_register'));



            //-----Update invoice total price

            $this->db->set('invoice_grand_totel', 'invoice_grand_totel - '.$event_price, FALSE);

            $this->db->where('invoice_id', $invoice_id);

            $this->db->where('event_id', $event_id);

            $this->db->update('event_invoices');

        }



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

            return FALSE;



        return TRUE;

    }

}













