<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * https://github.com/PHPMailer/PHPMailer/wiki/SMTP-Debugging
 * 0=Disable ,1=client message, 2=server resp,3,4
 */
class Sendmail_model extends CI_Model
{
    private $mail;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Address_model', 'Address');
    }

    //---
    public function resgier_login_info($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        $mail = new PHPMailer(false);
        #--Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = config('mail_account_host');
        $mail->SMTPAuth   = true;
        $mail->Username   = config('mail_account_username');
        $mail->Password   = config('mail_account_password');
        $mail->SMTPSecure = config('mail_account_SMTPsecure');
        $mail->CharSet    = 'UTF-8';
        $mail->Port       = config('mail_account_port');

        ob_start();
        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['username']    = $params['username'];
        $mail_params['password']    = $params['password'];
        $mail_params['mailt_title'] = !empty($params['event_title']) ? $params['event_title'] : lang('register_mail_title');
        #--Recipients
        $mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $mail->addAddress($email, $mail_params['fullname']);
        $mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));

        #--Content
        $mail->isHTML(true);
        $mail->Subject = $mail_params['mailt_title'];
        $mail->Body    = $this->load->view('email_templates/register_template', $mail_params, TRUE);
        ob_end_clean();
        $result = !$mail->send() ? FALSE : TRUE ;
        $mail->SmtpClose();
        return $result ;
    }

    //---
    public function sendInVoice($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        $mail = new PHPMailer(false);
        #--Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = config('mail_account_host');
        $mail->SMTPAuth   = true;
        $mail->Username   = config('mail_account_username');
        $mail->Password   = config('mail_account_password');
        $mail->SMTPSecure = config('mail_account_SMTPsecure');
        $mail->CharSet    = 'UTF-8';
        $mail->Port       = config('mail_account_port');

        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['mailt_title'] = lang('event_invoice_mail_title');
        #--Recipients
        ob_start();
        $mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $mail->addAddress($email, $mail_params['fullname']);
        $mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));
        #----Add attachments
        $mail->addAttachment($params['file_path']);
        #--Content
        $mail->isHTML(true);
        $mail->Subject = lang('event_invoice_mail_title');
        $mail->Body    = $this->load->view('email_templates/invoice_pay_in_slip', $mail_params, TRUE);
        ob_end_clean();
        $result = !$mail->send() ? FALSE : TRUE ;
        $mail->SmtpClose();
        return $result ;
    }

    //---
    public function sendReceiptSlip($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        $mail = new PHPMailer(false);
        #--Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = config('mail_account_host');
        $mail->SMTPAuth   = true;
        $mail->Username   = config('mail_account_username');
        $mail->Password   = config('mail_account_password');
        $mail->SMTPSecure = config('mail_account_SMTPsecure');
        $mail->CharSet    = 'UTF-8';
        $mail->Port       = config('mail_account_port');

        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['mailt_title'] = lang('event_receipt_mail_title');
        #--Recipients
        //ob_start();
        $mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $mail->addAddress($email, $mail_params['fullname']);
        $mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));
        #----Add attachments
        $mail->addAttachment($params['file_path']);
        #--Content
        $mail->isHTML(true);
        $mail->Subject = lang('event_receipt_mail_title');
        $mail->Body    = $this->load->view('email_templates/receipt_slip', $mail_params, TRUE);
        //ob_end_clean();
        $result = !$mail->send() ? FALSE : TRUE ;
        $mail->SmtpClose();
        return $result ;
    }

    //---
    public function sendForgotPassword($email, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        $mail = new PHPMailer(false);
        #--Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = config('mail_account_host');
        $mail->SMTPAuth   = true;
        $mail->Username   = config('mail_account_username');
        $mail->Password   = config('mail_account_password');
        $mail->SMTPSecure = config('mail_account_SMTPsecure');
        $mail->CharSet    = 'UTF-8';
        $mail->Port       = config('mail_account_port');

        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['link']    = !empty($params['link']) ? $params['link'] : '';
        $mail_params['mailt_title'] = lang('forgot_password_mail_title');

        //ob_start();
        $mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $mail->addAddress($email, $mail_params['fullname']);
        $mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));

        #--Content
        $mail->isHTML(true);
        $mail->Subject = lang('forgot_password_mail_title');
        $mail->Body    = $this->load->view('email_templates/forgot_password', $mail_params, TRUE);
        //ob_end_clean();
        $result = !$mail->send() ? FALSE : TRUE ;
        $mail->SmtpClose();
        return $result ;


    }



}