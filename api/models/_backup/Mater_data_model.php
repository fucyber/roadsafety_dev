<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mater_data_model extends CI_Model
{
    //---
    public function getOccupation()
    {
        $this->db->select('occupation_id,occupation_name');
        $this->db->order_by('occupation_sort', 'ASC');
        $query = $this->db->get( config('tb_occupation') );
        if($query->num_rows() === 0)
            return FALSE;

        $response_data = array();
        $data = $query->result();
        foreach ($data as $key => $val) {
            $response_data[$key]['occupation_id']   =  $val->occupation_id;
            $response_data[$key]['occupation_name'] =  $val->occupation_name;
        }

        return $response_data;
    }

    //---
    public function getDepartment()
    {
        $this->db->select('department_id,department_name');
        $this->db->order_by('department_sort', 'ASC');
        $this->db->order_by('department_name', 'ASC');
        $query = $this->db->get( config('tb_department') );
        if($query->num_rows() === 0)
            return FALSE;

        $response_data = array();
        $data = $query->result();
        foreach ($data as $key => $val) {
            $response_data[$key]['department_id']   =  $val->department_id;
            $response_data[$key]['department_name'] =  $val->department_name;
        }

        return $response_data;
    }

    //---
    public function getMinistry()
    {
        $this->db->select('ministry_id,ministry_name');
        $this->db->order_by('ministry_sort', 'ASC');
        $this->db->order_by('ministry_name', 'ASC');
        $query = $this->db->get( config('tb_ministry') );
        if($query->num_rows() === 0)
            return FALSE;

        $response_data = array();
        $data = $query->result();
        foreach ($data as $key => $val) {
            $response_data[$key]['ministry_id']   =  $val->ministry_id;
            $response_data[$key]['ministry_name'] =  $val->ministry_name;
        }

        return $response_data;
    }

    //---
    public function getFoodType()
    {
        $this->db->select('food_id,food_name');
        $this->db->order_by('food_name', 'ASC');
        $query = $this->db->get( config('tb_food_type') );
        if($query->num_rows() === 0)
            return FALSE;

        $response_data = array();
        $data = $query->result();
        foreach ($data as $key => $val) {
            $response_data[$key]['food_id']   =  $val->food_id;
            $response_data[$key]['food_name'] =  $val->food_name;
        }

        return $response_data;
    }

    //---
    public function getBank()
    {
        $this->db->select('bank_id,bank_name,bank_account,bank_number');
        $this->db->order_by('bank_name', 'ASC');
        $query = $this->db->get( config('tb_bank') );
        if($query->num_rows() === 0)
            return FALSE;

        $response_data = array();
        $data = $query->result();
        foreach ($data as $key => $val) {
            $response_data[$key]['bank_id']   =  $val->bank_id;
            $response_data[$key]['bank_name'] =  $val->bank_name.' ('.$val->bank_number.')';
        }

        return $response_data;
    }
}