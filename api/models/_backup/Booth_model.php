<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booth_model extends CI_Model
{
    //---@
    public function getBooth($event_id, $filters=[])
    {
        if(empty($event_id))
            return FALSE;

        $limit  = !empty($filters['limit']) ? $filters['limit'] : 10 ;
        $offset = !empty($filters['offset']) ? $filters['offset'] : 0 ;

        //--
        $this->db->from('booths AS B');
        $this->db->join('booth_types AS T', 'T.type_id=B.booth_type_id', 'LEFT');
        $this->db->limit($limit, $offset);
        if(!empty($filters['search'])){
            $this->db->like('booth_no', $filters['search']);
            $this->db->or_like('booth_name', $filters['search']);
        }
        $this->db->order_by('B.booth_no', 'ASC');
        $this->db->order_by('B.booth_name', 'ASC');
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    //---@
    public function getBoothById($event_id, $booth_id)
    {
        if(empty($event_id) || empty($booth_id))
            return FALSE;
        //--
        $this->db->from('booths AS B');
        $this->db->join('booth_types AS T', 'T.type_id=B.booth_type_id', 'LEFT');
        $this->db->where('B.event_id', $event_id);
        $this->db->where('B.booth_id', $booth_id);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

    public function saveVotePoint($event_id, $booth_id, $customer_id, $vote_point)
    {
        //-- เช็คว่าเคยโวตหือยัง
        $checked = $this->db->where('event_id', $event_id)
                    ->where('customer_id', $customer_id)
                    ->where('booth_id', $booth_id)
                    ->get('booths_vote');

        if($checked->num_rows() > 0)
            return 1;

        $data['event_id']    = $event_id;
        $data['customer_id'] = $customer_id;
        $data['booth_id']    = $booth_id;
        $data['vote_point']  = $vote_point;
        $query = $this->db->insert('booths_vote', $data);
        if(!$query)
            return  2;

        return 3;
    }

    public function getCustomerVote($event_id, $booth_id, $customer_id)
    {
        if(empty($event_id) || empty($booth_id) || empty($customer_id))
            return 0;

        $query = $this->db->where('event_id', $event_id)
                    ->where('booth_id', $booth_id)
                    ->where('customer_id', $customer_id)
                    ->limit(1)
                    ->get('booths_vote');
        if($query->num_rows() === 0)
            return 0;

        return $query->row()->vote_point;
    }

    public function updateBoothScanStatus($event_id, $booth_id, $customer_id, $point=1)
    {
        $data['event_id']    = $event_id;
        $data['customer_id'] = $customer_id;
        $data['booth_id']    = $booth_id;
        $data['type']        = 'booth_scan';
        $data['booth_point'] = $point;
        $this->db->insert('booth_activities', $data);
        $this->Common->updatedCustomerPoint($event_id, $customer_id, $point);
        return TRUE;
    }

    public function checkBoothScanStatus($event_id, $booth_id, $customer_id)
    {
        $data['event_id']    = $event_id;
        $data['customer_id'] = $customer_id;
        $data['booth_id']    = $booth_id;
        $data['type']        = 'booth_scan';
        $this->db->where($data);
        $query = $this->db->get('booth_activities');
        return $query->num_rows();
    }


}