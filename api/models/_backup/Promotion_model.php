<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotion_model extends CI_Model
{
    //--@
    public function  checkPromotionCode($code)
    {
        $today = date('Y-m-d H:i:s');
        $query = $this->db->where('status', 'active')
                  ->where('code', $code)
                  ->where('balance > ', 0)
                  ->where('start_datetime <=', $today)
                  ->where('end_datetime >=', $today)
                  ->limit(1)
                  ->get('event_promotions');
        if($query->num_rows() === 0)
            return FALSE;

        $result['promotion_code'] = $query->row()->code;
        $in_date = date('d/m', strtotime($query->row()->end_datetime));
        $result['message']        = sprintf(lang('promotion_discount_message'), $in_date);
        $result['discount']       = number_format($query->row()->discount, 0);

      return $result;
    }

    //--@
    public function  updatePromotionBalance($code)
    {
      $this->db->set('balance', 'balance - 1', FALSE);
      $this->db->where('code', $code);
      $this->db->where('status !=', 'discard');
      $this->db->update('event_promotions');
      return TRUE;
    }
}