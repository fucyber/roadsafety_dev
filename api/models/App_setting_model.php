<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class App_setting_model extends CI_Model

{

    //---

    public function event()

    {

        $prefix = 'app_setting_event_display';

        #---

        $this->db->where('prefix', $prefix);

        $this->db->limit(1);

        $query = $this->db->get( config('tb_setting') );

        if($query->num_rows() === 0)

            return FALSE;



        $encode_data   = $query->row()->value;

        return json_decode($encode_data, TRUE);

    }

}