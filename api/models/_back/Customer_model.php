<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model
{

    //---
    public function checkDuplicateEmailAddress($email)
    {
        if(empty($email))
            return FALSE;

        $this->db->select('customer_id');
        $this->db->where('customer_email', $email);
        $this->db->where('customer_status !=', 'discard');
        $query = $this->db->get( config('tb_customer') );
        if($query->num_rows() === 0)
            return TRUE;
        return FALSE;
    }

    //---
    public function haveAccount($email)
    {
        $this->db->select('customer_id');
        $this->db->where('customer_email', $email);
        $this->db->where('customer_status !=', 'discard');
        $query = $this->db->get( config('tb_customer') );
        if($query->num_rows() === 0)
            return FALSE;

        return (int)$query->row()->customer_id;
    }

    //--
    public function insertAccount($data)
    {
        if(empty($data))
            return FALSE;

        $query = $this->db->insert(config('tb_customer'), $data);
        if($query === FALSE)
            return FALSE;

        //---Update customer code
        $insert_id     = $this->db->insert_id();
        $customer_code = $this->generateCustomerCode($insert_id);
        $this->db->set('customer_code', $customer_code);
        $this->db->where('customer_id', $insert_id);
        $this->db->update( config('tb_customer') );
        return $insert_id;
    }

    //-----update customer data
    public function updateAccount($customer_id, $data)
    {
        if(empty($customer_id) || empty($data))
            return FALSE;

        $this->db->where('customer_id', $customer_id);
        $this->db->update(config('tb_customer'), $data);
        return $customer_id;
    }


        //--@
    public function generateCustomerCode($id)
    {
        if(empty($id)) return FALSE;

        $prefix = 'M';
        $code = $prefix.sprintf('%05d',$id);

        $checked = $this->__checkDuplicateCode($code);
        if($checked === FALSE){
            $this->generateCustomerCode($plaza_id);
        }else{
            $result = $code;
        }
        return $result;
    }

    //---@
    private function __checkDuplicateCode($code)
    {
        if(empty($code)) return FALSE;

        $this->db->where('customer_code',$code);
        $query = $this->db->get( config('tb_customer') );
        if($query->num_rows() > 0)
            return FALSE;

        return TRUE;
    }


    //--@Update Device Token
    public function updateDeviceToken($customer_id, $params)
    {
        if(empty($customer_id))
            return FALSE;

        if(empty($params['android_token']) && empty($params['ios_token']))
            return FALSE;

        if(!empty($params['android_token']))
            $this->db->set('android_token', $params['android_token']);
        if(!empty($params['ios_token']))
            $this->db->set('ios_token', $params['ios_token']);
        $this->db->where('customer_id', $customer_id);
        $this->db->update( config('tb_customer') );
        return TRUE;
    }

    //---Check Login form
    public function checkedLogin($username, $password)
    {
        if(empty($username) || empty($password))
            return FALSE;

        $encryptPassword = $this->Common->encryptionPassword($password);
        //$this->db->select('customer_id,customer_code,customer_name_title,customer_firstname,customer_lastname');
        //$this->db->select('customer_gender,customer_email');
        //$this->db->select('customer_picture,customer_mobile,customer_tel,customer_age');
        //$this->db->select('occupation_id,job_description,food_id');
        $this->db->select('customer_id');
        $this->db->where('customer_email', $username);
        $this->db->where('customer_password', $encryptPassword);
        $this->db->where('customer_status', 'active');
        $this->db->limit(1);
        $query = $this->db->get( config('tb_customer') );
        if($query->num_rows() === 0)
            return FALSE;

        return  $query->row()->customer_id;
    }

    /**
     * @param  [Object] $data
     * @return array
     */
    private function getLoginToken($customer_email, $customer_id)
    {
        if(empty($customer_email) || empty($customer_id))
            return FALSE;

        //--Token => Save & Return token key
        $token_data['data_email'] = $customer_email;
        $token_data['data_id']    = $customer_id;
        $token_data['data_ref']   = 'customer';
        $token = $this->Common->setTokenKey($token_data);
        if(!$token) return FALSE;

        return $token;
    }

    //--Query db
    public function queryProfile($customer_id)
    {
        if(empty($customer_id))
            return FALSE;

        $this->db->select('C.customer_id,C.customer_code,C.customer_email,C.customer_name_title,C.customer_firstname,C.customer_lastname');
        $this->db->select('C.customer_gender,C.customer_age,C.customer_tel,C.customer_mobile,C.occupation_id,C.job_description,C.food_id');
        $this->db->select('A.*');
        $this->db->select('D.department_name,F.food_name,M.ministry_name,O.occupation_name');
        $this->db->from(config('tb_customer').' AS C');
        $this->db->join(config('tb_customer_agency').' AS A', 'A.customer_id=C.customer_id', 'LEFT');
        $this->db->join(config('tb_department').' AS D', 'D.department_id=A.department_id', 'LEFT');
        $this->db->join(config('tb_ministry').' AS M', 'M.ministry_id=A.ministry_id', 'LEFT');
        $this->db->join(config('tb_occupation').' AS O', 'O.occupation_id=C.occupation_id', 'LEFT');
        $this->db->join(config('tb_food_type').' AS F', 'F.food_id=C.food_id', 'LEFT');
        $this->db->where('C.customer_id', $customer_id);
        $this->db->where('C.customer_status !=', 'discard');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;

        return $query->row();
    }

    //--Query table
    public function getProfile($customer_id, $action='profile')
    {
        if(empty($customer_id))
            return FALSE;

        $this->db->select('customer_id,customer_code,customer_email,customer_name_title,customer_firstname,customer_lastname');
        $this->db->select('customer_gender,customer_age,customer_tel,customer_mobile,occupation_id,job_description,food_id');
        $this->db->from(config('tb_customer'));
        $this->db->where('customer_id', $customer_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() === 0)
            return FALSE;
        $profile = $query->row();

        #---Profile
        if($action === 'login')
            $response_data['token_key']          = $this->getLoginToken($profile->customer_email, $profile->customer_id);
        $response_data['customer_id']         = !empty($profile->customer_id) ? $profile->customer_id : '';
        $response_data['customer_code']       = !empty($profile->customer_code) ? $profile->customer_code : '';
        $response_data['customer_email']      = !empty($profile->customer_email) ? $profile->customer_email : '';
        $response_data['customer_name_title'] = !empty($profile->customer_name_title) ? $profile->customer_name_title : '';
        $response_data['customer_firstname']  = !empty($profile->customer_firstname) ? $profile->customer_firstname : '';
        $response_data['customer_lastname']   = !empty($profile->customer_lastname) ? $profile->customer_lastname : '';
        $response_data['customer_gender']     = !empty($profile->customer_gender) ? $profile->customer_gender : '';
        $response_data['customer_age']        = !empty($profile->customer_age) ? $profile->customer_age : 0;
        $response_data['customer_tel']        = !empty($profile->customer_tel) ? $profile->customer_tel : '';
        $response_data['customer_mobile']     = !empty($profile->customer_mobile) ? $profile->customer_mobile : '';
        $response_data['occupation_id']       = !empty($profile->occupation_id) ? $profile->occupation_id : '';
        $response_data['job_description']     = !empty($profile->job_description) ? $profile->job_description : '';
        $response_data['food_id']             = !empty($profile->food_id) ? $profile->food_id : '';

        #---Agency data
        $this->db->select('ministry_id,department_id,agency_name');
        $this->db->from(config('tb_customer_agency'));
        $this->db->where('customer_id', $customer_id);
        $this->db->where('agency_status !=', 'deleted');
        $this->db->limit(1);
        $queryAgency = $this->db->get();
        if($queryAgency->num_rows() === 0)
            $agency = array();
        $agency = $queryAgency->row();
        $response_data['department_id']  = !empty($agency->department_id) ? $agency->department_id : '';
        $response_data['ministry_id']    = !empty($agency->ministry_id) ? $agency->ministry_id : '';

        return $response_data;
    }

    //---
    public function insertAgency($data)
    {
        if(empty($data))
            return FALSE;

        $query = $this->db->insert(config('tb_customer_agency'), $data);
        if($query === FALSE)
            return FALSE;

        return $this->db->insert_id();
    }

    //---
    public function updateAgency($id, $data)
    {
        if(empty($id) || empty($data))
            return FALSE;

        $this->db->where('agency_id', $id);
        $query = $this->db->update(config('tb_customer_agency'), $data);
        if($query === FALSE)
            return FALSE;

        return $id;
    }

    //---
    public function insertAddress($data)
    {
        if(empty($data))
            return FALSE;

        $query = $this->db->insert(config('tb_customer_address'), $data);
        if($query === FALSE)
            return FALSE;

        return $this->db->insert_id();
    }

    //---
    public function updateAddress($id, $data)
    {
        if(empty($id) || empty($data))
            return FALSE;

        $this->db->where('address_id', $id);
        $query = $this->db->update(config('tb_customer_address'), $data);
        if($query === FALSE)
            return FALSE;

        return $id;
    }

    //---
    public function checkAgencyRedundant($customer_id)
    {
        $this->db->select('agency_id');
        $this->db->where('customer_id', $customer_id);
        $query = $this->db->get(config('tb_customer_agency'));
        return $query->num_rows();
    }
}

