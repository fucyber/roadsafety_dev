<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API_model extends CI_Model
{
    private $api_address;
    private $client;

    public  function __construct()
    {
        parent::__construct();
        $this->api_address = config('api_address');
    }

    public function get($api_uri, $input_params=array())
    {
        if(empty($api_uri))
          return FALSE;

        $tokens['token_key'] = 'guest';
        $params = array_merge($tokens,$input_params);

        $service_url = $api_uri.'?'.http_build_query($params);

        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = \Httpful\Request::get($service_url)->expectsJson()->send();

        return $response->body;
    }

    public function get_self($api_uri, $input_params=array())
    {
        if(empty($api_uri))
          return FALSE;

        $tokens['token_key'] = 'guest';
        $params = array_merge($tokens, $input_params);
        $service_url = $_SERVER['SERVER_ADDR'].'/api/'.$api_uri.'?'.http_build_query($params);
        //-----
        $json_handler = new Httpful\Handlers\JsonHandler(array('decode_as_array' => true));
        Httpful\Httpful::register('application/json', $json_handler);
        $response = \Httpful\Request::get($service_url)->expectsJson()->send();

        return $response->body;
    }

    public function post($api_uri, $input_params=array())
    {
        if(empty($api_uri))
          return FALSE;

        $tokens['token_key'] = 'guest';
        $params = array_merge($tokens,$input_params);

    }

}