<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * https://github.com/PHPMailer/PHPMailer/wiki/SMTP-Debugging
 * 0=Disable ,1=client message, 2=server resp,3,4
 */
class Sendmail_model extends CI_Model
{
    private $mail;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Address_model', 'Address');

        $this->mail = new PHPMailer(false);
        #--Server settings
        $this->mail->SMTPDebug = 1;
        $this->mail->isSMTP();
        $this->mail->Host       = config('mail_account_host');
        $this->mail->SMTPAuth   = true;
        $this->mail->Username   = config('mail_account_username');
        $this->mail->Password   = config('mail_account_password');
        $this->mail->SMTPSecure = 'ssl'; //tls
        $this->mail->CharSet    = 'UTF-8';
        $this->mail->Port       = 465;
    }

    //---
    public function resgier_login_info($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

         ob_start();
        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['username']    = $params['username'];
        $mail_params['password']    = $params['password'];
        $mail_params['mailt_title'] = !empty($params['event_title']) ? $params['event_title'] : lang('register_mail_title');
        #--Recipients
        $this->mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $this->mail->addAddress($email, $mail_params['fullname']);
        $this->mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));

        #--Content
        $this->mail->isHTML(true);
        $this->mail->Subject = $mail_params['mailt_title'];
        $this->mail->Body    = $this->load->view('email_templates/register_template', $mail_params, TRUE);
        $result = !$this->mail->send() ? FALSE : TRUE ;
        ob_end_clean();
        return $result ;
    }

    //---
    public function sendInVoice($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['mailt_title'] = lang('event_invoice_mail_title');
        #--Recipients
        ob_start();
        $this->mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $this->mail->addAddress($email, $mail_params['fullname']);
        $this->mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));
        #----Add attachments
        $this->mail->addAttachment($params['file_path']);
        #--Content
        $this->mail->isHTML(true);
        $this->mail->Subject = lang('event_invoice_mail_title');
        $this->mail->Body    = $this->load->view('email_templates/invoice_pay_in_slip', $mail_params, TRUE);
        $result = !$this->mail->send() ? FALSE : TRUE ;
        ob_end_clean();
        return $result ;
    }

    //---
    public function sendReceiptSlip($email = NULL, $params=array())
    {
        if(empty($email) || empty($params))
            return FALSE;

        #--Params
        $mail_params['fullname']    = !empty($params['fullname']) ? $params['fullname'] : '';
        $mail_params['mailt_title'] = lang('event_receipt_mail_title');
        #--Recipients
        ob_start();
        $this->mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $this->mail->addAddress($email, $mail_params['fullname']);
        $this->mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));
        #----Add attachments
        $this->mail->addAttachment($params['file_path']);
        #--Content
        $this->mail->isHTML(true);
        $this->mail->Subject = lang('event_receipt_mail_title');
        $this->mail->Body    = $this->load->view('email_templates/receipt_slip', $mail_params, TRUE);
        $result = !$this->mail->send() ? FALSE : TRUE ;
        ob_end_clean();
        return $result ;
    }


}