<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

class Stamp_model extends CI_Model

{



    //--

    public function getBoothStamps($event_id, $customer_id)

    {

        if(empty($customer_id) || empty($event_id))

            return FALSE;



        $this->db->select('b.booth_id,b.booth_no,b.booth_name,b.booth_image');

        $this->db->select('b.booth_point AS stamp_point,a.created_at AS stamp_datetime');

        $this->db->select('a.id as stamp_id');

        $this->db->from('booths AS b');

        $this->db->join('booth_activities AS a',

                    'a.booth_id = b.booth_id AND a.event_id='.$event_id.'

                    AND a.type="booth_scan" AND a.status="active"  AND a.customer_id='.$customer_id.'',

                    'LEFT');

        $this->db->where('b.booth_status', 'active');

        $this->db->order_by('b.booth_no', 'ASC');

        $this->db->order_by('b.booth_name', 'ASC');

        $query = $this->db->get();

        if($query->num_rows() > 0)

            return $query->result();

        else

            return FALSE;

    }



    //--

    public function getGiftList($event_id, $customer_id)

    {

        if(empty($customer_id) || empty($event_id))

            return FALSE;



        $this->db->select('G.gift_id,G.image,G.name,G.redeem_point');

        $this->db->select('G.amount,G.redeemed,A.id as tranction_id');

        $this->db->from('event_gifts AS G');

        $this->db->join('event_gift_activities AS A',

                    'A.gift_id=G.gift_id AND G.event_id='.$event_id.'

                    AND A.status="active"

                    AND A.customer_id='.$customer_id.'', 'LEFT');

        $this->db->where('G.status', 'active');

        $this->db->group_by('G.gift_id');

        $this->db->order_by('G.redeem_point', 'ASC');

        $query = $this->db->get();

        if($query->num_rows() > 0)

            return $query->result();

        else

            return FALSE;

    }



    public function getGiftById($event_id, $gift_id)

    {

      $this->db->where('gift_id', $gift_id);

      $this->db->where('event_id', $event_id);

      $this->db->where('status !=', 'deleted');

      $this->db->limit(1);

      $query = $this->db->get('event_gifts');

      if($query->num_rows() === 0)

        return FALSE;



      return $query->row();

    }



    public function checkIsRedeem($event_id, $gift_id, $customer_id)

    {

      $this->db->where('gift_id', $gift_id);

      $this->db->where('event_id', $event_id);

      $this->db->where('customer_id', $customer_id);

      $this->db->where('status !=', 'discard');

      $query = $this->db->get('event_gift_activities');

      return $query->num_rows();

    }



    public function  saveGiftRedeem($event_id, $gift_id, $customer_id, $redeem_point)

    {

      $this->db->trans_start();

      $redeem_code = $this->genAirCode($event_id, $customer_id);



      //--Insert redeem transaction

      $data['gift_id']      = $gift_id;

      $data['event_id']     = $event_id;

      $data['customer_id']  = $customer_id;

      $data['redeem_point'] = $redeem_point;

      $data['redeem_code']  = $redeem_code;

      $data['created_at']   = date('Y-m-d H:i:s');

      $data['status']       = 'active';

      $this->db->insert('event_gift_activities', $data);



      //--Update gift  redeemed point

      $this->db->set('redeemed', 'redeemed + 1', FALSE);

      $this->db->where('status !=', 'deleted');

      $this->db->where('gift_id', $gift_id);

      $this->db->where('event_id', $event_id);

      $this->db->update('event_gifts');



      //--Update customer balance point

      $this->db->set('customer_point', 'customer_point - '.$redeem_point, FALSE);

      $this->db->where('event_id', $event_id);

      $this->db->where('customer_id', $customer_id);

      $this->db->update('event_registers');

      $this->db->trans_complete();



      if($this->db->trans_status() === FALSE)

        return FALSE;



      return $redeem_code;

    }



    public function getCustomerBalancePoint($event_id, $customer_id)

    {

      $query = $this->db->select('customer_point')

                    ->where('status', 'active')

                    ->where('event_id', $event_id)

                    ->where('customer_id', $customer_id)

                    ->limit(1)

                    ->get('event_registers');



      if($query->num_rows() === 0)

        return 0;



      return $query->row()->customer_point;

    }



    //---@

    public function genAirCode($event_id, $customer_id)

    {

        $redeem_code =  strtoupper(ramdomString(6));

        $data['event_id']     = $event_id;

        $data['customer_id']  = $customer_id;

        $data['redeem_code']  = $redeem_code;

        $data['status']       = 'active';

        $this->db->where($data);

        $query = $this->db->get('event_gift_activities');

        if($query->num_rows() > 0){

          $this->genAirCode($event_id, $customer_id);

        }else{

          return $redeem_code;

        }

    }



    public function getRedemmTransaction($event_id, $customer_id)

    {

      $this->db->where('T.status !=', 'discard');

      $this->db->from('event_gift_activities T');

      $this->db->join('event_gifts G', 'G.gift_id=T.gift_id', 'LEFT');

      $this->db->where('T.event_id', $event_id);

      $this->db->where('T.customer_id', $customer_id);

      $query = $this->db->get();

      if($query->num_rows() === 0)

        return FALSE;



      return $query->result();

    }



    public function checkRegistered($event_id, $customer_id)

    {

      $query = $this->db->where('event_id', $event_id)

                ->where('customer_id', $customer_id)

                ->where('status', 'active')

                ->get('event_registers');



      return $query->num_rows();

    }

}