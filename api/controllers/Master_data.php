<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Master_data extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mater_data_model', 'Data');
    }

    //--@Occupation
    public function occupation_get()
    {
        $dataOcc = $this->Data->getOccupation();
        if($dataOcc === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $dataOcc
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

    //--department
    public function department_get()
    {
        $department = $this->Data->getDepartment();
        if($department === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $department
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

    //--ministry
    public function ministry_get()
    {
        $ministry = $this->Data->getMinistry();
        if($ministry === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $ministry
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }


    //--Food Type
    public function food_type_get()
    {
        $food_type = $this->Data->getFoodType();
        if($food_type === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $food_type
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }


    //--Banks
    public function bank_get()
    {
        $food_type = $this->Data->getBank();
        if($food_type === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $food_type
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

}