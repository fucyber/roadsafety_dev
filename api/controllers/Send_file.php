<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

/**
 * https://github.com/PHPMailer/PHPMailer/wiki/SMTP-Debugging
 * 0=Disable, 1=client message, 2=server resp , 3, 4
 */
class Send_file extends REST_Controller {

    //----@
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Barcode_model', 'Barcode');
        $this->load->model('Invoice_model', 'Invoice');
        $this->load->model('Sendmail_model', 'Sendmail');
        $this->load->model('Event_register_model', 'Register');
    }
    //----@
    public function send_pay_in_slip_post()
    {
        $event_id    = (int)$this->uri->segment(2);
        $customer_id = (int)$this->uri->segment(4);
        $slip = $this->Invoice->queryInvoice($customer_id, $event_id);
        $send_mail = !empty($this->input->post('send_mail')) ? $this->input->post('send_mail') : 'no';
        if(empty($slip)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('HTTP_NO_CONTENT'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            return;
        }
        #----Create invoice PDF by API
        $url = config('inside_api_url').'event/'.$event_id.'/pay-in-slip/'.$customer_id;
        $result = $this->API->get($url);
        if($send_mail === 'yes' && $result['status'] === TRUE){
            $filename = $result['data']['filename'];
            #------Send invoice to email
            $customer = $this->Customer->queryProfile($customer_id);
            if(!empty($customer->customer_email)){
                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;
                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;
                $sendmail = $this->Sendmail->sendInVoice($customer->customer_email, $params);
                if($sendmail){
                    //--Updated send status
                    $this->Invoice->setSendmailStatus($event_id, $customer_id, '1');
                }
            }
        }
        printr($result);
        return;
    }
    //----@
    private function __getCustomerByInvoiceId($invoice_id)
    {
       $this->db->select('C.customer_id,customer_code');
       $this->db->select('C.customer_name_title,C.customer_firstname,C.customer_lastname,C.customer_nickname,C.customer_email');
       $this->db->select('I.*');
       $this->db->from('customers C');
       $this->db->join('event_invoices I', 'I.customer_id=C.customer_id', 'LEFT');
       $this->db->where('I.status !=', 'discard');
       $this->db->where('I.invoice_id', $invoice_id);
       $query  = $this->db->get();
       if($query->num_rows() === 0)
        return FALSE;
        return $query->result();
    }

    //----@
    public function send_receipt_post()
    {
        $event_id    = (int)$this->uri->segment(2);
        $customer_id = (int)$this->uri->segment(4);
        $inputs = $this->input->get();
        $send_mail = !empty($this->input->post('send_mail')) ? $this->input->post('send_mail') : 'no';
        #-- Create Receipt data
        $receipt_id = $this->Invoice->createReceiptDetail($event_id, $customer_id);
        if(!is_numeric($receipt_id)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        #------Query data
        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);
        if(!$invoice){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        #-----Assign data
        $params['company_logo']          = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'organizer_logo');
        $params['company_name']          = invoice_setting($event_id, 'organizer_name');
        $params['company_address']       = invoice_setting($event_id, 'organizer_address');
        $params['company_tel_fax']       = 'โทร. '.invoice_setting($event_id, 'organizer_tel').', โทรสาร '.invoice_setting($event_id, 'organizer_fax');
        $params['company_website']       = 'Website: '.invoice_setting($event_id, 'organizer_website');
        $params['company_email']         = 'Email: '.invoice_setting($event_id, 'organizer_email');
        #-----
        $params['event_title']           = invoice_setting($event_id, 'invoice_title');
        $params['event_sub_title']       = invoice_setting($event_id, 'invoice_sub_title');
        #-----
        $params['customer_fullname']     = $invoice->customer_name_title.' '.$invoice->customer_firstname.' '.$invoice->customer_lastname;
        $params['customer_bill_address'] = prettyInvoiceAddress($invoice->billing_id);
        #-----getReceiptNameType($customer_id)
        $ReceiptNameType = getReceiptNameType($customer_id);
        switch ($ReceiptNameType) {
            case 'personal':
                $params['customer_agency'] = $params['customer_fullname'];
                break;
            default:
                $params['customer_agency'] = getAgencyName($customer_id);
                break;
        }
        #-----
        $taxNo = getTaxNo($customer_id);
        $params['customer_agency_tax_no'] = !empty($taxNo) ? 'เลขประจำตัวผู้เสียภาษี '.$taxNo : '';
        $params['receipt_no']           =  getReceiptNo($event_id, $customer_id);
        $params['receipt_date']         =  getPaymentTime($event_id, $customer_id);
        $params['authorized_signature'] = invoice_setting($event_id, 'receipt_signature');
        #-----
        $params['invoice_ref_1']        = $invoice->invoice_ref_1;
        $params['invoice_ref_2']        = $invoice->invoice_ref_2;
        $params['banck_company_code']   = invoice_setting($event_id, 'invoice_bank_company_code');
        $params['bank_logo']            = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'invoice_bank_logo');
        $params['invoice_remark']       = invoice_setting($event_id, 'invoice_remark');
        $params['invoice_ref_1_barcode'] = $this->Barcode->barcode($params['invoice_ref_1']);
        $params['invoice_ref_2_barcode'] = $this->Barcode->barcode($params['invoice_ref_2']);
        $params['company_code_barcode']  = $this->Barcode->barcode($params['banck_company_code']);
        #-----
        $params['watermark_bg'] = config('inside_upload_dir').'watermark_copy.png';
        #-----
        $event_price               = $this->Register->getEventPrice($event_id);
        $session_total_price       = $this->Register->getTotalSessionPrice($event_id);
        $params['event_registers'] = $this->Register->getGroupParticipant($event_id, $customer_id);
        (int)$event_qty            = count($params['event_registers']);
        $event_unit_price          = !empty($event_price) ? $event_price : $session_total_price;
        $params['invoice_items'][] = array(
          'item_no'          =>  1,
          'item_description' =>  'ค่าลงทะเบียน'.invoice_setting($event_id, 'invoice_title'),
          'item_qty'         =>  $event_qty,
          'item_price'       =>  number_format($event_unit_price, 0),
          'item_amount'      =>  number_format($event_qty * $event_unit_price, 0)
        );
        $invoice_grand_total_price = number_format($invoice->invoice_grand_totel, 0);
        $params['invoice_grand_total']        = $invoice_grand_total_price;
        $params['invoice_grand_total_string'] = numberToWordsThai($invoice_grand_total_price).'บาทถ้วน';
        #--- Create Invoice----------
        $filename = '';
        ini_set('memory_limit','64M');
        try {
          $pdf   = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 10, 10));
          ob_start();
          $pdf->setDefaultFont("THSarabun");
          $params['title'] = '';
          $content   = $this->load->view('invoice_template/receipt_template', $params, TRUE);
          $pdf->writeHTML($content);
          $filename      = 'receipt'.'_'.$event_id.'_'.$customer_id.'_'.$params['invoice_ref_1'].'.pdf';
          $pdfFilePath   = config('inside_upload_dir')."event_invoices/".$filename;
          ob_clean();
          $pdf->output($pdfFilePath, 'F');

          //----copy
          $pdf_copy   = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 10, 10));
          ob_start();
          $pdf_copy->setDefaultFont("THSarabun");
          $params['title'] = '';
          $content_copy   = $this->load->view('invoice_template/receipt_template_copy', $params, TRUE);
          $pdf_copy->writeHTML($content_copy);
          $filename_copy      = 'copy_receipt'.'_'.$event_id.'_'.$customer_id.'_'.$params['invoice_ref_1'].'.pdf';
          $pdfFilePath_copy   = config('inside_upload_dir')."event_invoices/".$filename_copy;
          ob_clean();
          $pdf_copy->output($pdfFilePath_copy, 'F');

        }catch(Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            $result_execute['message'] = $formatter->getHtmlMessage();
        }
        #---------------
        if(!empty($filename)){
            $this->Invoice->updateReceiptFilename($event_id, $customer_id, $filename, $filename_copy);
            #------Send email
            if($send_mail === 'yes'){
                $customer = $this->Customer->queryProfile($customer_id);
                $this->load->model('Sendmail_model', 'Sendmail');
                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;
                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename_copy;
                $sendmail = $this->Sendmail->sendReceiptSlip($customer->customer_email, $params);
                if($sendmail)
                    $this->Invoice->setReceiptSendmailStatus($receipt_id, '1');
            }
        }
        #------
        $result_execute['filename']      = config('upload_url')."event_invoices/".$filename;
        $result_execute['filename_copy'] = config('upload_url')."event_invoices/".$filename_copy;
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_OK,
            config('field_rest_message')       => lang('data_save_success'),
            config('field_rest_data')          => $result_execute
            );
        $this->response($response_data, REST_Controller::HTTP_OK);

    }
}
