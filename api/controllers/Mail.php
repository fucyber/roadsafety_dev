<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
/**
 * https://github.com/PHPMailer/PHPMailer/wiki/SMTP-Debugging
 * 0=Disable ,1=client message, 2=server resp,3,4
 */
class Mail extends CI_Controller {

    private $mail;

    public function __construct()
    {
        parent::__construct();

        $this->mail = new PHPMailer();
        #--Server settings
        $this->mail->SMTPDebug = 1;
        $this->mail->isSMTP();
        $this->mail->Host       = config('mail_account_host');
        $this->mail->SMTPAuth   = true;
        $this->mail->Username   = config('mail_account_username');
        $this->mail->Password   = config('mail_account_password');
        $this->mail->SMTPSecure = 'ssl'; //
        $this->mail->CharSet    = 'UTF-8';
        $this->mail->Port       = 465;

    }

    //---
    public function test()
    {
        $input_email = $this->input->get('email');

        $email = filter_var($input_email, FILTER_VALIDATE_EMAIL) ? $input_email : 'fucyber@gmail.com';
        ob_start();

        #--Recipients
        $this->mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));
        $this->mail->addAddress($email, $mail_params['fullname']);
        $this->mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));

        #--Content
        $this->mail->isHTML(true);
        $this->mail->Subject = 'Mailjet.com from '.$_SERVER['SERVER_ADDR'];
        $this->mail->Body    = 'Test mailjet port 465 ssl';
        $result = !$this->mail->send() ? FALSE : TRUE ;
        ob_end_clean();

        if(!$result){
          echo 'Message could not be sent.<br>';
          echo 'Mailer Error: ' . $this->mail->ErrorInfo;
        }else{
          echo 'Message has been sent';
        }
      }


}