<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class CreatePDF extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Barcode_model', 'Barcode');
        $this->load->model('Invoice_model', 'Invoice');
        $this->load->model('Sendmail_model', 'Sendmail');
        $this->load->model('Event_register_model', 'Register');
    }

    public function index()
    {
        $event_id    = (int)1;
        $customer_id = (int)1;

        #------Query data
        $invoice = $this->Invoice->queryInvoice($customer_id, $event_id);
        #-----
        $get_agency = getCustomerAgency($customer_id);
        if($get_agency){
            $agency_name = $get_agency->agency_name;
        }

        #-----Assign data
        $params['company_logo']          = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'organizer_logo');
        $params['company_name']          = invoice_setting($event_id, 'organizer_name');
        $params['company_address']       = invoice_setting($event_id, 'organizer_address');
        $params['company_tel_fax']       = 'โทร. '.invoice_setting($event_id, 'organizer_tel').', โทรสาร '.invoice_setting($event_id, 'organizer_fax');
        $params['company_website']       = 'Website: '.invoice_setting($event_id, 'organizer_website');
        $params['company_email']         = 'Email: '.invoice_setting($event_id, 'organizer_email');
        #-----
        $params['event_title']           = invoice_setting($event_id, 'invoice_title');
        $params['event_sub_title']       = invoice_setting($event_id, 'invoice_sub_title');
        #-----
        $params['customer_fullname']     = $invoice->customer_name_title.' '.$invoice->customer_firstname.' '.$invoice->customer_lastname;
        $params['customer_bill_address'] = prettyInvoiceAddress($invoice->billing_id);
        $params['customer_agency']       = $agency_name;

        #-----
        $params['invoice_ref_1']  = $invoice->invoice_ref_1;
        $params['invoice_ref_2']  = $invoice->invoice_ref_2;
        $params['bank_code']    = invoice_setting($event_id, 'invoice_bank_company_code');
        $params['bank_company_code']   = 'บจก.กรุงไทย Company Code: '.$params['bank_code'];
        $params['bank_logo']            = config('inside_upload_dir').'event_settings/'.invoice_setting($event_id, 'invoice_bank_logo');
        $params['invoice_remark']       = invoice_setting($event_id, 'invoice_remark');
        $params['invoice_ref_1_barcode'] = $this->Barcode->barcode($params['invoice_ref_1']);
        $params['invoice_ref_2_barcode'] = $this->Barcode->barcode($params['invoice_ref_2']);
        $params['company_code_barcode']  = $this->Barcode->barcode($params['bank_code']);

        #-----
        $event_price               = $this->Register->getEventPrice($event_id);
        $session_total_price       = $this->Register->getTotalSessionPrice($event_id);
        $params['event_registers'] = $this->Register->getGroupParticipant($event_id, $customer_id);
        (int)$event_qty            = count($params['event_registers']);
        $event_unit_price          = !empty($event_price) ? $event_price : $session_total_price;
        $params['invoice_items'][] = array(
          'item_no'          =>  1,
          'item_description' =>  invoice_setting($event_id, 'invoice_title'),
          'item_qty'         =>  $event_qty,
          'item_price'       =>  number_format($event_unit_price, 0),
          'item_amount'      =>  number_format($event_qty * $event_unit_price, 0)
        );
        $invoice_grand_totel_price = number_format($invoice->invoice_grand_totel, 0);
        $params['invoice_grand_totel']        = $invoice_grand_totel_price;
        $params['invoice_grand_totel_string'] = numberToWordsThai($invoice_grand_totel_price).'บาทถ้วน';

        //echo $_SERVER['DOCUMENT_ROOT']."/_uploads/event_invoices/";
        //printr($params); die();
        ini_set('memory_limit','64M');
        try {
            ob_start();
            $html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 10, 10));
            $html2pdf->setDefaultFont("THSarabun");
            $params['title'] = '';
            $content = $this->load->view('invoice_template/pay_in_slip_template', $params, TRUE);
            $html2pdf->writeHTML($content);
            ob_clean();
            $filename    = 'invoice_'.$params['invoice_ref_1'].'_'.$event_id.'-'.$customer_id.'.pdf';
            $pdfFilePath = $_SERVER['DOCUMENT_ROOT']."/_uploads/event_invoices/".$filename;
            $html2pdf->output('pay_in_slip.pdf');

        }catch(Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }

        #--- Create Invoice----------


    }

    public function html2pdf()
    {
        try {
            ob_start();
            include dirname(__FILE__).'/res/exemple05.php';
            $content = ob_get_clean();

            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont("THSarabun");
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->writeHTML($content);
            $html2pdf->output('exemple05.pdf');
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }

    }
}