<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Promotion extends REST_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('Promotion_model', 'Promotion');
    }

    //----
    public function check_code_get()
    {
       $event_id    = $this->uri->segment(2);
       $customer_id = $this->uri->segment(4);
       $inputs   = $this->input->get();
       $code = !empty($inputs['promotion_code']) ? trim($inputs['promotion_code']) : NULL;
       if(empty($code)){
            $response = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('promotion_code_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response, REST_Controller::HTTP_OK);
            return;
       }

       $result_array = $this->Promotion->checkPromotionCode($event_id, $code);
       if(!$result_array){
        $response = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => sprintf(lang('promotion_not_found'), $code),
                config('field_rest_data')          => NULL
             );
            $this->response($response, REST_Controller::HTTP_OK);
            return;
       }

      $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_array
                );
      $this->response($response_data, REST_Controller::HTTP_OK);

    }


}