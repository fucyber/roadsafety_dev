<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
require APPPATH . '/libraries/REST_Controller.php';
class Single_register extends REST_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Event_register_model', 'Event_register');
    }
    //---@
    public function register_profile_post()
    {
        //---
        $params['event_id']    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $params['customer_id'] = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;
        $inputs = $this->input->post();
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---
        if($inputs['register_type'] !== 'single'){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        $params['register_type']    = $inputs['register_type'];
        $params['email']            = !empty($inputs['email']) ? trim($inputs['email']) : NULL;
        $params['name_title']       = !empty($inputs['name_title']) ? trim($inputs['name_title']) : '';
        $params['firstname']        = !empty($inputs['firstname']) ? trim($inputs['firstname']) : '';
        $params['lastname']         = !empty($inputs['lastname']) ? trim($inputs['lastname']) : '';
        $params['gender']           = !empty($inputs['gender']) ? trim($inputs['gender']) : '';
        $params['age']              = !empty($inputs['age']) ? trim($inputs['age']) : '';
        $params['occupation_id']    = !empty($inputs['occupation_id']) ? trim($inputs['occupation_id']) : NULL;
        $params['job_description']  = !empty($inputs['job_description']) ? trim($inputs['job_description']) : '';
        $params['tel']              = !empty($inputs['tel']) ? trim($inputs['tel']) : '';
        $params['mobile']           = !empty($inputs['mobile']) ? trim($inputs['mobile']) : '';
        $params['food_id']          = !empty($inputs['food_id']) ? trim($inputs['food_id']) : NULL;
        $params['promotion_code']   = !empty($inputs['promotion_code']) ? trim($inputs['promotion_code']) : NULL;
        $params['session_data']     = !empty($inputs['session_data']) ? trim($inputs['session_data']) : NULL;
        //-----Check required
        $this->__checkProfileRequiredData($params);
        //----Return custormer_id
        $custormer_id = $this->__singleRegister($params);
        //----
        if($custormer_id === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                config('field_rest_message')       => lang('event_register_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,
            config('field_rest_message')       => lang('event_register_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }
    //-----
    private function __checkProfileRequiredData($params)
    {
        //---Validate required data
        if (empty($params['register_type'])
            || empty($params['email'])
            || empty($params['name_title'])
            || empty($params['firstname'])
            || empty($params['lastname'])
            || empty($params['food_id']))
        {
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---Validate Email
        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_email_address'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---
        if(empty($params['session_data'])){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('event_register_validate_session'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
    }
    //----
    private function __singleRegister($params=array())
    {
        #-- Check have account or/no
        $result_id = $this->Customer->haveAccount($params['email']);
        if(is_numeric($result_id)){
            $customer_id = $this->Event_register->updateCustomerProfile($result_id, $params);
        }else{
            $customer_id = $this->Event_register->insertCustomerProfile($params['event_id'], $params['email'], $params);
        }
        #-- Check registration redundant
        $redundant = $this->Event_register->checkRegisterRedundant($params['customer_id'], $params['event_id']);
        if($redundant > 0) #--เคยลงทะเบียนแล้ว
            return $customer_id;
        #--Save new participant
        $event_id    = $params['event_id'];
        $save_result = $this->Event_register->saveEventParticipant($params['customer_id'], $params['customer_id'], $params['event_id'], $params);
        return ($save_result === TRUE ?  $customer_id : FALSE);
    }
    //---@
    public function register_agency_post()
    {
        //---
        $params['event_id']    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $params['customer_id'] = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;
        $inputs = $this->input->post();
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---เช็คว่าผ่านขึ้นตอนแรกหรือยัง?
        $event_checked = $this->Event_register->checkRegisterRedundant($params['customer_id'], $params['event_id']);
        if($event_checked === 0){ //ไม่ผ่านขึ้นตอนแรก
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('event_register_step_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //----agency
        $inputs = $this->input->post();
        $params['agency_id']         = !empty($inputs['agency_id']) ? trim($inputs['agency_id']) : NULL;
        $params['agency_name']       = !empty($inputs['agency_name']) ? trim($inputs['agency_name']) : '';
        $params['department_id']     = !empty($inputs['department_id']) ? trim($inputs['department_id']) : '';
        $params['ministry_id']       = !empty($inputs['ministry_id']) ? trim($inputs['ministry_id']) : '';
        $params['agency_tel']        = !empty($inputs['agency_tel']) ? trim($inputs['agency_tel']) : '';
        $params['agency_tel_ext']    = !empty($inputs['agency_tel_ext']) ? trim($inputs['agency_tel_ext']) : '';
        $params['agency_fax']        = !empty($inputs['agency_fax']) ? trim($inputs['agency_fax']) : '';
        $params['tax_no']            = !empty($inputs['tax_no']) ? trim($inputs['tax_no']) : '';
        //---Validate required data
        if (empty($params['agency_name'])
            || empty($params['department_id'])
            || empty($params['ministry_id']))
        {
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        $redundant_checked = $this->Customer->checkAgencyRedundant($params['customer_id']);
        if($redundant_checked == 0){
            $result = $this->Event_register->insertAffiliatedAgency($params['customer_id'], $params);
        }else{
            $result = $this->Event_register->updateAffiliatedAgency($params['customer_id'], $params);
        }
        //----
        if($result === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                config('field_rest_message')       => lang('data_save_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,
            config('field_rest_message')       => lang('data_save_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }
    //---@
    public function register_billing_post()
    {
        //---
        $params['event_id']    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $params['customer_id'] = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;
        $inputs = $this->input->post();
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---เช็คว่าผ่านขึ้นตอนแรกหรือยัง?
        $event_checked = $this->Event_register->checkRegisterRedundant($params['customer_id'], $params['event_id']);
        if($event_checked === 0){ //ไม่ผ่านขึ้นตอนแรก
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('event_register_step_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //----Billing
        $inputs = $this->input->post();
        $params['billing_id']              = !empty($inputs['billing_id']) ? trim($inputs['billing_id']) : NULL;
        $params['billing_address']         = !empty($inputs['billing_address']) ? trim($inputs['billing_address']) : '';
        $params['billing_soi']             = !empty($inputs['billing_soi']) ? trim($inputs['billing_soi']) : '';
        $params['billing_road']            = !empty($inputs['billing_road']) ? trim($inputs['billing_road']) : '';
        $params['billing_province_id']     = !empty($inputs['billing_province_id']) ? trim($inputs['billing_province_id']) : '';
        $params['billing_district_id']     = !empty($inputs['billing_district_id']) ? trim($inputs['billing_district_id']) : '';
        $params['billing_sub_district_id'] = !empty($inputs['billing_sub_district_id']) ? trim($inputs['billing_sub_district_id']) : '';
        $params['billing_zipcode']         = !empty($inputs['billing_zipcode']) ? trim($inputs['billing_zipcode']) : '';
        $params['tax_no']                  = !empty($inputs['tax_no']) ? trim($inputs['tax_no']) : '';
        $params['receipt_name_type']       = !empty($inputs['receipt_name_type']) ? trim($inputs['receipt_name_type']) : '';
        //---Validate required data
        if (empty($params['billing_address'])
            || empty($params['billing_province_id'])
            || empty($params['billing_district_id'])
            || empty($params['billing_sub_district_id'])
            || empty($params['billing_zipcode']))
        {
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        $haveAddressId = $this->Customer->haveAddress($params['customer_id']);
        if($haveAddressId === FALSE){
            $bill_id = $this->Event_register->insertBillingAddress($params['customer_id'], $params);
        }else{
            $this->Event_register->updateBillingAddress($params['customer_id'], $params);
            $bill_id = $haveAddressId;
        }
        //----
        if(!is_numeric($bill_id)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                config('field_rest_message')       => lang('data_save_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---Update bill id to invoice
        $this->Invoice->updateBillAddress($params['event_id'], $params['customer_id'], $bill_id);
        #----Create invoice PDF by API
        $url = config('inside_api_url').'event/'.$params['event_id'].'/pay-in-slip/'.$params['customer_id'];
        $invoice = $this->API->get($url);
        if($invoice['status'] === TRUE){
            $filename = $invoice['data']['filename'];
            // $this->Event_register->updateInvoiceFilename($params['event_id'], $params['customer_id'], $filename);
            #------Send invoice to email
            $customer = $this->Customer->queryProfile($params['customer_id']);
            if(!empty($customer->customer_email)){
                $this->load->model('Sendmail_model', 'Sendmail');
                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;
                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;
                $sendmail = $this->Sendmail->sendInVoice($customer->customer_email, $params);
                if($sendmail){
                    $this->load->model('Invoice_model', 'Invoice');
                    $this->Invoice->setSendmailStatus($params['event_id'], $params['customer_id'], '1');
                }
            }
        }
        //----
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,
            config('field_rest_message')       => lang('data_save_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }
}
