<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Group_register extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Event_register_model', 'Event_register');
    }

    //----
    public function test_participant($data)
    {
        printr($data);
        echo 'registers--------------------';
        printr(json_decode($data['registers'], TRUE));
        die();
    }

    //---@
    public function save_participant_post()
    {

        #---
        $event_id     = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id  = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();
        #---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        #-----Dev
        if($inputs['evn'] == 'show_params')
            $this->test_participant($inputs);

        #-----
        if($inputs['register_type'] !== 'group'){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        #-----
        $params['register_type']    = $inputs['register_type'];
        $params['promotion_code']   = !empty($inputs['promotion_code']) ? trim($inputs['promotion_code']) : NULL;
        $params['session_data']     = !empty($inputs['session_data'])  ? json_decode($inputs['session_data'], TRUE) : NULL;
        #----Agency
        $params['agency_id']         = !empty($inputs['agency_id']) ? trim($inputs['agency_id']) : NULL;
        $params['agency_name']       = !empty($inputs['agency_name']) ? trim($inputs['agency_name']) : '';
        $params['department_id']     = !empty($inputs['department_id']) ? trim($inputs['department_id']) : '';
        $params['ministry_id']       = !empty($inputs['ministry_id']) ? trim($inputs['ministry_id']) : '';
        $params['agency_tel']        = !empty($inputs['agency_tel']) ? trim($inputs['agency_tel']) : '';
        $params['agency_tel_ext']    = !empty($inputs['agency_tel_ext']) ? trim($inputs['agency_tel_ext']) : '';
        $params['agency_fax']        = !empty($inputs['agency_fax']) ? trim($inputs['agency_fax']) : '';
        $params['tax_no']            = !empty($inputs['tax_no']) ? trim($inputs['tax_no']) : '';
        #----Billing address
        $params['billing_id']              = !empty($inputs['billing_id']) ? trim($inputs['billing_id']) : NULL;
        $params['billing_address']         = !empty($inputs['billing_address']) ? trim($inputs['billing_address']) : '';
        $params['billing_soi']             = !empty($inputs['billing_soi']) ? trim($inputs['billing_soi']) : '';
        $params['billing_road']            = !empty($inputs['billing_road']) ? trim($inputs['billing_road']) : '';
        $params['billing_province_id']     = !empty($inputs['billing_province_id']) ? trim($inputs['billing_province_id']) : '';
        $params['billing_district_id']     = !empty($inputs['billing_district_id']) ? trim($inputs['billing_district_id']) : '';
        $params['billing_sub_district_id'] = !empty($inputs['billing_sub_district_id']) ? trim($inputs['billing_sub_district_id']) : '';
        $params['billing_zipcode']         = !empty($inputs['billing_zipcode']) ? trim($inputs['billing_zipcode']) : '';
        $params['receipt_name_type']       = !empty($inputs['receipt_name_type']) ? trim($inputs['receipt_name_type']) : '';
        #----
        $registers  = !empty($inputs['registers']) ? json_decode($inputs['registers'], TRUE) : NULL;
        $total_participant    = count($registers);

        #-----Check required
        $this->__checkProfileRequiredData($registers, $params);

        #---Check/Create invoice
        $rs_invoice = $this->Event_register->checkInvoiceRedundant($event_id, $customer_id);
        if($rs_invoice === 0){
            $invoice_id = $this->Event_register->createInvoice($event_id, $customer_id, $total_participant, $params);
        }else{
            $invoice_id = $this->Event_register->updateInvoice($event_id, $customer_id, $total_participant, $params);
        }
        //---

        if(!is_numeric($invoice_id)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                config('field_rest_message')       => lang('event_register_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }


        #--- Create/update user account
        if(is_array($registers)){
            foreach ($registers as $key => $val) {

                if(!filter_var($val['email'], FILTER_VALIDATE_EMAIL))
                    break;
                $user_data['email']           = !empty($val['email']) ? trim($val['email']) : '';
                $user_data['name_title']      = !empty($val['name_title']) ? trim($val['name_title']) : '';
                $user_data['firstname']       = !empty($val['firstname']) ? trim($val['firstname']) : '';
                $user_data['lastname']        = !empty($val['lastname']) ? trim($val['lastname']) : '';
                $user_data['gender']          = !empty($val['gender']) ? trim($val['gender']) : '';
                $user_data['age']             = !empty($val['age']) ? trim($val['age']) : '';
                $user_data['job_description'] = !empty($val['job_description']) ? trim($val['job_description']) : '';
                $user_data['tel']             = !empty($val['tel']) ? trim($val['tel']) : '';
                $user_data['mobile']          = !empty($val['mobile']) ? trim($val['mobile']) : '';
                $user_data['occupation_id']   = !empty($val['occupation_id']) ? trim($val['occupation_id']) : '';
                $user_data['food_id']         = !empty($val['food_id']) ? trim($val['food_id']) : '';

                #-- Check have account
                $result_id = $this->Customer->haveAccount($val['email']);
                if(is_numeric($result_id)){
                    $rs_customer_id = $this->Event_register->updateCustomerProfile($result_id, $user_data);
                }else{
                    $rs_customer_id = $this->Event_register->insertCustomerProfile($event_id, $val['email'], $user_data);
                }

                #-- User Affiliate Agency (ลงข้อมูลสำหรับสมาชิกอื่นที่ไม่ใช่เจ้าของ account)
                if($rs_customer_id !== $customer_id){
                    $redundant_checked = $this->Customer->checkAgencyRedundant($rs_customer_id);
                    if($redundant_checked === 0){
                        $result = $this->Event_register->insertAffiliatedAgency($rs_customer_id, $params);
                    }else{
                        $result = $this->Event_register->updateAffiliatedAgency($rs_customer_id, $params);
                    }
                }

                $rsRegisterRedundant = $this->Event_register->checkRegisterRedundant($rs_customer_id, $event_id);
                if($rsRegisterRedundant === 0){
                    $this->Event_register->insertRegister($event_id, $rs_customer_id, $customer_id, $invoice_id, 'group');
                }

            }#-- foreach
        }#-- if


        #-- Create/update registrant_id agency (ลงข้อมูลสำหรับเจ้าของ account)
        $redundant_checked = $this->Customer->checkAgencyRedundant($customer_id);
        if($redundant_checked === 0){
            $result = $this->Event_register->insertAffiliatedAgency($customer_id, $params);
        }else{
            $result = $this->Event_register->updateAffiliatedAgency($customer_id, $params);
        }

        #---billing info
        if(empty($params['billing_id'])){
            $bill_id = $this->Event_register->insertBillingAddress($customer_id, $params);
        }else{
            $bill_id = $this->Event_register->updateBillingAddress($params['billing_id'], $params);
        }

        if($bill_id){
            $this->Event_register->updateBillId($event_id, $customer_id, $bill_id);
            #----Create invoice PDF by API
            $url = 'event/'.$event_id.'/pay-in-slip/'.$customer_id;
            $api_params['token_key'] = 'guest';
            $invoice = $this->API->get_self($url, $api_params);
            if($invoice['status'] === TRUE){
                $filename = $invoice['data']['filename'];
                $this->Event_register->updateInvoiceFilename($event_id, $customer_id, $filename);
                #------Send invoice to email
                $customer = $this->Customer->queryProfile($customer_id);
                if(!empty($customer->customer_email)){
                    $this->load->model('Sendmail_model', 'Sendmail');
                    $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;
                    $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;
                    $sendmail = $this->Sendmail->sendInVoice($customer->customer_email, $params);
                    if($sendmail){
                        $this->load->model('Invoice_model', 'Invoice');
                        $this->Invoice->setSendmailStatus($event_id, $customer_id, '1');
                    }
                }
            }
        }

        //---Success
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,
            config('field_rest_message')       => lang('event_register_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

    //-----
    private function __checkProfileRequiredData($registers, $params)
    {
        #---Validate required data
        if (empty($registers)
            || empty($params['session_data'])
            || empty($params['agency_name'])
            || empty($params['department_id'])
            || empty($params['ministry_id'])
            || empty($params['billing_address'])
            || empty($params['billing_province_id'])
            || empty($params['billing_district_id'])
            || empty($params['billing_sub_district_id'])
            || empty($params['billing_zipcode']))
        {
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            die();
        }
    }

    //----
    public function edit_register_post()
    {
        //---
        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $data['customer_name_title'] = !empty($inputs['name_title']) ? trim($inputs['name_title']) : '';
        $data['customer_firstname']  = !empty($inputs['firstname']) ? trim($inputs['firstname']) : '';
        $data['customer_lastname']   = !empty($inputs['lastname']) ? trim($inputs['lastname']) : '';
        $data['customer_gender']     = !empty($inputs['gender']) ? trim($inputs['gender']) : '';
        $data['customer_age']        = !empty($inputs['age']) ? trim($inputs['age']) : '';
        $data['job_description']     = !empty($inputs['job_description']) ? trim($inputs['job_description']) : '';
        $data['customer_tel']        = !empty($inputs['tel']) ? trim($inputs['tel']) : '';
        $data['customer_mobile']     = !empty($inputs['mobile']) ? trim($inputs['mobile']) : '';
        $data['food_id']             = !empty($inputs['food_id']) ? trim($inputs['food_id']) : NULL;
        $data['occupation_id']       = !empty($inputs['occupation_id']) ? trim($inputs['occupation_id']) : NULL;

        $result = $this->Event_register->updateGroupRegisterProfile($customer_id, $data);
        if(!$result){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('data_save_failed'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            die();
        }


        //---
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_OK,
            config('field_rest_message')       => lang('data_save_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);

    }


    //-----
    public function delete_register_get()
    {
        //---
        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $result = $this->Event_register->deleteGroupRegisterProfile($event_id, $customer_id);
        if(!$result){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('data_delete_failed'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //---
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_OK,
            config('field_rest_message')       => lang('data_delete_success'),
            config('field_rest_data')          => NULL
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

}