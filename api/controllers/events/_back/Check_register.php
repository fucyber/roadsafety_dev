<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Check_register extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Event_register_model', 'Event_register');
    }

    public function progress_group_child_get()
    {
        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;
        //---
        $register_data = $this->Event_register->groupChildProgress($event_id, $customer_id);
        if(empty($register_data)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //--Success
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $register_data
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);
        return;

    }
    //------------
    public function progress_get()
    {
        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        //---
        $register_data = $this->Event_register->checkRegisterProgress($event_id,$customer_id);
        if(empty($register_data)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //--Success
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $register_data
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);
        return;
    }

    //------------
    public function payment_post()
    {
        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs    = $this->input->post();
        $date      = !empty($inputs['date']) ? trim($inputs['date']) : NULL;
        $time      = !empty($inputs['time']) ? trim($inputs['time']) : NULL;
        $params['bank_id']   = !empty($inputs['bank_id']) ? trim($inputs['bank_id']) : NULL;
        $params['data']      = !empty($inputs['data']) ? trim($inputs['data']) : NULL;

        //---Validate required data
        if (empty($date)
            || empty($time)
            || empty($params['bank_id'])
            || empty($params['data']))
        {
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        //---
        $params['date_time'] = date('Y-m-d', strtotime($date)).' '.$time;

        $savedFileName = $this->Event_register->uploadSlip($customer_id, $event_id, $params);
        if($savedFileName === FALSE){
            $responseData = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                config('field_rest_message')       => lang('event_slip_upload_failed'),
                config('field_rest_data')          => NULL
             );
            $this->response($responseData, REST_Controller::HTTP_OK);
            exit();
        }

        $responseData = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_ACCEPTED,
                config('field_rest_message')       => lang('event_slip_upload_success'),
                config('field_rest_data')          => NULL
                );
        $this->response($responseData, REST_Controller::HTTP_OK);

    }
}