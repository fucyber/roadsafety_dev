<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Stamp extends REST_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('Stamp_model', 'Stamp');
    }

    public function stamp_gift_list_get()
    {
       $event_id    = $this->uri->segment(2);
       $customer_id = $this->uri->segment(4);
      //--
      $stamps = $this->Stamp->getBoothStamps($event_id, $customer_id);
      if(empty($stamps)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('booth_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            return;
      }

      $gift_list   = array();
      $stamp_list  = array();
      $total_point = 0;
      foreach ($stamps as $key => $val) {
          $stamp_list[$key]['booth_id']     = $val->booth_id;
          $stamp_list[$key]['booth_no']     = $val->booth_no;
          $stamp_list[$key]['booth_name']   = $val->booth_name;
          $stamp_list[$key]['stamp_exist']  = !empty($val->stamp_id) ? TRUE : FALSE;
          !empty($val->stamp_id) ? $total_point++ : FALSE;
      }

      $gifts = $this->Stamp->getGiftList($event_id, $customer_id);
      if(!empty($gifts)){
        foreach ($gifts as $key => $val) {
            $gift_list[$key]['gift_id']      = $val->gift_id;
            $gift_list[$key]['gift_image']   = !empty($val->image) ? config('upload_url').'events/'.$val->image : config('upload_url').'gift_default.png';
            $gift_list[$key]['gift_name']    = $val->name;
            $gift_list[$key]['redeem_point'] = $val->redeem_point;
            $gift_list[$key]['gift_balance'] = ($val->amount - $val->redeemed);
            $gift_list[$key]['gift_exist']   = !empty($val->tranction_id) ? TRUE : FALSE;
        }
      }

       $return_data['total_point'] = $this->Stamp->getCustomerBalancePoint($event_id, $customer_id);
       $return_data['stamp_list']  = $stamp_list;
       $return_data['gift_list']   = $gift_list;
       $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $return_data
                );
      $this->response($response_data, REST_Controller::HTTP_OK);

    }


    public function gift_redeem_get()
    {
      $event_id    = $this->uri->segment(2);
      $customer_id = $this->uri->segment(4);
      $inputs       = $this->input->get();
      $gift_id      = !empty($inputs['gift_id']) ? $inputs['gift_id'] : NULL;
      $redeem_point = !empty($inputs['redeem_point']) ? $inputs['redeem_point'] : NULL;
      if(empty($gift_id) || empty($redeem_point)){
          $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
              config('field_rest_message')       => lang('require_content'),
              config('field_rest_data')          => NULL
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }

      /*$checked = $this->Stamp->checkIsRedeem($event_id, $gift_id, $customer_id);
      if($checked > 0){
          $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
              config('field_rest_message')       => lang('redeem_redeem_used'),
              config('field_rest_data')          => NULL
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }*/

      $gift = $this->Stamp->getGiftById($event_id, $gift_id);
      if($gift->redeemed >= $gift->amount){
        $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_NOT_FOUND,
              config('field_rest_message')       => lang('gift_soldout'),
              config('field_rest_data')          => NULL
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }

      $customer_balance_point = $this->Stamp->getCustomerBalancePoint($event_id, $customer_id);
      if($customer_balance_point < $gift->redeem_point){
        $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
              config('field_rest_message')       => sprintf(lang('redeem_point_not_enough'), $customer_balance_point),
              config('field_rest_data')          => NULL
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }

      $save_redeem = $this->Stamp->saveGiftRedeem($event_id, $gift_id, $customer_id, $redeem_point);
      if(!$save_redeem){
        $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
              config('field_rest_message')       => lang('system_error'),
              config('field_rest_data')          => NULL
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }

      $result = array(
        'redeem_code'  => $save_redeem,
        'redeem_point' => $redeem_point,
        'message'      => "คุณได้รับ ".$gift->name,
        'remark'       => "ติดต่อรับของรางวัลที่บูธฝ่ายประชาสัมพันธ์",
      );
      //---
      $response_data = array(
          config('field_rest_status')        => TRUE,
          config('field_rest_status_code')   => REST_Controller::HTTP_OK,
          config('field_rest_message')       => lang('request_succeeded'),
          config('field_rest_data')          => $result
       );
      $this->response($response_data, REST_Controller::HTTP_OK);
      exit();

    }
}