<?php

defined('BASEPATH') OR exit('No direct script access allowed');


date_default_timezone_set('Asia/Bangkok');

require APPPATH . '/libraries/REST_Controller.php';



class Group_register extends REST_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('Event_model', 'Event');

        $this->load->model('Customer_model', 'Customer');

        $this->load->model('Event_register_model', 'Event_register');

        $this->load->model('Group_register_model', 'Group');

        $this->load->model('Promotion_model', 'Promotion');

    }



    //---@

    public function save_all_data_post()

    {

        #---

        $event_id     = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $registrant_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();

        #---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }

        #--- Check group type

        if($inputs['register_type'] !== 'group'){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_type'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        //---ตรวจสอบการลงทะเบียซ้ำจาก Pay in slip/Invoice

        $checked_invoice = $this->Group->checkDuplicateInvoice($event_id, $registrant_id);

        if($checked_invoice > 0){

            $registrant_email = $this->Group->getCustomerEmail($registrant_id);

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => sprintf(lang('event_register_duplicate'), $registrant_email),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #-----

        $params['register_type']    = $inputs['register_type'];

        $params['promotion_code']   = !empty($inputs['promotion_code']) ? trim($inputs['promotion_code']) : NULL;

        $params['session_data']     = !empty($inputs['session_data'])  ? json_decode($inputs['session_data'], TRUE) : NULL;

        #----Agency

        $params['agency_id']         = !empty($inputs['agency_id']) ? trim($inputs['agency_id']) : NULL;

        $params['agency_name']       = !empty($inputs['agency_name']) ? trim($inputs['agency_name']) : '';

        $params['department_id']     = !empty($inputs['department_id']) ? trim($inputs['department_id']) : '';

        $params['ministry_id']       = !empty($inputs['ministry_id']) ? trim($inputs['ministry_id']) : '';

        $params['agency_tel']        = !empty($inputs['agency_tel']) ? trim($inputs['agency_tel']) : '';

        $params['agency_tel_ext']    = !empty($inputs['agency_tel_ext']) ? trim($inputs['agency_tel_ext']) : '';

        $params['agency_fax']        = !empty($inputs['agency_fax']) ? trim($inputs['agency_fax']) : '';

        $params['tax_no']            = !empty($inputs['tax_no']) ? trim($inputs['tax_no']) : '';

        #----Billing address

        $params['billing_id']              = !empty($inputs['billing_id']) ? trim($inputs['billing_id']) : NULL;

        $params['billing_address']         = !empty($inputs['billing_address']) ? trim($inputs['billing_address']) : '';

        $params['billing_soi']             = !empty($inputs['billing_soi']) ? trim($inputs['billing_soi']) : '';

        $params['billing_road']            = !empty($inputs['billing_road']) ? trim($inputs['billing_road']) : '';

        $params['billing_province_id']     = !empty($inputs['billing_province_id']) ? trim($inputs['billing_province_id']) : '';

        $params['billing_district_id']     = !empty($inputs['billing_district_id']) ? trim($inputs['billing_district_id']) : '';

        $params['billing_sub_district_id'] = !empty($inputs['billing_sub_district_id']) ? trim($inputs['billing_sub_district_id']) : '';

        $params['billing_zipcode']         = !empty($inputs['billing_zipcode']) ? trim($inputs['billing_zipcode']) : '';

        $params['receipt_name_type']       = !empty($inputs['receipt_name_type']) ? trim($inputs['receipt_name_type']) : '';

        #----

        $registers_list  = !empty($inputs['registers']) ? json_decode($inputs['registers'], TRUE) : NULL;



        #----Check required data

        $this->__checkProfileRequiredData($registers_list, $params);



        #----Create app user (Register for application login)

        $customer_ids = $this->Group->appRegister($event_id, $registers_list, $params);

        if(!is_array($customer_ids)){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('system_error'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #----Register event

        $registers_event = $this->Group->registerEvent($event_id, $registrant_id, $customer_ids, 'group', 'mobile');

        if(!$registers_event){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('system_error'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        //----Create & Insert invoice data

        $invoice_id = $this->Group->createInvoice($event_id, $registrant_id, $params);

        if(!$invoice_id){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('system_error'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #----Create PDF width API

        $url = config('inside_api_url').'event/'.$event_id.'/pay-in-slip/'.$registrant_id;

        $api_params['token_key'] = 'guest';

        $invoice = $this->API->get($url, $api_params);

        if($invoice['status'] === TRUE){

            $filename = $invoice['data']['filename'];

            //$this->Event_register->updateInvoiceFilename($event_id, $registrant_id, $filename);

            #------Send invoice to email

            $customer = $this->Customer->queryProfile($registrant_id);

            if(!empty($customer->customer_email)){

                $this->load->model('Sendmail_model', 'Sendmail');

                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;

                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;

                $sendmail = $this->Sendmail->sendInVoice($customer->customer_email, $params);

                if($sendmail){

                    $this->load->model('Invoice_model', 'Invoice');

                    $this->Invoice->setSendmailStatus($event_id, $registrant_id, '1');

                }

            }

        }





        #----Success

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,

            config('field_rest_message')       => lang('event_register_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);

    }



    //-----

    private function __checkProfileRequiredData($registers, $params)

    {

        #---Validate required data

        if (empty($registers)

            || empty($params['session_data'])

            || empty($params['agency_name'])

            || empty($params['department_id'])

            || empty($params['ministry_id'])

            || empty($params['billing_address'])

            || empty($params['billing_province_id'])

            || empty($params['billing_district_id'])

            || empty($params['billing_sub_district_id'])

            || empty($params['billing_zipcode']))

        {

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_data'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            die();

        }

    }





    //----

    public function edit_register_post()

    {

        //---

        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;



        $inputs = $this->input->post();

        //---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        $data['customer_name_title'] = !empty($inputs['name_title']) ? trim($inputs['name_title']) : '';

        $data['customer_firstname']  = !empty($inputs['firstname']) ? trim($inputs['firstname']) : '';

        $data['customer_lastname']   = !empty($inputs['lastname']) ? trim($inputs['lastname']) : '';

        $data['customer_gender']     = !empty($inputs['gender']) ? trim($inputs['gender']) : '';

        $data['customer_age']        = !empty($inputs['age']) ? trim($inputs['age']) : '';

        $data['job_description']     = !empty($inputs['job_description']) ? trim($inputs['job_description']) : '';

        $data['customer_tel']        = !empty($inputs['tel']) ? trim($inputs['tel']) : '';

        $data['customer_mobile']     = !empty($inputs['mobile']) ? trim($inputs['mobile']) : '';

        $data['food_id']             = !empty($inputs['food_id']) ? trim($inputs['food_id']) : NULL;

        $data['occupation_id']       = !empty($inputs['occupation_id']) ? trim($inputs['occupation_id']) : NULL;



        $result = $this->Event_register->updateGroupRegisterProfile($customer_id, $data);

        if(!$result){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('data_save_failed'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            die();

        }





        //---

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_OK,

            config('field_rest_message')       => lang('data_save_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);



    }





    //-----

    public function delete_register_get()

    {

        //---

        $event_id    = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $customer_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;



        $inputs = $this->input->get();

        //---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        $result = $this->Event_register->deleteGroupRegisterProfile($event_id, $customer_id);

        if(!$result){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('event_register_delete_failed'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        //---

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_OK,

            config('field_rest_message')       => lang('data_delete_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);

    }





    //---@

    public function save_participant_post()

    {

        #---

        $event_id     = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $registrant_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();



        #---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }

        #--- Check group type

        if($inputs['register_type'] !== 'group'){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_type'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #-----

        $params['register_type']    = $inputs['register_type'];

        $params['promotion_code']   = !empty($inputs['promotion_code']) ? trim($inputs['promotion_code']) : NULL;
        $params['level_id'] = !empty($inputs['level_id']) ? trim($inputs['level_id']) : NULL;
        $params['session_data']     = !empty($inputs['session_data'])  ? json_decode($inputs['session_data'], TRUE) : NULL;
        $registers_list  = !empty($inputs['registers']) ? json_decode($inputs['registers'], TRUE) : NULL;

        #---Validate required data

        if (empty($registers_list) || empty($params['session_data'])){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_data'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            die();

        }



        #----Create app user (Register for application login)

        $customer_ids = $this->Group->createParticipantAccount($event_id, $registers_list, $params['level_id']);

        if(!is_array($customer_ids)){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('system_error'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #----Register event

        $registers_event = $this->Group->registerEvent($event_id, $registrant_id, $customer_ids, 'group', 'mobile');

        if(!$registers_event){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('system_error'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }

        //----

        $this->Group->createInvoiceTwo($event_id, $registrant_id, $params);

        #----Success

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,

            config('field_rest_message')       => lang('event_register_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);

    }



    //---@

    public function save_agency_post()

    {

        #---

        $event_id      = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $registrant_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();

        #---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #----Agency

        $params['agency_id']         = !empty($inputs['agency_id']) ? trim($inputs['agency_id']) : NULL;

        $params['agency_name']       = !empty($inputs['agency_name']) ? trim($inputs['agency_name']) : '';

        $params['department_id']     = !empty($inputs['department_id']) ? trim($inputs['department_id']) : '';

        $params['ministry_id']       = !empty($inputs['ministry_id']) ? trim($inputs['ministry_id']) : '';

        $params['agency_tel']        = !empty($inputs['agency_tel']) ? trim($inputs['agency_tel']) : '';

        $params['agency_tel_ext']    = !empty($inputs['agency_tel_ext']) ? trim($inputs['agency_tel_ext']) : '';

        $params['agency_fax']        = !empty($inputs['agency_fax']) ? trim($inputs['agency_fax']) : '';

        $params['tax_no']            = !empty($inputs['tax_no']) ? trim($inputs['tax_no']) : '';

        #---Validate required data

        if (empty($params['agency_name']) || empty($params['department_id']) || empty($params['ministry_id'])){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_data'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            die();

        }



        $customer_ids = $this->Group->getCustomerRegisterEventIds($event_id, $registrant_id);

        if(empty($customer_ids)){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => 'ผิดพลาด ข้ามขั้นตอน',

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        foreach ($customer_ids as $customer_id) {

            $result = $this->Group->insertAgency($customer_id, $params);

        }



        #----Success

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_OK,

            config('field_rest_message')       => lang('data_save_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);



    }



    //---@

    public function save_bill_post()

    {

        #---

        $event_id      = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;

        $registrant_id = !empty($this->uri->segment(4)) ? (int)$this->uri->segment(4) : NULL;

        $inputs = $this->input->post();

        #---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('event_register_not_allow'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        #----Bill address

        $params['billing_id']              = !empty($inputs['billing_id']) ? trim($inputs['billing_id']) : NULL;

        $params['billing_address']         = !empty($inputs['billing_address']) ? trim($inputs['billing_address']) : '';

        $params['billing_soi']             = !empty($inputs['billing_soi']) ? trim($inputs['billing_soi']) : '';

        $params['billing_road']            = !empty($inputs['billing_road']) ? trim($inputs['billing_road']) : '';

        $params['billing_province_id']     = !empty($inputs['billing_province_id']) ? trim($inputs['billing_province_id']) : '';

        $params['billing_district_id']     = !empty($inputs['billing_district_id']) ? trim($inputs['billing_district_id']) : '';

        $params['billing_sub_district_id'] = !empty($inputs['billing_sub_district_id']) ? trim($inputs['billing_sub_district_id']) : '';

        $params['billing_zipcode']         = !empty($inputs['billing_zipcode']) ? trim($inputs['billing_zipcode']) : '';

        $params['receipt_name_type']       = !empty($inputs['receipt_name_type']) ? trim($inputs['receipt_name_type']) : '';

        #---Validate required data

        if (empty($params['billing_address'])

            || empty($params['billing_province_id'])

            || empty($params['billing_district_id'])

            || empty($params['billing_sub_district_id'])

            || empty($params['billing_zipcode'])){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_data'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            die();

        }



        $customer_ids = $this->Group->getCustomerRegisterEventIds($event_id, $registrant_id);

        if(empty($customer_ids)){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => 'ผิดพลาด ข้ามขั้นตอน',

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        foreach ($customer_ids as $customer_id) {

            $result = $this->Group->insertBillAddress($customer_id, $params);

        }



        $this->Group->updateBillIdForInvoice($event_id, $registrant_id);

        #----Create PDF width API

        $url = config('inside_api_url').'event/'.$event_id.'/pay-in-slip/'.$registrant_id;

        $api_params['token_key'] = 'guest';

        $invoice = $this->API->get($url, $api_params);

        if($invoice['status'] === TRUE){

            $filename = $invoice['data']['filename'];

            //$this->Event_register->updateInvoiceFilename($event_id, $registrant_id, $filename);

            #------Send invoice to email

            $customer = $this->Customer->queryProfile($registrant_id);

            if(!empty($customer->customer_email)){

                $this->load->model('Sendmail_model', 'Sendmail');

                $params['fullname']  = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;

                $params['file_path'] = config('inside_upload_dir').'event_invoices/'.$filename;

                $sendmail = $this->Sendmail->sendInVoice($customer->customer_email, $params);

                if($sendmail){

                    $this->load->model('Invoice_model', 'Invoice');

                    $this->Invoice->setSendmailStatus($event_id, $registrant_id, '1');

                }

            }

        }



        #----Success

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_OK,

            config('field_rest_message')       => lang('data_save_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);



    }



}