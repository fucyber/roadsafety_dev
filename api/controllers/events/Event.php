<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Event extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Event_model', 'Event');
    }

    //-- Get detail
    public function detail_get()
    {
        $event_id = !empty($this->uri->segment(2)) ? (int)$this->uri->segment(2) : NULL;
        if(empty($event_id))
            show_404();

        $event_data = $this->Event->getEventDetail($event_id);
        if(empty($event_data)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //--Success
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $event_data
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);
        return;
    }


    //-- Get event list
    public function list_get()
    {
        $inputs = $this->input->get();
        $param['action']  = !empty($inputs['action']) ?  trim($inputs['action']) : 'coming';
        $param['limit']  = !empty($inputs['limit']) ?  trim($inputs['limit']) : NULL;
        $param['offset'] = !empty($inputs['offset']) ? trim($inputs['offset']) : NULL;

        $event_list = $this->Event->getEventList($param);
        if(empty($event_list)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('event_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //--Success
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $event_list
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);
    }

    public function file_list_get()
    {
      $event_id    = $this->uri->segment(2);

      $response['event_title'] = $this->Event->getEventTitle($event_id);
      $response['description'] = $this->Event->getDocuemntDescription($event_id);
      $response['documents'] = $this->Event->getDocuemntType($event_id);
      // if(empty($description)){
      //     $response_data = array(
      //         config('field_rest_status')        => FALSE,
      //         config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
      //         config('field_rest_message')       => lang('event_not_found'),
      //         config('field_rest_data')          => NULL
      //      );
      //     $this->response($response_data, REST_Controller::HTTP_OK);
      //     exit();
      // }

      //--Success
      $response_data = array(
              config('field_rest_status')        => TRUE,
              config('field_rest_status_code')   => REST_Controller::HTTP_OK,
              config('field_rest_message')       => lang('request_succeeded'),
              config('field_rest_data')          => $response
              );
      $this->response($response_data,  REST_Controller::HTTP_OK);
    }


    public function vdo_list_get(){
       $event_id = $this->uri->segment(2);
       $vdo_list = $this->Event->vdo_list($event_id);
       if(empty($vdo_list)){
           $response_data = array(
               config('field_rest_status')        => FALSE,
               config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
               config('field_rest_message')       => lang('coming_soon'),
               config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
       $response_data = array(
               config('field_rest_status')        => TRUE,
               config('field_rest_status_code')   => REST_Controller::HTTP_OK,
               config('field_rest_message')       => lang('request_succeeded'),
               config('field_rest_data')          => $vdo_list
               );
       $this->response($response_data,  REST_Controller::HTTP_OK);
    }
}
