<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Crontab extends MY_Controller
{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('Customer_model', 'Customer');
    }



    public function send_mail()

    {

      $this->db->select('J.customer_id as mem_id,customer_email,customer_firstname,customer_lastname');

      $this->db->from('cronjob AS J');

      $this->db->join('customers AS C', 'C.customer_id = J.customer_id', 'LEFT');

      $this->db->where('cron_status', 0);

      $this->db->where('forgot_status', 0);

      $this->db->where('type', 'new_member');

      $this->db->or_where('send_status', 0);

      $this->db->limit(10);

      $sql = $this->db->get();



      if($sql->num_rows() > 0){

        $members = $sql->result_array();

        $send_ids = [];

        foreach ($members as  $value) {

          if(filter_var($value['customer_email'], FILTER_VALIDATE_EMAIL)){



            $name = $value['customer_firstname'].' '.$value['customer_lastname'];

            //----

            $mail = new PHPMailer(false);

            #--Server settings

            $mail->SMTPDebug = 0;

            $mail->isSMTP();

            $mail->Host       = config('mail_account_host');

            $mail->SMTPAuth   = true;

            $mail->Username   = config('mail_account_username');

            $mail->Password   = config('mail_account_password');

            $mail->SMTPSecure = config('mail_account_SMTPsecure');

            $mail->CharSet    = 'UTF-8';

            $mail->Port       = config('mail_account_port');

            $mail->setFrom(config('mail_account_reply'), lang('register_mail_name'));

            $mail->addAddress($value['customer_email'], $name);

            $mail->addReplyTo(config('mail_account_reply'), lang('register_mail_name'));





            #--Content

            $mail->isHTML(true);

            $mail->Subject = 'ข้อมูลการเข้าสู่ระบบ Thailand road safety';

            $password = ramdomNumber(6);

            //-----

            $html = '<h4>ข้อมูลการเข้าสู่ระบบ Thailand road safety</h4>';

            $html .='Username = '.$value['customer_email'].'<br>';

            $html .='Password = '.$password;

            $data_arr = array(

              'fname' => $name,

              'html'  => $html

            );

            $data['data_arr'] = $data_arr;

                $mail->Body    = $this->load->view('email_templates/username_password', $data, TRUE);

                if($mail->send()){

                  $this->db->set('cron_status', 1)

                          ->set('send_status', 1)

                          ->set('action_time', date('Y-m-d H:i:s'))

                          ->where('customer_id', $value['mem_id'])

                          ->update('cronjob');

                  $encodePassword = $this->Common->encryptionPassword($password);

                  $this->Customer->updatePassword($value['customer_email'], $encodePassword);

                }

                $send_ids[] = $value['mem_id'];

            }

          }//foreach

            echo '<pre>';
              print_r($send_ids);
            echo '</pre>';

      }else{
        echo 'Empty data.';
      }

    }



}

