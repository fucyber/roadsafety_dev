<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Customer_model', 'Customer');
    }

    //---@
    public function profile_get()
    {
        $token_key = !empty($this->input->get('token_key')) ? $this->input->get('token_key') : NULL;
        //---Not allow gust user
        if($token_key === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('HTTP_UNAUTHORIZED'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $customer_id = !empty($this->uri->segment(3)) ? (int)$this->uri->segment(3) : NULL;
        //-----
        $customer_data = $this->Customer->getProfile($customer_id);
        if($customer_data === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('unauthorized_access'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //---
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_ACCEPTED,
            config('field_rest_message')       => lang('request_succeeded'),
            config('field_rest_data')          => $customer_data
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }


}
