<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

require APPPATH . '/libraries/REST_Controller.php';



class Register extends REST_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('Customer_model', 'Customer');

    }



    //---@

    public function register_post()

    {

        //--

        $inputs = $this->input->post();

        $param['email']            = !empty($inputs['email']) ? trim($inputs['email']) : '';

        $param['password']         = !empty($inputs['password']) ? trim($inputs['password']) : '';

        $param['confirm_password'] = !empty($inputs['confirm_password']) ? trim($inputs['confirm_password']) : '';

        $param['name_title']       = !empty($inputs['name_title']) ? trim($inputs['name_title']) : '';

        $param['firstname']        = !empty($inputs['firstname']) ? trim($inputs['firstname']) : '';

        $param['lastname']         = !empty($inputs['lastname']) ? trim($inputs['lastname']) : '';

        $param['gender']           = !empty($inputs['gender']) ? trim($inputs['gender']) : '';

        $param['age']              = !empty($inputs['age']) ? trim($inputs['age']) : '';

        $param['occupation_id']    = !empty($inputs['occupation_id']) ? trim($inputs['occupation_id']) : '';

        $param['job_description']  = !empty($inputs['job_description']) ? trim($inputs['job_description']) : '';

        $param['tel']              = !empty($inputs['tel']) ? trim($inputs['tel']) : '';

        $param['mobile']           = !empty($inputs['mobile']) ? trim($inputs['mobile']) : '';

        $param['android_token']    = !empty($inputs['android_token']) ? trim($inputs['android_token']) : NULL;

        $param['ios_token']        = !empty($inputs['ios_token']) ? trim($inputs['ios_token']) : NULL;



        //---Required data

        if (empty($param['email'])

            || empty($param['password'])

            || empty($param['confirm_password'])

            || empty($param['name_title'])

            || empty($param['firstname'])

            || empty($param['lastname'])){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_required_data'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        //---Validate Email

        if (!filter_var($param['email'], FILTER_VALIDATE_EMAIL)){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_email_address'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }

        //-----

        if(empty($param['password']) || ($param['password'] !== $param['confirm_password'])){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                config('field_rest_message')       => lang('validate_confirm_password'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        //---Check Duplicate email address

        if(!empty($param['email'])){

            $duplicate_emaill = $this->Customer->checkDuplicateEmailAddress($param['email']);

            if(!$duplicate_emaill){

                $response_data = array(

                    config('field_rest_status')        => FALSE,

                    config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                    config('field_rest_message')       => lang('validate_email_address_duplicate'),

                    config('field_rest_data')          => NULL

                 );

                $this->response($response_data, REST_Controller::HTTP_OK);

                exit();

            }

        }



        #---Insert

        $insert_data['customer_email']       = $param['email'];

        $insert_data['customer_password']    = $this->Common->encryptionPassword($param['password']);

        #----

        $insert_data['customer_name_title']  = $param['name_title'];

        $insert_data['customer_firstname']   = $param['firstname'];

        $insert_data['customer_lastname']    = $param['lastname'];

        $insert_data['customer_gender']      = !empty($param['gender']) ? $param['gender'] : '';

        $insert_data['customer_age']         = !empty($param['age']) ? $param['age'] : '';

        $insert_data['occupation_id']        = !empty($param['occupation_id']) ? $param['occupation_id'] : '';

        $insert_data['job_description']      = !empty($param['job_description']) ? $param['job_description'] : '';

        $insert_data['customer_tel']         = !empty($param['tel']) ? $param['tel'] : '';

        $insert_data['customer_mobile']      = !empty($param['mobile']) ? $param['mobile'] : '';

        #----

        $insert_data['created_dtm']          = date('Y-m-d H:i:s');

        $insert_data['updated_dtm']          = date('Y-m-d H:i:s');

        $insert_data['created_ip']           = $this->input->ip_address();

        if(!empty($param['android_token']))

            $insert_data['android_token']    = $param['android_token'];

        if(!empty($param['ios_token']))

            $insert_data['ios_token']        = $param['ios_token'];

        $execunt_result = $this->Customer->insertAccount($insert_data);

        if($execunt_result === FALSE){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,

                config('field_rest_message')       => lang('register_failed'),

                config('field_rest_data')          => NULL

                );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_CREATED,

            config('field_rest_message')       => lang('register_success'),

            config('field_rest_data')          => NULL

            );

        $this->response($response_data, REST_Controller::HTTP_OK);

    }





}

