<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

require APPPATH . '/libraries/REST_Controller.php';



class Reset_password extends REST_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('Customer_model', 'Customer');

    }



    public function forgot_password_post()

    {



      $inputs = $this->input->post();

      $email  = isset($inputs['email']) ? trim($inputs['email']) : NULL;

      if (empty($email)){

          $response_data = array(

              config('field_rest_status')        => FALSE,

              config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

              config('field_rest_message')       => lang('validate_required_data'),

              config('field_rest_data')          => NULL

           );

          $this->response($response_data, REST_Controller::HTTP_OK);

          exit();

      }



      if($this->Customer->checkDuplicateEmailAddress($email)){

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

            config('field_rest_message')       => lang('forgot_password_failed'),

            config('field_rest_data')          => NULL

        );

        $this->response($response_data, REST_Controller::HTTP_OK);

        exit();

      }





      $params = [];

      //$customer = $this->Customer->getProfileByEmail($email);

      $customer = $this->Customer->getCustomerForForgotPassword($email);

      //printr($customer);

      //return;

      $params['fullname'] = $customer->customer_name_title.' '.$customer->customer_firstname.' '.$customer->customer_lastname;

      //$params['password'] = ramdomNumber(6);

      $token = ramdomString(40).time();

      $params['link'] = config('base_url').'reset_password/'.$customer->customer_id.'/'.$token;



      $save_data['forgot_type'] = 'customer';

      $save_data['ref_id']      = $customer->customer_id;

      $save_data['email']       = $email;

      $save_data['token']       = $token;

      $saveResult = $this->Customer->saveForgotToken($save_data);

      if(!$saveResult){

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

            config('field_rest_message')       => lang('forgot_password_error'),

            config('field_rest_data')          => NULL

        );

        $this->response($response_data, REST_Controller::HTTP_OK);

        exit();

      }



      //--send mail

      $this->load->model('Sendmail_model', 'Sendmail');

      $sendmail = $this->Sendmail->sendForgotPassword($email, $params);

      if(!$sendmail){

        $response_data = array(

            config('field_rest_status')        => TRUE,

            config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

            config('field_rest_message')       => lang('forgot_password_error'),

            config('field_rest_data')          => NULL

        );

        $this->response($response_data, REST_Controller::HTTP_OK);

        exit();

      }



      //$new_password = $this->Common->encryptionPassword($params['password']);

      //$this->Customer->updatePassword($email, $new_password);

      $response_data = array(

          config('field_rest_status')        => TRUE,

          config('field_rest_status_code')   => REST_Controller::HTTP_ACCEPTED,

          config('field_rest_message')       => sprintf(lang('forgot_password_success'), $email),

          config('field_rest_data')          => NULL

      );

      $this->response($response_data, REST_Controller::HTTP_OK);

    }





    public function save_new_password_post()

    {



          $inputs           = $this->input->post();

          $token            = isset($inputs['token']) ? trim($inputs['token']) : NULL;

          $customer_id      = isset($inputs['customer_id']) ? trim($inputs['customer_id']) : NULL;

          $password         = isset($inputs['password']) ? trim($inputs['password']) : NULL;

          //----

          if (empty($token) || empty($token) || empty($customer_id) || empty($password)){

              $response_data = array(

                  config('field_rest_status')        => FALSE,

                  config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

                  config('field_rest_message')       => lang('validate_required_data'),

                  config('field_rest_data')          => NULL

               );

              $this->response($response_data, REST_Controller::HTTP_OK);

              die();

          }



          $haveAccount = $this->Customer->queryProfile($customer_id);

          if(!$haveAccount || empty($haveAccount->customer_email)){

            $response_data = array(

                  config('field_rest_status')        => FALSE,

                  config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                  config('field_rest_message')       => lang('HTTP_UNAUTHORIZED'),

                  config('field_rest_data')          => NULL

               );

              $this->response($response_data, REST_Controller::HTTP_OK);

            die();

          }



          $email  = $haveAccount->customer_email;



          //----

          $newPassword = $this->Common->encryptionPassword($password);

          $updated = $this->Customer->updatePassword($email, $newPassword);

          if(!$updated){

            $response_data = array(

                  config('field_rest_status')        => FALSE,

                  config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                  config('field_rest_message')       => lang('system_error'),

                  config('field_rest_data')          => NULL

               );

              $this->response($response_data, REST_Controller::HTTP_OK);

            die();

          }



          //---

          $updated = $this->Customer->removeForgotToken($email);

          $response_data = array(

              config('field_rest_status')        => TRUE,

              config('field_rest_status_code')   => REST_Controller::HTTP_ACCEPTED,

              config('field_rest_message')       => lang('request_succeeded'),

              config('field_rest_data')          => NULL

          );

          $this->response($response_data, REST_Controller::HTTP_OK);



    }



}