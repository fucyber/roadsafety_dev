<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Customer_model', 'Customer');
    }

    //---@
    public function checked_post()
    {
        //--
        $inputs = $this->input->post();
        $email             = isset($inputs['email']) ? trim($inputs['email']) : NULL;
        $password          = isset($inputs['password']) ? trim($inputs['password']) : NULL;
        $android_token     = isset($inputs['android_token']) ? trim($inputs['android_token']) : NULL;
        $ios_token         = isset($inputs['ios_token']) ? trim($inputs['ios_token']) : NULL;

        //---Validate required data
        if (empty($email) || empty($password)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        #-----
        $customer_id = $this->Customer->checkedLogin($email, $password);
        if($customer_id === FALSE){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('login_failed'),
                config('field_rest_data')          => NULL
                );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        #-----
        if(!empty($android_token) && !empty($ios_token)){
            if(!empty($android_token))
                $update_data['android_token']    = $android_token;
            if(!empty($ios_token))
                $update_data['ios_token']        = $ios_token;
            $this->Customer->updateDeviceToken($customer_id, $update_data);
        }
        #-----
        $login_data = $this->Customer->getProfile($customer_id, 'login');
        $response_data = array(
            config('field_rest_status')        => TRUE,
            config('field_rest_status_code')   => REST_Controller::HTTP_ACCEPTED,
            config('field_rest_message')       => lang('login_success'),
            config('field_rest_data')          => $login_data
            );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

}
