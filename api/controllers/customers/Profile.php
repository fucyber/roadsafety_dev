<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Profile extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
          $this->load->model('Profile_model', 'profile');
    }

    //-----
    public function profile_edit_post()
    {

        $inputs         = $this->input->post();
        $customer_id    = !empty($inputs['customer_id']) ? trim($inputs['customer_id']) : NULL;
        $customer_email = !empty($inputs['customer_email']) ? trim($inputs['customer_email']) : NULL;
        //---Not allow guset user
        if($inputs['token_key'] === config('api_guest_key')){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,
                config('field_rest_message')       => lang('event_register_not_allow'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        if(empty($customer_id) || empty($customer_email) || empty($inputs)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('HTTP_UNAUTHORIZED'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $checked = $this->profile->checkDuplicateEmailWidthOutId($customer_id, $customer_email);
        if($checked > 0){
          $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NOT_ACCEPTABLE,
                config('field_rest_message')       => lang('validate_email_address_duplicate'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        //---Upload profile image
        if(!empty($_FILES["file"]["name"])){

          $temp = explode(".", $_FILES["file"]["name"]);
          $gen_filename = $customer_id.round(microtime(true)).'.'.end($temp);
          preg_match_all('#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si', $gen_filename, $cut_filename);
          $newfilename = $cut_filename[0][0];
          $target_path = config('upload_dir').'customers/'.$newfilename;
          //---
          if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            $profile['customer_picture'] = $newfilename;
          }
        }

        //-----
        $profile['customer_firstname'] = $inputs['customer_firstname'];
        $profile['customer_lastname']  = $inputs['customer_lastname'];
        $profile['customer_gender']    = $inputs['customer_gender'];
        $profile['customer_age']       = !empty($inputs['customer_age']) ? $inputs['customer_age'] : '';
        $profile['customer_mobile']    = !empty($inputs['customer_mobile']) ? $inputs['customer_mobile'] : '';
        $profile['customer_email']     = $inputs['customer_email'];
        $profile['job_description']    = !empty($inputs['job_description']) ? $inputs['job_description'] : '';
        $profile['updated_dtm']        = date('Y-m-d H:i:s');
        $profile['updated_ip']         = $this->input->ip_address();
        //-----
        $profile_edit = $this->profile->profile_edit($customer_id, $profile);
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('data_save_success'),
                config('field_rest_data')          =>TRUE
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);

    }

    //-----
    public function profile_event_get()
    {
         $customer_id=  $this->uri->segment(2);

         $profile_event = $this->profile->profile_event($customer_id);
         if(empty($profile_event)){
             $response_data = array(
                 config('field_rest_status')        => FALSE,
                 config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                 config('field_rest_message')       => lang('event_not_found'),
                  config('field_rest_data')          => NULL
               );
              $this->response($response_data, REST_Controller::HTTP_OK);
              exit();
          }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          =>$profile_event
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);

    }

    //-----
    public function event_detail_get()
    {
         $customer_id  =  $this->uri->segment(2);
         $event_id     =  $this->uri->segment(4);
         $event_detail = $this->profile->event_detail($customer_id,$event_id);
         if(empty($event_detail)){
             $response_data = array(
                 config('field_rest_status')        => FALSE,
                 config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                 config('field_rest_message')       => lang('event_not_found'),
                  config('field_rest_data')          => NULL
               );
              $this->response($response_data, REST_Controller::HTTP_OK);
              exit();
          }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          =>$event_detail
                );
        $this->response($response_data,  REST_Controller::HTTP_OK);
    }

}
