<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Bangkok');

require APPPATH . '/libraries/REST_Controller.php';



class Vote extends REST_Controller

{



    public function __construct()

    {

        parent::__construct();

        $this->load->model('Profile_model', 'profile');

        $this->load->model('Booth_model', 'Booth');

    }





    public function save_vote_post()

    {

      $inputs      = $this->input->post();

      $event_id    = $this->uri->segment(2);

      $booth_id    = !empty($inputs['booth_id']) ? $inputs['booth_id'] : NULL;

      $customer_id = !empty($inputs['customer_id']) ? $inputs['customer_id'] : NULL;

      $vote_point  = !empty($inputs['vote_point']) ? $inputs['vote_point'] : NULL;



      //---Not allow guset user

        if($inputs['token_key'] === config('api_guest_key')){

            $response_data = array(

                config('field_rest_status')        => FALSE,

                config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

                config('field_rest_message')       => lang('unauthorized_access'),

                config('field_rest_data')          => NULL

             );

            $this->response($response_data, REST_Controller::HTTP_OK);

            exit();

        }



      //---Validate required data

      if (empty($event_id) || empty($booth_id) || empty($customer_id) || empty($vote_point))

      {

          $response_data = array(

              config('field_rest_status')        => FALSE,

              config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,

              config('field_rest_message')       => lang('validate_required_data'),

              config('field_rest_data')          => NULL

           );

          $this->response($response_data, REST_Controller::HTTP_OK);

          die();

      }





      $result = $this->Booth->saveVotePoint($event_id, $booth_id, $customer_id, $vote_point);

      switch ($result){

          case '1':

            $response_data = array(

              config('field_rest_status')        => FALSE,

              config('field_rest_status_code')   => REST_Controller::HTTP_UNAUTHORIZED,

              config('field_rest_message')       => lang('vote_redundant'),

               config('field_rest_data')          => NULL

            );

            $this->response($response_data, REST_Controller::HTTP_OK);

            break;

          case '2':

            $response_data = array(

              config('field_rest_status')        => FALSE,

              config('field_rest_status_code')   => REST_Controller::HTTP_SERVICE_UNAVAILABLE,

              config('field_rest_message')       => lang('vote_failed'),

               config('field_rest_data')          => NULL

            );

            $this->response($response_data, REST_Controller::HTTP_OK);

            break;

          default:

            $response_data = array(

                  config('field_rest_status')        => TRUE,

                  config('field_rest_status_code')   => REST_Controller::HTTP_OK,

                  config('field_rest_message')       => lang('vote_success'),

                  config('field_rest_data')          => NULL

                  );

            $this->response($response_data,  REST_Controller::HTTP_OK);

           break;

      }



    }

}

