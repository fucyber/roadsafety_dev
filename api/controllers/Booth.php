<?php  defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Booth extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Booth_model', 'Booth');
        $this->load->model('Stamp_model', 'Stamp');
    }

    //--@
    public function data_list_get()
    {
        $event_id = $this->uri->segment(2);
        $inputs = $this->input->get();
        $filters['limit'] = !empty($inputs['limit']) ? trim($inputs['limit']) : 10;
        $filters['offset'] = !empty($inputs['offset']) ? trim($inputs['offset']) : 0;
        $filters['search'] = !empty($inputs['search']) ? trim($inputs['search']) : NULL;
        $customer_id = !empty($inputs['customer_id']) ? trim($inputs['customer_id']) : NULL;

        $result_obj = $this->Booth->getBooth($event_id, $filters);
        if(!$result_obj){
            $response = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response, REST_Controller::HTTP_OK);
            exit();
        }

        $data = array();
        $booth_url = config('upload_url').'booths/';
        foreach ($result_obj as $key => $val) {
            $data[$key]['type_id']     = $val->type_id;
            $data[$key]['type_code']   = $val->type_code;
            $data[$key]['type_name']   = $val->type_name;
            $data[$key]['booth_id']    = $val->booth_id;
            $data[$key]['booth_no']    = $val->booth_no;
            $data[$key]['booth_name']  = $val->booth_name;
            $data[$key]['image']       = !empty($val->booth_image) ? $booth_url.$val->booth_image : config('upload_url').'road_video_thumbnail.jpg';
            $data[$key]['file']        = !empty($val->booth_file) ? $booth_url.$val->booth_file : '';
            $data[$key]['department']  = !empty($val->department) ? $val->department : '';
            $data[$key]['excerpt']     = !empty($val->booth_excerpt) ? $val->booth_excerpt : '';
            $data[$key]['vote_point'] = $this->Booth->getCustomerVote($event_id, $val->booth_id, $customer_id);
        }

        $response = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $data
                );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    //---
    public function booth_scan_get()
    {
        $event_id    = $this->uri->segment(2);
        $customer_id = $this->uri->segment(4);
        $booth_id    = !empty($this->input->get('booth_id')) ? $this->input->get('booth_id') : NULL;
        if(empty($booth_id)){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_BAD_REQUEST,
                config('field_rest_message')       => lang('validate_required_data'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }



      /*$checkRegistered = $this->Stamp->checkRegistered($event_id, $customer_id);
      if($checkRegistered  === 0){
            $dataFalse = array(
                "scan_status"   => false,
                'title'         => 'ยังไม่ได้ลงทะเบียนอีเวนท์',
                'point_message' => '',
                'booth_name'    => '',
                'booth_file'    => ''
            );
          $response_data = array(
              config('field_rest_status')        => FALSE,
              config('field_rest_status_code')   => REST_Controller::HTTP_NOT_FOUND,
              config('field_rest_message')       => 'ยังไม่ได้ลงทะเบียนอีเวนท์',
              config('field_rest_data')          => $dataFalse
           );
          $this->response($response_data, REST_Controller::HTTP_OK);
          exit();
      }*/

        $booth = $this->Booth->getBoothById($event_id, $booth_id);
        if(!$booth  || empty($booth->booth_file)){
            $response = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response, REST_Controller::HTTP_OK);
            return;
        }

        $dataNot = array(
                "scan_status"   => false,
                'title'         => 'บูธนี้เคยสแกน QR Code แล้ว',
                'point_message' => $booth->booth_point.' คะแนนสะสม',
                'booth_name'    => $booth->booth_name,
                'booth_file'    => ''
            );
        $check_scan = $this->Booth->checkBoothScanStatus($event_id, $booth_id, $customer_id);
        if($check_scan > 0){
            $response = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NOT_ACCEPTABLE,
                config('field_rest_message')       => lang('point_scan_not_acceptable'),
                config('field_rest_data')          => $dataNot
             );
            $this->response($response, REST_Controller::HTTP_OK);
            return;
        }

        $link_file = !empty($booth->booth_file) ? config('upload_url').'booths/'.$booth->booth_file : '';
        $data = array(
                "scan_status"   => true,
                'title'         => 'ยินดีด้วยคุณได้รับคะแนนสะสม',
                'point_message' => $booth->booth_point.' คะแนนสะสม',
                'booth_name'    => $booth->booth_name,
                'booth_file'    => $link_file
            );
        $this->Booth->updateBoothScanStatus($event_id, $booth_id, $customer_id, $booth->booth_point);
        $response = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $data
                );
        $this->response($response, REST_Controller::HTTP_OK);
    }

}
