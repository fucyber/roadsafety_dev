<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Error_controller extends CI_Controller {

    //---@
    private function _json($data = array()){

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    //---@
    public function show_404()
    {
        $response_data = array(
            'result'         => FALSE,
            'status_code'    => 404,
            'status_message' => 'The request Not Found'
            );
        $this->_json($response_data);
    }


}
