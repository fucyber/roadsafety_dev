<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class App_setting extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App_setting_model', 'App_setting');
    }

    //--@Province
    public function preload_get()
    {
        $result_obj = $this->App_setting->event();
        if(!$result_obj){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }
        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_obj
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }
}