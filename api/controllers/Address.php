<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Address extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Address_model', 'Address');
    }

    //--@Province
    public function province_get()
    {
        $result_obj = $this->Address->getProvince();
        if(!$result_obj){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_obj
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

    //--@District
    public function district_get()
    {
        $province_id  = $this->get('province_id');

        $result_obj = $this->Address->getDistrict($province_id);
        if(!$result_obj){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_obj
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }

    //--@Sub district
    public function sub_district_get()
    {
        $district_id  = $this->get('district_id');

        $result_obj = $this->Address->getSubDistrict($district_id);
        if(!$result_obj){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_obj
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }



    //--@zipcode
    public function zipcode_get()
    {
        $district_code  = $this->input->get('district_code');
        $result_obj = $this->Address->getZipcode($district_code);
        if(!$result_obj){
            $response_data = array(
                config('field_rest_status')        => FALSE,
                config('field_rest_status_code')   => REST_Controller::HTTP_NO_CONTENT,
                config('field_rest_message')       => lang('data_not_found'),
                config('field_rest_data')          => NULL
             );
            $this->response($response_data, REST_Controller::HTTP_OK);
            exit();
        }

        $response_data = array(
                config('field_rest_status')        => TRUE,
                config('field_rest_status_code')   => REST_Controller::HTTP_OK,
                config('field_rest_message')       => lang('request_succeeded'),
                config('field_rest_data')          => $result_obj
                );
        $this->response($response_data, REST_Controller::HTTP_OK);
    }
}