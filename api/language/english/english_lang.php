<?php

/*
 * English language
 */
//--@System message
$lang['data_save_success']        = "บันทึกข้อมูลสำเร็จ"; //Data has been successfully saved.
$lang['data_save_failed']         = "บันทึกข้อมูลล้มเหลว กรุณาลองใหม่"; //save data failed.
$lang['data_delete_success']      = "ลบข้อมูลสำเร็จ";
$lang['data_delete_failed']       = "ลบข้อมูลล้มเหลว กรุณาลองใหม่";
$lang['unauthorized_access']      = "ไม่สามารถดำเนินการได้ กรุณาล็อกอินเข้าสู่ระบบ";  //Unauthorized access

//--500
$lang['system_error']             = "HTTP 500 - That's an error";
$lang['data_invalid']             = 'Invalid get data';
$lang['database_timeout']         = 'Get data timeout';

//--200
$lang['request_succeeded']        = 'The request has succeeded.';

//--400
$lang['HTTP_NO_CONTENT']          = 'ไม่มีข้อมูล'; //NO CONTENT
$lang['HTTP_UNAUTHORIZED']        = "การเข้าถึงไม่ได้รับอนุญาต"; //Unauthorized access
$lang['require_content']          = "'Context-param' ไม่สมบูรณ์"; //Require data: The content of element 'context-param' is not complete
$lang['data_not_found']           = 'ไม่พบข้อมูล'; //Data not found.
$lang['parameters_not_found']     = 'พารามิเตอร์ไม่ครบถ้วน';
$lang['require_email_tel']        = "ไม่พบข้อมูลอีเมลเบอร์โทรศัพท์"; //Require email/tel data.
$lang['coming_soon']              = 'Coming soon.'; //NO CONTENT


//--Register
$lang['validate_required_data']           = 'กรุณากรอกข้อมูล(*)ดอกจันทร์ให้ครบถ้วน';
$lang['validate_email_address']           = 'รูปแบบอีเมลไม่ถูกต้อง!';
$lang['validate_email_address_duplicate'] = "อีเมลนี้ผู้ใช้แล้ว กรุณาเลือกใหม่";
$lang['validate_confirm_password']        = "รหัสผ่านไม่ตรงกัน กรุณาตรวจสอบ";
$lang['register_failed']                  = "ลงทะเบียนล้มเหลว กรุณาลองใหม่หรือติดต่อเจ้าหน้าที่";
$lang['register_success']                 = "ลงทะเบียนสำเร็จ ท่านสามารถเข้าสู่ระบบจากอีเมลและรหัสที่ท่านกำหนด";
$lang['validate_required_type']   = "(Group only) เลือกประเภทการลงทะเบียนไม่ถูกต้อง";
//----Email
$lang['register_mail_title'] = "ข้อมูลลงทะเบียน: Thailand road safety";
$lang['register_mail_from']  = "thainhf.org";
$lang['register_mail_name']  = "Thailand road safety";

//--Login
$lang['login_failed']                     = "ข้อมูลล็อกอินไม่ถูกต้อง กรุณาตรวจสอบใหม่";
$lang['login_success']                    = "เข้าสู่ระบบสำเร็จ";

//--Event
$lang['event_not_found']                   = "Coming soon";
$lang['event_register_type_invalid']       = "ระบุประเภทการลงทะเบียนไม่ถูกต้อง";
$lang['event_register_validate_session']   = "กรุณาเลือกวันที่สัมมนา";
$lang['event_register_duplicate']          = "อีเมลนี้ %s เคยลงทะเบียนแล้วไม่สามารถลงทะเบียนซ้ำ";
$lang['event_register_step_failed']        = "ท่านทำขั้นตอนไม่ถูกต้อง กรุณากรอกข้อมูลผู้สมัครก่อน";
$lang['event_register_not_allow']          = "กรุณาล็อกอินเข้าสู่ระบบก่อนลงทะเบียน Event";
$lang['event_register_failed']             = "บันทึกข้อมูลลงทะเบียนไม่สำเร็จ กรุณาตรวจสอบข้อมูลให้ถูกต้อง";
$lang['event_register_success']            = "บันทึกข้อมูลลงทะเบียนสำเร็จ";
$lang['event_slip_upload_failed']          = "บันทึกข้อมูลล้มเหลว กรุณาลองใหม่และตรวจสอบความเร็วอินเตอร์เน็ต";
$lang['event_slip_upload_success']         = "บันทึกข้อมูลสำเร็จ อยู่ระหว่างการตรวจสอบจากเจ้าหน้าที่";
$lang['event_invoice_payment_unpaid']       = "ยังไม่ได้ชำระเงิน";
$lang['event_invoice_payment_paid']         = "อยู่ระหว่างการตรวจสอบข้อมูลจากเจ้าหน้าที่";
$lang['event_invoice_payment_failed']       = "ชำระไม่ครบจำนวน กรุณาตรวจสอบการชำระใหม่";
$lang['event_invoice_payment_approved']     = "ลงทะเบียนสำเร็จ";

$lang['event_register_delete_failed']  = "ไม่พบข้อมูลที่ต้องการลบ";

//--Event mail
$lang['event_invoice_mail_title']     = "ข้อมูลฟอร์มการชำระเงิน (Pay-in-slip)";
$lang['event_receipt_mail_title']     = "ใบเสร็จรับเงิน (RECEIPT)";

//---@
$lang['forgot_password_failed']     = "ไม่พบอีเมลในระบบ กรุณาตรวจสอบใหม่";
$lang['forgot_password_success']    = "กรุณาเปิดอีเมล %s เพื่อกำหนดรหัสผ่านของท่านใหม่";
$lang['forgot_password_mail_title'] = "รหัสผ่านใหม่";
$lang['forgot_password_error']      = "ไม่สามารถขอรหัสผ่านได้ กรุณาติดต่อเจ้าหน้าที่";

//Vote
$lang['vote_redundant']            = 'ท่านโหวตไปแล้ว ไม่สามารถโหวตซ้ำได้!';
$lang['vote_failed']               = 'เกิดข้อผิดพลาด ขออภัยกรุณาลองใหม่!';
$lang['vote_success']              = 'โหวตสำเร็จ';

$lang['booth_not_found']           = "ไม่พบรายชื่อบูธ";
$lang['point_scan_not_acceptable'] = "ท่านได้สแกนไปแล้ว ไม่สามารถสแกนซ้ำได้";

$lang['promotion_code_not_found']  = 'กรุณากรอกรหัสโปรโมชั่นเพื่อตรวจสอบ';
$lang['promotion_not_found']       = "ขออภัย %s รหัสโปรโมชั่นนี้หมดอายุหรือหมดโควตา";
$lang['promotion_discount_message'] = 'ส่วนลดโปรโมชั่น (ลงทะเบียนภายใน %s)';


//--@Redeem
$lang['redeem_point_not_enough']  = 'ขออภัยแต้มของท่านไม่พอแต้มของรางวัลนี้ (ท่านคงเหลือ %s แต้ม)';
$lang['redeem_unavailable']       = 'Redeem is unavailable';
$lang['redeem_redeem_used']       = 'สิทธิพิเศษนี้ถูกใช้หมดแล้ว';
$lang['gift_soldout']             = 'ขออภัย ของรางวัลนี้แลกไปหมดแล้ว';
