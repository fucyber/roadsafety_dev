<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="width=device-width" name="viewport"/>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title><?php echo !empty($mailt_title) ? $mailt_title : '';?></title>
<style>
/* -------------------------------------
        GLOBAL
------------------------------------- */
* {
    margin:0;
    padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img {
    max-width: 100%;
}
.collapse {
    margin:0;
    padding:0;
}
body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%!important;
    height: 100%;
    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
    background: #91d1ea;
}


/* -------------------------------------
        ELEMENTS
------------------------------------- */
a { color: #2BA6CB;}

.btn {
    text-decoration:none;
    border-style:none;
    border:0;
    padding:0;
    margin:20;
    font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;
    font-size:16px;
    line-height:22px;
    font-weight:500;
    color:#ffffff;
    text-align:center;
    text-decoration:none;
    border-radius:4px;
    padding:11px 30px;
    border:1px solid #0068d1;
    display:inline-block;
    background: #0068d1
}

p.callout {
    padding:15px;
    background-color:#ECF8FF;
    margin-bottom: 15px;
}
.callout a {
    font-weight:bold;
    color: #2BA6CB;
}

table.social {
/*  padding:15px; */
    background-color: #ebebeb;

}
.social .soc-btn {
    padding: 3px 7px;
    font-size:12px;
    margin-bottom:10px;
    text-decoration:none;
    color: #FFF;font-weight:bold;
    display:block;
    text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn {
    display:block;
    width:100%;
}

/* -------------------------------------
        HEADER
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* -------------------------------------
        BODY
------------------------------------- */
table.body-wrap { margin-top: 0px; width: 100%;}


/* -------------------------------------
        FOOTER
------------------------------------- */
table.footer-wrap { width: 100%;    clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
    font-size:10px;
    font-weight: bold;

}


/* -------------------------------------
        TYPOGRAPHY
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul {
    margin-bottom: 10px;
    font-weight: normal;
    font-size:14px;
    line-height:1.6;
}

.footer-content{
  color:#666;
  font-size:12px;
  line-height:18px;
  text-align: center;
  margin-top: 10px;
}
/* Odds and ends */

.clear { display: block; clear: both; }


/* -------------------------------------------
        PHONE
        For clients that support media queries.
        Nothing fancy.
-------------------------------------------- */
@media only screen and (max-width: 600px) {

    a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

    div[class="column"] { width: auto!important; float:none!important;}

    table.social div[class="column"] {
        width:auto!important;
    }

}
</style>

    </head>
    <body>
    <div bgcolor="#91d1ea" style="background-color:#91d1ea; width:100%; padding:10px;">
      <div style="max-width: 600px; margin:0 auto;">
        <table class="body-wrap" style="background:#ffffff;">
            <tr>
                <td style="padding:10px;">
                    <h5> เรียน <?php echo $fullname;?> </h5>
                    <p class="lead">
                        ระบบได้บันทึกข้อมูลการลงทะเบียนของท่านเรียบร้อยแล้ว กรุณาดาวน์โหลดแบบฟอร์มการชำระเงิน (Pay-in-Slip) ตามไฟล์แนบ
                        สามารถชำระเงินได้ที่ธนาคารกรุงไทย จำกัด ทุกสาขาทั่วประเทศ (ค่าบริการโอนเงิน25บาท/1รายการ ถ้าจำนวนเงินเกิน50,000 คิดอัตราร้อยละ0.1 แต่ไม่เกิน1,000บาท)
                        ชำระเงินเรียบร้อยแล้วกรุณาแจ้งการโอนเงินพร้อมแนบ Pay-in-Slip
                    </p>
                </td>
            </tr>
        </table>
        <table class="footer-wrap" style="background:#ffffff;">
            <tr>
                <td class="footer-content" style="padding:10px 0; ">
                  <hr style="margin:10px 0; border-top: 1px solid #ccc;">
                    <strong>มูลนิธินโยบายถนนปลอดภัย</strong><br>
                    สัมมนาวิชาการระดับชาติ เรื่อง ความปลอดภัยทางถนน ครั้งที่ 13<br>
                    407-408 อาคารพร้อมพันธุ์ 2 ซอยลาดพร้าว 3 แขวงจอมพล  เขตจตุจักร  กรุงเทพฯ 10900<br>
                    เบอร์โทรศัพท์: 02-938-8490 แฟกซ์: 02-938-8827<br>
                    เว็บไซต์: http://www.roadsafetythai.org<br>
                </td>
            </tr>
        </table>
      </div>
      </div>
    </body>
</html>