<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <table style="margin-bottom: 15px;width: 100%;">
        <tr>
            <td valign="top">
                <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
                    <tr>
                        <td valign="top">
                            <img src="<?php echo $company_logo;?>" width="70" style="float: left; display: block; padding:0px 5px 0px 0px;">
                        </td>
                        <td valign="top">
                            <h1 style="font-size:10pt; font-weight: bold; padding:0px; margin:0px;">
                                <?php echo $company_name;?>
                            </h1>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_address;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_tel_fax;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_website;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_email;?></p>
                        </td>
                    </tr>
                </table>
            </td>
            <td  valign="top" align="right">
                <table class="form-name" style="width:250px;text-align: center; float: right;">
                    <tr>
                        <td  style="padding-top:10px; padding-bottom:10px;">
                            <h1 class="form-name-title">ใบเสร็จรับเงิน</h1>
                            <h1 class="form-name-sub-title">(RECEIPT)</h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table  style="border:1px;border-color:#808080;border-style:solid;width: 100%;" cellspacing="0">
        <tr>
            <td valign="top" colspan="5">
                <table style="width:100%;border-collapse: collapse;">
                    <tr>
                    <td valign="top" style="padding:5px;">
                        <p style="font-weight: bold; font-size:8pt;padding-bottom:6px;">ได้รับเงินจาก</p>
                        <p style="font-size:9pt;padding-bottom:10px;"><?php echo $customer_agency;?></p><br>
                        <p style="font-size:8pt;padding-bottom:6px;"><?php echo $customer_bill_address;?></p>
                        <p style="font-size:8pt;padding-bottom:6px;"><?php echo $customer_agency_tax_no;?></p>
                    </td>
                    <td style="width:250px;padding:0px;border-left:1px;border-color:#808080;border-style:solid;">
                        <table style="border-collapse: collapse;">
                            <tr>
                                <td style="padding:0px;border-bottom:1px;border-color:#808080;border-style:solid;" colspan="2">
                                    <table style="border-collapse: collapse;">
                                        <tr>
                                            <td style="padding:3px;">
                                                <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">No.</h2>
                                                <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">เลขที่</p>
                                            </td>
                                            <td style="padding:3px;">
                                                <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $receipt_date;?></h1></td>
                                            <td style="text-align:right;padding: 3px 8px; border-collapse: collapse; border: none;">
                                                <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $receipt_no;?></h1></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="border-bottom:1px;border-color:#808080;border-style:solid;">
                                <td style="width:50%; padding: 3px; border-collapse: collapse; border: none;">
                                    <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 1</h2>
                                    <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสชำระ</p>
                                </td>
                                <td style="text-align:right;width:50%; padding: 3px 8px; border-collapse: collapse; border: none;">
                                    <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_1;?></h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:50%; padding: 3px;">
                                    <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 2</h2>
                                    <p style="line-height:8pt;font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสบุคคล/กลุ่ม</p>
                                </td>
                                <td style="text-align:right;width:50%; padding: 3px 8px;">
                                    <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_2;?></h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr style="background: #e0ffff;">
            <td valign="top" colspan="5"  style="border-top:1px;border-bottom:1px;border-color:#808080;border-style:solid;">
                <table style="border-collapse: collapse;">
                    <tr>
                    <td style="width:50px;padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">No.</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">ลำดับ/รหัส</p>
                    </td>
                    <td style="padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px; ">Description</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">รายละเอียด</p>
                    </td>
                    <td style="width:80px; padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Quantity</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">จำนวน</p>
                    </td>
                    <td style="width:80px; padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Price</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">หน่วยละ</p>
                    </td>
                    <td style="width:90px; padding:5px 0px;text-align: center;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Amount</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">จำนวน</p>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td style="padding:0px;" valign="top" colspan="5">
                <table border="0" style="width:100%;border-collapse: collapse;">
                    <?php foreach ($invoice_items as $key => $val): ?>
                    <tr>
                        <td style="width:50px; height:600px; font-size:8pt;text-align: center;padding-top:10;padding-bottom:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val ["item_no"];?>
                        </td>
                        <td style="font-size:8pt;padding-top:10;padding-bottom:10px; padding-left:10;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_description'];?><br><br>
                            <?php
                            $no = 0;
                            foreach ($event_registers as $regis):
                                echo ++$no.'.&nbsp;';
                                echo $regis['customer_name_title'];
                                echo $regis['customer_firstname'].'&nbsp;&nbsp;';
                                echo $regis['customer_lastname'].'<br>';
                            endforeach;
                            ?>
                        </td>
                        <td style="width:80px;font-size:8pt;text-align:center;padding-top:10;padding-bottom:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_qty'];?>
                        </td>
                        <td style="width:80px;font-size:8pt;text-align:right;padding-top:10;padding-bottom:10px;padding-right:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_price'];?>
                        </td>
                        <td style="width:90px;font-size:8pt;text-align:right;padding-top:10;padding-bottom:10px;padding-right:10px;">
                            <?php echo $val['item_amount']; ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </td>
          </tr>
          <tr style="background:#e0ffff;">
            <td valign="top" colspan="5" style="border-top:1px;border-color:#808080;border-style:solid;">
                <table style="width:100%;border-collapse: collapse;">
                    <tr>
                    <td style="padding:10px 0; text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h2 style="font-size: 10pt; font-weight: bold; padding: 0px; margin:0px;">
                            (<?php echo $invoice_grand_totel_string;?>)
                        </h2>
                    </td>
                    <td style="width:160px;padding:5px 8px;text-align: right;border-right:1px;border-color:#808080;border-style:solid;">
                        <h2 style="font-size:8pt; font-weight: bold; padding: 0px; margin:0px;">GRAND TOTAL</h2>
                        <h3 style="color: #686868;font-size: 7pt; padding: 0px; margin:0px;">รวมทั้งสิ้น</h3>
                    </td>
                    <td style="width:90px;padding:10px 8px;text-align: right;">
                        <h2 style="font-size: 9pt;padding-top:10px;"><?php echo $invoice_grand_totel;?></h2>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
    </table>
    <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
        <tr>
            <td style="padding:10px 5px 0 5px;">
                <table>
                    <tr>
                        <td>
                            <h1 style="font-size:7pt; font-weight: normal; padding:0px; margin:0px;">
                                [ &nbsp; ] เงินสด/Cash&nbsp;  [ &nbsp; ] ธนาบัติ/Money Order เลขที่/No.................................ปณ./Post Office..........................
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px 0px;">
                            <h1 style="font-size:7pt; font-weight: normal; padding:4px; margin:0px;">
                                [ &nbsp; ] เช็ค/Cheque ธนาคาร/Bank................................. เลขที่/No.................................ลงวันที่..........................
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px 0px;">
                            <h1 style="font-size:7pt; font-weight: normal; padding:0px; margin:0px;">
                                ในกรณีชำระเงินด้วยเช็ค ใบเสร็จจะสมบูรณ์เมื่อเก็บเงินได้แล้ว
                            </h1>
                            <h1 style="font-size:7pt; font-weight: normal; padding:0px; margin:0px;">
                                In the case of cheque payment this receipt will be effective when the cheque has been cleared by the bank.
                            </h1>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="text-align:right;padding:0px;" valign="bottom">
                <table style="width:300px;margin-top:5px;padding:0px;border:1px;border-color:#686868;border-style:solid;">
                    <tr>
                        <td style="text-align: left;height:80px;padding:5px;">
                            <span style="color:#686868;font-size:10px;">ผู้มีอำนาจลงนาม</span>
                            <br><span style="font-size:12px;font-weight:bold;">Authorized Signature</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" valign="bottom">
                            <span style="color:#686868;font-size:10px;">(<?php echo $authorized_signature;?>)</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>