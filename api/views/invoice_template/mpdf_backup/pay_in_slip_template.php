<style type="text/css">
<!--
table {
    width:  100%;
    padding:0px;
    margin:0px;
}
th{ text-align: center; padding:1px 3px 2px 3px; margin:0px;}

.clear-li ul { padding: 0px; margin: 0px; }
.clear-li ul li { padding-bottom: 1px; font-size:12px; margin: 0px;}
.clear-li ol { padding: 0px; margin: 0px; }
.clear-li ol li { padding-bottom: 1px; font-size:12px; margin: 0px;}
hr{  height: 1px; background: #ccc; }

td { padding:0px; margin:0px; }
h1,h2,h3,h4,h5,h6,p,div{ padding: 0px; margin: 0px; }
.top{
    display: inline-block;
vertical-align: top;
}
.text-center{ text-align: center; }
.text-right{ text-align: right; }
.address{ padding-top:5px; line-height:14px; font-size: 14px; padding:0px; margin:0px; }
.page-header{ font-size:20px; line-height:16px;}
.page-label-box{
    width:100%;
    margin-top: 5px;
    height: 25px;
    border:1px #afafaf solid;
    background: #cceeff;
    border-radius: 4px;
    padding: 10px 10px;
    text-align: center;
}
.color-bg{ background: #cceeff; }
.color-gray{ color: #6d6d6d; }
.page-label-title{
    font-size: 24px;
    font-weight: bold;
}
.event-title{
    line-height:16px;
    font-size: 18px;
}
.ref-title{ line-height:16px; font-size:18px; font-weight:bold; }
.ref-sub-title{ line-height:16px; font-size:12px; color:#6d6d6d; font-weight:bold;}
.ref-result{ line-height:14px; font-size:24px; font-weight:bold; }

.table-header{ font-size:14px;}
.table-header-sub{ font-size:10px; padding: 0px; margin:0px; color:#6d6d6d; }
.box-signature{
    width:100%;
    margin-top: 5px;
    height: 55px;
    border:1px #afafaf solid;
    border-radius: 0px;
    padding: 3px 4px;
}
.box-signature-text{font-size:14px; color:#878787; }
.bar-code-title{ line-height:14pt; padding-left:15px; font-size:16px; font-weight:bold; }
.bar-code-number{ line-height:14pt; padding-left:15px;  font-size:24px; font-weight:bold; }
-->
</style>


<table cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top">
            <table>
                <tr>
                    <td>
                        <img src="<?php echo $company_logo;?>" width="80" style="float:left;">
                    </td>
                    <td style="padding-left:5px;">
                        <h1 class="page-header"><?php echo $company_name;?></h1>
                        <p class="address">
                            <?php echo $company_address;?><br>
                            <?php echo $company_tel_fax;?><br>
                            <?php echo $company_website;?>
                        </p>
                    </td>
                </tr>
            </table>

        </td>
        <td class="top">
            <div class="page-label-box">
                <h1 class="page-label-title">แบบฟอร์มการชำระเงิน</h1>
                <h1 class="page-label-title">(PAY-IN-SLIP)</h1>
            </div>
        </td>
    </tr>
    <tr>
        <td class="text-right" colspan="2" style="padding-top:0px;">
            <h1 class="event-title"><?php echo $event_title;?>
            <br><?php echo $event_sub_title;?></h1>
        </td>
    </tr>
</table>
<table style="border:1xp #afafaf solid;margin-top:5px;" cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top" style="padding:5px;">
            <p style="font-size:16px;font-weight:bold;">รายละเอียดผู้เข้าร่วม</p>
            <p style="padding-top:5px; font-size:12px;"><?php echo $customer_fullname;?></p>
            <p style="padding-top:5px; font-size:12px;"><?php echo $customer_agency;?></p>
            <p style="padding-top:5px; font-size:12px;"><?php echo $customer_bill_address;?></p>
        </td>
        <td>
            <table style="border:0px;padding:0px; margin:0px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:50%;padding:3px;border-left:1px #afafaf solid;">
                        <h2 class="ref-title">Ref 1</h2>
                        <p class="ref-sub-title">รหัสชำระ</p>
                    </td>
                    <td style="text-align:right;width:50%; padding-right: 5px;">
                        <h1 class="ref-result"><?php echo $invoice_ref_1;?></h1>
                    </td>
                </tr>
                <tr>
                    <td style="border-left:1px #afafaf solid;width:50%;padding:3px;border-bottom:1px #afafaf solid; border-top:1px #afafaf solid;">
                        <h2 class="ref-title">Ref 2</h2>
                        <p class="ref-sub-title">รหัสบุคคล/กลุ่ม</p>
                    </td>
                    <td style="text-align:right;width:50%;padding-right:5px;border-bottom:1px #afafaf solid;border-top:1px #afafaf solid;">
                        <h1 class="ref-result"><?php echo $invoice_ref_2;?></h1>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;padding:3px;border-left:1px #afafaf solid;">
                        <h2 class="ref-title">Company Code</h2>
                        <p class="ref-sub-title">เลขที่บัญชี</p>
                    </td>
                    <td style="text-align:right;width:50%;padding-right: 5px;">
                        <h1 class="ref-result"><?php echo $bank_code;?></h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="color-bg">
        <td colspan="2" class="top"  style="border-top:1px #afafaf solid;border-buttom:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:5%;">
                <col style="width:60%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <tr>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">No.</h3>
                        <p class="table-header-sub">ลำดับ</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Description</h3>
                        <p class="table-header-sub">รายละเอียด</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Quantity</h3>
                        <p class="table-header-sub">จำนวน</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Price</h3>
                        <p class="table-header-sub">หน่วยละ</p>
                    </th>
                    <th>
                        <h3 class="table-header">Amount</h3>
                        <p class="table-header-sub">จำนวน</p>
                    </th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="top"  style="border-top:1px #afafaf solid;border-buttom:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:5%;">
                <col style="width:60%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <?php foreach ($invoice_items as $key => $val): ?>
                <tr>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val ["item_no"];?>
                    </td>
                    <td  style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val['item_description'];?>
                    </td>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val['item_qty'];?>
                    </td>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val['item_price'];?>
                    </td>
                    <td class="text-right" style="padding:5px;">
                        <?php echo $val['item_amount']; ?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </td>
    </tr>
    <tr class="color-bg">
        <td class="top" colspan="2"  style="border-top:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:65%;">
                <col style="width:23.32%;">
                <col style="width:11.66%;">
                <tr>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:4px;">
                        <h3 style="font-size: 20px;padding-top:20px;">(<?php echo $invoice_grand_total_string;?>)</h3>
                    </td>
                    <td class="text-right" style="border-right:1px #afafaf solid; padding:4px;">
                        <h2 style="font-size:14px; font-weight: bold;">GRAND TOTAL</h2>
                        <h3 style="font-size:10px;">รวมทั้งสิ้น</h3>
                    </td>
                    <td class="text-right" style="padding:4px;">
                        <h4 style="font-size: 16px; font-weight: bold; padding-top:20px;">
                            <?php echo $invoice_grand_total;?>
                        </h4>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
</table>

<table cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top clear-li">
            <h4 style="margin-top:10px;">รายละเอียดเพิ่มเติม</h4>
            <?php echo $invoice_remark;?>
        </td>
        <td class="top">
            <div class="box-signature">
                <p class="box-signature-text">สำหรับเจ้าหน้าที่ธนาคารประทับตรา และลงนาม</p>
            </div>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" style="margin-top:10px;width:90%;" align="center">
    <col style="width: 33.34%">
    <col style="width: 33.34%">
    <col style="width: 33.34%">
    <tr>
        <td class="top">
            <p class="bar-code-title">รหัสการชำระ (Ref 1)</p>
            <img src="<?php echo $invoice_ref_1_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $invoice_ref_1;?></p>
        </td>
        <td class="top">
            <p class="bar-code-title">รหัสการชำระ (Ref 2)</p>
            <img src="<?php echo $invoice_ref_2_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $invoice_ref_2;?></p>
        </td>
        <td class="top">
            <p class="bar-code-title">เลขที่บัญชี</p>
            <img src="<?php echo $company_code_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $bank_code;?></p>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Barcode นี้สำหรับการลงทะเบียนเท่านั้น ไม่เกี่ยวข้องกับการชำระเงินผ่านทางธนาคาร
        </td>
    </tr>
</table>
<!-- Section 2 -->

<table cellspacing="0" cellpadding="0" style="margin-top:5px;border-top:1px #333 dotted;">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr><td colspan="2" style="height:15px;"></td></tr>
    <tr>
        <td class="top">
            <table>
                <tr>
                    <td>
                        <img src="<?php echo $company_logo;?>" width="80" style="float:left;">
                    </td>
                    <td style="padding-left:5px;">
                        <h1 class="page-header"><?php echo $company_name;?></h1>
                        <p class="address">
                            <?php echo $company_address;?><br>
                            <?php echo $company_tel_fax;?><br>
                            <?php echo $company_website;?>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
        <td class="top">
            <div class="page-label-box">
                <h1 class="page-label-title">แบบฟอร์มการชำระเงิน</h1>
                <h1 class="page-label-title">(PAY-IN-SLIP)</h1>
            </div>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" style="margin-top:5px;">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top">
            <h5>เลขที่บัญชี</h5>
            <p><?php echo $event_title;?></p>
            <table>
                <tr>
                    <td style="width:30px;"><img src="<?php echo $bank_logo;?>" style="padding:0px; width:25px;"></td>
                    <td ><p style="padding-top:5px;">[&nbsp;&nbsp;] <?php echo $bank_company_code;?></p></td>
                </tr>
            </table>
            <h5>ผู้ชำระ</h5>
            <p><?php echo $customer_fullname;?></p>
        </td>
        <td class="top">
            <table style="border:0px;padding:0px; margin:0px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:50%;">
                        <p style="color:#616161;">สาขา</p>
                    </td>
                    <td style="width:50%;">
                        <p style="color:#616161;">วันที่</p>
                    </td>
                </tr>
            </table>
            <table style="border:1xp #afafaf solid; padding:0px; margin:0px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:50%;padding:3px;">
                        <h2 class="ref-title">Ref 1</h2>
                        <p class="ref-sub-title">รหัสชำระ</p>
                    </td>
                    <td style="width:50%; padding-right: 5px;">
                        <h1 class="ref-result"><?php echo $invoice_ref_1;?></h1>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;padding:3px;border-bottom:1px #afafaf solid; border-top:1px #afafaf solid;">
                        <h2 class="ref-title">Ref 2</h2>
                        <p class="ref-sub-title">รหัสบุคคล/กลุ่ม</p>
                    </td>
                    <td style="width:50%;padding-right:5px;border-bottom:1px #afafaf solid;border-top:1px #afafaf solid;">
                        <h1 class="ref-result"><?php echo $invoice_ref_2;?></h1>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;padding:3px;">
                        <h2 class="ref-title">Company Code</h2>
                        <p class="ref-sub-title">ชื่อบัญชี</p>
                    </td>
                    <td style="width:50%;padding-right: 5px;">
                        <h1 class="ref-result"><?php echo $bank_code;?></h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" style="margin-top:5px;border:1xp #afafaf solid;">
    <col style="width: 15%">
    <col style="width: 50%">
    <col style="width: 17.5%">
    <col style="width: 17.5%">
    <tr class="color-bg">
        <th style="border-right:1px #afafaf solid; border-bottom:1px #afafaf solid;"> <h4>DATE</h4> <p class="color-gray">วันที่</p></th>
        <th style="border-right:1px #afafaf solid; border-bottom:1px #afafaf solid;"> <h4>Amount in Words</h4> <p class="color-gray">จำนวนเงินเป็นตัวอักษร</p></th>
        <th style="border-right:1px #afafaf solid; border-bottom:1px #afafaf solid;"> <h4>Amount</h4> <p class="color-gray">จำนวนเงิน</p></th>
        <th style="border-bottom:1px #afafaf solid;"> <h4>Receiver</h4> <p class="color-gray">ผู้รับเงิน</p></th>
    </tr>
    <tr>
        <td class="top text-center" style="border-right:1px #afafaf solid;padding:5px;">

        </td>
        <td class="top text-center" style="border-right:1px #afafaf solid;padding:5px;">
            <h3>(<?php echo $invoice_grand_total_string;?>)</h3>
        </td>
        <td class="top text-center" style="border-right:1px #afafaf solid;padding:5px;">
            <h3><?php echo $invoice_grand_total;?></h3>
        </td>
        <td class="top text-center">

        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" style="margin-top:10px;width:90%;" align="center">
    <col style="width: 33.34%">
    <col style="width: 33.34%">
    <col style="width: 33.34%">
    <tr>
        <td class="top">
            <p class="bar-code-title">รหัสการชำระ (Ref 1)</p>
            <img src="<?php echo $invoice_ref_1_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $invoice_ref_1;?></p>
        </td>
        <td class="top">
            <p class="bar-code-title">รหัสการชำระ (Ref 2)</p>
            <img src="<?php echo $invoice_ref_2_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $invoice_ref_2;?></p>
        </td>
        <td class="top">
            <p class="bar-code-title">เลขที่บัญชี</p>
            <img src="<?php echo $company_code_barcode;?>" style="height:40px;padding:0px;margin:0px;">
            <p class="bar-code-number"><?php echo $bank_code;?></p>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Barcode นี้สำหรับการลงทะเบียนเท่านั้น ไม่เกี่ยวข้องกับการชำระเงินผ่านทางธนาคาร
        </td>
    </tr>
</table>
