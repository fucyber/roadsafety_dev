<style type="text/css">
<!--
table {
    width:  100%;
    padding:0px;
    margin:0px;
}
th{ text-align: center; padding:1px 3px 2px 3px; margin:0px;}

.clear-li ul { padding: 0px; margin: 0px; }
.clear-li ul li { padding-bottom: 1px; font-size:12px; margin: 0px;}
.clear-li ol { padding: 0px; margin: 0px; }
.clear-li ol li { padding-bottom: 1px; font-size:12px; margin: 0px;}
hr{  height: 1px; background: #ccc; }

td { padding:0px; margin:0px; }
h1,h2,h3,h4,h5,h6,p,div{ padding: 0px; margin: 0px; }
.top{
    display: inline-block;
vertical-align: top;
}
.text-center{ text-align: center; }
.text-right{ text-align: right; }
.address{ padding-top:5px; line-height:14px; font-size: 14px; padding:0px; margin:0px; }
.page-header{ font-size:20px; line-height:16px;}
.page-label-box{
    width:100%;
    margin-top: 5px;
    height: 25px;
    border:1px #afafaf solid;
    background: #cceeff;
    border-radius: 4px;
    padding: 10px 10px;
    text-align: center;
}
.color-bg{ background: #cceeff; }
.color-gray{ color: #6d6d6d; }
.page-label-title{
    font-size: 24px;
    font-weight: bold;
}
.event-title{
    line-height:16px;
    font-size: 18px;
}
.ref-title{ line-height:16px; font-size:18px; font-weight:bold; }
.ref-sub-title{ line-height:16px; font-size:12px; color:#6d6d6d; font-weight:bold;}
.ref-result{ line-height:14px; font-size:24px; font-weight:bold; }

.table-header{ font-size:14px;}
.table-header-sub{ font-size:10px; padding: 0px; margin:0px; color:#6d6d6d; }
.box-signature{
    width:100%;
    margin-top: 5px;
    height: 100px;
    border:1px #757575 solid;
    border-radius: 0px;
    padding: 3px 4px;
}
.signature-name{ padding-top: 55px; text-align: center;}
.box-signature-text{ font-size:14px; color:#6d6d6d; }
.bar-code-title{
    line-height:14pt; font-size:16px; font-weight:bold;
}
.bar-code-number{
    line-height:14pt; font-size:24px; font-weight:bold;
}
.text-cash-checked{
    padding-bottom: 2mm;
}
.text-cash-remark{
    padding-bottom: 2mm;
}

.watermark_copy{
    background-image: url(<?php echo $watermark_bg;?>);
    background-position: center center;
    background-repeat: no-repeat;
}
-->
</style>

<!-- Copy -->
<table cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top">
            <table>
                <tr>
                    <td>
                        <img src="<?php echo $company_logo;?>" width="80" style="float:left;">
                    </td>
                    <td style="padding-left:5px;">
                        <h1 class="page-header"><?php echo $company_name;?></h1>
                        <p class="address">
                            <?php echo $company_address;?><br>
                            <?php echo $company_tel_fax;?><br>
                            <?php echo $company_website;?>
                        </p>
                    </td>
                </tr>
            </table>

        </td>
        <td class="top">
            <div class="page-label-box">
                <h1 class="page-label-title">ใบเสร็จรับเงิน</h1>
                <h1 class="page-label-title">(RECEIPT)</h1>
            </div>
        </td>
    </tr>
</table>
<table style="border:1xp #afafaf solid;margin-top:5px;" cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top" style="padding:5px;">
            <p style="font-size:16px;font-weight:bold;">ได้รับเงินจาก</p>
            <p style="padding-top:5px; font-size:12px;"><?php echo $customer_agency;?></p>
            <p style="padding-top:5px; font-size:12px;">
                <?php echo $customer_bill_address;?>
            </p>
            <p style="padding-top:5px; font-size:12px;">
                <?php echo $customer_agency_tax_no;?>
            </p>
        </td>
        <td>
            <table style="border:0px;padding:0px; margin:0px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2" style="width:50%;padding:3px;border-left:1px #afafaf solid;">
                        <table cellspacing="0" cellpadding="0">
                            <col style="width:25%;">
                            <col style="width:20%;">
                            <col style="width:55%;">
                            <tr>
                                <td>
                                    <h2 class="ref-title">No.</h2>
                                    <p class="ref-sub-title">เลขที่</p>
                                </td>
                                <td>
                                    <h1 class="ref-result"><?php echo $receipt_no;?></h1>
                                </td>
                                <td class="text-right" style="padding-right: 5px;">
                                    <h2><?php echo $receipt_date;?></h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-left:1px #afafaf solid;width:50%;padding:3px;border-bottom:1px #afafaf solid; border-top:1px #afafaf solid;">
                        <h2 class="ref-title">Ref 1</h2>
                        <p class="ref-sub-title">รหัสชำระ</p>
                    </td>
                    <td style="text-align:right;width:50%;padding-right:5px;border-bottom:1px #afafaf solid;border-top:1px #afafaf solid;">
                        <h1 class="ref-result"><?php echo $invoice_ref_1;?></h1>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;padding:3px;border-left:1px #afafaf solid;">
                        <h2 class="ref-title">Ref 2</h2>
                        <p class="ref-sub-title">รหัสบุคคล/กลุ่ม</p>
                    </td>
                    <td style="text-align:right;width:50%;padding-right: 5px;">
                        <h1 class="ref-result"><?php echo $invoice_ref_2;?></h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="color-bg">
        <td colspan="2" class="top"  style="border-top:1px #afafaf solid;border-buttom:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:5%;">
                <col style="width:60%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <tr>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">No.</h3>
                        <p class="table-header-sub">ลำดับ</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Description</h3>
                        <p class="table-header-sub">รายละเอียด</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Quantity</h3>
                        <p class="table-header-sub">จำนวน</p>
                    </th>
                    <th style="border-right:1px #afafaf solid;">
                        <h3 class="table-header">Price</h3>
                        <p class="table-header-sub">หน่วยละ</p>
                    </th>
                    <th>
                        <h3 class="table-header">Amount</h3>
                        <p class="table-header-sub">จำนวน</p>
                    </th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="top"  style="border-top:1px #afafaf solid;border-buttom:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:5%;">
                <col style="width:60%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <col style="width:11.66%;">
                <?php foreach ($invoice_items as $key => $val): ?>
                <tr>
                    <td class="text-center" style=" height:600px;border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val ["item_no"];?>
                    </td>
                    <td class="watermark_copy" style="border-right:1px #afafaf solid;padding:5px;">
                        <p><?php echo $val['item_description'];?></p><br>
                        <?php
                            $no = 0;
                            foreach ($event_registers as $regis):
                                echo ++$no.'.&nbsp;';
                                echo $regis['customer_name_title'];
                                echo $regis['customer_firstname'].'&nbsp;&nbsp;';
                                echo $regis['customer_lastname'].'<br>';
                            endforeach;
                        ?>
                    </td>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val['item_qty'];?>
                    </td>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:5px;">
                        <?php echo $val['item_price'];?>
                    </td>
                    <td class="text-right" style="padding:5px;">
                        <?php echo $val['item_amount']; ?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </td>
    </tr>
    <tr class="color-bg">
        <td class="top" colspan="2"  style="border-top:1px #afafaf solid;">
            <table style="width:100%;" cellspacing="0" cellpadding="0">
                <col style="width:65%;">
                <col style="width:23.32%;">
                <col style="width:11.66%;">
                <tr>
                    <td class="text-center" style="border-right:1px #afafaf solid;padding:4px;">
                        <h3 style="font-size: 20px;padding-top:20px;">(<?php echo $invoice_grand_total_string;?>)</h3>
                    </td>
                    <td class="text-right" style="border-right:1px #afafaf solid; padding:4px;">
                        <h2 style="font-size:14px; font-weight: bold;">GRAND TOTAL</h2>
                        <h3 style="font-size:10px;">รวมทั้งสิ้น</h3>
                    </td>
                    <td class="text-right" style="padding:4px;">
                        <h3 style="font-size: 18px;padding-top:20px;">
                            <?php echo $invoice_grand_total;?>
                        </h3>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
</table>

<table cellspacing="0" cellpadding="0">
    <col style="width: 65%">
    <col style="width: 35%">
    <tr>
        <td class="top" style="padding-top:10px;">
            <p class="text-cash-checked">
                [ &nbsp; ] เงินสด/Cash&nbsp;  [ &nbsp; ] ธนาบัติ/Money Order เลขที่/No...........................................
                ปณ./Post Office......................................</p>
            <p class="text-cash-checked">[ &nbsp; ] เช็ค/Cheque ธนาคาร/Bank.....................................
            เลขที่/No......................................ลงวันที่..........................................</p>
            <p style="padding-top:10px;" class="text-cash-remrk">ในกรณีชำระเงินด้วยเช็ค ใบเสร็จจะสมบูรณ์เมื่อเก็บเงินได้แล้ว</p>
            <p class="text-cash-remrk">In the case of cheque payment this receipt will be effective when the cheque has been cleared by the bank.</p>
        </td>
        <td class="top">
            <div class="box-signature">
                <p class="box-signature-text">ผู้มีอำนาจลงนาม</p>
                <p>Authorized Signature</p>

                <p class="signature-name">(<?php echo $authorized_signature;?>)</p>
            </div>
        </td>
    </tr>
</table>
