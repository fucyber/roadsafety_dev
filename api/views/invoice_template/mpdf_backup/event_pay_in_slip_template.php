<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <table style="margin-bottom: 15px;width: 100%;">
        <tr>
            <td valign="top">
                <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
                    <tr>
                        <td valign="top">
                            <img src="<?php echo $company_logo;?>" width="70" style="float: left; display: block; padding:0px 5px 0px 0px;">
                        </td>
                        <td valign="top">
                            <h1 style="font-size:10pt; font-weight: bold; padding:0px; margin:0px;">
                                <?php echo $company_name;?>
                            </h1>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_address;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_tel_fax;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_website;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_email;?></p>
                        </td>
                    </tr>
                </table>
            </td>
            <td  valign="top" align="right">
                <table class="form-name" style="width:250px;text-align: center; float: right;">
                    <tr>
                        <td  style="padding-top:10px; padding-bottom:10px;">
                            <h1 class="form-name-title">แบบฟอร์มการชำระเงิน</h1>
                            <h1 class="form-name-sub-title">(PAY-IN-SLIP)</h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right;" valign="top">
                <h1 class="event_title"><?php echo $event_title;?></h1>
                <h1 class="event_sub_title"><?php echo $event_sub_title;?></h1>
            </td>
        </tr>
    </table>
    <table  style="border:1px;border-color:#808080;border-style:solid;width: 100%;" cellspacing="0">
          <tr>
            <td valign="top" colspan="5">
                <table style="width:100%;border-collapse: collapse;">
                    <tr>
                    <td valign="top" style="padding:5px;">
                        <p style="font-weight: bold; font-size:8pt;margin-bottom:6pt;">รายละเอียดผู้เข้าร่วม</p><br>
                        <p style="font-size:8pt;margin-bottom:6pt;"><?php echo $customer_fullname;?></p>
                        <p style="font-size:8pt;margin-bottom:6pt;"><?php echo $customer_agency;?></p>
                        <p style="font-size:8pt;margin-bottom:6pt;"><?php echo $customer_bill_address;?></p>
                    </td>
                    <td style="width:250px;padding:0px;border-left:1px;border-color:#808080;border-style:solid;">
                        <table style="border-collapse: collapse;">
                            <tr style="border-bottom:1px;border-color:#808080;border-style:solid;">
                                <td style="width:50%; padding: 3px; border-collapse: collapse; border: none;">
                                    <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 1</h2>
                                    <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสชำระ</p>
                                </td>
                                <td style="text-align:right;width:50%; padding: 3px 8px; border-collapse: collapse; border: none;">
                                    <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_1;?></h1>
                                </td>
                            </tr>
                            <tr style="border-bottom:1px;border-color:#808080;border-style:solid;">
                                <td style="width:50%; padding: 3px; border-bottom:1px #5b5b5b solid; ">
                                    <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 2</h2>
                                    <p style="line-height:8pt;font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสบุคคล/กลุ่ม</p>
                                </td>
                                <td style="text-align:right;width:50%; padding: 3px 8px; border-bottom:1px #5b5b5b solid; ">
                                    <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_2;?></h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:50%; padding: 3px; ">
                                    <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Company Code</h2>
                                    <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">เลขที่บัญชี</p>
                                </td>
                                <td style="text-align:right;width:50%; padding: 3px 8px; border-collapse: collapse; border: none;">
                                    <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $banck_company_code;?></h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr style="background: #e0ffff;">
            <td valign="top" colspan="5"  style="border-top:1px;border-bottom:1px;border-color:#808080;border-style:solid;">
                <table style="border-collapse: collapse;">
                    <tr>
                    <td style="width:50px;padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">No.</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">ลำดับ/รหัส</p>
                    </td>
                    <td style="padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px; ">Description</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">รายละเอียด</p>
                    </td>
                    <td style="width:80px; padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Quantity</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">จำนวน</p>
                    </td>
                    <td style="width:80px; padding:5px 0px;text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Price</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">หน่วยละ</p>
                    </td>
                    <td style="width:90px; padding:5px 0px;text-align: center;">
                        <h3 style="font-size:8pt; padding: 0px; margin:0px;">Amount</h3>
                        <p style="font-size:6pt; padding: 0px; margin:0px;color:#6d6d6d;">จำนวน</p>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td style="padding:0px;" valign="top" colspan="5">
                <table border="0" style="width:100%;border-collapse: collapse;">
                    <?php foreach ($invoice_items as $key => $val): ?>
                    <tr>
                        <td style="width:50px; font-size:8pt;text-align: center;padding-top:10;padding-bottom:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val ["item_no"];?>
                        </td>
                        <td style="font-size:8pt; padding-top:10;padding-bottom:10px; padding-left:10;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_description'];?>
                        </td>
                        <td style="width:80px;font-size:8pt;text-align:center;padding-top:10;padding-bottom:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_qty'];?>
                        </td>
                        <td style="width:80px;font-size:8pt;text-align:right;padding-top:10;padding-bottom:10px;padding-right:10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <?php echo $val['item_price'];?>
                        </td>
                        <td style="width:90px;font-size:8pt;text-align:right;padding-top:10;padding-bottom:10px;padding-right:10px;">
                            <?php echo $val['item_amount']; ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </td>
          </tr>
          <tr style="background:#e0ffff;">
            <td valign="top" colspan="5" style="border-top:1px;border-color:#808080;border-style:solid;">
                <table style="width:100%;border-collapse: collapse;">
                    <tr>
                    <td style="padding:10px 0; text-align: center;border-right:1px;border-color:#808080;border-style:solid;">
                        <h2 style="font-size: 10pt; font-weight: bold; padding: 0px; margin:0px;">
                            (<?php echo $invoice_grand_totel_string;?>)
                        </h2>
                    </td>
                    <td style="width:160px;padding:5px 8px;text-align: right;border-right:1px;border-color:#808080;border-style:solid;">
                        <h2 style="font-size:8pt; font-weight: bold; padding: 0px; margin:0px;">GRAND TOTAL</h2>
                        <h3 style="color: #686868;font-size: 7pt; padding: 0px; margin:0px;">รวมทั้งสิ้น</h3>
                    </td>
                    <td style="width:90px;padding:10px 8px;text-align: right;">
                        <h2 style="font-size: 9pt;padding-top:10px;"><?php echo $invoice_grand_totel;?></h2>
                    </td>
                    </tr>
                </table>
            </td>
          </tr>
    </table>
    <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
        <tr>
            <td style="padding:5px 5px 0 5px;">
                <h2 style="font-size: 7pt; font-weight: bold; padding:0px; margin:0px;">
                    รายละเอียดเพิ่มเติม</h2>
                <div style="font-size: 7pt; font-weight: normal; padding:0px; margin:0px;">
                    <?php echo $invoice_remark;?>
                </div>
            </td>
            <td style="text-align: right; float: right;" valign="top">
                <table style="width:250px;margin-top:5px;padding: 3px 0 20px 3px;border:1px;border-color:#686868;border-style:solid;">
                    <tr>
                        <td style="text-align: left;height:30px;">
                            <span style="color:#686868;font-size:10px;">สำหรับเจ้าหน้าที่ธนาคารประทับตรา และลงนาม</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding:5px;"> </td>
        </tr>
        <tr style="border-bottom:1px #333 dotted;">
            <td colspan="2" style="padding:5px;">
                <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
                    <tr>
                        <td style="padding:0px;">
                            <p style="padding-left:100px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;รหัสการชำระ (Ref 1)</p>
                            <p><img src="<?php echo $invoice_ref_1_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding-left:100px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $invoice_ref_1;?></p>
                        </td>
                        <td style="padding:0px;">
                            <p style="padding:0px;margin:0px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;รหัสการชำระ (Ref 2)</p>
                            <p> <img src="<?php echo $invoice_ref_2_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding:0px;margin:0px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $invoice_ref_2;?></p>
                        </td>
                        <td style="padding:0px;">
                            <p style="padding:0px;margin:0px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;เลขที่บัญชี</p>
                            <p><img src="<?php echo $company_code_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding:0px;margin:0px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $banck_company_code;?></p>
                        </td>
                    </tr>
                </table>
                <p style="padding:5px 0px;margin:0px;font-size:9px;font-weight:normal;">
                    Barcode นี้สำหรับการลงทะเบียนเท่านั้น ไม่เกี่ยวข้องกับการชำระเงินผ่านทางธนาคาร</p>
            </td>
        </tr>
    </table>
    <hr style="margin:10px 0;">
    <!-- Bottom -->
        <table style="margin-bottom: 5px;width: 100%;">
        <tr>
            <td valign="top">
                <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
                    <tr>
                        <td valign="top">
                            <img src="<?php echo $company_logo;?>" width="70" style="float: left; display: block; padding:0px 5px 0px 0px;">
                        </td>
                        <td valign="top">
                            <h1 style="font-size:10pt; font-weight: bold; padding:0px; margin:0px;">
                                <?php echo $company_name;?>
                            </h1>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_address;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_tel_fax;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_website;?></p>
                            <p style="font-size: 8pt; padding:0px; margin:0px;"><?php echo $company_email;?></p>
                        </td>
                    </tr>
                </table>
            </td>
            <td  valign="top" align="right">
                <table class="form-name" style="width:250px; text-align: center; float: right;">
                    <tr>
                        <td  style="padding-top:10px; padding-bottom:10px;">
                            <h1 class="form-name-title">แบบฟอร์มการชำระเงิน</h1>
                            <h1 class="form-name-sub-title">(PAY-IN-SLIP)</h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table  cellspacing="0" style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
        <tr>
            <td ></td>
            <td style="width:250px;" valign="top">
                <table cellspacing="0">
                    <tr>
                        <td style="width:50%; padding: 0px;">
                            <p style="font-size:6pt; font-weight:bold; padding:0px; margin:0px;">สาขา</p>
                        </td>
                        <td style="width:50%; padding: 0px;">
                            <p style="font-size:6pt; font-weight:bold; padding:0px; margin:0px;">วันที่</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width:250px;" cellspacing="0">
                    <tr>
                        <td style="width:100%; padding: 0px;">
                            <p style="font-size: 10px; font-weight: bold; padding:0px; margin:0px;">เลขที่บัญชี</p>
                            <p style="font-size: 10px;"><?php echo $event_title;?></p>
                            <p><img src="<?php echo $bank_logo;?>" style="padding: 0px; width:20px;">
                            <span style="font-size: 10px; margin-top: 0px; padding: 15px 0 0 10px;">
                                [&nbsp;&nbsp;] บจก.กรุงไทย Company Code: <?php echo $banck_company_code;?>
                            </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%; padding: 0px;">
                            <h2 style="font-size: 10px; font-weight: bold; padding:10px 0 0 0; margin:0px;">ผู้ชำระ:</h2>
                            <span style="font-size: 9px;padding:0px;"><?php echo $customer_fullname;?></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:250px;" valign="top">
                <table style="width:250px;border-collapse: collapse;border:1px;border-color:#808080;border-style:solid;">
                    <tr style="border-bottom:1px;border-color:#808080;border-style:solid;">
                        <td style="width:50%; padding: 3px; border-collapse: collapse; border: none;">
                            <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 1</h2>
                            <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสชำระ</p>
                        </td>
                        <td style="width:50%; padding: 3px 8px; border-collapse: collapse; border: none;">
                            <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_1;?></h1>
                        </td>
                    </tr>
                    <tr style="border-bottom:1px;border-color:#808080;border-style:solid;">
                        <td style="width:50%; padding: 3px; border-bottom:1px #5b5b5b solid; ">
                            <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Ref 2</h2>
                            <p style="line-height:8pt;font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">รหัสบุคคล/กลุ่ม</p>
                        </td>
                        <td style="width:50%; padding: 3px 8px; border-bottom:1px #5b5b5b solid; ">
                            <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $invoice_ref_2;?></h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; padding: 3px; ">
                            <h2 style="font-size:10pt; font-weight:bold; padding:0px; margin:0px;">Company Code</h2>
                            <p style="font-size:8pt; color:#6d6d6d; font-weight:bold; padding:0px; margin:0px;">เลขที่บัญชี</p>
                        </td>
                        <td style="width:50%; padding: 3px 8px; border-collapse: collapse; border: none;">
                            <h1 style="font-size:11pt; font-weight:bold; padding:0px; margin:0px;"><?php echo $banck_company_code;?></h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="0" style="border:1px;border-color:#808080;border-style:solid;margin-top:5px;">
                    <tr style="background:#e0ffff;">
                        <td style="width:120px;text-align:center;padding:3px 0; border-bottom:1px;border-right:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:0px;margin:0px;">DATE</h1>
                            <p style="color:#6d6d6d;font-weight:bold;font-size:8pt;padding:0px;margin:0px;">วันที่</p>
                        </td>
                        <td style="text-align:center;padding:3px 0; border-bottom:1px;border-right:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:0px;margin:0px;">Amount in Words</h1>
                            <p style="color:#6d6d6d;font-weight:bold;font-size:8pt;padding:0px;margin:0px;">จำนวนเงินเป็นตัวอักษร</p>
                        </td>
                        <td style="width:120px;text-align:center;padding:3px 0; border-bottom:1px;border-right:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:0px;margin:0px;">Amount</h1>
                            <p style="color:#6d6d6d;font-weight:bold;font-size:8pt;padding:0px;margin:0px;">จำนวนเงิน</p>
                        </td>
                        <td style="width:130px;text-align:center;padding:3px 0;border-bottom:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:0px;margin:0px;">Receiver</h1>
                            <p style="color:#6d6d6d;font-weight:bold;font-size:8pt;padding:0px;margin:0px;">ผู้รับเงิน</p>
                        </td>
                    </tr>
                    <tr style="">
                        <td style="text-align:center;padding:3px 0;border-right:1px;border-color:#808080;border-style:solid;">
                        </td>
                        <td style="text-align:left;padding:5px 10px;border-right:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:5px;margin:0px;">
                                (<?php echo $invoice_grand_totel_string;?>)
                            </h1>
                         </td>
                        <td style="text-align:center;padding:5px 0;border-right:1px;border-color:#808080;border-style:solid;">
                            <h1 style="font-weight:bold;font-size:10pt;padding:0px;margin:0px;">
                                <?php echo $invoice_grand_totel;?>
                            </h1>
                         </td>
                        <td style="text-align:center;padding:0px; border:1px #5b5b5b solid;"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 20px;">
                <table style="width: 100%; padding:0px; margin:0px; border-collapse: collapse;">
                    <tr>
                        <td style="padding:0px;">
                            <p style="padding-left:100px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;รหัสการชำระ (Ref 1)</p>
                            <p><img src="<?php echo $invoice_ref_1_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding-left:100px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $invoice_ref_1;?></p>
                        </td>
                        <td style="padding:0px;">
                            <p style="padding:0px;margin:0px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;รหัสการชำระ (Ref 2)</p>
                            <p> <img src="<?php echo $invoice_ref_2_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding:0px;margin:0px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $invoice_ref_2;?></p>
                        </td>
                        <td style="padding:0px;">
                            <p style="padding:0px;margin:0px;font-size:10px;font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;เลขที่บัญชี</p>
                            <p><img src="<?php echo $company_code_barcode;?>" style="height:40px;padding:0px;margin:0px;"></p>
                            <p style="padding:0px;margin:0px;font-size:14px;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo $banck_company_code;?></p>
                        </td>
                    </tr>
                </table>
                <p style="padding:5px 0px;margin:0px;font-size:9px;font-weight:normal;">
                    Barcode นี้สำหรับการลงทะเบียนเท่านั้น ไม่เกี่ยวข้องกับการชำระเงินผ่านทางธนาคาร</p>
            </td>
        </tr>
    </table>


</body>
</html>