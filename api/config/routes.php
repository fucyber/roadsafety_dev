<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']   = 'app_setting/preload'; //welcome
$route['404_override']         = 'MY_exception/show_404';
$route['translate_uri_dashes'] = FALSE;

//---@App setting
$route['app/setting']     = 'app_setting/preload';   //--@Get

//--@Address
$route['province']     = 'address/province';       //--@Get
$route['district']     = 'address/district';       //--@Get
$route['sub_district'] = 'address/sub_district';   //--@Get
$route['zipcode']      = 'address/zipcode';        //--@Get

//--@Master data
$route['occupation']  = 'master_data/occupation';       //--@Get
$route['department']  = 'master_data/department';       //--@Get
$route['ministry']    = 'master_data/ministry';         //--@Get
$route['food-type']   = 'master_data/food_type';        //--@Get
$route['bank']        = 'master_data/bank';             //--@Get

//--@User
$route['register']            = 'customers/register/register';      //--@post
$route['login']               = 'customers/login/checked';          //--@post
$route['user/profile/(:num)'] = 'customers/user/profile';
$route['forgot-password']     = 'customers/reset_password/forgot_password';  //--@post            //--@GET
//--web
$route['web/reset-password']   = 'customers/reset_password/save_new_password'; //POST

//---@Event
$route['event']                                 = 'events/event/list';               //--@GET
//---event/{event_id}
$route['event/(:num)']                          = 'events/event/detail';             //--@GET
//---event/{event_id}/register-profile/{customer_id}
#---Single register
$route['event/(:num)/register-profile/(:num)']  = 'events/single_register/register_profile';  //--@POST
$route['event/(:num)/register-agency/(:num)']   = 'events/single_register/register_agency';   //--@POST
$route['event/(:num)/register-billing/(:num)']  = 'events/single_register/register_billing';  //--@POST
#---Group register
$route['event/(:num)/group-register/(:num)']         = 'events/group_register/save_all_data';  //--@POST
#---Add on
$route['event/(:num)/group-register-profile/(:num)'] = 'events/group_register/save_participant';  //--@POST
$route['event/(:num)/group-register-agency/(:num)']  = 'events/group_register/save_agency';  //--@POST
$route['event/(:num)/group-register-billing/(:num)'] = 'events/group_register/save_bill';  //--@POST

$route['event/(:num)/group-register-edit/(:num)']    = 'events/group_register/edit_register';     //--@POST
$route['event/(:num)/group-register-delete/(:num)']  = 'events/group_register/delete_register';   //--@GET

//----
$route['event/(:num)/register-progress/(:num)']             = 'events/check_register/progress';  //--@GET
$route['event/(:num)/register-progress-group-child/(:num)'] = 'events/check_register/progress_group_child';  //--@GET

$route['event/(:num)/register-payment/(:num)']     = 'events/check_register/payment';   //--@POST
$route['event/(:num)/upload-payment-slip/(:num)']  = 'events/check_register/payment_slip';   //--@POST
//$route['event/(:num)/save-payment-data/(:num)']    = 'events/check_register/save_payment_data';   //--@POST
//$route['event/(:num)/upload-payment-slip/(:num)']  = 'events/check_register/upload_payment_slip';   //--@POST


//---event/{event_id}/pay-in-slip/{customer_id}
$route['event/(:num)/pay-in-slip/(:num)']       = 'events/invoice/pay_in_slip';  //--@GET
$route['event/(:num)/receipt-slip/(:num)']      = 'events/invoice/receipt_slip'; //--@GET

//---Event booth
$route['event/(:num)/booth']         = 'booth/data_list';  //--@GET
$route['event/(:num)/files']         = 'events/event/file_list';  //--@GET
$route['event/(:num)/vdo']           = 'events/event/vdo_list';  //--@GET

//---Api
$route['profile/edit']                       = 'customers/profile/profile_edit';    //--@post
$route['profile/(:num)/event']               = 'customers/profile/profile_event';   //--@get
$route['profile/(:num)/event_detail/(:num)'] = 'customers/profile/event_detail';    //--@get
$route['event/(:num)/vote-booth']            = 'customers/vote/save_vote'; //--@POST

$route['event/(:num)/import/(:num)']  = 'events/import_customer/import'; //POST

$route['crontab/send_mail/member']    = 'crontab/send_mail';

//--API for web application
//$route['event/(:num)/save-payment-data/(:num)']   = 'send_file/save_payment_data';  //--POST
//$route['event/(:num)/upload_pay_in_slip/(:num)']  = 'send_file/upload_pay_in_slip';  //--POST

// 9.Booth scan to get pdf file
// event/{eventId}/booth-scan/{customerId}
$route['event/(:num)/booth-scan/(:num)']  = 'booth/booth_scan';  //--get

//--Promotion
$route['event/(:num)/check-promotion/(:num)']    = 'promotion/check_code';  //--get

//--Stamp
$route['event/(:num)/stamp-list/(:num)']  = 'events/stamp/stamp_gift_list';  //--get
$route['event/(:num)/redeem/(:num)']      = 'events/stamp/gift_redeem';  //--get

//---
$route['event/(:num)/send_pay_in_slip/(:num)'] = 'send_file/send_pay_in_slip';  //--post
$route['event/(:num)/send_receipt/(:num)'] = 'send_file/send_receipt';  //--post
