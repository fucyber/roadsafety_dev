<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']  = array();

$autoload['libraries'] = array('database');

$autoload['drivers']   = array();

$autoload['helper']    = array('url','form','language','application');

$autoload['config']    = array('rest');

$autoload['language']  = array('english');

$autoload['model'] = array(
        'Common_model' => 'Common',
        'API_model'    => 'API'
    );
