var $event_id = $('#event_id').val();
var $booth_id = $('#booth_id').val();
$(function() {
   BoothForm._init();

   //---
   $( "#page_content" ).on("submit", "form#boothForm", function(e) {
      var form     = $('#boothForm')[0];
      var formData = new FormData(form);
      BoothForm._saveData(formData);
      e.preventDefault();
   });

});


BoothForm = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [booth_form.js]" );

        $('#menu_booth').addClass("active");
        $('#menu_booth_create').addClass("active");
    },
    _saveData: function(params){
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/'+ $event_id + '/booth/save-data',
              type: 'POST',
              data: params,
              dataType: 'JSON',
              contentType: false,
              processData: false,
              success: function(resp){
                if(resp.status === false){
                  Common._notify('error',  resp.message);
                  return false;
                }
                //---Success
                Common._notify('success',  resp.message, BASE_URL + 'event/'+ $event_id + '/booth');
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    }
}
