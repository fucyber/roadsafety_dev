$(function() {
   //---
   $( "#page_content" ).on("submit", "form#userForm", function(e) {
      var formData = $('#userForm').serialize();
      User._saveData(formData);
      e.preventDefault();
   });

});


User = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [User.js]" );
    },
    _saveData: function(params){
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'user/save-data',
              type: 'POST',
              data: params,
              dataType: 'JSON',
              success: function(resp){
                if(resp.status === false){
                  Common._alert('error','Error: ',resp.message);
                  return false;
                }
                //---Success
                Common._notify('success',  resp.message, BASE_URL + 'user/list');
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    }
}
