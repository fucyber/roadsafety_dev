
$(function() {
   Login._init();

   //---
   $('body').on("submit", "form#loginForm", function(e) {
      var form     = $('form#loginForm')[0];
      var formData = new FormData(form);

      Login._checkLogin(formData);
      e.preventDefault();
   });

});


Login = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [login.js]" );
    },
    _checkLogin: function(params){

        //----------------------
        Common._loading('.card');

        $.ajax({
              url:  BASE_URL + 'login/check_login',
              type: 'POST',
              data: params,
              contentType: false,
              processData: false,
              dataType: 'JSON',
              success: function(resp){
                console.log(resp);
                if(resp.status === false){
                  Common._alert('error',  resp.message);
                  return false;
                }
                //---Success
                Common._notify('success',  resp.message, BASE_URL);
              }
          }).always(function() {
             Common._stopLoading('.card');
          });
          return false;
    }
}
