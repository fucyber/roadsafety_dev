$(function() {
    Payment._init();
    $('#receipt_date').hide();
    //------
    $( "body" ).on("click", "#btn-approved", function(e) {

        // if($('#receipt_date').val() === ''){
        //   alert('กรุณาเลือกวันที่รับเงิน');
        //   $('#receipt_date').focus();
        //   return false;
        // }

        // var event_id     = $(this).data('event_id');
        // var customer_id  = $(this).data('customer_id');
        // var invoice_id   = $(this).data('invoice_id');
        // var receipt_date = $('#receipt_date').val();
        // var params = 'status=approved&invoice_id='+invoice_id+'&customer_id='+customer_id+'&event_id='+event_id+'&receipt_date='+receipt_date;
        var params = {
          event_id: $(this).data('event_id'),
          customer_id: $(this).data('customer_id'),
          invoice_id: $(this).data('invoice_id'),
          receipt_date: $('#receipt_date').val(),
          receipt_type: $('#receipt_type').val(),
        }
        Payment._savePyamentStatus(params);
        e.preventDefault();
    });
});

function change_receipt_type() {
  $('#receipt_date').hide();
  if ($('#receipt_type').val()=='approved') {
    $('#receipt_date').show();
  }
}

//---------------------------------------------------
Payment = {
  _init: function(){
    if(ENVIRONMENT == 'development')
        console.log( "Invoke [event_register_payment.js]" );

    },
    _savePyamentStatus: function(params){
        Common._loading();
        $('button#btn-submit').prop('disabled', true);

        var form_data = new FormData();

        if ($('#receipt_upload').prop('files')) {
          var file_data = $('#receipt_upload').prop('files')[0];
          form_data.append('file', file_data);
        }

        form_data.append('event_id', params.event_id);
        form_data.append('customer_id', params.customer_id);
        form_data.append('invoice_id', params.invoice_id);
        form_data.append('receipt_date', params.receipt_date);
        form_data.append('receipt_type', params.receipt_type);

        $.ajax({
             url: BASE_URL + 'event-register/event-register-update', // point to server-side controller method
             dataType: 'text', // what to expect back from the server
             cache: false,
             contentType: false,
             processData: false,
             data: form_data,
             type: 'post',
             success: function (response) {
               setTimeout(function() {
                   parent.jQuery.fancybox.getInstance().close();
               }, 2000);
             }
         });


        // $.ajax({
        //       url:  BASE_URL + 'event-register/save-payment-status',
        //       type: 'POST',
        //       data: params,
        //       dataType: 'JSON',
        //       success: function(resp){
        //         if(resp.status === false){
        //           Common._notify('warning', 'Error: '+resp.message);
        //           return false;
        //         }
        //         Common._notify('success', resp.message);
        //         setTimeout(function() {
        //             parent.jQuery.fancybox.getInstance().close();
        //         }, 2000);
        //         parent.Register._loadTabRegister();
        //       },
        //       error: function(jqXHR, textStatus, errorThrown){
        //         Common._alert('warning', 'Error: ',  textStatus + GLANG['or_contact_admin']);
        //       }
        //   }).always(function() {
        //       Common._stopLoading();
        //      $('button#btn-submit').prop('disabled', false);
        //   });
          return false;
    }
}
