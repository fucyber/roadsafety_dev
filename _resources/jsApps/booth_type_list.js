var $event_id = $('#event_id').val();
$(function() {
  //---
  BoothType._init();

  //---
  $( "#page_content" ).on("click", "#btn-delete", function(e) {
     var params = 'type_ids[]=' + $(this).data('type_id');

      swal({
          title: GLANG['confirm'],
          text: GLANG['confirm_delete_choose_data'],
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, I am sure!',
          cancelButtonText: "No, cancel it!",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function(isConfirm) {
          if(isConfirm){
              BoothType._deleteBoothType(params);
          }
      });
      e.preventDefault();
  });
});


BoothType = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [booth_type_list.js]" );
        BoothType._loadData();
        $('#menu_booth').addClass("active");
        $('#menu_booth_create').addClass("active");
    },
    _loadData: function(action_url){
        var params = 'event_id='+$event_id;
        if(action_url == undefined)
          var URL = BASE_URL + 'event/'+ $event_id + '/booth-type/data-list';
        else
          var URL = action_url;

        //----------------------
        Common._loading();
        $.ajax({
              url:  URL,
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $("#display_datatable" ).html( resp );
                Common._tooltip();
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    },
    _deleteBoothType: function(params)
    {
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'event/'+ $event_id + '/booth-type/delete-data',
            type: 'POST',
            data: params,
            dataType: 'JSON',
            success: function(resp){
              if(resp.status === false){
                Common._notify('error',  resp.message);
                return false;
              }
              //---Success
              Common._notify('success',  resp.message);
              BoothType._loadData();
            }
        }).always(function() {
           Common._stopLoading();
        });
        return false;
    }
}
