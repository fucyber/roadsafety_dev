var BASE_URL    = $("meta[name=BASE_URL]").attr("content");
var PLUGIN_URL  = $("meta[name=PLUGIN_URL]").attr("content");
var ENVIRONMENT = $("meta[name=ENVIRONMENT]").attr("content");

//--------------------------
$(document).ready(function(){
    Common._init();
});


//---@
Common = {
  _init: function()
  {
      if(ENVIRONMENT == 'development')
        console.log( "Invoke [Common.js]" );
      Common._tooltip();
  },
  _tooltip: function(){
      $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
      });
  },
  _loading: function (element) {
        var section = 'body';
        if(element != undefined){
          section = element;
        }
        $(section).waitMe({
          effect: 'ios',      //win8_linear, win8, bounce, ios
          text: 'Processing.... Please Wait.',
          bg: 'rgba(115, 115, 115, 0.7)', //rgba(204, 204, 204, 0.7)
          color:'#000'
        });
    },
    _stopLoading:  function(element){
        var section = 'body';
        if(element != undefined){
          section = element;
        }
        $(section).waitMe('hide');
    },
    _isNumber: function(e)
      {
            if(window.event){ // IE
              var charCode = e.keyCode;
              if (((charCode < 48) || (charCode > 57)) && (charCode != 13) && (charCode != 8) && charCode != 46) window.event.returnValue = false;
            } else if (e.which) { // Safari 4, Firefox 3.0.4
              var charCode = e.which
              if (((charCode < 48) || (charCode > 57)) && (charCode != 13) && (charCode != 8) && charCode != 46) charCode = e.preventDefault();
            }
      },
    _notify: function (type,  message, $redirect) {
        if (type == undefined) { type = 'info'; }

        var defaultTimeOut  = 2000;
        var allowDismiss    = true;
        $.notify({
            icon: 'glyphicon glyphicon-ok',
            message: message,
        },{
            type: 'alert-'+type,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timeout: defaultTimeOut,
            placement: {
                from: 'top',
                align: 'center'
            },
            url_target: "_self",
            onClose:null,
            onClosed:null,
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
        //--Auto redirect to.....
        if($redirect != undefined){
          setTimeout(function () {
              window.location.href = $redirect;
          }, defaultTimeOut);
        }
    },
    _alert: function(type, title, msg, redirect){
          swal({
            title: title,
            text: msg,
            type: type,
            closeOnConfirm: true
          }, function(){
            if(redirect != undefined)
               window.location.href = redirect;
          });
    },
    _alert_timer: function(type, title, msg, redirect){
          swal({
            title: title,
            text:  msg,
            type:  type,
            timer: 2000,
            showConfirmButton: false
          }, function(){
            if(redirect != undefined)
               window.location.href = redirect;
          });
    },
    _tinymce: function() {
        var $tinymce = '#editor_tinymce';
        if($($tinymce).length) {
            tinymce.init({
                skin_url: BASE_URL + "_resources/plugins/tinymce/skins/material_design",
                selector: "#editor_tinymce",
                menubar: false,
                plugins: ["lists preview  visualblocks code fullscreen hr"],
                toolbar: "undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | preview | removeformat | code"
            });
        }
    }
}