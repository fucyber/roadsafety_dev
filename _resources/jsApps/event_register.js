var $event_id    = $('#event_id').val();

$(function() {
    Register._init();

    //---@Pagination
    $( "body" ).on( "click", ".pagination  a", function(e) {
        var url = $(this).attr('href') +'&'+ $('#filterForm').serialize();
        Register._loadTabRegister(url);
        e.preventDefault();
    });

    //--@Submit event to parent page
    $( "body" ).on( "click", "#btn_reset_filter", function(e) {
        Register._loadTabRegister();
        $('input[name="name"]').val('');
        $('input[name="ref_1"]').val('');
        $('input[name="date_start"]').val('');
        $('input[name="date_end"]').val('');
        $('select[name="payment_status"]').prop('selectedIndex',0);
        e.preventDefault();
    });

    //--@Filters
    $( "body" ).on( "click", "#btn-filter", function(e) {
        var url = BASE_URL + 'event-register/data-list?'+$('#filterForm').serialize();
        Register._loadTabRegister(url);
        e.preventDefault();
    });
   //--@Filters
   $( "#filterForm" ).submit(function( e ) {

    var url = BASE_URL + 'event-register/data-list?'+$('#filterForm').serialize();
    Register._loadTabRegister(url);
    e.preventDefault();
});
    //------
    $( "body" ).on("click", "#btn-approved", function(e) {
        var params = 'status=approved&invoice_id='+$(this).data('invoice_id');
        Payment._savePyamentStatus(params);
        e.preventDefault();
    });

     //--@Import register from excel
     $( "body" ).on( "click", "#btn_import_excel", function(e) {
        window.location='/event-register/import_excel/'+$event_id;
        e.preventDefault();
    });
});



//---------------------------------------------------
Register = {
  _init: function(){
    $('.datepicker').datepicker();
    if(ENVIRONMENT == 'development')
        console.log( "Invoke [event_register.js]" );
      Register._loadTabRegister();
  },
  _loadTabRegister: function(action_url){

        var paramsData = 'event_id='+$event_id;
        if(action_url == undefined)
          var URL = BASE_URL + 'event-register/data-list';
        else
          var URL = action_url;
          $.ajax({
            url:   BASE_URL + 'event-register/summary-data',
            type: 'POST',
            data: paramsData,
            dataType: 'HTML',
            success: function(resp){
                var respData = $.parseJSON(resp);
              console.log(respData);
            $('#unpaid_summary').html(respData.data.unpaid.toLocaleString());
            $('#paid_summary').html(respData.data.paid.toLocaleString());
            $('#cancel_summary').html(respData.data.cancel.toLocaleString());
            $('#approved_summary').html(respData.data.approved.toLocaleString());
            }
        }).always(function() {

        });
        //----------------------
        Common._loading('#display_datalist');
        $.ajax({
              url:  URL,
              type: 'POST',
              data: paramsData,
              dataType: 'HTML',
              success: function(resp){
                $("#display_datalist").html(resp);
                Common._tooltip();
              }
          }).always(function() {
             Common._stopLoading('#display_datalist');
          });

  },
}
