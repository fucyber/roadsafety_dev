var $event_id    = $('#event_id').val();
var $current_tab = $('#current_tab').val();

$(function() {

    Event._init();
    //------
    $( "#page_content" ).on("click", "#tab-event-info", function(e) {
        Event._loadTabInfo();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-setting", function(e) {
        Event._loadTabSetting();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-document", function(e) {
        Event._loadTabDocument();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-video", function(e) {
        Event._loadTabVideo();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-customergroup", function(e) {
        Event._loadTabCustomerGroup();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-promotion", function(e) {
        Event._loadTabPromotion();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("click", "#tab-event-gift", function(e) {
        Event._loadTabGift();
        e.preventDefault();
    });

    //------
    $( "#page_content" ).on("submit", "form#eventInfoForm", function(e) {
        Event._saveEventInfo();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("submit", "form#cardSettingForm", function(e) {
        Event._saveEventCardSetting();
        e.preventDefault();
    });
    //------
    $( "#page_content" ).on("submit", "form#invoiceSettingForm", function(e) {
        Event._saveEventInvoiceSetting();
        e.preventDefault();
    });
});


Event = {
    _init: function(){
        //----
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [event_detail.js]" );
          //--@Load page content
          switch($current_tab) {
            case 'setting':
                Event._loadTabSetting();
                break;
            default:
              Event._loadTabInfo();
          }
    },
    _loadTabInfo: function(){
        $('.nav-tabs a[href="#event-info"]').tab('show');
        var params = 'event_id='+$event_id;
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/detail-tab-info',
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $("#event-info").html( resp );
              }
          }).always(function() {
             Common._stopLoading();
          });
    },
    _loadTabSetting: function(){
        $('.nav-tabs a[href="#event-setting"]').tab('show');
        var params = 'event_id='+$event_id;
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/detail-tab-setting',
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $.getScript(BASE_URL + "_resources/plugins/tinymce/tinymce.js");
                $("#event-setting").html( resp );
                Common._tinymce();
              }
          }).always(function() {
             Common._stopLoading();
          });
    },
    _loadTabDocument: function(){
        $('.nav-tabs a[href="#event-document"]').tab('show');
        var params = 'event_id='+$event_id;
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/detail-tab-document',
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                // $.getScript(BASE_URL + "_resources/plugins/tinymce/tinymce.js");
                $.getScript(BASE_URL + "_resources/jsApps/event_document.js");
                $("#event-document").html( resp );
                Common._tinymce();
              }
          }).always(function() {
             Common._stopLoading();
          });
    },
    _loadTabVideo : function() {
      $('.nav-tabs a[href="#event-video"]').tab('show');
      var params = 'event_id='+$event_id;
      //----------------------
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'event/detail-tab-video',
            type: 'POST',
            data: params,
            dataType: 'HTML',
            success: function(resp){
            $.getScript(BASE_URL + "_resources/jsApps/event_document.js");
              $("#event-video").html( resp );
              Common._tinymce();
            }
        }).always(function() {
           Common._stopLoading();
        });
    },
    _loadTabCustomerGroup : function() {
      $('.nav-tabs a[href="#event-customergroup"]').tab('show');
      var params = 'event_id='+$event_id;
      //----------------------
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'event/detail-tab-event-customergroup',
            type: 'POST',
            data: params,
            dataType: 'HTML',
            success: function(resp){
            $.getScript(BASE_URL + "_resources/jsApps/event_document.js");
              $("#event-customergroup").html( resp );
              Common._tinymce();
            }
        }).always(function() {
           Common._stopLoading();
        });
    },
    _loadTabPromotion : function() {
      $('.nav-tabs a[href="#event-promotion"]').tab('show');
      var params = 'event_id='+$event_id;
      //----------------------
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'event/detail-tab-event-promotion',
            type: 'POST',
            data: params,
            dataType: 'HTML',
            success: function(resp){
              $("#event-promotion").html( resp );
            }
        }).always(function() {
           Common._stopLoading();
        });
    },
      _loadTabGift : function() {
        $('.nav-tabs a[href="#event-gift"]').tab('show');
        var params = 'event_id='+$event_id;
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/detail-tab-event-gift',
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $.getScript(BASE_URL + "_resources/jsApps/event_document.js");
                $("#event-gift").html( resp );
                Common._tinymce();
              }
          }).always(function() {
             Common._stopLoading();
          });
      },
    _saveEventInfo: function(){

        var form     = $('form#eventInfoForm')[0];
        var formData = new FormData(form);
        $('button#btn-submit').prop('disabled', true);
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/save-event-info',
              type: 'POST',
              data: formData,
              contentType: false,
              processData: false,
              dataType: 'JSON',
              success: function(resp){
                if(resp.status === false){
                  Common._notify('warning', 'Error: '+resp.message);
                  return false;
                }
                Common._notify('success', resp.message);
                Event._loadTabInfo();
              },
              error: function(jqXHR, textStatus, errorThrown){
                Common._alert('warning', 'Error: ',  textStatus + GLANG['or_contact_admin']);
              }
          }).always(function() {
             Common._stopLoading();
             $('button#btn-submit').prop('disabled', false);
          });
          return false;
    },
    _saveEventCardSetting: function(){

        var form     = $('form#cardSettingForm')[0];
        var formData = new FormData(form);
        $('button#btn-submit').prop('disabled', true);
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/save-event-card-setting',
              type: 'POST',
              data: formData,
              contentType: false,
              processData: false,
              dataType: 'JSON',
              success: function(resp){
                if(resp.status === false){
                  Common._notify('warning', 'Error: '+resp.message);
                  return false;
                }
                Common._notify('success', resp.message);
                Event._loadTabSetting();
              },
              error: function(jqXHR, textStatus, errorThrown){
                Common._alert('warning', 'Error: ',  textStatus + GLANG['or_contact_admin']);
              }
          }).always(function() {
             Common._stopLoading();
             $('button#btn-submit').prop('disabled', false);
          });
          return false;
    },
    _saveEventInvoiceSetting: function(){

        var form     = $('form#invoiceSettingForm')[0];
        var formData = new FormData(form);
        $('button#btn-submit').prop('disabled', true);
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/save-event-invoice-setting',
              type: 'POST',
              data: formData,
              contentType: false,
              processData: false,
              dataType: 'JSON',
              success: function(resp){
                if(resp.status === false){
                  Common._notify('warning', 'Error: '+resp.message);
                  return false;
                }
                Common._notify('success', resp.message);
                Event._loadTabSetting();
              },
              error: function(jqXHR, textStatus, errorThrown){
                Common._alert('warning', 'Error: ',  textStatus + GLANG['or_contact_admin']);
              }
          }).always(function() {
             Common._stopLoading();
             $('button#btn-submit').prop('disabled', false);
          });
          return false;
    }
}
