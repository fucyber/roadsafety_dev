var $event_id = $('#event_id').val();
$(function() {
   Booth._init();

  //---
  $( "#page_content" ).on("click", "#btn-delete", function(e) {
     var params = 'booth_ids[]=' + $(this).data('booth_id');

      swal({
          title: GLANG['confirm'],
          text: GLANG['confirm_delete_choose_data'],
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, I am sure!',
          cancelButtonText: "No, cancel it!",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function(isConfirm) {
          if(isConfirm){
              Booth._deleteBooth(params);
          }
      });
      e.preventDefault();
  });

});


Booth = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [booth_list.js]" );
        Booth._loadData();
        $('#menu_booth').addClass("active");
        $('#menu_booth_create').addClass("active");
    },
    _loadData: function(action_url){
        var params = 'event_id='+$event_id;
        if(action_url == undefined)
          var URL = BASE_URL + 'event/'+ $event_id + '/booth/data-list';
        else
          var URL = action_url;

        //----------------------
        Common._loading();
        $.ajax({
              url:  URL,
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $("#display_datatable" ).html( resp );
                Common._tooltip();
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    },
    _deleteBooth: function(params)
    {
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'event/'+ $event_id + '/booth/delete-data',
            type: 'POST',
            data: params,
            dataType: 'JSON',
            success: function(resp){
              if(resp.status === false){
                Common._notify('error',  resp.message);
                return false;
              }
              //---Success
              Common._notify('success',  resp.message);
              Booth._loadData();
            }
        }).always(function() {
           Common._stopLoading();
        });
        return false;
    }
}
