$(function () {
    $('#form_customer').validate({
      highlight: function (input) {
          $(input).parents('.form-line').addClass('error');
      },
      unhighlight: function (input) {
          $(input).parents('.form-line').removeClass('error');
      },
      errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
      },
      submitHandler: function(form, e) {
        e.preventDefault();
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'customer/save-data',
              type: 'POST',
              dataType: 'json',
              data: $(form).serialize(),
              success: function(resp){
                console.log(resp);
                if(resp.status){
                  $('#myModalLabel').html("Success");
                  $('#myModalBody').html(resp.message);
                  $('#myModal').modal();
                }else{
                  $('#myModalLabel').html("Error");
                  $('#myModalBody').html(resp.message);
                  $('#myModal').modal();
                }
                Common._tooltip();
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
      }
    });
    FucConfirmDelete = function(title){
      $('#myModalConfirmHeader').html("Delete");
      $('#myModalConfirmBody').html("Confirm delete Email : "+title+" ?");
      $('#confirm-delete').modal();
    }

    FucDelete = function(customer_id){
      $('#confirm-delete').modal('hide');
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'customer/delete-data',
            type: 'POST',
            dataType: 'json',
            data: {'customer_id' : customer_id},
            success: function(resp){
              if(resp.status){
                window.location.replace(BASE_URL + 'customer/list?ref=delete');
              }else{
                $('#myModalLabel').html("Error");
                $('#myModalBody').html(resp.message);
                $('#myModal').modal();
              }
              Common._tooltip();
            }
        }).always(function() {
           Common._stopLoading();
        });


    }
    $('#button-filter').on('click', function() {
        var url = BASE_URL + 'customer/list?';
        location = url + getURL();
    });

    function getURL() {

        var url = '';

        var filter_event_id = $('select[name=\'event_id\']').val();

        if (filter_event_id != '*') {
            url += 'filter_event_id=' + encodeURIComponent(filter_event_id);
        }

        var filter_type_id = $('select[name=\'type_id\']').val();

        if (filter_type_id != '*') {
            url += '&filter_type_id=' + encodeURIComponent(filter_type_id);
        }

        return url;
    }

    $("#remember_me").click(function(){
        $('input:checkbox.checked_in').not(this).prop('checked', this.checked);
    });

    $("select[name='province']").change(function(){
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'district',
            type: 'POST',
            dataType: 'json',
            data: {'province_id' : $(this).val()},
            success: function(resp){
              if(resp.status){
                 $("select[name='sub_district'] option").remove();
                 $("select[name='sub_district']").append($('<option>', {
                     value: '',
                     text : '-- เลือกตำบล/แขวง --'
                 }));
                 $("select[name='district'] option").remove();
                 $("select[name='district']").append($('<option>', {
                     value: '',
                     text : '-- เลือกอำเภอ/เขต --'
                 }));
                $.each(resp.data, function (i, item) {
                   $("select[name='district']").append($('<option>', {
                       value: item.district_id,
                       text : item.district_name_th
                   }));

                });
              }
              Common._tooltip();
            }
        }).always(function() {
           Common._stopLoading();
        });
    })

    $("select[name='district']").change(function(){
      Common._loading();
      $.ajax({
            url:  BASE_URL + 'sub-district',
            type: 'POST',
            dataType: 'json',
            data: {'district_id' : $(this).val()},
            success: function(resp){
              if(resp.status){
                 $("select[name='sub_district'] option").remove();
                 $("select[name='sub_district']").append($('<option>', {
                     value: '',
                     text : '-- เลือกตำบล/แขวง --'
                 }));
                $.each(resp.data, function (i, item) {
                   $("select[name='sub_district']").append($('<option>', {
                       value: item.sub_district_id,
                       text : item.sub_district_name
                   }));

                });
              }
              Common._tooltip();
            }
        }).always(function() {
           Common._stopLoading();
        });
    })

    $(".checked_in").each(function() {
        if( !this.checked ){
          $("#remember_me").prop('checked', false);
          return;
        }
    });
		var interval;
		function applyAjaxFileUpload(element) {
			$(element).AjaxFileUpload({
				action: BASE_URL + 'customer/upload',
        dataType: 'json',
        onChange: function(filename) {
					// Create a span element to notify the user of an upload in progress
					var $span = $("<span />")
						.attr("class", $(this).attr("id"))
						.text("Uploading")
						.insertAfter($(this));
					$(this).remove();
					interval = window.setInterval(function() {
						var text = $span.text();
						if (text.length < 13) {
							$span.text(text + ".");
						} else {
							$span.text("Uploading");
						}
					}, 200);
				},
        onSubmit: function(filename) {
          return true;
				},
				onComplete: function(filename, response) {
					window.clearInterval(interval);
					var $span = $("span." + $(this).attr("id")).text(filename + " "),
						$fileInput = $("<input />")
							.attr({
								type: "file",
								name: $(this).attr("name"),
								id: $(this).attr("id")
							});
					if (typeof(response.error) === "string") {
						$span.replaceWith($fileInput);
						applyAjaxFileUpload($fileInput);
						alert(response.error);
						return;
					}
					$("<a />")
						.attr("href", "#")
						.text("x")
						.bind("click", function(e) {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
						})
						.appendTo($span);
            $("input[name='payment_slip']").val(response.upload_data.file_name);
				}
			});
		}
		applyAjaxFileUpload("#paymentslip");
	});
