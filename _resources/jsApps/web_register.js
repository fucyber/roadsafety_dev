var $event_id    = $('#event_id').val();
var $default_frm='';
var $current_user_no=0;
var $total_price=$event_price;

$(function() {
   Register._init();

    //---@select province
   $( "body" ).on( "change", "#billing_province_id", function(e) {
    var province_id= $(this).val();
    Register._loadDistrictsList(province_id);
    e.preventDefault();
});
$( "body" ).on( "change", "#billing_district_id", function(e) {
    var district_id= $(this).val();
    Register._loadSubDistrictsList(district_id);
    e.preventDefault();
});
$( "body" ).on( "change", "#type1", function(e) {
    $('#userlist').html(''); 
    $current_user_no=0;
    $total_price=$event_price;
    $('#event_price').html( $total_price.toLocaleString());
    Register._loadUserForm();
    $('#btn-addusers').hide();
    $('#btn-removeusers').hide();
   
   
});
$( "body" ).on( "change", "#type2", function(e) {
    $total_price=$event_price;
    $('#event_price').html( $total_price.toLocaleString());
    $('#btn-addusers').show();
    $('#btn-removeusers').show();
   
});

    $( "body" ).on( "click", "#btn-addusers", function(e) {
        Register._loadUserForm();
    
        e.preventDefault();
    });
    $( "body" ).on( "click", "#btn-removeusers", function(e) {
        Register._removeUserForm();
    
        e.preventDefault();
    });
    $( "body" ).on( "blur", "#discount", function(e) {
        
        Register._calPrice();
        if( $total_price <0){ 
            alert('กรุณาตรวจสอบส่วนลด');
    }
       });
    $( "body" ).on( "click", "#btn-submit", function(e) {
        var error='';
     if($('#billing_address').val().length >0 || $('#tax_no').val().length >0 ){
         
        if($('#tax_no').val().length ==0 ){
            error+='กรุณากรอกเลขประจำตัวผู้เสียภาษี\n';
        }
        if($('#billing_address').val().length ==0 ){
            error+='กรุณากรอกที่อยู่\n';
        }
        if($('#billing_district_id').val() == '' ){
            error+='กรุณากรอกอำเภอ/เชต\n';
        }
        if($('#billing_sub_district_id').val() == '' ){
            error+='กรุณากรอกตำบล/แขวง\n';
        }
        if($('#billing_zipcode').val() == '' ){
            error+='กรุณากรอกรหัสไปรษณีย์\n';
        }
        if( error.length >0){
            alert(error);
        }
     }
        Register._validFrm();
        e.preventDefault();
    });

});


//---------------------------------------------------/
Register = {
  _init: function(){
   
    if(ENVIRONMENT == 'development')
        console.log( "Invoke [event_register.js]" );
        var province_id= $('#billing_province_id').val();
        Register._loadDistrictsList(province_id);
        Register._loadUserForm();
       $('#btn-addusers').hide();
       $('#btn-removeusers').hide();
  },
  _loadDistrictsList: function(province_id){

       
          $.ajax({
            url:   BASE_URL + 'system/getDistrictsList/'+province_id,
            type: 'GET',
             dataType: 'HTML',
            success: function(resp){
                var respData = $.parseJSON(resp);
            //  console.log(respData);
            $('#billing_district_id').empty(); 
            $.each(respData.result, function (i, item) {
                $('#billing_district_id').append($('<option>', { 
                    value: item.district_id,
                    text : item.district_name_th 
                }));
            });
            var district_id= $('#billing_district_id').val();
            Register._loadSubDistrictsList(district_id);
            }
        }).always(function() {

        });


  },
  _loadSubDistrictsList: function(district_id){
    
           
              $.ajax({
                url:   BASE_URL + 'system/getSubDistrictsList/'+district_id,
                type: 'GET',
                 dataType: 'HTML',
                success: function(resp){
                    var respData = $.parseJSON(resp);
                //  console.log(respData);
                $('#billing_sub_district_id').empty(); 
                $.each(respData.result, function (i, item) {
                    $('#billing_sub_district_id').append($('<option>', { 
                        value: item.sub_district_id,
                        text : item.sub_district_name 
                    }));
                });
                }
            }).always(function() {
    
            });
    
    
      },
      _loadUserForm: function(){
        
               
                  $.ajax({
                    url:   BASE_URL + 'web_register/user_form',
                    type: 'GET',
                     dataType: 'HTML',
                    success: function(resp){                                      
              //    console.log(resp);       
                  $default_frm= resp;  
                  Register._addUserForm();     
                    }
                }).always(function() {
        
                });
        
        
          },
          _addUserForm: function(){
            $current_user_no++;
         var   $user_frm =$default_frm.replace('{_NO_}',$current_user_no);
         $('#userlist').append($user_frm);
          },
          _removeUserForm: function(){
              if( $current_user_no ==1){
                  return;
              }
            $current_user_no--;
         $('#userlist tr:last').remove();
         Register._calPrice();
          },
          _calPrice: function(){
              var count=0;
            $(".classcheck").each(function(e) {
                if($(this).val().length >0){
                count++;
                }
             });
             var total_discount=parseInt($('#discount').val()) * count;
            $total_price=(parseInt($event_price * count) - total_discount);
            $('#event_price').html( $total_price.toLocaleString());
            $('#discount_total_txt').html( total_discount.toLocaleString());
            $('#discount_total').val(total_discount);
              console.log(count);
              console.log($total_price);
          },
          _validFrm: function(){
            
                   
                      $.ajax({
                        url:   BASE_URL + 'web_register/register/',
                        type: 'POST',
                         dataType: 'HTML',
                         data:$('#registerForm').serialize(),
                        success: function(resp){
                          //  var respData = $.parseJSON(resp);
                          console.log(resp);
                       
                        }
                    }).always(function() {
            
                    });
            
            
              },
}