function changeprovince() {
  var string_district = '';
  $('#sub_district_name').html('กรุณาเลือก อำเภอ/เขต');
  $.ajax({
      url: BASE_URL+'api/district?token_key=guest&province_id='+$('#province_name select').val(),
      success: function (data) {
        string_district += '<select class="" name="" onchange="changedistrict()">';
        for (a of data.data) {
          if (a.district_id == customeraddress.district_id) {
            string_district +='<option value="'+a.district_id+'" selected>'+a.district_name_th+'</option>';
          }else{
            string_district +='<option value="'+a.district_id+'">'+a.district_name_th+'</option>';
          }
        }
        string_district += '</select>';
        $('#district_name').html(string_district);
      }
    });
}

function changedistrict() {
  var string_sub_district = '';
  $.ajax({
      url: BASE_URL+'api/sub_district?token_key=guest&district_id='+$('#district_name select').val(),
      success: function (data) {
        string_sub_district += '<select class="" name="">';
        for (a of data.data) {
          if (a.sub_district_id == customeraddress.sub_district_id) {
            string_sub_district +='<option value="'+a.sub_district_id+'" selected>'+a.sub_district_name+'</option>';
          }else{
            string_sub_district +='<option value="'+a.sub_district_id+'">'+a.sub_district_name+'</option>';
          }
        }
        string_sub_district += '</select>';
        $('#sub_district_name').html(string_sub_district);
      }
    });
}



function resent_payin_slip() {
  $('.page-loader-wrapper').fadeIn();
  $.ajax({
      url: BASE_URL+'api/event/'+editeventid+'/send_pay_in_slip/'+editcustomerid,
      data: {
        token_key : 'guest',
        send_mail : 'yes'
      },
      type: 'post',
      success: function (data) {
        $('.page-loader-wrapper').fadeOut();
        swal("ส่ง Payin Slip ไปทาง Email เรียบร้อย","","success");
      }
    });
}

function resent_receilpt() {
  $('.page-loader-wrapper').fadeIn();
  $.ajax({
      url: BASE_URL+'api/event/'+editeventid+'/send_receipt/'+editcustomerid,
      data: {
        token_key : 'guest',
        send_mail : 'yes'
      },
      type: 'post',
      success: function (data) {
        $('.page-loader-wrapper').fadeOut();
        swal("ส่ง Receipt ไปทาง Email เรียบร้อย","","success");
      }
    });
}

var inpdata;

$(function() {
   $('.datepicker').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
   $('#saveeventregistercustomer').hide();
   $('#canceleventregistercustomer').hide();

   $('#editeventregistercustomer').click(function(e) {
     $('#saveeventregistercustomer').show();
     $('#editeventregistercustomer').hide();
     $('#canceleventregistercustomer').show();
      var customer_name_title = $('#customer_name_title').html();
      var customer_firstname = $('#customer_firstname').html();
      var customer_lastname = $('#customer_lastname').html();
      // var customer_email = $('#customer_email').html();
      // var customer_mobile = $('#customer_mobile').html();
      var agency_name = $('#agency_name').html();
      var tax_no = $('#tax_no').html();
      var address = $('#address').html();
      var address_road = $('#address_road').html();
      var zipcode = $('#zipcode').html();

      $('#customer_name_title').html('<input style="width: 50px;" class="editregis" type="text" value="'+customer_name_title+'">');
      $('#customer_firstname').html('<input style="width: 100px;" class="editregis" type="text" value="'+customer_firstname+'">');
      $('#customer_lastname').html('<input style="width: 100px;" class="editregis"  type="text" value="'+customer_lastname+'">');
      // $('#customer_email').html('<input class="editregis" type="text" value="'+customer_email+'">');
      // $('#customer_mobile').html('<input class="editregis" type="text" value="'+customer_mobile+'">');
      $('#agency_name').html('<input class="editregis" type="text" value="'+agency_name+'">');
      $('#tax_no').html('<input class="editregis" type="text" value="'+tax_no+'">');
      $('#address').html('<input style="width: 50px;" class="editregis" type="text" value="'+address+'">');
      $('#address_road').html('<input style="width: 70px;" class="editregis" type="text" value="'+address_road+'">');
      $('#zipcode').html('<input style="width: 50px;" class="editregis" type="text" value="'+zipcode+'">');
      var string_province = '';
      var string_district = '';
      var string_sub_district = '';

      $.ajax({
          url: BASE_URL+'api/province?token_key=guest',
          success: function (data) {
            string_province += '<select class="" name="" onchange="changeprovince()">';
            for (a of data.data) {
              if (a.province_id == customeraddress.province_id) {
                string_province +='<option value="'+a.province_id+'" selected>'+a.province_name_th+'</option>';
              }else{
                string_province +='<option value="'+a.province_id+'">'+a.province_name_th+'</option>';
              }
            }
            string_province += '</select>';
            $('#province_name').html(string_province);
          }
        });

        $.ajax({
            url: BASE_URL+'api/district?token_key=guest&province_id='+customeraddress.province_id,
            success: function (data) {
              string_district += '<select class="" name="" onchange="changedistrict()">';
              for (a of data.data) {
                if (a.district_id == customeraddress.district_id) {
                  string_district +='<option value="'+a.district_id+'" selected>'+a.district_name_th+'</option>';
                }else{
                  string_district +='<option value="'+a.district_id+'">'+a.district_name_th+'</option>';
                }
              }
              string_district += '</select>';
              $('#district_name').html(string_district);
            }
          });

          $.ajax({
              url: BASE_URL+'api/sub_district?token_key=guest&district_id='+customeraddress.district_id,
              success: function (data) {
                string_sub_district += '<select class="" name="">';
                for (a of data.data) {
                  if (a.sub_district_id == customeraddress.sub_district_id) {
                    string_sub_district +='<option value="'+a.sub_district_id+'" selected>'+a.sub_district_name+'</option>';
                  }else{
                    string_sub_district +='<option value="'+a.sub_district_id+'">'+a.sub_district_name+'</option>';
                  }
                }
                string_sub_district += '</select>';
                $('#sub_district_name').html(string_sub_district);
              }
            });
        $('#resent_receilpt').hide();
        $('#resent_payin_slip').hide();

        inpdata = {
          customer_name_title : customer_name_title ,
          customer_firstname : customer_firstname ,
          customer_lastname : customer_lastname ,
          // customer_email : customer_email ,
          // customer_mobile : customer_mobile ,
          agency_name : agency_name ,
          tax_no : tax_no ,
          address : address ,
          address_road : address_road ,
          zipcode : zipcode
        }

   });

   $('#canceleventregistercustomer').click(function() {
     $('#customer_name_title').html(inpdata.customer_name_title);
     $('#customer_firstname').html(inpdata.customer_firstname);
     $('#customer_lastname').html(inpdata.customer_lastname);
     // $('#customer_email').html(inpdata.customer_email);
     // $('#customer_mobile').html(inpdata.customer_mobile);
     $('#agency_name').html(inpdata.agency_name);
     $('#tax_no').html(inpdata.tax_no);
     $('#address').html(inpdata.address);
     $('#address_road').html(inpdata.address_road);
     $('#zipcode').html(inpdata.zipcode);

     $('#province_name').html(customeraddress.province_name);
     $('#district_name').html(customeraddress.district_name);
     $('#sub_district_name').html(customeraddress.sub_district_name);

     $('#editeventregistercustomer').show();
     $('#canceleventregistercustomer').hide();
     $('#saveeventregistercustomer').hide();
   });

   $('#saveeventregistercustomer').click(function(e) {
     $('.page-loader-wrapper').fadeIn();
     var customer_name_title = $('#customer_name_title input').val();
     var customer_firstname = $('#customer_firstname input').val();
     var customer_lastname = $('#customer_lastname input').val();
     // var customer_email = $('#customer_email input').val();
     // var customer_mobile = $('#customer_mobile input').val();
     var agency_name = $('#agency_name input').val();
     var tax_no = $('#tax_no input').val();
     var address = $('#address input').val();
     var address_road = $('#address_road input').val();
     var zipcode = $('#zipcode input').val();
     var province = $('#province_name select').val();
     var district = $('#district_name select').val();
     var sub_district = $('#sub_district_name select').val();

     var province_name = $('#province_name select').find('option:selected').text();
     var district_name = $('#district_name select').find('option:selected').text();
     var sub_district_name = $('#sub_district_name select').find('option:selected').text();

     $('#customer_name_title').html(customer_name_title);
     $('#customer_firstname').html(customer_firstname);
     $('#customer_lastname').html(customer_lastname);
     // $('#customer_email').html(customer_email);
     // $('#customer_mobile').html(customer_mobile);
     $('#agency_name').html(agency_name);
     $('#tax_no').html(tax_no);
     $('#address').html(address);
     $('#address_road').html(address_road);
     $('#zipcode').html(zipcode);

     $('#province_name').html(province_name);
     $('#district_name').html(district_name);
     $('#sub_district_name').html(sub_district_name);

     $('#editeventregistercustomer').show();
     $('#canceleventregistercustomer').hide();
     $('#saveeventregistercustomer').hide();
     // console.log(editcustomerid);
     $.ajax({
         url: BASE_URL+"event-register/save-event-register-customer",
         data: {
          customer_id : editcustomerid,
          customer_name_title : customer_name_title,
          customer_firstname  : customer_firstname,
          customer_lastname : customer_lastname,
          // customer_email  : customer_email,
          // customer_mobile : customer_mobile,
          agency_name : agency_name,
          tax_no  : tax_no,
          address : address,
          address_road  : address_road,
          zipcode : zipcode,
          province  : province,
          district  : district,
          sub_district  : sub_district
         },
         type: 'post',
         success: function (data) {


           $.ajax({
               url: BASE_URL+'api/event/'+editeventid+'/send_pay_in_slip/'+editcustomerid,
               data: {
                 token_key : 'guest',
                 send_mail : 'no'
               },
               type: 'post',
               success: function (data) {
                 $('.page-loader-wrapper').fadeOut();
                 swal("บันทึกข้อมูลเรียบร้อย","","success");
               }
             });
           $.ajax({
               url: BASE_URL+'api/event/'+editeventid+'/send_receipt/'+editcustomerid,
               data: {
                 token_key : 'guest',
                 send_mail : 'no'
               },
               type: 'post',
               success: function (data) {
               }
             });

         }
       });
       $('#resent_receilpt').show();
       $('#resent_payin_slip').show();

   });

   save_billing_dtm = function(invoice_id){
     var billing = $('#billing_dtm').val();
     if(!billing){
       alert('กรุณาใส่วันที่ออกใบเสร็จ');
       return;
     }
     var params = 'invoice_id='+invoice_id;
     params += '&billing='+billing;
     Common._loading();
     $.ajax({
           url:  BASE_URL + 'event-register/save-billing',
           type: 'POST',
           data: params,
           dataType: 'JSON',
           success: function(resp){
             alert(resp.message);
             Common._tooltip();
           }
       }).always(function() {
          Common._stopLoading();
       });
   }
});
