$(function() {
   Event._init();

   //---
   $( "#page_content" ).on("click", "#loading", function(e) {
        //Common._loading();
        Common._showNotification('success','Message......', BASE_URL+'event');
        e.preventDefault();
   });
});


Event = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [event_list.js]" );
        Event._loadData();
    },
    _loadData: function(action_url){
        var params = '';
        if(action_url == undefined)
          var URL = BASE_URL + 'event/load-data';
        else
          var URL = action_url;

        //----------------------
        Common._loading();
        $.ajax({
              url:  URL,
              type: 'POST',
              data: params,
              dataType: 'HTML',
              success: function(resp){
                $("#display_datatable" ).html( resp );
                Common._tooltip();
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    }
}