if(ENVIRONMENT == 'development')
    console.log( "Invoke [Global language]" );

var GLANG = [];

//---@Button
GLANG['btn_confirm']           = "ยืนยัน ฉันแน่ใจ";
GLANG['btn_cancel']            = "ยกเลิก";
GLANG['require_category']      = '*ข้อมูลสำคัญ กรุณาเลือกหมวดข่าว';
GLANG['require_language']      = '*ข้อมูลสำคัญ กรุณาเลือกภาษา';

GLANG['ajax_network_time_out'] = 'Network call has timed out, check your internet connection try again.';
GLANG['contact_admin']         = ' Contact administrator';
GLANG['or_contact_admin']      = ' or contact administrator';

//-- @Alert
GLANG['alert_save_success_msg'] = 'Data has been saved successfully.';
GLANG['alert_save_error_msg']   = 'Data not saved, Please try again later or contact administrator';

GLANG['confirm']              = "Confirm ";
GLANG['confirm_active']       = " ยืนยันดำเนินการ Active ข้อมูลที่เลือกใช่หรือไม่?";
GLANG['confirm_inactive']     = " ยืนยันดำเนินการ Disable ข้อมูลที่เลือกใช่หรือไม่?";
GLANG['confirm_delete_choose_data']     = " ยืนยันลบข้อมูลที่เลือกหรือไม่?";
GLANG['confirm_delete_image'] = " ยืนยันลบรูปภาพ";
GLANG['delete_choose_data']   = " คุณต้องการลบข้อมูลที่เลือกใช่หรือไม่?";
GLANG['no_seleted']           = " ไม่ได้เลือกข้อมูล!!!!";
GLANG['empty_data']              = "กรอกข้อมูลไม่ครบกรุณาตรวจสอบ!";
GLANG['empty_search_data']     = "ไม่ได้เลือกข้อมูลค้นหา!";

GLANG['login_success']     = "Login in successfully.";