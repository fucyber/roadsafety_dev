var $event_id = $('#event_id').val();

//------
$(function() {
   BoothTypeForm._init();

   //---
   $( "#page_content" ).on("submit", "form#boothTypeForm", function(e) {
      var params = $('#boothTypeForm').serialize();
      BoothTypeForm._saveData(params);
      e.preventDefault();
   });

});


BoothTypeForm = {
    _init: function(){
        if(ENVIRONMENT == 'development')
            console.log( "Invoke [booth_type_form.js]" );

        $('#menu_booth').addClass("active");
        $('#menu_booth_create').addClass("active");
    },
    _saveData: function(params){
        //----------------------
        Common._loading();
        $.ajax({
              url:  BASE_URL + 'event/'+ $event_id + '/booth-type/save-data',
              type: 'POST',
              data: params,
              dataType: 'JSON',
              success: function(resp){
                console.log(resp);
                if(resp.status == false){
                  Common._alert('error',  resp.message);
                  return false;
                }
                //---Success
                Common._notify('success',  resp.message, BASE_URL + 'event/'+ $event_id + '/booth-type');
              }
          }).always(function() {
             Common._stopLoading();
          });
          return false;
    }
}
